#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in uint aIndex;

out vec3 vPos;
out vec2 vTexCoord;
out vec4 vColor;
out vec3 vNormal;
out vec3 fPos;

uniform samplerBuffer tex_offsets;

uniform mat4 projection;
uniform mat4 model;

void main()
{
    gl_Position = projection * model * vec4(aPos, 1.0);
    float y_offset = texelFetch(tex_offsets, int(aIndex)).r;
    vTexCoord = vec2(aTexCoord.x, aTexCoord.y + y_offset);
    vColor = aColor;
    vNormal = aNormal;
    fPos = vec3(model * vec4(aPos, 1.0));
    vPos = aPos;
}
