#version 330 core

layout (location = 0) in vec4 vertex;
layout (location = 1) in vec4 vColor;

out vec2 fTexCoords;
out vec4 fColor;

uniform mat4 projection;
uniform mat4 model;

void main() {
    gl_Position = projection * model * vec4(vertex.xy, 0.0, 1.0);
    fTexCoords = vec2(vertex.zw);
    fColor = vColor;
}
