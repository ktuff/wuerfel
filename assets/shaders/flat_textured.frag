#version 330 core

in vec2 f_texcoord;
in vec4 f_color;
in vec3 f_normal;

out vec4 frag_color;

uniform sampler2D texture0;


void main()
{
	vec4 color = texture(texture0, f_texcoord);
	if (color.a < 0.2) {
		discard;
	} else {
	}
	frag_color = color * f_color;
}

