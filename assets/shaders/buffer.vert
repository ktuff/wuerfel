#version 330 core

layout (location = 0) in vec4 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec3 aNormal;

out vec3 vNormal;
out vec2 vTexCoord;
out vec4 vColor;

uniform mat4 projection;
uniform mat4 model;

void main() {
    vec4 pos = projection * model * aPos;
    gl_Position = vec4(pos.xy, 0.0f, 1.0f);
    vNormal = aNormal;
    vTexCoord = aTexCoord;
    vColor = aColor;
}
