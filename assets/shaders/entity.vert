#version 330 core

layout (location = 0) in vec4 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 3) in vec3 aNormal;

out vec2 vTexCoord;

uniform mat4 projection;
uniform mat4 model;

void main() {
	gl_Position = projection * model * aPos;
	vTexCoord = vec2(aTexCoord.x, 1-aTexCoord.y);
}

