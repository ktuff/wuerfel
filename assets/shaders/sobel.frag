#version 330 core

in vec2 tex_coords;

out vec4 frag_color;

uniform sampler2D image;
uniform vec2 texel;


float sobel(vec4 m[9], int c)
{
	float gx = m[0][c] - m[6][c] + m[2][c] - m[8][c] + 2*m[1][c] - 2*m[7][c];
	float gy = m[0][c] + m[6][c] - m[2][c] - m[8][c] + 2*m[3][c] - 2*m[5][c];
	float sum = ((gx < 0) ? -gx : gx) + ((gy < 0) ? -gy : gy);
	float result = (sum < 0.0) ? 0.0 : ((sum > 1.0) ? 1.0 : sum);
	return result;
}


void main()
{
	vec4 m[9];
	m[0] = texture2D(image, tex_coords + vec2(-texel.x, -texel.y));
	m[1] = texture2D(image, tex_coords + vec2(-texel.x,  	 0.0));
	m[2] = texture2D(image, tex_coords + vec2(-texel.x,  texel.y));
	m[3] = texture2D(image, tex_coords + vec2( 	   0.0, -texel.y));
//	m[4] = texture2D(image, tex_coords + vec2( 	   0.0,  	 0.0));
	m[5] = texture2D(image, tex_coords + vec2( 	   0.0,  texel.y));
	m[6] = texture2D(image, tex_coords + vec2( texel.x, -texel.y));
	m[7] = texture2D(image, tex_coords + vec2( texel.x,  	 0.0));
	m[8] = texture2D(image, tex_coords + vec2( texel.x,  texel.y));

	float r = sobel(m, 0);
	float g = sobel(m, 1);
	float b = sobel(m, 2);
	float a = sobel(m, 3);

	float v = (r > 0.0 || g > 0.0 || b > 0.0 || a > 0.0) ? 1.0 : 0.0;
	frag_color = vec4(v, v, v, 1.0);
}
