#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in mat4 aInstanceModel;

out vec3 vNormal;
out vec2 vTexCoord;
out vec4 vColor;

uniform mat4 projection;

void main() {
    gl_Position = projection * aInstanceModel * aPos;
    vNormal = aNormal;
    vTexCoord = aTexCoord;
    vColor = aColor;
}
