#version 330 core

in vec2 vTexCoords;

out vec4 color;

uniform sampler2D tex;
uniform vec4 base_color;

void main() {
    color = texture(tex, vTexCoords)*base_color;
    if(color.a <= 0.00)
        discard;
}
