#version 330 core

layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec2 a_texcoord;
layout (location = 2) in vec4 a_color;
layout (location = 3) in vec3 a_normal;

out vec2 f_texcoord;
out vec4 f_color;
out vec3 f_normal;

uniform mat4 projection;
uniform mat4 model;


void main()
{
	gl_Position = projection * model * vec4(a_pos, 1.0);
	f_texcoord = a_texcoord;
	f_color = a_color;
	f_normal = a_normal;
}

