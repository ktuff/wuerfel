#version 330 core
layout (location = 0) in vec4 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec3 aNormal;

out vec3 vNormal;
out vec2 vTexCoord;
out vec4 vColor;
//out vec3 fPos;

uniform mat4 projection;
uniform mat4 model;

void main() {
    //fPos = vec3(model*aPos);
    gl_Position = projection * model * aPos;
    vNormal = aNormal;
    vTexCoord = aTexCoord;
    vColor = aColor;
}
