#version 330 core

layout (location = 0) in vec3 v_position;
layout (location = 1) in vec2 v_texcoord;
layout (location = 2) in vec3 v_normal;

out vec2 f_texcoord;
out vec4 f_normal;

uniform mat4 projection;
uniform mat4 model;
uniform float extent_factor;


void main()
{
	gl_Position = projection * model * vec4(v_position, 1.0);
	f_texcoord = v_texcoord;
	f_normal = vec4(v_normal, dot(v_position, v_normal) * extent_factor);
}

