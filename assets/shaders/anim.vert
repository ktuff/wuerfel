#version 330 core

layout (location = 0) in vec4 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec4 aColor;
layout (location = 3) in vec3 aNormal;
layout (location = 4) in int aIndex;

out vec3 vNormal;
out vec2 vTexCoord;
out vec4 vColor;
flat out int vIndex;

uniform mat4 projection;
uniform mat4 model;

void main() {
    vec4 pos = projection * model * aPos;
    gl_Position = vec4(pos.xy, 0.0f, 1.0f);
    vNormal = aNormal;
    vTexCoord = aTexCoord.xy;
    vIndex = aIndex;
    vColor = aColor;
}

