#version 330 core

in vec2 f_texcoord;
in vec4 f_normal;

out vec4 frag_color;

uniform sampler2D tex;


void main()
{
	if (texture(tex, f_texcoord).a <= 0.0) {
		frag_color = vec4(0.5, 0.5, 0.5, 1.0);
	} else {
		frag_color = vec4(f_normal * 0.5 + 0.5);
	}
}
