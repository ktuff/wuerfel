#version 330 core

in vec3 vPos;
in vec2 vTexCoord;
in vec4 vColor;
in vec3 vNormal;
in vec3 fPos;

out vec4 color;

uniform sampler2D texture0;

uniform vec3 view_pos;

uniform vec3 light_dir;
uniform vec3 light_color;

uniform vec3 ambient_color;

uniform vec3 specular_color;
uniform float specular_strength;
uniform float specular_exponent;

uniform float opacity;


void main()
{
	vec3 ambient = ambient_color;
	vec3 light_color = vec3(1.0);
	vec4 object_color = texture(texture0, vTexCoord) * vColor;

	vec3 ldir = normalize(light_dir);
	vec3 norm = normalize(vNormal);
	float diff = max(dot(norm, ldir), 0.0);
	vec3 diffuse = diff * light_color;

	vec4 object = vec4(max(ambient, diffuse), opacity) * object_color;

	vec3 view_dir = normalize(view_pos - fPos);
	vec3 reflect_dir = reflect(-ldir, norm);
	float spec = pow(max(dot(view_dir, reflect_dir), 0.0), specular_exponent);
	vec4 specular = vec4(spec * specular_color, opacity);

	color = object + specular_strength * specular;
}
