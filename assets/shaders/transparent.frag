#version 330 core

in vec3 vNormal;
in vec2 vTexCoord;
in vec4 vColor;
//in vec3 fPos;

out vec4 color;

//uniform bool multiTextured;
//uniform bool hasColor;
uniform sampler2D texture0;
//uniform sampler2D texture1;

void main() {
    //vec3 lightPos = vec3(0.0, 140.0, 0.0);
    //vec3 lightDir = normalize(lightPos - fPos);
    
    //vec3 lightDir = vec3(1.0);
    //vec3 norm = normalize(vNormal);
    //float diff = max(dot(norm, lightDir), 0.0);
    
    //vec3 ambient = vec3(0.01);
    //vec3 lightColor = vec3(1.0);
    //vec3 diffuse = diff*lightColor;
    //vec4 result = vec4(max(ambient, diffuse), 1.0)*texture(texture0, vTexCoord);
    //color = result;
    
    color = texture(texture0, vTexCoord)*vColor;
    if(color.a <= 0.00)
        discard;
}
