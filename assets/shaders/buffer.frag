#version 330 core

in vec3 vNormal;
in vec2 vTexCoord;
in vec4 vColor;

out vec4 color;

uniform sampler2D texture0;

void main() {
    color = texture(texture0, vTexCoord);
}
