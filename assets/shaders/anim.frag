#version 330 core

in vec3 vNormal;
in vec2 vTexCoord;
in vec4 vColor;
flat in int vIndex;

out vec4 color;

uniform sampler2D texture0;
uniform samplerBuffer tex_offsets;

void main() {
    float y_offset = texelFetch(tex_offsets, vIndex).r;
    color = texture(texture0, vec2(vTexCoord.x, vTexCoord.y + y_offset));
}

