#version 330 core

in vec2 tex_coords;

out vec4 frag_color;

uniform sampler2D image;
uniform vec2 texel;

// to function properly the texture should use GL_CLAMP_TO_EDGE

#define R 1
#define N ((1 + 2 * R) * (1 + 2 * R))

void main()
{
	vec4 a[N];
	/* for R = 1
	int num = 0;
	for (int i=-R; i<=R; ++i) {
		for (int j=-R; j<=R; ++j) {
			a[num] = texture2D(image, tex_coords + vec2(j*texel.x, i*texel.y)).rgb;
			num += 1;
		}
	}
	*/
	a[0] = texture2D(image, tex_coords + vec2(-texel.x, -texel.y));
	a[1] = texture2D(image, tex_coords + vec2(-texel.x,  	 0.0));
	a[2] = texture2D(image, tex_coords + vec2(-texel.x,  texel.y));
	a[3] = texture2D(image, tex_coords + vec2( 	   0.0, -texel.y));
	a[4] = texture2D(image, tex_coords + vec2( 	   0.0,  	 0.0));
	a[5] = texture2D(image, tex_coords + vec2( 	   0.0,  texel.y));
	a[6] = texture2D(image, tex_coords + vec2( texel.x, -texel.y));
	a[7] = texture2D(image, tex_coords + vec2( texel.x,  	 0.0));
	a[8] = texture2D(image, tex_coords + vec2( texel.x,  texel.y));

	int ii[N];
	float n[N];

	for (int e=0; e<N; ++e) {
		ii[e] = e;
		n[e] = 0.0;
	}
	for (int e=0; e<N; ++e) {
		for (int f=e+1; f<N; ++f) {
			float vn = length(vec4(a[e].x - a[f].x, a[e].y - a[f].y, a[e].z - a[f].z, a[e].w - a[f].w));
			n[e] += vn;
			n[f] += vn;
		}
	}

	/*
	int j = 0;
	float f = n[0];
	for (int i=1; i<N ++i) {
		if (n[i] < f) {
			f = n[i];
			j = i;
		}
	}
	frag_color = a[j];
	*/
	for (int i=1; i<N; ++i) {
		int j = i;
		float vi = n[i];
		int nn = ii[i];
		while (j > 0 && n[j-1] > vi) {
			n[j] = n[j-1];
			ii[j] = ii[j-1];
			j = j-1;
		}
		n[j] = vi;
		ii[j] = nn;
	}

	frag_color = vec4(a[0]);
}
