#version 330 core

in vec2 fTexCoords;
in vec4 fColor;

out vec4 color;

uniform sampler2D texture0;
uniform vec4 text_color;

void main() {
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(texture0, fTexCoords).r);
    color = text_color * fColor * sampled;
}
