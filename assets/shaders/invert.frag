#version 330 core

in vec2 tex_coords;

out vec4 frag_color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform vec2 texel;


void main()
{
	vec4 color = texture2D(texture0, tex_coords);
	vec4 mask = texture2D(texture1, tex_coords);
	float depth_color = texture2D(texture2, tex_coords).r;

	float d[9];
	d[0] = texture2D(texture3, tex_coords + vec2(-texel.x, -texel.y)).r;
	d[1] = texture2D(texture3, tex_coords + vec2(-texel.x,  	0.0)).r;
	d[2] = texture2D(texture3, tex_coords + vec2(-texel.x,  texel.y)).r;
	d[3] = texture2D(texture3, tex_coords + vec2( 	  0.0, -texel.y)).r;
	d[4] = texture2D(texture3, tex_coords + vec2( 	  0.0,  	0.0)).r;
	d[5] = texture2D(texture3, tex_coords + vec2( 	  0.0,  texel.y)).r;
	d[6] = texture2D(texture3, tex_coords + vec2( texel.x, -texel.y)).r;
	d[7] = texture2D(texture3, tex_coords + vec2( texel.x,  	0.0)).r;
	d[8] = texture2D(texture3, tex_coords + vec2( texel.x,  texel.y)).r;

	float depth_mask = 1.0;
	for (int i=0; i<9; ++i) {
		depth_mask = min(depth_mask, d[i]);
	}

	if (depth_color < depth_mask - 0.000005 || mask.b < 0.5) {
		frag_color = color;
	} else {
		frag_color = vec4(1.0 - color.rgb, color.a);
	}
}
