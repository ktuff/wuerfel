#pragma once

#include <nlohmann/json.hpp>

#include <common/entity/systems/system_module.hpp>


namespace server {

class packager;

} // namespace server


namespace entity {

class network_system : public ::entity::system_module {

	public:
		static ::entity::system_module_id const s_id;

	private:
		server::packager* m_network;


	public:
		network_system(::entity::core_system* core_system, server::packager* network);
		~network_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void update();
		void process_packet(nlohmann::json const& data);


};

} // namespace entity
