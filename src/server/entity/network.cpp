#include <common/entity/core_system.hpp>
#include <common/entity/entity.hpp>
#include <common/entity/entities/tile.hpp>
#include <common/entity/systems/events.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/entity/systems/net_types.hpp>
#include <common/net/types.hpp>
#include <server/packager.hpp>

#include "network.hpp"


::entity::system_module_id const entity::network_system::s_id = 257;


entity::network_system::network_system(::entity::core_system* core_system, server::packager* network)
: system_module(core_system, s_id)
, m_network(network)
{
}

entity::network_system::~network_system()
{
}


void entity::network_system::on_entity_creation(::entity::entity* entity)
{
	nlohmann::json data;
	data["op"] = e_net_creation;
	data["id"] = entity->m_id;
	data["type"] = entity->m_type;

	switch (entity->m_type) {
	case ::entity::e_player:
		break;
	case ::entity::e_item:
		break;
	case ::entity::e_chest:
	case ::entity::e_furnace:
	case ::entity::e_workbench: {
		auto const& pos = dynamic_cast<::entity::tile*>(entity)->get_voxel_position();
		data["voxel"] = nlohmann::json::object();
		data["voxel"]["x"] = pos.x;
		data["voxel"]["y"] = pos.y;
		data["voxel"]["z"] = pos.z;
	}	break;
	default:
		break;
	}

	this->m_network->push_entity_packet(-1, data);
}

void entity::network_system::on_entity_destruction(::entity::entity* entity)
{
	nlohmann::json data;
	data["op"] = e_net_destruction;
	data["id"] = entity->m_id;
	data["type"] = entity->m_type;

	switch (entity->m_type) {
	case ::entity::e_player:
	case ::entity::e_item:
	case ::entity::e_chest:
	case ::entity::e_furnace:
	case ::entity::e_workbench:
	default:
		break;
	}

	this->m_network->push_entity_packet(-1, data);
}

void entity::network_system::on_event(::entity::event* event)
{
	if (event->origin == this) {
		return;
	}

	nlohmann::json data;
	data["id"] = event->entity_id;

	switch (event->type) {
	case ::entity::e_position: {
		auto* ep = static_cast<::entity::event_position*>(event);
		data["op"] = e_net_pos;
		data["x"] = ep->position.x;
		data["y"] = ep->position.y;
		data["z"] = ep->position.z;
	} 	break;
	case ::entity::e_rotation: {
		auto* er = static_cast<::entity::event_position*>(event);
		data["op"] = e_net_rot;
		data["x"] = er->position.x;
		data["y"] = er->position.y;
		data["z"] = er->position.z;
	} 	break;
	case ::entity::e_item_change: {
		auto* pe = static_cast<::entity::event_item_change*>(event);
		data["op"] = ::entity::e_net_item;
		data["target"] = pe->target;
		data["index"] = pe->index;
		data["type"] = pe->new_item.type;
		data["count"] = pe->new_item.count;
	}	break;
	case ::entity::e_selection_change: {
		auto* e = static_cast<::entity::event_selection_change*>(event);
		data["op"] = e_net_item_selection;
		data["oldIndex"] = e->old_index;
		data["newIndex"] = e->new_index;
	}	break;
	case ::entity::e_hitbox:
	case ::entity::e_damage:
		return;
	case ::entity::e_container: {
		auto* pe = static_cast<::entity::event_container*>(event);
		data["op"] = ::entity::e_net_container;
		data["container"] = pe->container;
	}	break;
	}

	this->m_network->push_entity_packet(-1, data);
}

void entity::network_system::update()
{
}

void entity::network_system::process_packet(const nlohmann::json& data)
{
	::entity::op_code op = data["op"].get<::entity::op_code>();
	::entity::id id = data["id"].get<::entity::id>();
	switch (op) {
	case e_net_invalid:
		throw std::runtime_error("Invalid network package for entity");
		break;
	case e_net_creation:
		break;
	case e_net_destruction:
		break;
	case e_net_pos:
		break;
	case e_net_rot:
		break;
	case e_net_item: {
		::entity::inventory_group target = data["target"].get<::entity::inventory_group>();
		int index = data["index"].get<int>();
		::item::item_type_id type = data["type"].get<::item::item_type_id>();
		::item::stack_size_t count = data["count"].get<::item::stack_size_t>();
		::item::item_stack stack(type, count);
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
		inv->set_item(id, target, index, stack);
	}	break;
	case ::entity::e_net_item_selection: {
		int new_index = data["newIndex"].get<int>();
		// TODO validate
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
		inv->set_selected_index(id, new_index);
	}	break;
	case ::entity::e_net_container: {
		::entity::id container_id = data["container"].get<::entity::id>();
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
		inv->set_container_id(id, container_id);
	}	break;
	}

	// TODO
	// bool valid = validate_event();
	// if (valid) {
	// 	this->process_event();
	// } else {
	// 	this->nack_event();
	// }
}
