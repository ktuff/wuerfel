#pragma once

#include <memory>
#include <unordered_map>

#include <common/entity/systems/system_module.hpp>
#include <common/util/aabb.hpp>


namespace entity {

struct collision {
	bool onground = false;
	aabb hitbox;
};


class physics_system : public system_module {

	private:
		std::unordered_map<::entity::id, std::unique_ptr<collision>> m_collision_map;


	public:
		physics_system(::entity::core_system* core_system);
		~physics_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void enable_collision(::entity::id id);
		void disable_collision(::entity::id id);
		bool onground(::entity::id id) const;
		bool collides(::entity::id id) const;
		aabb get_hitbox(::entity::id id) const;

		void update();

	private:
		void update_hitbox(::entity::id id);
		void set_onground(::entity::id id, bool value);

};

} // namespace entity
