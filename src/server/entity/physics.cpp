#include <common/entity/entity.hpp>
#include <common/entity/core_system.hpp>
#include <common/entity/systems/events.hpp>

#include "physics.hpp"


entity::physics_system::physics_system(::entity::core_system* core_system)
: system_module(core_system, 3) // TODO
{
}

entity::physics_system::~physics_system()
{
}


void entity::physics_system::on_entity_creation(::entity::entity* entity)
{
	switch (entity->m_type) {
	case e_player:
	case e_item:
	case e_pig:
		this->enable_collision(entity->m_id);
		break;
	case e_chest:
	case e_furnace:
	case e_workbench:
		break;
	case e_max_num_entity_types:
		break;
	}
}

void entity::physics_system::on_entity_destruction(::entity::entity* entity)
{
	this->disable_collision(entity->m_id);
}

void entity::physics_system::on_event(::entity::event* event)
{
	switch (event->type) {
	case e_position:
		this->update_hitbox(event->entity_id);
		break;
	case e_rotation:
	case e_hitbox:
	case e_item_change:
	case e_selection_change:
	case e_container:
	case e_damage:
		break;
	}
}

void entity::physics_system::enable_collision(::entity::id id)
{
	if (m_collision_map.find(id) == m_collision_map.end()) {
		this->m_collision_map.emplace(id, std::make_unique<collision>());
	}
}

void entity::physics_system::disable_collision(::entity::id id)
{
	if (m_collision_map.find(id) != m_collision_map.end()) {
		this->m_collision_map.erase(id);
	}
}

bool entity::physics_system::collides(::entity::id id) const
{
	return m_collision_map.find(id) != m_collision_map.end();
}

bool entity::physics_system::onground(::entity::id id) const
{
	auto it = m_collision_map.find(id);
	if (it == m_collision_map.end()) {
		return false;
	} else {
		return it->second->onground;
	}
}

aabb entity::physics_system::get_hitbox(::entity::id id) const
{
	auto it = m_collision_map.find(id);
	if (it == m_collision_map.end()) {
		return aabb();
	} else {
		return it->second->hitbox;
	}
}

void entity::physics_system::update()
{

}

void entity::physics_system::update_hitbox(::entity::id id)
{
	auto it = m_collision_map.find(id);
	if (it == m_collision_map.end()) {
		return;
	}
	aabb& hitbox = it->second->hitbox;

	::entity::entity* entity = m_core->get_entity(id);
	auto const& position = entity->get_position();

	switch (entity->m_type) {
	case e_player:
		hitbox.set_bounds(position + ktf::vec3<double>(-0.40, 0.00, -0.40), position + ktf::vec3<double>(0.40, 1.80, 0.40));
		break;
	case e_item:
		hitbox.set_bounds(position + ktf::vec3<double>(-0.15, 0.00, -0.15), position + ktf::vec3<double>(0.15, 0.30, 0.15));
		break;
	case e_pig:
		hitbox.set_bounds(position + ktf::vec3<double>(-0.50, 0.00, -0.50), position + ktf::vec3<double>(0.50, 1.00, 0.50)); // TODO
		break;
	default:
		break;
	}
}

void entity::physics_system::set_onground(::entity::id id, bool value)
{
	auto it = m_collision_map.find(id);
	if (it != m_collision_map.end()) {
		it->second->onground = value;

		::entity::event_hitbox event(this, id, value);
		this->m_core->notify(&event);
	}
}
