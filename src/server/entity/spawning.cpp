#include <common/entity/core_system.hpp>
#include <common/entity/entities/chicken.hpp>
#include <common/entity/entities/cow.hpp>
#include <common/entity/entities/pig.hpp>

#include "spawning.hpp"


entity::spawning::spawning(::entity::core_system* core)
: m_core(core)
, m_delay(20)
{
}

entity::spawning::~spawning()
{
}


void entity::spawning::tick()
{
	if (m_delay <= 0) {
		this->m_delay = 50000;

		auto* pig = m_core->create_entity_pig();
		double x1 = (::rand() % 100) - 50;
		double z1 = (::rand() % 100) - 50;
		pig->set_position({x1, 150.0, z1});

		auto* cow = m_core->create_entity_cow();
		double x2 = (::rand() % 100) - 50;
		double z2 = (::rand() % 100) - 50;
		cow->set_position({x2, 150.0, z2});

		auto* chicken = m_core->create_entity_chicken();
		double x3 = (::rand() % 100) - 50;
		double z3 = (::rand() % 100) - 50;
		chicken->set_position({x3, 150.0, z3});
	} else {
		this->m_delay -= 1;
	}
}
