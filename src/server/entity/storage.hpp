#pragma once

#include <unordered_map>

#include <common/entity/systems/system_module.hpp>
#include <common/types.hpp>


namespace game {

class level;

} // namespace game


namespace entity {

class storage_system : public ::entity::system_module {

	public:
		static ::entity::system_module_id const s_id;

	private:
		std::unordered_map<::entity::id, ccoord_t> m_entity_chunk_map;
		std::unordered_map<ccoord_t, std::set<::entity::id>> m_chunk_entity_map;
		game::level const* m_level;


	public:
		storage_system(::entity::core_system* core_system, game::level const* level);
		~storage_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void load_chunk(ccoord_t const& chunk);
		void save_chunk(ccoord_t const& chunk);
		size_t get_batch_json(ccoord_t const& chunk, nlohmann::json& data) const;

	private:
		void register_entity_position(ccoord_t const& chunk, ::entity::id entity);
		void deregister_entity_position(ccoord_t const& chunk, ::entity::id entity);

};

} // namespace entity
