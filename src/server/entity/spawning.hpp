#pragma once


namespace entity {

class core_system;


class spawning { // TODO extend system_module

	private:
		::entity::core_system* m_core;
		int m_delay;


	public:
		spawning(::entity::core_system* core);
		~spawning();


	public:
		void tick();

};

} // namespace entity
