#include <filesystem>

#include <nlohmann/json.hpp>

#include <common/entity/core_system.hpp>
#include <common/entity/entity.hpp>
#include <common/entity/systems/events.hpp>
#include <common/level.hpp>
#include <common/world/config.hpp>
#include <server/util/filesystem.hpp>

#include "storage.hpp"


::entity::system_module_id const entity::storage_system::s_id = 126; // TODO


entity::storage_system::storage_system(::entity::core_system* core_system, game::level const* level)
: system_module(core_system, s_id)
, m_level(level)
{
}

entity::storage_system::~storage_system()
{
}


void entity::storage_system::on_entity_creation(::entity::entity* entity)
{
	auto const& pos = entity->get_position();
	coord_t chunk_x = ::floor((long double)pos.x / CHUNK_SIZE);
	coord_t chunk_z = ::floor((long double)pos.z / CHUNK_SIZE);
	ccoord_t coord(chunk_x, chunk_z);
	this->register_entity_position(coord, entity->m_id);
}

void entity::storage_system::on_entity_destruction(::entity::entity* entity)
{
	auto const& pos = entity->get_position();
	coord_t chunk_x = ::floor((long double)pos.x / CHUNK_SIZE);
	coord_t chunk_z = ::floor((long double)pos.z / CHUNK_SIZE);
	ccoord_t coord(chunk_x, chunk_z);
	this->deregister_entity_position(coord, entity->m_id);
}

void entity::storage_system::on_event(::entity::event* event)
{
	switch (event->type) {
	case ::entity::e_position: {
		auto* pevent = static_cast<::entity::event_position*>(event);

		ccoord_t old_chunk = m_entity_chunk_map.at(event->entity_id);
		this->deregister_entity_position(old_chunk, event->entity_id);

		coord_t chunk_x2 = ::floor((long double)pevent->position.x / CHUNK_SIZE);
		coord_t chunk_z2 = ::floor((long double)pevent->position.z / CHUNK_SIZE);
		ccoord_t new_chunk(chunk_x2, chunk_z2);
		this->register_entity_position(new_chunk, event->entity_id);
	}	break;
	default:
		break;
	}
}

void entity::storage_system::load_chunk(const ccoord_t& chunk)
{
	coord_t xi = chunk.x / coord_t(io::NUM_CHUNKS_SIDE) - (chunk.x < 0);
	coord_t zi = chunk.y / coord_t(io::NUM_CHUNKS_SIDE) - (chunk.y < 0);

	std::string const level_name = m_level->get_name();
	std::string const path("saves/" + level_name + "/entity/chunk." + std::to_string(xi) + "." + std::to_string(zi) + ".mca");
	io::chunk_file file(path);
	if (!file.is_open()) {
		return;
	}
	std::unique_ptr<char[]> compressed_data;
	size_t size;
	bool valid = file.load_chunk(chunk.x, chunk.y, compressed_data, size);
	if (!valid) {
		return;
	}

	// TODO decompress data
	nlohmann::json chunk_entities = nlohmann::json::parse(std::string(compressed_data.get(), size));
	for (auto const& entity_json : chunk_entities) {
		this->m_core->from_json(entity_json);
	}
}

void entity::storage_system::save_chunk(const ccoord_t& chunk)
{
	auto it = m_chunk_entity_map.find(chunk);
	if (it == m_chunk_entity_map.end()) {
		return;
	}

	coord_t xi = chunk.x / coord_t(io::NUM_CHUNKS_SIDE) - (chunk.x < 0);
	coord_t zi = chunk.y / coord_t(io::NUM_CHUNKS_SIDE) - (chunk.y < 0);

	auto const& entities = it->second;
	std::string const level_name = m_level->get_name();
	std::string const path("saves/" + level_name + "/entity/chunk." + std::to_string(xi) + "." + std::to_string(zi) + ".mca");
	if (entities.empty()) {
		if (std::filesystem::exists(path)) {
			std::filesystem::remove(path);
		}
		return;
	}

	nlohmann::json chunk_entities = nlohmann::json::array();
	for (auto id : entities) {
		nlohmann::json entity_json;
		if (m_core->to_json(id, entity_json)) {
			chunk_entities += entity_json;
		}
	}

	io::chunk_file file(path);
	if (!file.is_open()) {
		return;
	}

	std::string data = chunk_entities.dump();
	// TODO compress data
	file.save_chunk(chunk.x, chunk.y, data.data(), data.size());
}

size_t entity::storage_system::get_batch_json(const ccoord_t& chunk, nlohmann::json& data) const
{
	data = nlohmann::json::array();
	auto it = m_chunk_entity_map.find(chunk);
	if (it != m_chunk_entity_map.end()) {
		for (auto const& c : it->second) {
			nlohmann::json entity;
			this->m_core->to_json(c, entity);
			data += entity;
		}
		return it->second.size();
	}
	return 0;
}

void entity::storage_system::register_entity_position(ccoord_t const& chunk, ::entity::id entity)
{
	auto it = m_chunk_entity_map.find(chunk);
	if (it == m_chunk_entity_map.end()) {
		it = m_chunk_entity_map.emplace(chunk, std::set<::entity::id>()).first;
	}
	it->second.emplace(entity);
	this->m_entity_chunk_map.emplace(entity, chunk);
}

void entity::storage_system::deregister_entity_position(ccoord_t const& chunk, ::entity::id entity)
{
	auto it = m_chunk_entity_map.find(chunk);
	if (it == m_chunk_entity_map.end()) {
		return;
	}
	auto& ids = it->second;
	ids.erase(entity);
	if (ids.empty()) {
		this->m_chunk_entity_map.erase(it);
	}
	this->m_entity_chunk_map.erase(entity);
}
