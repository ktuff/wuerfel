#pragma once

#include <common/entity/systems/inventory_system.hpp>
#include <common/level.hpp>

#include "packager.hpp"
#include "entity/network.hpp"
#include "entity/spawning.hpp"
#include "entity/storage.hpp"
#include "systems/block_tick_system.hpp"
#include "systems/circuit_scheduler.hpp"
#include "systems/light_scheduler.hpp"
#include "util/generation.hpp"
#include "util/tree_generation.hpp"


namespace server {

class packager;


class controller : public world_listener {

	private:
		// base info
		game::level m_level;
		packager m_network;
		uint64_t m_ticks;

		// player states
		std::vector<::entity::entity*> m_players;
		std::vector<ccoord_t> m_player_positions;

		std::unordered_map<::entity::id, int> m_connections;
		std::unordered_map<ccoord_t, std::set<::entity::id>> m_loaded_chunks;
		bool m_player_change;

		// systems world
		generation m_world_generator;
		tree_generation m_tree_generator;
		circuit::scheduler m_circuit_system;
		light::scheduler m_light_system;
		server::block_tick_system m_block_tick_system;

		// systems entity
		::entity::inventory_system m_inventory;
		::entity::network_system m_entity_network;
		::entity::spawning m_entity_spawning;
		::entity::storage_system m_entity_storage;


	public:
		controller();
		~controller();


	public:
		int open(std::string const& level_name, int port);
		void close();

		void on_destruction() override final;
		void on_chunk_add(coord_t x, coord_t z) override final;
		void on_chunk_remove(coord_t x, coord_t z) override final;
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block) override final;
		void on_rotation_change(coord_t x, coord_t y, coord_t z, side_t value) override final;
		void on_state_change(coord_t x, coord_t y, coord_t z, state_t value) override final;
		void on_power_change(coord_t x, coord_t y, coord_t z, power_t value) override final;
		void on_light_change(coord_t x, coord_t y, coord_t z, light_t value) override final;

		void tick(int64_t ns);
		void set_player_limit(size_t limit);

	private:
		void join_player(int index);
		void kick_player(int index);

		void reset();
		void update_chunks();
		void load_chunks();
		void unload_chunks();

		void load_player_area(const ccoord_t& pos, int index, bool force = false);
		void unload_player_area(int index);
		void save_chunks(bool all);
		void sweep_empty_chunks();
		void send_entities(int index, ::entity::id player);

		void load_batch(std::vector<ccoord_t> const& batch, coord_t xi, coord_t zi);
		void save_batch(std::vector<ccoord_t> const& batch, coord_t xi, coord_t zi);

		void generate_world_folder(std::string const& world_name);
		void load_level(std::string const& level_name);
		void save_level();

		void update_entities(int64_t ns);

};

} // namespace server
