#pragma once

#include <functional>
#include <string>
#include <vector>

#include <common/net/rsa_key.hpp>


struct sqlite3;


namespace server {

class packager;


enum authentication_state {
	no_auth_attempt,
	waiting_for_auth,
	initiated_first_auth,
	initiated_reauth
};


class authentication {

	private:
		server::packager* m_network;
		rsa_key m_rsa_server;
		std::vector<rsa_key> m_rsa_clients;
		std::vector<std::string> m_usernames;
		std::vector<authentication_state> m_states;
		std::vector<std::vector<unsigned char>> m_challenges;
		sqlite3* m_key_database;
		std::function<void(int, bool)> m_auth_callback;


	public:
		authentication(server::packager* network, int num_connections);
		~authentication();


	public:
		void set_authentication_callback(std::function<void(int, bool)> const& callback);
		void prepare_login(int connection_index);
		void init_login(int connection_index, std::string const& username, std::string const& public_key_b64);
		bool validate_login(int connection_index, std::string const& response_b64);
		void abort_login(int connection_index);

	private:
		void save_to_database(std::string const& username, std::vector<char> const& key);
		bool load_from_database(std::string const& username, std::vector<char>& key);
		void check(int error_code);

};

} // namespace server
