#include <string.h>
#include <zlib.h>
#include <filesystem>

#include <ktf/util/logger.hpp>
#include <ktf/util/pointer.h>

#include <server/config.hpp>
#include <common/entity/entities/item.hpp>
#include <common/entity/entities/player.hpp>
#include <common/item/inventory.hpp>
#include <common/util/chunk_compression.hpp>
#include <common/world/voxel.hpp>

#include "controller.hpp"
#include "packager.hpp"
#include "util/filesystem.hpp"


server::controller::controller()
: m_level(true)
, m_network(&m_level)
, m_player_change(false)
, m_world_generator(&m_tree_generator)
, m_tree_generator(&m_level)
, m_circuit_system(m_level.get_world())
, m_light_system(m_level.get_world())
, m_block_tick_system(&m_level, &m_tree_generator)
, m_inventory(m_level.get_entity_system())
, m_entity_network(m_level.get_entity_system(), &m_network)
, m_entity_spawning(m_level.get_entity_system())
, m_entity_storage(m_level.get_entity_system(), &m_level)
{
	this->m_level.get_world()->add_listener(this);
	this->m_network.set_authentication_callback([this](int index, bool success) {
		if (success) {
			this->join_player(index);
		} else {
			this->kick_player(index);
		}
	});

	this->m_players.resize(64);
	this->m_player_positions.resize(64);
	for (size_t i=0; i<m_players.size(); ++i) {
		this->m_players[i] = nullptr;
	}
}

server::controller::~controller()
{
	this->close();
	this->m_level.get_world()->remove_listener(this);
}


int server::controller::open(std::string const& level_name, int port)
{
	assert(!m_network.is_open());

	if (!std::filesystem::exists("saves")) {
		std::filesystem::create_directory("saves");
	}
	this->generate_world_folder(level_name);
	this->load_level(level_name);
	this->m_ticks = 0;
	return m_network.create(port);
}

void server::controller::close()
{
	this->save_level();
	this->reset();
}

void server::controller::on_destruction()
{
}

void server::controller::on_chunk_add(coord_t x, coord_t z)
{
	auto chunk = m_level.get_chunk(x, z);

	this->m_light_system.on_chunk_change(x, z);

	this->m_entity_storage.load_chunk(ccoord_t(x, z));
}

void server::controller::on_chunk_remove(coord_t x, coord_t z)
{
	this->m_entity_storage.save_chunk(ccoord_t(x, z));
}

void server::controller::on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block)
{
	if (new_block == e_block_air && old_block != e_block_air) {
		// TODO remove after time
		auto* entities = m_level.get_entity_system();
		auto* item = entities->create_entity_item();
		item->set_position(ktf::vec3<double>(x, y, z));
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_level.get_entity_system()->get_module(::entity::inventory_system::s_id));
		inv->set_item(item->m_id, ::entity::inventory_group::e_items, 0, item::item_stack(item::get_type(old_block), 1));
	}
	if (def::block_defs[old_block].circuit || def::block_defs[new_block].circuit) {
		this->m_circuit_system.on_block_change(x, y, z, old_block, new_block);
	}

	this->m_light_system.on_block_change(x, y, z, old_block, new_block);
	side_t side = this->m_level.get_orientation(x, y, z);
	this->m_network.send_set_block(-1, x, y, z, new_block, side);
}

void server::controller::on_rotation_change(coord_t x, coord_t y, coord_t z, side_t value)
{
	this->m_network.send_set_rotation(-1, x, y, z, value);
}

void server::controller::on_state_change(coord_t x, coord_t y, coord_t z, state_t value)
{
	this->m_network.send_set_state(-1, x, y, z, value);
}

void server::controller::on_power_change(coord_t x, coord_t y, coord_t z, power_t value)
{
	this->m_network.send_set_power(-1, x, y, z, value);
}

void server::controller::on_light_change(coord_t x, coord_t y, coord_t z, light_t value)
{
	this->m_network.send_set_light(-1, x, y, z, value);
}

void server::controller::tick(int64_t ns)
{
	// process network
	this->m_network.tick();

	int time_slice = m_ticks % 4;
	if (time_slice == 0) {
		this->update_chunks();
	} else if (time_slice == 1) {
		this->m_light_system.tick();
	} else if (time_slice == 2) {
		this->m_circuit_system.tick();
	} else if (time_slice == 3) {
		this->m_entity_spawning.tick();
	}

	this->m_block_tick_system.update();
	this->update_entities(ns);
	this->m_ticks = m_ticks + 1;
}

void server::controller::set_player_limit(size_t limit)
{
	if (limit > m_players.size()) {
		this->m_players.resize(limit);
		this->m_player_positions.resize(limit);
		this->m_network.set_player_limit(limit);
	} else {
		ktf::logger::error("Can not decrease number of maximum players");
	}
}

void server::controller::join_player(int index)
{
	ktf::logger::info("Player connect: " + std::to_string(index));
	this->m_network.register_player(index, 0); // enable packet distribution

	// setup entity info
	auto* entities = m_level.get_entity_system();
	::entity::player* player = entities->create_entity_player();
	this->m_players[index] = player;
	this->m_network.register_player(index, player->m_id);
	this->m_connections[player->m_id] = index;
	this->m_network.send_auth_result(index, true, player->m_id);

	// init area info
	ccoord_t pos(0, 0); // TODO
	this->m_player_positions[index] = pos;
	this->load_player_area(pos, index, true);
	this->send_entities(index, player->m_id);
	this->m_network.send_chunks_loaded(index);

	this->m_player_change = true;
}

void server::controller::kick_player(int index)
{
	this->m_network.send_auth_result(index, false, 0);

	if (m_players[index] != nullptr) {
		// delete player info
		this->unload_player_area(index);
		// NOTE reset m_player_positions[index]?
		this->m_connections.erase(m_players[index]->m_id);
		auto* entities = m_level.get_entity_system();
		entities->destroy_entity(m_players[index]->m_id);
		this->m_network.unregister_player(index);
		this->m_players[index] = nullptr;
		this->m_player_change = true;
		ktf::logger::info("Player disconnect: " + std::string("<not implemented>"));
	}
}

void server::controller::reset()
{
	this->m_network.destroy();
	for (size_t i=0; i<m_players.size(); ++i) {
		this->kick_player(i);
	}

	// should be unneccessary
	for (size_t i=0; i<m_players.size(); ++i) {
		this->m_players[i] = nullptr;
	}
	this->unload_chunks();
	this->m_loaded_chunks.clear();

	this->m_circuit_system.reset();
	this->m_light_system.reset();
	this->m_block_tick_system.reset();

	this->m_level.reset();
	this->m_player_change = false;
}

void server::controller::update_chunks()
{
	this->load_chunks();
	this->unload_chunks();

	for (auto& [id, index] : m_connections) {
		auto const& position = m_players[index]->get_position();
		ccoord_t pos(position.x / CHUNK_SIZE, position.z / CHUNK_SIZE);
		this->m_player_positions[index] = pos;
	}
}

void server::controller::load_chunks()
{
	for (auto& [id, index] : m_connections) {
		auto const& position = m_players[index]->get_position();
		ccoord_t pos(position.x / CHUNK_SIZE, position.z / CHUNK_SIZE);
		this->load_player_area(pos, index);
	}
}

void server::controller::unload_chunks()
{
	const int dist = server::config.get_int("distance") + 2;
	for (auto& [id, index] : m_connections) {
		auto const* p = m_players[index];
		auto const& position = p->get_position();
		ccoord_t pos(position.x / CHUNK_SIZE, position.z / CHUNK_SIZE);
		ccoord_t const& prev = m_player_positions[m_connections.at(p->m_id)];
		if (prev != pos) {
			this->m_player_change = true;
			for (auto& c : m_loaded_chunks) { // NOTE runtime could be reduced by reverse mapping
				if (pos.dist(c.first) > dist) {
					if (c.second.find(p->m_id) != c.second.end()) {
						this->m_network.send_chunk_remove(m_connections[p->m_id], c.first);
						c.second.erase(p->m_id);
					}
				}
			}
		}
	}
	this->save_chunks(false);
	this->sweep_empty_chunks();
}

void server::controller::load_player_area(const ccoord_t& pos, int index, bool force)
{
	const ccoord_t& prev = m_player_positions[index];
	if (prev != pos || force) {
		std::vector<std::pair<ccoord_t, uint64_t>> chunks_load;
		const int dist = server::config.get_int("distance");
		auto world = m_level.get_world();
		for (coord_t x=pos.x-dist; x<pos.x+dist; x++) {
			for (coord_t z=pos.y-dist; z<pos.y+dist; z++) {
				if (!world->chunk_exists(x, z) && pos.dist(ccoord_t(x, z)) <= dist) {
					chunks_load.push_back(std::make_pair(ccoord_t(x, z), m_players[index]->m_id));
				}
			}
		}
		this->m_player_change |= !chunks_load.empty();

		std::unordered_map<ccoord_t, std::vector<ccoord_t>> batches;
		for (auto const& c : chunks_load) {
			coord_t xi = c.first.x / coord_t(io::NUM_CHUNKS_SIDE) - (c.first.x < 0);
			coord_t zi = c.first.y / coord_t(io::NUM_CHUNKS_SIDE) - (c.first.y < 0);
			ccoord_t batch_id(xi, zi);
			batches[batch_id].push_back(c.first);
		}
		for (auto const& b : batches) {
			this->load_batch(b.second, b.first.x, b.first.y);
		}
		for (auto const& c : chunks_load) {
			this->m_loaded_chunks[c.first].emplace(c.second);
			auto chunk = m_level.get_chunk(c.first.x, c.first.y);
			this->m_network.send_chunk_add(m_connections[c.second], chunk);
		}
	}
}

void server::controller::unload_player_area(int index)
{
	::entity::id id = m_players[index]->m_id;
	for (auto& c : m_loaded_chunks) {
		if (c.second.find(id) != c.second.end()) {
			this->m_network.send_chunk_remove(m_connections[id], c.first);
			c.second.erase(id);
		}
	}
	this->m_player_change = true;
}

void server::controller::save_chunks(bool all)
{
	if (m_player_change) {
		std::vector<ccoord_t> chunk_list;
		for (auto& c : m_loaded_chunks) {
			if (all || c.second.empty()) {
				chunk_list.push_back(c.first);
			}
		}

		std::unordered_map<ccoord_t, std::vector<ccoord_t>> batches;
		for (auto const& c : chunk_list) {
			coord_t xi = c.x / coord_t(io::NUM_CHUNKS_SIDE) - (c.x < 0);
			coord_t zi = c.y / coord_t(io::NUM_CHUNKS_SIDE) - (c.y < 0);
			ccoord_t batch_id(xi, zi);
			batches[batch_id].push_back(c);
		}
		for (auto const& b : batches) {
			this->save_batch(b.second, b.first.x, b.first.y);
		}
		this->m_player_change = false;
	}
}

void server::controller::sweep_empty_chunks()
{
	std::vector<ccoord_t> chunks_unload;
	for (auto& c : m_loaded_chunks) {
		if (c.second.empty()) {
			chunks_unload.push_back(c.first);
		}
	}
	for (auto const& c : chunks_unload) {
		this->m_level.remove_chunk(c.x, c.y);
		this->m_loaded_chunks.erase(c);
	}
}

void server::controller::send_entities(int index, ::entity::id player)
{
	for (auto& c : m_loaded_chunks) {
		if (c.second.find(player) != c.second.end()) {
			nlohmann::json data;
			size_t num_entities = m_entity_storage.get_batch_json(c.first, data);
			if (num_entities > 0) {
				this->m_network.send_entity_batch(index, data);
			}
		}
	}
}

void server::controller::load_batch(std::vector<ccoord_t> const& batch, coord_t xi, coord_t zi)
{
	std::string level_name = m_level.get_name();
	std::string const path("saves/" + level_name + "/region/chunk." + std::to_string(xi) + "." + std::to_string(zi) + ".mca");
	io::chunk_file file(path);
	if (!file.is_open()) {
		return;
	}
	size_t initialized_chunks = 0;
	for (auto const& c : batch) {
		std::unique_ptr<char[]> compressed_data;
		size_t size;
		bool valid = file.load_chunk(c.x, c.y, compressed_data, size);
		if (valid) {
			auto chunk = ::wfl_decompress_chunk(m_level.get_world(), compressed_data.get(), size);
			if (chunk) {
				this->m_level.put_chunk(c.x, c.y, chunk);
			} else {
				valid = false;
			}
		}
		if (!valid) {
			std::shared_ptr<game::chunk> chunk = m_world_generator.create_chunk(m_level.get_world(), c.x, c.y);
			chunk->set_is_modified(true);
			this->m_level.put_chunk(c.x, c.y, chunk);
			this->m_light_system.init_chunk(chunk);
			++initialized_chunks;
		}
		if (initialized_chunks >= 16) {
			this->m_light_system.tick();
			initialized_chunks = 0;
		}
	}
	// process remaining
	this->m_light_system.tick();
}

void server::controller::save_batch(std::vector<ccoord_t> const& batch, coord_t xi, coord_t zi)
{
	std::string const& level_name = m_level.get_name();
	std::string const path("saves/" + level_name + "/region/chunk." + std::to_string(xi) + "." + std::to_string(zi) + ".mca");
	io::chunk_file file(path);
	if (!file.is_open()) {
		return;
	}
	for (auto const& c : batch) {
		auto chunk = m_level.get_chunk(c.x, c.y);
		if (chunk->is_modified()) {
			auto data = ::wfl_compress_chunk(chunk);
			if (data.first) {
				chunk->set_is_modified(false);
				file.save_chunk(c.x, c.y, data.first.get(), data.second);
			}
		}
	}
}

void server::controller::generate_world_folder(const std::string& world_name)
{
	if (world_name.empty()) {
		throw std::runtime_error("Empty world name is not allowed!");
	}

	std::string world_folder = "saves/" + world_name;
	if (!std::filesystem::exists(world_folder)) {
		if (!std::filesystem::create_directory(world_folder)) {
			throw std::runtime_error("Could not create world folder!");
		}
		if (!std::filesystem::create_directory(world_folder + "/region")) {
			throw std::runtime_error("Could not create world folder!");
		}

		nlohmann::json world_data;
		world_data["name"] = world_name;
		world_data["seed"] = 0;
		world_data["ticks"] = 0;
		world_data["nextEntityID"] = ::entity::id(1);

		std::ofstream data_file;
		data_file.open(world_folder + "/world.json");
		if (!data_file.is_open()) {
			throw std::runtime_error("Could not create world config!");
		}
		data_file << world_data.dump(4);
		data_file.close();
	}

	std::string region_folder = "saves/" + world_name + "/region";
	std::string entity_folder = "saves/" + world_name + "/entity";
	if (!std::filesystem::exists(region_folder)) {
		std::filesystem::create_directory(region_folder);
	}
	if (!std::filesystem::exists(entity_folder)) {
		std::filesystem::create_directory(entity_folder);
	}
}

void server::controller::load_level(std::string const& level_name)
{
	if (!m_level()) {
		std::string level_folder = "saves/" + level_name;
		std::ifstream data_file;
		data_file.open(level_folder + "/world.json");
		if (!data_file.is_open()) {
			throw std::runtime_error("Could not load world config!");
		}

		nlohmann::json world_data;
		data_file >> world_data;
		std::string name;
		uint64_t seed;
		uint64_t ticks;
		::entity::id start_id;
		world_data["name"].get_to(name);
		world_data["seed"].get_to(seed);
		world_data["ticks"].get_to(ticks);
		world_data["nextEntityID"].get_to(start_id);

		this->m_level.init(name, seed, ticks, start_id);
	} else {
		throw std::runtime_error("Level already open!");
	}
}

void server::controller::save_level()
{
	if (m_level()) {
		nlohmann::json world_data;
		std::string const& level_name = m_level.get_name();
		world_data["name"] = level_name;
		world_data["seed"] = m_level.get_seed();
		world_data["ticks"] = m_level.get_ticks();
		world_data["nextEntityID"] = m_level.get_entity_system()->get_next_id();

		std::string level_folder = "saves/" + level_name;
		std::ofstream data_file;
		data_file.open(level_folder + "/world.json");
		if (!data_file.is_open()) {
			throw std::runtime_error("Could not load world config!");
		}

		data_file << world_data;

		this->save_chunks(true);
	}
}

void server::controller::update_entities(int64_t ns)
{
	auto* entity_system = m_level.get_entity_system();

	entity_system->update(ns);

	entity_system->sweep_discarded();
}
