#include <string.h>

#include <fstream>
#include <string>

#include <server/config.hpp>
#include <server/server.hpp>

int main(int argc, char **argv)
{    
	server::server_instance app(true);

	int port = 25564;
	if (argc > 1) {
		for (int i=1; i<argc; i++) {
			if (!strcmp(argv[i], "-p")) {
				port = std::stoi(argv[i+1]);
				i++;
			}
		}
	}

	nlohmann::json server_config;
	std::string world_name;

	{
		std::ifstream config_file;
		config_file.open("server-config.json");
		if (config_file.is_open()) {
			config_file >> server_config;
		} else {
			server_config["world"] = std::string("world");
		}
	}
	server_config["world"].get_to(world_name);

	app.start(world_name, port, std::function<void(int)>());

	{
		std::ofstream config_file;
		config_file.open("server-config.json");
		if (config_file.is_open()) {
			config_file << server_config;
		} else {
			throw std::runtime_error("Could not save config file!");
		}
	}
    
	return 0;
}
