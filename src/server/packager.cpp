#include <string.h>

#include <ktf/util/logger.hpp>

#include <common/level.hpp>
#include <common/net/packet_handler.hpp>
#include <common/net/types.hpp>
#include <common/util/chunk_compression.hpp>
#include <common/validation/block.hpp>
#include <common/validation/state.hpp>
#include <server/entity/network.hpp>

#include "packager.hpp"


// TODO move to config
const int max_players = 64;


server::packager::packager(game::level* level)
: m_socket(max_players)
, m_auth(this, max_players)
, m_level(level)
, m_player_ids(max_players)
, m_packet_batches(max_players)
{
	for (size_t i=0; i<max_players; i++) {
		this->m_player_ids[i] = ::entity::id(0);
		this->m_packet_batches[i] = nlohmann::json::array();
	}
	this->m_socket.set_timeout_limit(30000);
	this->m_socket.set_callbacks(
		[this](size_t index) {
			this->m_auth.prepare_login(index);
			this->m_active_connections.emplace(index);

			std::chrono::seconds now = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
			std::lock_guard<std::mutex> guard(m_connection_mutex);
			this->m_waiting_players.emplace(std::make_pair(index, now));
		},
		[this](size_t index, tcp_connection_close_status code) {
			this->m_active_connections.erase(index);

			const char* reason = "";
			if (code == tcp_closed_normal) {
				reason = "logout / disconnect";
			} else if (code == tcp_closed_by_timeout) {
				reason = "timeout";
			} else if (code == tcp_closed_by_server) {
				reason = "closed by server";
			} else {
				reason = "other reason <not implemented>";
			}

			std::lock_guard<std::mutex> guard(m_connection_mutex);
			if (m_waiting_players.find(index) != m_waiting_players.end()) {
				this->m_waiting_players.erase(index);
			}
		});
}

server::packager::~packager()
{
}


int server::packager::create(int port)
{
	this->m_socket.launch(port, [](uint16_t) {
		// TODO ?
	});
	if (!m_socket.is_up()) {
		ktf::logger::error("Failed to launch server");
	}
	return m_socket.get_port();
}

void server::packager::destroy()
{
	this->m_socket.stop();
	for (size_t i=0; i<max_players; i++) {
		this->m_player_ids[i] = ::entity::id(0);
		this->m_packet_batches[i].clear();
	}
	this->m_socket_ids.clear();
}

void server::packager::set_player_limit(size_t limit)
{
	if (limit > m_player_ids.size()) {
		this->m_player_ids.resize(limit);
		this->m_packet_batches.resize(limit);
		// TODO
		// TODO m_socket
		// TODO m_auth
	}
}

bool server::packager::is_open() const
{
	return m_socket.is_up();
}

void server::packager::tick()
{
	if (!is_open()) {
		return;
	}

	this->m_socket.poll([this](size_t index, tcp_packet const& packet) {
		auto* m = m_level->get_entity_system()->get_module(::entity::network_system::s_id);
		auto* network_system = dynamic_cast<::entity::network_system*>(m);

		auto decompressed_data = ::decompress(packet.m_data.get(), packet.m_size);

		nlohmann::json packet_batch = nlohmann::json::parse(std::string(decompressed_data.first.get(), decompressed_data.second));
		for (nlohmann::json const& pkt : packet_batch) {
			packet_emitter_type op_code = pkt["emitter"].get<packet_emitter_type>();
			auto const& packet_data = pkt["data"];

			if (m_socket_ids.find(index) == m_socket_ids.end() && op_code != e_op_auth) {
				// ignore packages if player not authenticated
				continue;
			}

			switch (op_code) {
			case e_op_auth:
				this->process_auth_packet(index, packet_data);
				break;
			case e_op_general:
				this->process_general_packet(index, packet_data);
				break;
			case e_op_entity: {
				network_system->process_packet(packet_data);
				break;
			}
			case e_op_none:
				break;
			}
		}
	});

	for (int index : m_active_connections) {
		std::string data = m_packet_batches[index].dump();
		auto compressed_data = ::compress(data.data(), data.size());
		tcp_packet packet(compressed_data.first, compressed_data.second);
		this->m_socket.send(packet, index);
		this->m_packet_batches[index] = nlohmann::json::array();
	}

	// disconnect all connections without login after fixed time duration
	int seconds_timeout = 30; // TODO move timeout interval to config
	static std::chrono::seconds last = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::seconds now = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
	if ((now - last).count() >= 2) {
		std::lock_guard<std::mutex> guard(m_connection_mutex);
		std::vector<int> timeouted_connections;
		for (auto& con : m_waiting_players) {
			if ((now - con.second).count() > seconds_timeout) {
				timeouted_connections.push_back(con.first);
			}
		}
		for (int index : timeouted_connections) {
			this->m_socket.close_connection(index);
			this->m_waiting_players.erase(index);
		}
		last = now;
	}
}

void server::packager::set_authentication_callback(std::function<void (int, bool)> const& callback)
{
	this->m_auth.set_authentication_callback(callback);
}

void server::packager::register_player(int index, ::entity::id player)
{
	this->m_player_ids[index] = player;
	this->m_socket_ids.emplace(index);

	std::lock_guard<std::mutex> guard(m_connection_mutex);
	this->m_waiting_players.erase(index);
}

void server::packager::unregister_player(int index)
{
	this->m_player_ids[index] = ::entity::id(0);
	this->m_socket_ids.erase(index);
	this->m_packet_batches[index] = nlohmann::json::array();
}

void server::packager::send_auth_challenge(int index, const std::string& pbk_b64, const std::string& challenge_b64)
{
	nlohmann::json data;
	data["packetType"] = packet_type::packet_auth_challenge;
	data["key"] = pbk_b64;
	data["challenge"] = challenge_b64;

	this->push_auth_packet(index, data);
}

void server::packager::send_auth_result(int index, bool success, ::entity::id id)
{
	nlohmann::json data;
	data["packetType"] = packet_type::packet_auth_result;
	data["success"] = success;
	data["target"] = id;

	this->push_auth_packet(index, data);
}

void server::packager::send_chunks_loaded(int id)
{
	packets::packet_t packet = packets::s_chunks_loaded();
	this->push_general_packet(id, packet);
}

void server::packager::send_chunk_add(int id, const std::shared_ptr<game::chunk>& chunk)
{
	bool success = false;
	packets::packet_t packet = packets::s_chunk_add(chunk, success);
	if (success) {
		this->push_general_packet(id, packet);
	}
}

void server::packager::send_chunk_remove(int id, const ccoord_t& cc)
{
	packets::packet_t packet = packets::s_chunk_remove(cc.x, cc.y);
	this->push_general_packet(id, packet);
}

void server::packager::send_entity_batch(int index, const nlohmann::json& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["packetType"] = packet_entity_batch;
	packet["entities"] = data;
	this->push_general_packet(index, packet);
}

void server::packager::send_set_block(int id, coord_t x, coord_t y, coord_t z, block_t block, side_t side)
{
	packets::packet_t packet = packets::s_set_block(x, y, z, block, side);
	this->push_general_packet(id, packet);
}

void server::packager::send_set_rotation(int id, coord_t x, coord_t y, coord_t z, side_t value)
{
	packets::packet_t packet = packets::s_set_rotation(x, y, z, value);
	this->push_general_packet(id, packet);
}

void server::packager::send_set_state(int id, coord_t x, coord_t y, coord_t z, state_t value)
{
	packets::packet_t packet = packets::s_set_state(x, y, z, value);
	this->push_general_packet(id, packet);
}

void server::packager::send_set_power(int id, coord_t x, coord_t y, coord_t z, power_t value)
{
	packets::packet_t packet = packets::s_set_power(x, y, z, value);
	this->push_general_packet(id, packet);
}

void server::packager::send_set_light(int id, coord_t x, coord_t y, coord_t z, light_t value)
{
	packets::packet_t packet = packets::s_set_light(x, y, z, value);
	this->push_general_packet(id, packet);
}

void server::packager::send_player_pos(int id, uint64_t pid, ktf::vec3<double>& pos)
{
	packets::packet_t packet = packets::s_player_pos(pid, pos);
	this->push_general_packet(id, packet);
}

void server::packager::send_player_rot(int id, uint64_t pid, ktf::vec3<double>& rot)
{
	packets::packet_t packet = packets::s_player_rot(pid, rot);
	this->push_general_packet(id, packet);
}

void server::packager::push_auth_packet(int index, const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_auth;
	packet["data"] = data;
	this->enqueue_packet(index, packet);
}

void server::packager::push_general_packet(int index, const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_general;
	packet["data"] = data;
	this->enqueue_packet(index, packet);
}

void server::packager::push_entity_packet(int index, const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_entity;
	packet["data"] = data;
	this->enqueue_packet(index, packet);
}

void server::packager::enqueue_packet(int index, const packets::packet_t& packet)
{
	if (index == -1) {
		for (int socket_id : m_socket_ids) {
			this->m_packet_batches[socket_id] += packet;
		}
	} else {
		this->m_packet_batches[index] += packet;
	}
}

void server::packager::process_auth_packet(int index, const packets::packet_t& data)
{
	packet_type type = data["packetType"].get<packet_type>();
	switch (type) {
	case packet_auth_login:
		this->recv_auth_init(index, data);
		break;
	case packet_auth_response:
		this->recv_auth_response(index, data);
		break;
	default:
		// TODO error
		break;
	}
}

void server::packager::process_general_packet(int index, packets::packet_t const& data)
{
	packet_type type;
	data["packetType"].get_to(type);
	switch (type) {
	case packet_block:
		this->recv_set_block(index, data);
		break;
	case packet_rotation:
		this->recv_set_rotation(index, data);
		break;
	case packet_state:
		this->recv_set_state(index, data);
		break;
	case packet_power:
		this->recv_set_power(index, data);
		break;
	case packet_light:
		this->recv_set_light(index, data);
		break;
	case packet_player_pos:
		this->recv_player_pos(index, data);
		break;
	case packet_player_rot:
		this->recv_player_rot(index, data);
		break;
	default:
		break;
	}
}

void server::packager::recv_auth_init(int index, const packets::packet_t& packet)
{
	auto const& username = packet["user"].get<std::string>();
	auto const& public_key = packet["key"].get<std::string>();
	this->m_auth.init_login(index, username, public_key);
}

void server::packager::recv_auth_response(int index, const packets::packet_t& packet)
{
	auto const& reply = packet["reply"].get<std::string>();
	this->m_auth.validate_login(index, reply);
}

void server::packager::recv_set_block(int, const packets::packet_t& packet)
{
	coord_t x, y, z;
	block_t block;
	side_t side;
	packets::r_set_block(packet, x, y, z, block, side);
	if (common::block::is_placeable(m_level, block, side, x, y, z)) {
		this->m_level->set_block(x, y, z, block, side);
	} else {
		// TODO NACK
	}
}

void server::packager::recv_set_rotation(int, const packets::packet_t& packet)
{
	coord_t x, y, z;
	side_t value;
	packets::r_set_rotation(packet, x, y, z, value);
	// TODO validation
	this->m_level->set_orientation(x, y, z, value);
}

void server::packager::recv_set_state(int, const packets::packet_t& packet)
{
	coord_t x, y, z;
	state_t value;
	packets::r_set_state(packet, x, y, z, value);
	// TODO validation
	if (common::state::is_valid_state(m_level, value, x, y, z)) {
		this->m_level->set_state(x, y, z, value);
	} else {
		// TODO NACK
	}
}

void server::packager::recv_set_power(int, const packets::packet_t& packet)
{
	coord_t x, y, z;
	power_t value;
	packets::r_set_power(packet, x, y, z, value);
	// TODO validation
	this->m_level->set_power(x, y, z, value);

	// TODO remove
}

void server::packager::recv_set_light(int, const packets::packet_t& packet)
{
	coord_t x, y, z;
	light_t value;
	packets::r_set_light(packet, x, y, z, value);
	// TODO validation
	this->m_level->set_light(x, y, z, value);

	// TODO remove
}

void server::packager::recv_player_pos(int id, const packets::packet_t& packet)
{
	if (m_player_ids[id] == 0) {
		return;
	}
	uint64_t pid;
	ktf::vec3<double> pos;
	packets::r_player_pos(packet, pid, pos);

	if (auto* entity = m_level->get_entity(m_player_ids[id])) {
		entity->set_position(pos);
	}
}

void server::packager::recv_player_rot(int id, const packets::packet_t& packet)
{
	if (m_player_ids[id] == 0) {
		return;
	}
	uint64_t pid;
	ktf::vec3<double> rot;
	packets::r_player_rot(packet, pid, rot);

	if (auto* entity = m_level->get_entity(m_player_ids[id])) {
		entity->set_rotation(rot);
	}
}
