#include <string.h>

#include <common/world/chunk.hpp>
#include <common/world/voxel.hpp>

#include "generation.hpp"
#include "tree_generation.hpp"


const double noise_base = 0.44;
const double river_eps = 0.002;


generation::generation(server::tree_generation* tree_generation)
: m_noise(0)
, m_tree_generation(tree_generation)
{
}


void generation::set_seed(uint64_t seed)
{
	this->m_noise.set_seed(seed);
}

std::shared_ptr<game::chunk> generation::create_chunk(game::world* world, int64_t x, int64_t z)
{
	voxel* voxels[NUM_SUBCHUNKS];
	uint64_t bpsc[NUM_SUBCHUNKS];
	int16_t height[CHUNK_SIZE * CHUNK_SIZE];
	::memset(voxels, '\0', sizeof(voxels));
	::memset(bpsc, '\0', sizeof(bpsc));

	coord_t hmax = -1;
	ccoord_t const pos(x*CHUNK_SIZE, z*CHUNK_SIZE);
	for(coord_t x0=0; x0<CHUNK_SIZE; x0++) {
		for(coord_t z0=0; z0<CHUNK_SIZE; z0++) {
			float hf = height_terrain(x0 + pos.x, z0 + pos.y, 1200.0f, 64.0f);
			coord_t h = hf;
			height[x0*CHUNK_SIZE + z0] = h;
			hmax = std::max(hmax, h);
		}
	}

	coord_t n_subchunks = hmax / CHUNK_SIZE;
	for(coord_t c=0; c<=n_subchunks; c++) {
		voxels[c] = new voxel[BLOCKS_PER_SUBCHUNK];
		::memset(voxels[c], '\0', sizeof(voxel) * BLOCKS_PER_SUBCHUNK);
	}

	for(coord_t x0=0; x0<CHUNK_SIZE; x0++) {
		for(coord_t z0=0; z0<CHUNK_SIZE; z0++) {
			coord_t xi = x0 + pos.x, zi = z0 + pos.y;
			coord_t hi = height[x0*CHUNK_SIZE + z0];
			for (coord_t h = n_subchunks * CHUNK_SIZE + CHUNK_SIZE-1; h>hi; --h) {
				voxels[h/CHUNK_SIZE][game::chunk::get_index(xi, h, zi)].light_value = empty_voxel_sky.light_value;
			}
			voxels[hi/CHUNK_SIZE][game::chunk::get_index(xi, hi, zi)].block_id = e_block_grass;
			bpsc[hi/CHUNK_SIZE]++;
			for(coord_t h = hi-1; h>=0; --h) {
				voxels[h/CHUNK_SIZE][game::chunk::get_index(xi, h, zi)].block_id = e_block_stone;
				bpsc[h/CHUNK_SIZE]++;
			}
		}
	}

	std::shared_ptr<game::chunk> chunk = std::make_shared<game::chunk>(world, x, z);
	chunk->fill(voxels, bpsc);
	this->decorate_chunk(chunk);

	for(coord_t c=0; c<=n_subchunks; c++) {
		delete[] voxels[c];
	}
	return chunk;
}

void generation::decorate_chunk(std::shared_ptr<game::chunk> chunk)
{
	for (int64_t w=0; w<0+CHUNK_SIZE; w++) {
		for (int64_t d=0; d<0+CHUNK_SIZE; d++) {
			int64_t height = chunk->get_height(w, d);
			if (height >= WORLD_MIN_Y && height < WORLD_MAX_Y) {
				if ((w+d+rand()) % 60 == 0 && height/40.0f * height/40.0f < 12) {
					this->m_tree_generation->generate_default_tree(chunk->posw.x+w, height+1, chunk->posw.y+d);
				}
			}
		}
	}
}


double generation::mix(double x, double y, double a) const
{
	return x * (1.0 - a) + y * a;
}

double generation::interpolate_linear(double p0, double p1, double fac, double c0, double c1) const
{
	double dx = 1.0 / (p1 - p0);
	double offset = dx * p0;
	fac = std::clamp(fac * dx - offset, 0.0, 1.0);
	return mix(c0, c1, fac);
}

double generation::interpolate_ease(double p0, double p1, double fac, double c0, double c1) const
{
	double dx = 1.0 / (p1 - p0);
	double offset = dx * p0;
	fac = std::clamp(fac * dx - offset, 0.0, 1.0);
	fac = (3.0 - 2.0 * fac) * fac * fac;
	return mix(c0, c1, fac);
}

double generation::noise_terrain(double x, double y, double scale) const
{
	x += scale * 7.0;
	double inv_scale = 1.0 / scale;
	double r1x = x, r1y = y;
	double r2x = x + scale * 1.0, r2y = y + scale*1.0;

	double noise_r1 = m_noise.noise_texture_2d(r1x, r1y, 0.2*inv_scale, 6.0, 0.5) / 2.0 + 0.5;
	double noise_r2 = m_noise.noise_texture_2d(r2x, r2y, 0.2*inv_scale, 6.0, 0.5) / 2.0 + 0.5;
	double nr1a = std::abs(0.5 - noise_r1);
	double nr2a = std::abs(0.5 - noise_r2);

	double t0 = 0.0;
	if ((nr1a < river_eps) || (nr2a < river_eps)) {
		// rivers
		double nr1b = std::clamp(river_eps - nr1a, 0.0, 1.0);
		double nr2b = std::clamp(river_eps - nr2a, 0.0, 1.0);
		double nr2c = std::max(nr1b, nr2b);
		nr2c = -0.02 * std::sqrt(nr2c);
		t0 = nr2c;
	} else {
		// terain
		double noise_t2 = m_noise.noise_texture_2d(x, y, 4.0*inv_scale, 6.0, 0.5) / 2.0 + 0.5;
		double nt2b = 0.0f;
		if (noise_t2 > noise_base) {
			nt2b = interpolate_ease(noise_base, 1.0, noise_t2, 0.0, 1.0);
		}

		// base noise - height factor
		double noise_b1 = m_noise.noise_texture_2d(x, y, 0.3*inv_scale, 1.0, 0.5) / 2.0 + 0.5;
		noise_b1 = std::clamp(noise_b1 + 0.350, 0.0, 1.0);
		noise_b1 = std::pow(noise_b1, 5.0);

		if (nt2b < 0.001f) {
			// valleys
			double noise_t1 = m_noise.noise_texture_2d(x, y, 4.0*inv_scale, 2.0, 0.5) / 2.0 + 0.5;
			noise_t1 = std::clamp(0.5 - noise_t1, 0.0, 1.0);
			noise_t1 = noise_t1 - 0.5;
			double nt2a = (noise_base - noise_t2) * 0.2;
			noise_t1 *= nt2a;
			nt2b += noise_t1;
		} else {
			// mountains
		}
		nt2b *= noise_b1;

		// smooth transition to river
		double nr2d = std::clamp(std::min(nr1a, nr2a) - 0.002, 0.0, 1.0);
		double th = nt2b * interpolate_linear(0.0, 0.015, nr2d, 0.0, 1.0);
		t0 = th;
	}
	if (t0 < 0) {
		return -std::sqrt(-t0);
	} else {
		return std::sqrt(t0);
	}
}

double generation::height_terrain(double x, double y, double scale, double base_height) const
{
	double fac = noise_terrain(x, y, scale);
	double max_height = 255.0f;
	double height = base_height + (max_height - base_height) * fac;
	return height;
}
