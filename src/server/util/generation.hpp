#pragma once

#include <stdint.h>

#include <memory>

#include <OpenSimplexNoise/fractal_noise.hpp>
#include <OpenSimplexNoise/open_simplex_noise.hpp>



namespace game {

class chunk;

} // namespace game


namespace server {

class tree_generation;

} // namespace server


class generation {

	private:
		fractal_noise<open_simplex_noise> m_noise;
		server::tree_generation* m_tree_generation;
		uint64_t seed;


	public:
		generation(server::tree_generation* tree_generation);


	public:
		void set_seed(uint64_t seed);
		std::shared_ptr<game::chunk> create_chunk(game::world* world, int64_t x, int64_t z);
		void decorate_chunk(std::shared_ptr<game::chunk> chunk);

	private:
		double mix(double x, double y, double a) const;
		double interpolate_linear(double p0, double p1, double fac, double c0, double c1) const;
		double interpolate_ease(double p0, double p1, double fac, double c0, double c1) const;
		double noise_terrain(double x, double y, double scale) const;
		double height_terrain(double x, double y, double scale, double base_height) const;
};
