#include <random>

#include <common/level.hpp>

#include "tree_generation.hpp"


server::tree_generation::tree_generation(game::level* level)
: m_level(level)
{
}


bool server::tree_generation::generate_default_tree(coord_t x, coord_t y, coord_t z)
{
	std::random_device dice;
	coord_t log_height = 3 + (dice() % 3) + 1;
	if (!::in_bounds(x-2, y, z-2) || !::in_bounds(x+2, y+log_height, z+2)) {
		return false;
	}

	if (!check_cuboid(x, y, z, x, y+log_height-1, z)) {
		return false;
	}
	if (!check_cuboid(x-1, y+log_height-1, z-1, x+1, y+log_height, z+1)) {
		return false;
	}
	if (!check_cuboid(x-2, y+log_height-3, z-2, x+2, y+log_height-1, z+2)) {
		return false;
	}

	for (coord_t yi=0; yi<log_height; ++yi) {
		coord_t radius = (yi < 4) * (coord_t(yi / 2) + 1);
		for (coord_t xi=x-radius; xi<=x+radius; ++xi) {
			for (coord_t zi=z-radius; zi<=z+radius; ++zi) {
				if ((std::abs(zi - z) + std::abs(xi - x) < 2 * radius) || (dice() % 2 == 0 && yi != 0)) {
					this->m_level->set_block(xi, y+(log_height-yi), zi, e_block_leaves_oak, e_up);
				}
			}
		}
	}
	for (coord_t yi=y; yi<y+log_height; ++yi) {
		this->m_level->set_block(x, yi, z, e_block_log_oak, e_up);
	}

	return true;
}

bool server::tree_generation::generate_big_tree()
{
	// TODO
	return false;
}

bool server::tree_generation::check_cuboid(coord_t x0, coord_t y0, coord_t z0, coord_t x1, coord_t y1, coord_t z1)
{
	for (coord_t yi=y0; yi<=y1; ++yi) {
		for (coord_t xi=x0; xi<=x1; ++xi) {
			for (coord_t zi=z0; zi<=z1; ++zi) {
				block_t block = m_level->get_block(xi, yi, zi);
				if (block != e_block_log_oak && block != e_block_leaves_oak && block != e_block_air) {
					return false;
				}
			}
		}
	}
	return true;
}
