#include <string.h>
#include <cmath>
#include <filesystem>
#include "filesystem.hpp"

namespace io {

constexpr size_t INODE_SIZE = (io::INODE_DATA_SIZE + sizeof(size_t)*2);
constexpr size_t ADDR_INODE_EMPTY_COUNT = (io::NUM_CHUNKS * sizeof(size_t) * 2);
constexpr size_t ADDR_INODE_COUNT = (ADDR_INODE_EMPTY_COUNT + sizeof(size_t));
constexpr size_t ADDR_INODE_CACHE = (ADDR_INODE_COUNT + sizeof(size_t));
constexpr size_t ADDR_DATA_BASE = (ADDR_INODE_CACHE + io::SIZE_INODE_CACHE*sizeof(size_t));

constexpr  size_t inval = (size_t)-1;
constexpr  size_t clear_values[2] = {
	inval,
	inval
};

} // namespace io



io::chunk_file::chunk_file(std::string const& path)
: m_empty_inodes(0)
, m_num_inodes(0)
, m_open(false)
{
	bool exists = std::filesystem::exists(path);
	std::ios_base::openmode mode = (std::ios::binary | std::ios::in | std::ios::out)
								 | (exists ? std::ios::ate : std::ios::trunc);
	this->m_file.open(path, mode);
	if (m_file.fail()) {
		return;
	}

	if (m_file.tellg() < (long)ADDR_DATA_BASE) {
		this->create_file();
	}

	this->m_file.seekg(ADDR_INODE_EMPTY_COUNT, std::ios::beg);
	this->m_file.read(reinterpret_cast<char*>(&m_empty_inodes), sizeof(size_t));
	this->m_file.seekg(ADDR_INODE_COUNT, std::ios::beg);
	this->m_file.read(reinterpret_cast<char*>(&m_num_inodes), sizeof(size_t));

	this->m_file.seekg(ADDR_INODE_CACHE, std::ios::beg);
	for (size_t i=0; i<SIZE_INODE_CACHE; i++) {
		size_t iaddr;
		this->m_file.read(reinterpret_cast<char*>(&iaddr), sizeof(size_t));
		this->m_cache[i] = iaddr;
	}

	::memset(m_padding, '\0', sizeof(m_padding));

	this->m_open = true;
}

io::chunk_file::~chunk_file()
{
	this->m_file.seekp(ADDR_INODE_EMPTY_COUNT, std::ios::beg);
	this->m_file.write(reinterpret_cast<char*>(&m_empty_inodes), sizeof(size_t));
	this->m_file.seekp(ADDR_INODE_COUNT, std::ios::beg);
	this->m_file.write(reinterpret_cast<char*>(&m_num_inodes), sizeof(size_t));

	this->m_file.seekp(ADDR_INODE_CACHE, std::ios::beg);
	for (size_t i=0; i<SIZE_INODE_CACHE; i++) {
		size_t iaddr = m_cache[i];
		this->m_file.write(reinterpret_cast<char*>(&iaddr), sizeof(size_t));
	}
	this->m_file.close();
}


bool io::chunk_file::is_open() const
{
	return m_open;
}

void io::chunk_file::create_file()
{
	this->m_file.seekp(0, std::ios::beg);
	for (size_t i=0; i<NUM_CHUNKS; i++) {
		this->m_file.write(reinterpret_cast<const char*>(clear_values), sizeof(clear_values));
	}
	this->m_empty_inodes = 0;
	this->m_num_inodes = 0;
	this->m_file.write(reinterpret_cast<char*>(&m_empty_inodes), sizeof(size_t));
	this->m_file.write(reinterpret_cast<char*>(&m_num_inodes), sizeof(size_t));
	for (size_t i=0; i<SIZE_INODE_CACHE; i++) {
		this->m_file.write(reinterpret_cast<const char*>(&inval), sizeof(size_t));
	}
}

size_t io::chunk_file::next_loc()
{
	size_t addr = 0;
	if (m_empty_inodes > 0) {
		// search in cache
		bool found_in_cache = false;
		for (size_t i=0; i<SIZE_INODE_CACHE; i++) {
			if (m_cache[i] != inval) {
				addr = m_cache[i];
				found_in_cache = true;
				this->m_cache[i] = inval;
				break;
			}
		}

		if (!found_in_cache) {
			this->m_file.seekg(ADDR_DATA_BASE, std::ios::beg);
			size_t buff[2];
			while (addr != inval) {
				this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
				this->m_file.seekg(INODE_SIZE, std::ios::cur);
				addr = buff[1];
			}
			addr = m_file.tellg();
		}
		// save empty inode count
		this->m_empty_inodes -= 1;
	} else {
		// end of file / no empty inodes
		addr = ADDR_DATA_BASE + INODE_SIZE * m_num_inodes;
		this->m_num_inodes += 1;
	}
	if (addr == 0 || addr == inval) {
		throw std::runtime_error("could not receive new inode address");
	}
	return addr;
}

size_t io::chunk_file::chunk_base_addr(int64_t x, int64_t z) const
{
	x = x & (NUM_CHUNKS_SIDE - 1);
	z = z & (NUM_CHUNKS_SIDE - 1);
	return (NUM_CHUNKS_SIDE * x + z) * sizeof(size_t) * 2;
}

void io::chunk_file::erase_old(int64_t x, int64_t z)
{
	// set base addr
	size_t offset = chunk_base_addr(x, z);
	this->m_file.seekg(offset, std::ios::beg);

	// invalidate old data
	size_t buff[2];
	this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
	size_t addr = buff[1];
	while (addr != inval) {
		if (m_empty_inodes < SIZE_INODE_CACHE) {
			for (size_t i=0; i<SIZE_INODE_CACHE; i++) {
				if (m_cache[i] == inval) {
					this->m_cache[i] = addr;
					break;
				}
			}
		}
		// get next address and overwrite old one
		this->m_file.seekg(addr, std::ios::beg);
		this->m_file.seekp(addr, std::ios::beg);
		this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
		this->m_file.write(reinterpret_cast<const char*>(clear_values), sizeof(clear_values));
		this->m_empty_inodes++;
		addr = buff[1];
	}
	this->m_file.seekp(offset, std::ios::beg);
	this->m_file.write(reinterpret_cast<const char*>(&inval), sizeof(size_t));
}

bool io::chunk_file::load_chunk(int64_t x, int64_t z, std::unique_ptr<char[]>& pdata, size_t& psize)
{
	size_t lutaddr = chunk_base_addr(x, z);
	size_t buff[2];
	this->m_file.seekg(lutaddr, std::ios::beg);
	this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
	if (buff[0] == inval || buff[1] == inval) {
		psize = 0;
		pdata = nullptr;
		return false;
	}
	size_t size = buff[0];
	size_t addr = buff[1];

	pdata = std::unique_ptr<char[]>(new char[size]);
	char * data = pdata.get();
	psize = size;

	size_t read = 0;
	while (read < size) {
		this->m_file.seekg(addr, std::ios::beg);
		this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
		addr = buff[1];
		size_t block_size = std::min(INODE_DATA_SIZE, size - read);
		this->m_file.read((data + read), block_size);
		read += block_size;
	}
	return true;
}

void io::chunk_file::save_chunk(int64_t x, int64_t z, char* data, size_t size)
{
	size_t lutaddr = chunk_base_addr(x, z);

	size_t buff[2];
	this->m_file.seekg(lutaddr, std::ios::beg);
	this->m_file.read(reinterpret_cast<char*>(buff), sizeof(buff));
	if (buff[0] != inval) {
		this->erase_old(x, z);
	}

	// don't write new chunk
	if (!data || !size) {
		return;
	}

	// write chunk to file
	size_t num_inodes = std::ceil(size / (long double)INODE_DATA_SIZE);
	size_t addr = next_loc();
	size_t prev = inval;
	size_t next = addr;
	// write header
	this->m_file.seekp(lutaddr, std::ios::beg);
	this->m_file.write(reinterpret_cast<char*>(&size), sizeof(size_t));
	this->m_file.write(reinterpret_cast<char*>(&next), sizeof(size_t));
	// write data
	size_t read = 0;
	size_t remaining = size - read;
	for (size_t i=0; i<num_inodes; i++) {
		remaining = size - read;
		this->m_file.seekp(next, std::ios::beg);
		this->m_file.write(reinterpret_cast<char*>(&prev), sizeof(size_t));
		prev = next;
		next = (remaining <= INODE_DATA_SIZE) ? inval : next_loc();
		this->m_file.write(reinterpret_cast<char*>(&next), sizeof(size_t));
		this->m_file.write(&data[read], std::min(INODE_DATA_SIZE, remaining));
		read += INODE_DATA_SIZE;
	}
	// zero padding
	if (remaining < INODE_DATA_SIZE) {
		this->m_file.write(m_padding, INODE_DATA_SIZE - remaining);
	}
}
