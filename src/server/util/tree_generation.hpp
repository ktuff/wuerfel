#pragma once

#include <common/types.hpp>


namespace game {

class level;

} // namespace game


namespace server {

class tree_generation {

	private:
		game::level* m_level;


	public:
		tree_generation(game::level* level);


	public:
		bool generate_default_tree(coord_t x, coord_t y, coord_t z);
		bool generate_big_tree();

	private:
		bool check_cuboid(coord_t x0, coord_t y0, coord_t z0, coord_t x1, coord_t y1, coord_t z1);
};

};
