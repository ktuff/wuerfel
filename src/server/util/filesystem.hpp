#pragma once

#include <fstream>
#include <memory>


/**
 * NOTE
 * files do not get reduced in size
 * unused values are just kept in place and get overwritten if necessary
 * 
 * could be solved by new creation of file if difference to big
 */

/**
 * NOTE
 * file layout:
 * 64*64 addresses(size_t)/sizes(size_t) for chunks
 * -> pattern: (addr, size) | ...
 * empty inode count
 * inode count
 * 32 addresses (size_t) for fast lookup of empty inodes
 * inodes with two addresses to previous and next node together with data 
 * -> pattern: (prev, next, data) | ...
 */

namespace io {

// CONSTANTS
constexpr size_t INODE_DATA_SIZE = 1024;
constexpr size_t SIZE_INODE_CACHE = 32;
constexpr size_t NUM_CHUNKS_SIDE = 64;
constexpr size_t NUM_CHUNKS = (NUM_CHUNKS_SIDE * NUM_CHUNKS_SIDE);


class chunk_file {

	private:
		std::fstream m_file;
		size_t m_empty_inodes;
		size_t m_num_inodes;
		size_t m_cache[SIZE_INODE_CACHE];
		char m_padding[INODE_DATA_SIZE];
		bool m_open;


	public:
		chunk_file(std::string const& path);
		~chunk_file();


	public:
		bool is_open() const;
		bool load_chunk(int64_t x, int64_t z, std::unique_ptr<char[]>& pdata, size_t& psize);
		void save_chunk(int64_t x, int64_t z, char* data, size_t size);

	private:
		void create_file();
		size_t next_loc();
		size_t chunk_base_addr(int64_t x, int64_t z) const;
		void erase_old(int64_t x, int64_t z);

};

} // namespace io
