#include <iostream>

#include <ktf/util/logger.hpp>

#include "server.hpp"

server::server_instance::server_instance(bool remote)
: m_remote(remote)
{
	this->m_instance.status = quit;
}

server::server_instance::~server_instance()
{
	this->halt();
}


void server::server_instance::start(std::string const& level_name, int port, std::function<void(int)> const& callback)
{
	if (m_instance.status == running) {
		return;
	}

	int rc_port = m_instance.game.open(level_name, port);
	if (rc_port < 0) {
		ktf::logger::error("Failed to start server (on port: " + std::to_string(port) + ")");
	} else {
		ktf::logger::info("Started server on port " + std::to_string(rc_port));
		this->m_instance.main_loop = std::thread([&] {
			this->run();
		});
		if (m_remote) {
			command_line();
		}
	}
	if (callback) {
		callback(rc_port);
	}
}

void server::server_instance::halt()
{
	if (m_instance.status != quit) {
		// end game loop
		this->m_instance.status = quit;
		this->m_instance.main_loop.join();
	}
}


void server::server_instance::run()
{
	this->m_instance.status = running;

	bool up = true;
	while (up) {
		switch (m_instance.status) {
		case down:
			this->loop_down();
			break;
		case running:
			this->loop_running();
			break;
		case quit:
			up = false;
			break;
		}
	}
}

void server::server_instance::command_line()
{
	while (1) {
		std::string s;
		std::cin >> s;

		if (s == "exit") {
			break;
		}
		if (s == "stop") {
			this->m_instance.status = down;
		}
		if (s == "continue") {
			this->m_instance.status = running;
		}
	}
	this->halt();
}

void server::server_instance::loop_down()
{
	while (m_instance.status == down) {
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

void server::server_instance::loop_running()
{
	std::chrono::nanoseconds const tick_duration(50 * 1000 * 1000);
	std::chrono::nanoseconds t0, t1;
	t0 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());

	// run
	while (m_instance.status == running) {
		int64_t ns = tick_duration.count(); // TODO actual tick difference

		// tick game
		this->m_instance.game.tick(ns);

		// limit to 20 ticks per second
		t1 = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch());
		std::this_thread::sleep_for(tick_duration - (t1 - t0));
		t0 = t1;
	}

	// stop
	this->m_instance.game.close();
}
