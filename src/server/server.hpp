#pragma once

#include <atomic>
#include <thread>

#include "controller.hpp"

namespace server {
    
enum state {
	down,
	running,
	quit
};


struct game_instance {
	controller game;
	std::thread main_loop;
	std::atomic<state> status;
};


class server_instance {

	private:
		bool m_remote;
		game_instance m_instance;


	public:
		server_instance(bool remote);
		~server_instance();


	public:
		void start(std::string const& level_name, int port, std::function<void(int)> const& callback);
		void halt();

	private:
		void run();
		void tick();
		void command_line();

		void loop_down();
		void loop_running();

};

} // namespace server
