#pragma once

#include <set>

#include <common/net/packet_handler.hpp>
#include <ktf/net/tcp_server.hpp>

#include "authentication.hpp"


namespace game {
    class level;
    class server;
} // namespace game


namespace server {

class controller;


class packager {

	private:
		tcp_server m_socket;
		server::authentication m_auth;
		controller* contr;
		game::level* m_level;
		game::server* server;

		std::set<int> m_active_connections;
		std::set<int> m_socket_ids;
		std::unordered_map<int, std::chrono::seconds> m_waiting_players;
		std::vector<::entity::id> m_player_ids;
		std::vector<nlohmann::json> m_packet_batches;
		std::mutex m_connection_mutex;


	public:
		packager(game::level *level);
		~packager();


	public:
		// control
		int create(int port);
		void destroy();
		void set_player_limit(size_t limit);
		bool is_open() const;
		void tick();

		void set_authentication_callback(std::function<void(int, bool)> const& callback);
		void register_player(int index, ::entity::id player);
		void unregister_player(int index);

		// data
		void send_auth_challenge(int index, std::string const& pbk_b64, std::string const& challenge_b64);
		void send_auth_result(int index, bool success, ::entity::id id);

		void send_chunks_loaded(int id);
		void send_chunk_add(int id, std::shared_ptr<game::chunk> const& chunk);
		void send_chunk_remove(int id, ccoord_t const& cc);
		void send_entity_batch(int index, nlohmann::json const& data);
		void send_set_block(int id, coord_t x, coord_t y, coord_t z, block_t block, side_t side);
		void send_set_rotation(int id, coord_t x, coord_t y, coord_t z, side_t value);
		void send_set_state(int id, coord_t x, coord_t y, coord_t z, state_t value);
		void send_set_power(int id, coord_t x, coord_t y, coord_t z, power_t value);
		void send_set_light(int id, coord_t x, coord_t y, coord_t z, light_t value);
		void send_player_pos(int id, uint64_t pid, ktf::vec3<double>& pos);
		void send_player_rot(int id, uint64_t pid, ktf::vec3<double>& rot);

		void push_auth_packet(int index, packets::packet_t const& data);
		void push_general_packet(int index, packets::packet_t const& data);
		void push_entity_packet(int index, packets::packet_t const& data);

	private:
		void enqueue_packet(int index, packets::packet_t const& packet);

		void process_auth_packet(int index, packets::packet_t const& data);
		void process_general_packet(int index, packets::packet_t const& data);

		// data
		void recv_auth_init(int index, packets::packet_t const& packet);
		void recv_auth_response(int index, packets::packet_t const& packet);

		void recv_set_block(int id, const packets::packet_t& packet);
		void recv_set_rotation(int id, const packets::packet_t& packet);
		void recv_set_state(int id, const packets::packet_t& packet);
		void recv_set_power(int id, const packets::packet_t& packet);
		void recv_set_light(int id, const packets::packet_t& packet);
		void recv_player_pos(int id, const packets::packet_t& packet);
		void recv_player_rot(int id, const packets::packet_t& packet);

};

} // namespace server
