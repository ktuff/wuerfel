#include <cstdlib>
#include <ctime>

#include <string.h>
#include <sqlite3.h>

#include <common/net/base64.hpp>

#include "authentication.hpp"
#include "packager.hpp"


char const* const sql_create_key_table = R"sql(
	CREATE TABLE IF NOT EXISTS client_keys (
		username 		TEXT NOT NULL PRIMARY KEY,
		public_key 		BLOB NOT NULL,
		public_key_size INT  NOT NULL
	)
)sql";

char const* const sql_insert_key = R"sql(
	INSERT INTO client_keys VALUES (@user, @key, @size)
)sql";

char const* const sql_query_key = R"sql(
	SELECT public_key, public_key_size FROM client_keys WHERE username = (?)
)sql";


server::authentication::authentication(server::packager* network, int num_connections)
: m_network(network)
, m_rsa_clients(num_connections)
, m_usernames(num_connections)
, m_states(num_connections)
, m_challenges(num_connections)
, m_key_database(NULL)
{
	std::string private_key_path("server_pk.pem");
	std::string public_key_path("server_pbk.pem");
	this->m_rsa_server = rsa_key::load_from_file(private_key_path, public_key_path);
	if (!m_rsa_server()) {
		rsa_key key = rsa_key::generate(4096, private_key_path, public_key_path);
		if (key()) {
			this->m_rsa_server = std::move(key);
		} else {
			throw std::runtime_error("Could not create RSA keys for server!");
		}
	}
	for (size_t i=0; i<m_states.size(); ++i) {
		this->m_states[i] = authentication_state::no_auth_attempt;
	}

	char* err_msg = nullptr;
	this->check(::sqlite3_open("keys.db", &m_key_database));
	this->check(::sqlite3_exec(m_key_database, sql_create_key_table, NULL, NULL, &err_msg));
	if (err_msg) {
		::sqlite3_free(err_msg);
	}
}

server::authentication::~authentication()
{
	::sqlite3_close(m_key_database);
}


void server::authentication::set_authentication_callback(const std::function<void (int, bool)>& callback)
{
	this->m_auth_callback = callback;
}

void server::authentication::prepare_login(int connection_index)
{
	this->m_states[connection_index] = authentication_state::waiting_for_auth;
}

void server::authentication::init_login(int connection_index, std::string const& username, std::string const& public_key_b64)
{
	if (m_states[connection_index] != authentication_state::waiting_for_auth) {
		this->abort_login(connection_index);
		return;
	}

	size_t length = 0;
	std::vector<char> client_pb_key(base64::decode_bound(public_key_b64.size()));
	length = base64::decode((unsigned char*)public_key_b64.data(), public_key_b64.size(), (unsigned char*)client_pb_key.data());
	client_pb_key.resize(length);

	std::vector<char> cached_key;
	bool cached = load_from_database(username, cached_key);
	if (cached && (cached_key.size() != client_pb_key.size() || ::memcmp(client_pb_key.data(), cached_key.data(), client_pb_key.size())) != 0) {
		this->abort_login(connection_index);
		return;
	}

	this->m_rsa_clients[connection_index] = rsa_key::load_from_buffer({}, client_pb_key);
	if (!m_rsa_clients[connection_index]()) {
		this->abort_login(connection_index);
		return;
	}
	auto& client_rsa = m_rsa_clients[connection_index];

	// generate new message
	size_t secret_length = 256;
	std::srand(std::time(0));
	std::vector<unsigned char> message(secret_length);
	for (size_t i=0; i<secret_length; ++i) {
		message[i] = std::rand() % 256;
	}
	this->m_challenges[connection_index] = message;

	// encrypt
	std::vector<unsigned char> cipher;
	if (!client_rsa.encrypt(message, cipher)) {
		this->abort_login(connection_index);
		return;
	}

	// send with public key
	auto pb_key = m_rsa_server.get_public_key();
	std::vector<char> encoded_key;
	base64::encode(pb_key, encoded_key);
	std::vector<unsigned char> encoded_challenge;
	base64::encode(cipher, encoded_challenge);

	this->m_usernames[connection_index] = username;
	this->m_states[connection_index] = cached ? authentication_state::initiated_reauth : authentication_state::initiated_first_auth;
	this->m_network->send_auth_challenge(
		connection_index,
		std::string(encoded_key.data(), encoded_key.size()),
		std::string((char*)encoded_challenge.data(), encoded_challenge.size())
	);
}

bool server::authentication::validate_login(int connection_index, std::string const& response_b64)
{
	authentication_state state = m_states[connection_index];
	if (state != authentication_state::initiated_reauth && state != authentication_state::initiated_first_auth) {
		return false;
	}

	size_t length = 0;
	std::vector<unsigned char> response(base64::decode_bound(response_b64.size()));
	length = base64::decode((unsigned char*)response_b64.data(), response_b64.size(), response.data());
	response.resize(length);

	bool equal = false;
	std::vector<unsigned char> message;
	std::vector<unsigned char>* challenge;
	if (!m_rsa_server.decrypt(response, message)) {
		goto finish;
	}
	challenge = &m_challenges[connection_index];

	if (message.size() != challenge->size()) {
		goto finish;
	}
	equal = (::memcmp(message.data(), challenge->data(), challenge->size()) == 0);

finish:
	if (equal) {
		if (state == authentication_state::initiated_first_auth) {
			this->save_to_database(m_usernames[connection_index], m_rsa_clients[connection_index].get_public_key());
		}
	}

	// reset authentication
	this->m_usernames[connection_index] = "";
	this->m_states[connection_index] = authentication_state::no_auth_attempt;
	this->m_challenges[connection_index] = {};

	if (m_auth_callback) {
		this->m_auth_callback(connection_index, equal);
	}

	return equal;
}

void server::authentication::abort_login(int connection_index)
{
	this->m_usernames[connection_index] = "";
	this->m_states[connection_index] = authentication_state::no_auth_attempt;
	this->m_challenges[connection_index] = {};
	if (m_auth_callback) {
		this->m_auth_callback(connection_index, false);
	}

	// TODO send deny message
}

void server::authentication::save_to_database(const std::string& username, const std::vector<char>& key)
{
	sqlite3_stmt* stmt;
	char const* tail = 0;
	char* err_msg = nullptr;

	this->check(::sqlite3_exec(m_key_database, "BEGIN TRANSACTION", NULL, NULL, &err_msg));
	this->check(::sqlite3_prepare_v2(m_key_database, sql_insert_key, -1, &stmt, &tail));
	if (err_msg) {
		::sqlite3_free(err_msg);
	}
	this->check(::sqlite3_bind_text(stmt,  1, username.c_str(), (int)username.size(), SQLITE_STATIC));
	this->check(::sqlite3_bind_blob(stmt,  2, key.data(), 		(int)key.size(), 	  SQLITE_STATIC));
	this->check(::sqlite3_bind_int (stmt,  3, key.size()));
	this->check(::sqlite3_step(stmt));
	this->check(::sqlite3_finalize(stmt));
	this->check(::sqlite3_exec(m_key_database, "END TRANSACTION", NULL, NULL, &err_msg));
	if (err_msg) {
		::sqlite3_free(err_msg);
	}
}

bool server::authentication::load_from_database(const std::string& username, std::vector<char>& key)
{
	bool saved = false;
	sqlite3_stmt * stmt;
	char const * tail = nullptr;
	this->check(::sqlite3_prepare_v2(m_key_database,  sql_query_key, -1, &stmt, &tail));
	this->check(::sqlite3_bind_text (stmt, 1, username.c_str(), (int)username.size(), SQLITE_STATIC));
	int sqlrc = ::sqlite3_step(stmt);
	this->check(sqlrc);
	if (sqlrc == SQLITE_ROW) {
		void const* key_data = ::sqlite3_column_blob(stmt, 0);
		int key_size = ::sqlite3_column_int(stmt, 1);
		key.resize(key_size);
		::memcpy(key.data(), key_data, key_size);
		saved = true;
	}
	this->check(::sqlite3_finalize(stmt));

	return saved;
}

void server::authentication::check(int error_code)
{
	if (error_code != SQLITE_OK && error_code != SQLITE_DONE && error_code != SQLITE_ROW) {
		throw std::runtime_error(std::string("Could not execute SQL statement: ") + ::sqlite3_errmsg(m_key_database));
	}
}
