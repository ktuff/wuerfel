#include "config.hpp"


common::config server::config(
	"server_config.json",
	{ // bool
	},
	{ // int
		{"distance", 8},
		{"num_threads", 12},
		{"max_players", 1}
	},
	{ // float

	},
	{ // string
	}
);

