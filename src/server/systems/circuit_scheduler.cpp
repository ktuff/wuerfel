#include <common/world/voxel.hpp>
#include <common/world/world.hpp>

#include "circuit_scheduler.hpp"


circuit::scheduler::scheduler(game::world* p_world)
: m_world(p_world)
, m_next_id(1)
{
}

circuit::scheduler::~scheduler()
{
}


void circuit::scheduler::on_block_change(coord_t x, coord_t y, coord_t z, block_t oblock, block_t nblock)
{
	ktf::vec3<coord_t> pos = {x, y, z};
	auto it = m_ids.find(pos);
	if (it != m_ids.end()) {
		for (uint64_t id : it->second) {
			this->m_ignore.emplace(id);
		}
		this->m_ids.erase(pos);
	}

	update u;
	u.pos = pos;
	{
		u.id = next_id();
		u.type = circuit_remove;
		u.dest_block = oblock;
		this->add_update(u, e_none, false);
	}
	{
		u.id = next_id();
		u.type = circuit_set;
		u.dest_block = nblock;
		this->add_update(u, e_none, false);
	}
}

void circuit::scheduler::on_state_change(coord_t x, coord_t y, coord_t z, state_t ostate, state_t nstate)
{
	pos_t p = {x, y, z};
	update u;
}

void circuit::scheduler::tick()
{
	uint64_t const max_updates = 1000000ul;
	uint64_t updates = 0;
	uint64_t index = 0;
	while ((index < m_updates.size()) && (updates < max_updates)) {
		// no reference because u gets invalidated if the vector changes
		update u = m_updates[index];
		if (m_ignore.find(u.id) != m_ignore.end()) {
			this->m_ignore.erase(u.id);
			this->m_updates.erase(m_updates.begin() + index);
		} else if (u.delay == 0) {
			auto& ids = m_ids.at(u.pos);
			if (ids.size() == 1ul) {
				this->m_ids.erase(u.pos);
			} else {
				ids.erase(u.id);
			}
			this->process(u);
			this->m_updates.erase(m_updates.begin() + index);
			++updates;
		} else {
			--m_updates[index].delay;
			++index;
		}
	}
	if (updates >= max_updates) {
		this->reset();
	}
}

void circuit::scheduler::reset()
{
	this->m_updates.clear();
	this->m_ignore.clear();
	this->m_ids.clear();
}

void circuit::scheduler::process(const circuit::update& u)
{
	switch (u.dest_block) {
	case e_block_circuit_wire:
		this->process_wire(u);
		break;
	case e_block_circuit_torch:
		this->process_torch(u);
		break;
	case e_block_circuit_repeater:
		this->process_repeater(u);
		break;
	case e_block_circuit_comparator:
		this->process_comparator(u);
		break;
	default:
		if (def::block_defs[u.dest_block].transparent) {
			this->process_insolid(u);
		} else {
			this->process_solid(u);
		}
		break;
	}
}

void circuit::scheduler::process_wire(const circuit::update& u)
{
	switch (u.type) {
	case circuit_set:
		this->wire_set(u);
		break;
	case circuit_remove:
		this->wire_remove(u);
		break;
	case circuit_update:
		this->wire_update(u);
		break;
	}
}

void circuit::scheduler::process_torch(const circuit::update& u)
{
	switch (u.type) {
	case circuit_set:
		this->torch_set(u);
		break;
	case circuit_remove:
		this->torch_remove(u);
		break;
	case circuit_update:
		this->torch_update(u);
		break;
	}
}

void circuit::scheduler::process_repeater(const circuit::update& u)
{
	switch (u.type) {
	case circuit_set:
		this->repeater_set(u);
		break;
	case circuit_remove:
		this->repeater_remove(u);
		break;
	case circuit_update:
		this->repeater_update(u);
		break;
	}
}

void circuit::scheduler::process_comparator(const circuit::update& u)
{
	switch (u.type) {
	case circuit_set:
		this->comparator_set(u);
		break;
	case circuit_remove:
		this->comparator_remove(u);
		break;
	case circuit_update:
		this->comparator_update(u);
		break;
	}
}

void circuit::scheduler::process_solid(const circuit::update& u)
{
	switch (u.type) {
	case circuit_set:
		this->solid_set(u);
		break;
	case circuit_remove:
		this->solid_remove(u);
		break;
	case circuit_update:
		this->solid_update(u);
		break;
	}
}

void circuit::scheduler::process_insolid(const circuit::update& u)
{
	switch (u.type) {
	case circuit_update:
		this->insolid_update(u);
		break;
	default:
		break;
	}
}

void circuit::scheduler::solid_set(const circuit::update& u)
{
	power_t const current_value = 0;
	power_t const new_value = 0;

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	// send "empty" update to reflect power value / use edge detection
	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = u.dest_block;
	u1.flags.decrease_bit = 1;
	u1.flags.block_bit = 1;
	u1.flags.request_bit = 1;

	for (uint64_t i=0; i<side_count-1; ++i) {
		this->add_update(u1, all_sides[i]);
	}
}

void circuit::scheduler::solid_remove(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	power_t new_value = 0;
	if (current_value == new_value) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = u.dest_block;
	u1.flags.decrease_bit = 1;
	u1.flags.block_bit = 1;

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	for (uint64_t i=0; i<side_count-1; ++i) {
		this->add_update(u1, all_sides[i]);
	}
}

void circuit::scheduler::solid_update(const circuit::update& u)
{
	if (u.flags.block_bit) {
		return;
	}

	bool valid_input = (u.src_block == e_block_circuit_wire && u.dir != e_down && u.dir != e_none)
					|| (u.src_block == e_block_circuit_torch && u.dir == e_down)
					|| (u.src_block == e_block_circuit_repeater && u.dir != e_down && u.dir != e_up && u.dir != e_none)
					|| (u.src_block == e_block_circuit_comparator && u.dir != e_down && u.dir != e_up && u.dir != e_none);
	if (!valid_input) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	u1.in_old = current_value;
	if (u.flags.request_bit) {
		u1.src_block = u.dest_block;
		u1.in_new = current_value;
		this->add_update(u1, switched_sides[u.dir]);
		return;
	}

	power_t new_value;
	if (u.in_new > current_value) {
		new_value = u.in_new;
	} else if (u.in_new < current_value) {
		// propagate "turn off"
		if (u.in_old >= current_value) {
			// decrease value -> propagate "turn off"
			new_value = 0;
			u1.flags.decrease_bit = 1;
		} else if (u.flags.decrease_bit) {
			// detect edge
			new_value = current_value;
			u1.flags.response_bit = 1;
		} else {
			return;
		}
	} else {
		return;
	}

	if (current_value != new_value) {
		this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);
	}
	if (current_value != new_value || u1.flags.response_bit) {
		u1.in_new = new_value;
		u1.src_block = u.dest_block;
		u1.flags.block_bit = 1;
		u1.flags.source_bit = u.flags.source_bit;

		for (uint64_t i=0; i<side_count-1; ++i) {
			this->add_update(u1, all_sides[i]);
		}
	}
}

void circuit::scheduler::insolid_update(const circuit::update& u)
{
	if (u.src_block != e_block_circuit_wire) {
		// only important for wire updates, ignore
		return;
	}

	update nu(u);
	nu.src_block = u.dest_block;
	if (u.dir == e_down) {
		this->add_update(nu, all_sides[0]);
		this->add_update(nu, all_sides[1]);
		this->add_update(nu, all_sides[2]);
		this->add_update(nu, all_sides[3]);
	} else if (u.dir != e_up && u.dir != e_none) {
		this->add_update(nu, e_down);
	}
}

void circuit::scheduler::wire_set(const circuit::update& u)
{
	power_t const current_value = 0;
	power_t const new_value = 0;

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	// send "empty" update to reflect power value / use edge detection
	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_wire;
	u1.flags.decrease_bit = 1;
	u1.flags.wire_bit = 1;
	u1.flags.request_bit = 1;

	for (uint64_t i=0; i<side_count-1; ++i) {
		this->add_update(u1, all_sides[i]);
	}
}

void circuit::scheduler::wire_remove(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	power_t new_value = 0;
	if (current_value == new_value) {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_wire;
	u1.flags.decrease_bit = 1;
	u1.flags.wire_bit = 1;

	for (uint64_t i=0; i<side_count-1; ++i) {
		this->add_update(u1, all_sides[i]);
	}
}

void circuit::scheduler::wire_update(const circuit::update& u)
{
	if (u.flags.block_bit && u.flags.wire_bit) {
		// update comes from wire block behind solid block => ignore
		return;
	}

	update u1;
	u1.pos = u.pos;
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	u1.in_old = current_value;

	if (u.flags.request_bit) {
		u1.in_new = current_value;
		u1.src_block = e_block_circuit_wire;
		u1.flags.wire_bit = 1;
		this->add_update(u1, u.dir);
		return;
	}

	power_t new_value = u.in_new + (u.in_new > 0) * (u.flags.source_bit - 1);
	if (new_value > current_value) {
		// increase value
	} else if (new_value < current_value) {
		if (u.in_old + u.flags.source_bit - 1 >= current_value) {
			// decrease value -> propagate "turn off"
			new_value = 0;
			u1.flags.decrease_bit = 1;
		} else if (u.flags.decrease_bit) {
			// detect edge
			new_value = current_value;
			u1.flags.response_bit = 1;
		} else {
			return;
		}
	} else {
		// no change expected, ignore
		return;
	}

	if (new_value != current_value) {
		this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);
	}
	if (new_value != current_value || u.flags.request_bit || u1.flags.response_bit) {
		u1.in_new = new_value;
		u1.src_block = e_block_circuit_wire;
		u1.flags.wire_bit = 1;

		for (uint64_t i=0; i<side_count-1; ++i) {
			this->add_update(u1, all_sides[i]);
		}
	}
}

void circuit::scheduler::torch_set(const circuit::update& u)
{
	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	power_t const current_value = 0;
	power_t const input_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, orientation);
	power_t new_value = (!input_value) * CIRCUIT_MAX_VALUE;

	if (current_value == new_value) {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_torch;
	u1.flags.source_bit = 1;

	for (uint64_t i=0; i<side_count-1; ++i) {
		if (all_sides[i] != orientation) {
			this->add_update(u1, all_sides[i]);
		}
	}
}

void circuit::scheduler::torch_remove(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	power_t const new_value = 0;
	if (current_value == new_value) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_torch;
	u1.flags.source_bit = 1;

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);

	for (uint8_t i=0; i<side_count-1; ++i) {
		if (all_sides[i] != orientation) {
			this->add_update(u1, all_sides[i]);
		}
	}
}

void circuit::scheduler::torch_update(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.src_block = e_block_circuit_torch;
	u1.flags.source_bit = 1;

	if (u.flags.request_bit) {
		if (u.dir != orientation) {
			u1.in_new = current_value;
			this->add_update(u1, u.dir);
		}
		return;
	}

	power_t new_value;
	if (orientation == u.dir) {
		new_value = (!u.in_new) * CIRCUIT_MAX_VALUE;
	} else if (u.flags.decrease_bit) {
		// edge detection
		new_value = current_value;
	} else {
		// keep value
		return;
	}

	if (new_value != current_value) {
		this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);
	}

	u1.in_new = new_value;
	for (uint64_t i=0; i<side_count-1; ++i) {
		if (all_sides[i] != orientation) {
			this->add_update(u1, all_sides[i]);
		}
	}
}

void circuit::scheduler::repeater_set(const circuit::update& u)
{
	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	power_t const current_value = 0;
	power_t const input_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, orientation);
	power_t new_value = (input_value > 0) * CIRCUIT_MAX_VALUE;

	if (current_value == new_value) {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_repeater;
	u1.flags.source_bit = 1;

	this->add_update(u1, switched_sides[orientation]);
}

void circuit::scheduler::repeater_remove(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	power_t new_value = 0;
	if (current_value == new_value) {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_repeater;
	u1.flags.source_bit = 1;

	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	this->add_update(u1, switched_sides[orientation]);
}

void circuit::scheduler::repeater_update(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.src_block = e_block_circuit_repeater;
	u1.flags.source_bit = 1;

	if (u.flags.request_bit) {
		if (u.dir == switched_sides[orientation]) {
			u1.in_new = current_value;
			this->add_update(u1, u.dir);
		}
		return;
	}

	power_t new_value;
	if (orientation == u.dir) {
		new_value = (u.in_new > 0) * CIRCUIT_MAX_VALUE;
	} else if (u.flags.decrease_bit) {
		// edge detection
		new_value = current_value;
	} else {
		// keep value
		return;
	}

	if (new_value != current_value) {
		this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);
	}

	u1.in_new = new_value;
	this->add_update(u1, switched_sides[orientation]);
}

void circuit::scheduler::comparator_set(const circuit::update& u)
{
	power_t const current_value = 0;
	power_t new_value;
	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	power_t const main_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, orientation);
	power_t const left_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, left_sides[orientation]);
	power_t const right_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, right_sides[orientation]);
	power_t const side_inputs = std::max(left_input, right_input);

	state_t state = m_world->get_state(u.pos.x, u.pos.y, u.pos.z);
	if (state == CIRCUIT_MODE_SUBSTRACT) {
		new_value = std::max(power_t(0), side_inputs);
	} else if (state == CIRCUIT_MODE_COMPARE) {
		new_value = main_input * (side_inputs <= main_input);
	} else {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_comparator;
	u1.flags.source_bit = 1;

	this->add_update(u1, switched_sides[orientation]);
}

void circuit::scheduler::comparator_remove(const circuit::update& u)
{
	power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
	power_t new_value = 0;
	if (current_value == new_value) {
		return;
	}

	this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value;
	u1.in_new = new_value;
	u1.src_block = e_block_circuit_comparator;
	u1.flags.source_bit = 1;

	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	this->add_update(u1, switched_sides[orientation]);
}

void circuit::scheduler::comparator_update(const circuit::update& u)
{
	update u1;
	u1.pos = u.pos;
	u1.src_block = e_block_circuit_comparator;
	u1.flags.source_bit = 1;

	side_t orientation = m_world->get_orientation(u.pos.x, u.pos.y, u.pos.z);
	if (u.flags.request_bit || u.flags.decrease_bit) {
		if (u.dir == switched_sides[orientation]) {
			power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);
			u1.in_old = current_value;
			u1.in_new = current_value;
			this->add_update(u1, u.dir);
		}
		return;
	}

	side_t left_side = left_sides[orientation];
	side_t right_side = right_sides[orientation];
	if ((u.dir == orientation) || (u.dir == left_side) || (u.dir == right_side)) {
		power_t const current_value = m_world->get_power(u.pos.x, u.pos.y, u.pos.z);

		power_t const main_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, orientation);
		power_t const left_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, left_side);
		power_t const right_input = m_world->get_power(u.pos.x, u.pos.y, u.pos.z, right_side);
		power_t const side_inputs = std::max(left_input, right_input);
		state_t const state = m_world->get_state(u.pos.x, u.pos.y, u.pos.z);

		power_t new_value;
		if (state == CIRCUIT_MODE_SUBSTRACT) {
			new_value = std::max(power_t(0), side_inputs);
		} else if (state == CIRCUIT_MODE_COMPARE) {
			new_value = main_input * (side_inputs <= main_input);
		} else {
			return;
		}

		if (current_value != new_value) {
			this->m_world->set_power(u.pos.x, u.pos.y, u.pos.z, new_value);
		}

		u1.in_old = current_value;
		u1.in_new = new_value;
		this->add_update(u1, switched_sides[orientation]);
	} else {
		// ignore update
	}
}

uint64_t circuit::scheduler::next_id()
{
	uint64_t id = m_next_id;
	this->m_next_id += 1;
	return id;
}

void circuit::scheduler::add_update(const circuit::update& u, side_t side, bool fetch_dest)
{
	update nu = u;
	game::world::apply_side(nu.pos.x, nu.pos.y, nu.pos.z, side);
	if (!in_bounds(nu.pos.x, nu.pos.y, nu.pos.z)) {
		return;
	}

	nu.dir = switched_sides[side];
	if (fetch_dest) {
		nu.dest_block = m_world->get_block(nu.pos.x, nu.pos.y, nu.pos.z);
	}

	if (!def::block_defs[nu.dest_block].circuit) {
		if (def::block_defs[nu.dest_block].transparent && (u.type == circuit_set || u.type == circuit_remove)) {
			return;
		} else if (def::block_defs[nu.dest_block].transparent && nu.src_block == e_block_circuit_wire) {
			// passthrough update => used for redstone diagonals
			this->process_insolid(nu);
			return;
		} else if (!def::block_defs[nu.src_block].circuit && def::block_defs[nu.src_block].transparent && nu.dest_block != e_block_circuit_wire) {
			// ignore passthrough updates to non wire blocks
			return;
		}
	}

	nu.id = next_id();
	nu.delay = get_delay(nu.dest_block, 0);
	this->m_ids[nu.pos].emplace(nu.id);
	this->m_updates.push_back(nu);
}

uint8_t circuit::scheduler::get_delay(block_t block, uint8_t state) const
{
	switch (block) {
	case e_block_circuit_wire:
		return 0u;
	case e_block_circuit_torch:
		return 1u;
	case e_block_circuit_repeater:
		return 1u + state;
	case e_block_circuit_comparator:
		return 1u;
	default:
		return 0u;
	}
}
