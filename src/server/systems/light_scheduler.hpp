#pragma once

#include <common/systems.hpp>
#include <common/types.hpp>

#include "light_update.hpp"


namespace game {

class world;

} // namespace game


#define CHUNK_UNINITIALIZED 1

enum chunk_flags {
	light_init,
	decorated
};


namespace light {


class scheduler {

	private:
		game::world* m_world;
		std::vector<update> m_updates;
		size_t m_changes;


	public:
		scheduler(game::world* world);
		~scheduler();


	public:
		void reset();
		void tick();
		void on_chunk_change(coord_t x, coord_t z);
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block);
		void on_state_change(coord_t x, coord_t y, coord_t z);
		void on_power_change(coord_t x, coord_t y, coord_t z);
		void init_chunk(game::chunk_ptr chunk);

	private:
		void add_update(update const& update, side_t side);
		void process(update const& update);
		void process_sky(update const& update);
		void process_block(update const& update);

		void add_init_update(update const& u, side_t side, std::vector<update>& down_updates, std::vector<update>& other_updates);
		void process_init_update(game::chunk_ptr chunk, update const& u, std::vector<update>& down_updates, std::vector<update>& other_updates);

};

} // namespace light
