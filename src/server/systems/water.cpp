#include <common/level.hpp>

#include "water.hpp"


server::node::node(pos_t const& p, uint8_t steps)
: p(p)
, num_steps(steps)
, distance(uint8_t(-1))
, xn(nullptr)
, xp(nullptr)
, zn(nullptr)
, zp(nullptr)
{
}

server::node::~node()
{
	if (xn) {
		delete xn;
	}
	if (xp) {
		delete xp;
	}
	if (zn) {
		delete zn;
	}
	if (zp) {
		delete zp;
	}
}


server::water_flow::water_flow(game::level* level, coord_t x, coord_t y, coord_t z, block_t block)
: m_level(level)
, m_block(block)
{
	this->m_center = add_node({x, y, z}, 0);
	this->forward();
	this->backward();
}

server::water_flow::~water_flow()
{
	if (m_center) {
		delete m_center;
	}
}


void server::water_flow::apply()
{
	// TODO dont expand if same
	state_t state = (m_block == e_block_water_source) ? 7 : m_level->get_state(m_center->p.x, m_center->p.y, m_center->p.z); // TODO
	if (state == 0) {
		return;
	}
	// extend down
	if (m_center->distance == 0) {
		// always passable
		block_t block_below = m_level->get_block(m_center->p.x, m_center->p.y - 1, m_center->p.z);
		state_t state_below = m_level->get_state(m_center->p.x, m_center->p.y - 1, m_center->p.z);
		if (block_below != e_block_water_source && (block_below != e_block_water_flowing && state_below < state)) {
			this->m_level->set_block(m_center->p.x, m_center->p.y - 1, m_center->p.z, e_block_water_flowing);
			this->m_level->set_state(m_center->p.x, m_center->p.y - 1, m_center->p.z, 7); // TODO
		}
		return;
	}

	// extend to the sides
	state_t nstate = state - 1;
	if (m_center->xn && m_center->xn->distance == m_center->distance) {
		block_t o_block = m_level->get_block(m_center->xn->p.x, m_center->xn->p.y, m_center->xn->p.z);
		state_t o_state = m_level->get_state(m_center->xn->p.x, m_center->xn->p.y, m_center->xn->p.z);
		if (o_block != e_block_water_source && (o_block != e_block_water_flowing && o_state < state)) {
			this->m_level->set_block(m_center->xn->p.x, m_center->xn->p.y, m_center->xn->p.z, e_block_water_flowing);
			this->m_level->set_state(m_center->xn->p.x, m_center->xn->p.y, m_center->xn->p.z, nstate);
		}
	}
	if (m_center->xp && m_center->xp->distance == m_center->distance) {
		block_t o_block = m_level->get_block(m_center->xp->p.x, m_center->xp->p.y, m_center->xp->p.z);
		state_t o_state = m_level->get_state(m_center->xp->p.x, m_center->xp->p.y, m_center->xp->p.z);
		if (o_block != e_block_water_source && (o_block != e_block_water_flowing && o_state < state)) {
			this->m_level->set_block(m_center->xp->p.x, m_center->xp->p.y, m_center->xp->p.z, e_block_water_flowing);
			this->m_level->set_state(m_center->xp->p.x, m_center->xp->p.y, m_center->xp->p.z, nstate);
		}
	}
	if (m_center->zn && m_center->zn->distance == m_center->distance) {
		block_t o_block = m_level->get_block(m_center->zn->p.x, m_center->zn->p.y, m_center->zn->p.z);
		state_t o_state = m_level->get_state(m_center->zn->p.x, m_center->zn->p.y, m_center->zn->p.z);
		if (o_block != e_block_water_source && (o_block != e_block_water_flowing && o_state < state)) {
			this->m_level->set_block(m_center->zn->p.x, m_center->zn->p.y, m_center->zn->p.z, e_block_water_flowing);
			this->m_level->set_state(m_center->zn->p.x, m_center->zn->p.y, m_center->zn->p.z, nstate);
		}
	}
	if (m_center->zp && m_center->zp->distance == m_center->distance) {
		block_t o_block = m_level->get_block(m_center->zp->p.x, m_center->zp->p.y, m_center->zp->p.z);
		state_t o_state = m_level->get_state(m_center->zp->p.x, m_center->zp->p.y, m_center->zp->p.z);
		if (o_block != e_block_water_source && (o_block != e_block_water_flowing && o_state < state)) {
			this->m_level->set_block(m_center->zp->p.x, m_center->zp->p.y, m_center->zp->p.z, e_block_water_flowing);
			this->m_level->set_state(m_center->zp->p.x, m_center->zp->p.y, m_center->zp->p.z, nstate);
		}
	}
}

server::node* server::water_flow::add_node(pos_t const& pos, uint8_t steps)
{
	if (!is_passable(pos)) {
		return nullptr;
	} else if (steps <= get_distance(pos)) {
		node* nn = new node(pos, steps);
		this->m_queue.push_back(nn);
		this->m_nodes[nn->p] = nn;
		if (m_steps.size() <= steps) {
			this->m_steps.push_back({});
		}
		this->m_steps[steps].emplace(nn);
		return nn;
	} else {
		return nullptr;
	}

	// TODO reuse node
}

uint8_t server::water_flow::get_distance(pos_t const& pos) const
{
	auto it = m_nodes.find(pos);
	if (it == m_nodes.end()) {
		return (uint8_t)-1;
	} else {
		return it->second->num_steps;
	}
}

void server::water_flow::forward()
{
	uint8_t last_iteration(-1);
	while (!m_queue.empty()) {
		node* n = *m_queue.begin();
		this->m_queue.erase(m_queue.begin());
		if (n->num_steps > last_iteration) {
			continue;
		}

		if (is_passable({n->p.x, n->p.y - 1, n->p.z})) {
			last_iteration = n->num_steps;
			n->distance = n->num_steps;
			continue;
		}

		if (n->num_steps >= 8) {
			// leaf node
			continue;
		}

		uint8_t next_step = n->num_steps + 1;
		n->xn = add_node({n->p.x - 1, n->p.y, n->p.z}, next_step);
		n->xp = add_node({n->p.x + 1, n->p.y, n->p.z}, next_step);
		n->zn = add_node({n->p.x, n->p.y, n->p.z - 1}, next_step);
		n->zp = add_node({n->p.x, n->p.y, n->p.z + 1}, next_step);
	}
}

void server::water_flow::backward()
{
	for (int index = m_steps.size()-2; index >= 0; --index) {
		auto nodes = m_steps[index];
		for (node* n : nodes) {
			uint8_t dxn = n->xn ? n->xn->distance : uint8_t(-1);
			uint8_t dxp = n->xp ? n->xp->distance : uint8_t(-1);
			uint8_t dzn = n->zn ? n->zn->distance : uint8_t(-1);
			uint8_t dzp = n->zp ? n->zp->distance : uint8_t(-1);
			uint8_t distance = std::min({n->distance, dxn, dxp, dzn, dzp});
			n->distance = distance;
		}
	}
}

bool server::water_flow::is_passable(const pos_t& pos) const
{
	block_t block = m_level->get_block(pos.x, pos.y, pos.z);
	return def::block_defs[block].transparent;
}
