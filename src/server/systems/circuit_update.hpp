#pragma once

#include <common/types.hpp>


namespace circuit {

enum update_t : uint8_t {
	circuit_set,
	circuit_remove,
	circuit_update
};


struct update {
	uint64_t id;
	update_t type;
	uint8_t delay;
	pos_t pos;

	side_t dir;
	power_t in_old;
	power_t in_new;
	block_t src_block;
	block_t dest_block;
	union {
		uint8_t value;
		struct {
			uint8_t source_bit : 1;
			uint8_t decrease_bit : 1;
			uint8_t block_bit : 1;
			uint8_t wire_bit : 1;
			uint8_t request_bit : 1;
			uint8_t response_bit : 1;
			uint8_t _7 : 1;
			uint8_t _8 : 1;
		};
	} flags;

	update();
	update(uint64_t id);
	update(update&& u);
	update(update const& u);
	update& operator=(update && u);
	update& operator=(update const& u);
};

} // namespace circuit
