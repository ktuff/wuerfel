#include <random>

#include <common/level.hpp>
#include <server/util/tree_generation.hpp>

#include "block_tick_system.hpp"
#include "water.hpp"


server::block_tick_system::block_tick_system(game::level* level, server::tree_generation* tree_generation)
: m_level(level)
, m_tree_generation(tree_generation)
, m_next_id(1)
{
	this->m_level->get_world()->add_listener(this);
}

server::block_tick_system::~block_tick_system()
{
	this->m_level->get_world()->remove_listener(this);
}


void server::block_tick_system::on_block_change(coord_t x, coord_t y, coord_t z, block_t, block_t new_block)
{
	pos_t pos(x, y, z);
	auto it = m_pos_id_map.find(pos);
	if (it != m_pos_id_map.end()) {
		for (block_tick_id id : it->second) {
			this->m_ignore.emplace(id);
		}
		this->m_pos_id_map.erase(pos);
	}

	block_tick tick;
	tick.voxel_pos = pos;
	tick.type = server::block_tick_type::e_block_set;
	this->add_update(tick, e_none);

	tick.type = server::block_tick_type::e_block_update;
	this->add_update(tick, e_north);
	this->add_update(tick, e_south);
	this->add_update(tick, e_east);
	this->add_update(tick, e_west);
	this->add_update(tick, e_up);
	this->add_update(tick, e_down);
}

void server::block_tick_system::on_state_change(coord_t x, coord_t y, coord_t z, state_t value)
{
// 	pos_t pos(x, y, z);
// 	auto it = m_pos_id_map.find(pos);
// 	if (it != m_pos_id_map.end()) {
// 		for (block_tick_id id : it->second) {
// 			this->m_ignore.emplace(id);
// 		}
// 		this->m_pos_id_map.erase(pos);
// 	}
//
// 	block_tick tick;
// 	tick.voxel_pos = pos;
// 	tick.type = server::block_tick_type::e_block_set;
// 	this->add_update(tick, e_none);
//
// 	tick.type = server::block_tick_type::e_block_update;
// 	this->add_update(tick, e_north);
// 	this->add_update(tick, e_south);
// 	this->add_update(tick, e_east);
// 	this->add_update(tick, e_west);
// 	this->add_update(tick, e_up);
// 	this->add_update(tick, e_down);

	// TODO combine with block change
}

void server::block_tick_system::update()
{
	size_t const max_updates = 1000000ul;
	size_t updates = 0;
	size_t index = 0;
	while ((index < m_updates.size()) && (updates < max_updates)) {
		// no reference because u gets invalidated if the vector changes
		block_tick u = m_updates[index];
		if (m_ignore.find(u.id) != m_ignore.end()) {
			this->m_ignore.erase(u.id);
			this->m_updates.erase(m_updates.begin() + index);
		} else if (u.num_ticks == 0) {
			auto& ids = m_pos_id_map.at(u.voxel_pos);
			if (ids.size() == 1ul) {
				this->m_pos_id_map.erase(u.voxel_pos);
			} else {
				ids.erase(u.id);
			}
			this->process(u);
			this->m_updates.erase(m_updates.begin() + index);
			++updates;
		} else {
			--m_updates[index].num_ticks;
			++index;
		}
	}
	if (updates >= max_updates) {
		this->reset();
	}
}

void server::block_tick_system::reset()
{
	this->m_updates.clear();
	this->m_ignore.clear();
	this->m_pos_id_map.clear();
}

void server::block_tick_system::process(const server::block_tick& bt)
{
	switch (bt.block) {
	case e_block_sapling_oak:
		this->process_sapling(bt);
		break;
	case e_block_bamboo:
		break;
	case e_block_water_source:
	case e_block_water_flowing:
		this->process_water(bt);
		break;
	case e_block_flower_red:
	case e_block_flower_yellow:
		break;
	}
}

void server::block_tick_system::process_sapling(block_tick const& bt)
{
	if (bt.type == e_block_set) {
		return;
	}

	block_t block = m_level->get_block(bt.voxel_pos.x, bt.voxel_pos.y - 1, bt.voxel_pos.z);
	if (block != e_block_dirt && block != e_block_grass) {
		this->m_level->set_block(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z, e_block_air);
		return;
	}

	if (eval_prob(30, 100)) {
		// remove sapling
		this->m_level->set_block(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z, e_block_air);

		// generate tree
		this->m_tree_generation->generate_default_tree(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z);
	} else {
		this->add_update(bt, e_none);
	}
}

void server::block_tick_system::process_bamboo(block_tick const& bt)
{
	if (bt.type == e_block_set) {
		return;
	}

	uint16_t num_bamboo = 0;
	for (uint16_t yi=bt.voxel_pos.y; yi>0; --yi) {
		if (m_level->get_block(bt.voxel_pos.x, yi, bt.voxel_pos.z) == e_block_bamboo) {
			++num_bamboo;
		} else {
			break;
		}
	}

	if (num_bamboo < 2) {
		if (eval_prob(30, 100)) {
			this->m_level->set_block(bt.voxel_pos.x, bt.voxel_pos.y + 1, bt.voxel_pos.z, e_block_leaves_oak);
		} else {
			this->add_update(bt, e_none);
		}
	} else {
		// no further growth
	}
}

void server::block_tick_system::process_flower(block_tick const& bt)
{
	if (bt.type == e_block_set) {
		return;
	}

	// TODO logic
	// check if too many flowers in area
	// create new if not
}

void server::block_tick_system::process_water(const server::block_tick& bt)
{
	block_t current_block = bt.block;
	if (bt.block == e_block_water_flowing) {
		block_t blocks[4];
		blocks[0] = m_level->get_block(bt.voxel_pos.x - 1, bt.voxel_pos.y, bt.voxel_pos.z);
		blocks[1] = m_level->get_block(bt.voxel_pos.x + 1, bt.voxel_pos.y, bt.voxel_pos.z);
		blocks[2] = m_level->get_block(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z - 1);
		blocks[3] = m_level->get_block(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z + 1);
		int num_sources = int(blocks[0] == e_block_water_source)
						+ int(blocks[1] == e_block_water_source)
						+ int(blocks[2] == e_block_water_source)
						+ int(blocks[3] == e_block_water_source);
		if (num_sources > 1) {
			this->m_level->set_block(bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z, e_block_water_source);
			current_block = e_block_water_source;
		}
	}

	water_flow flow(m_level, bt.voxel_pos.x, bt.voxel_pos.y, bt.voxel_pos.z, current_block);
	flow.apply();
}

void server::block_tick_system::add_update(block_tick const& bt, side_t dir)
{
	block_tick nbt;
	nbt.voxel_pos = bt.voxel_pos;
	game::world::apply_side(nbt.voxel_pos.x, nbt.voxel_pos.x, nbt.voxel_pos.x, dir);
	nbt.block = m_level->get_block(nbt.voxel_pos.x, nbt.voxel_pos.y, nbt.voxel_pos.z);
	if (nbt.block == e_block_air) {
		return;
	}
	nbt.dir = switched_sides[dir];
	nbt.num_ticks = get_delay(bt.block);
	nbt.repeat = bt.repeat;
	nbt.id = next_id();

	this->m_pos_id_map[nbt.voxel_pos].emplace(nbt.id);
	this->m_updates.push_back(nbt);
}

server::block_tick_id server::block_tick_system::next_id()
{
	block_tick_id id = m_next_id;
	this->m_next_id += 1;
	return id;
}

server::tick_delay_t server::block_tick_system::get_delay(block_t block) const
{
	switch (block) {
	case e_block_sapling_oak:
		return 100;
	case e_block_water_source:
	case e_block_water_flowing:
		return 100; // TODO
	default:
		return 10;
	}
}

bool server::block_tick_system::eval_prob(unsigned int treshold, unsigned int interval) const
{
	std::random_device dice;
	return (dice() % interval) < treshold;
}
