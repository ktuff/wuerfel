#include "light_update.hpp"


light::update::update()
: light_type(e_light_sky)
, pos(0, 0, 0)
, dir(e_none)
, in_old(0)
, in_new(0)
, flags({0, 0, 0})
{
}

light::update::update(const light::update& u)
: light_type(u.light_type)
, pos(u.pos)
, dir(u.dir)
, in_old(u.in_old)
, in_new(u.in_new)
, flags(u.flags)
{
}

light::update::update(light::update && u)
: light_type(u.light_type)
, pos(u.pos)
, dir(u.dir)
, in_old(u.in_old)
, in_new(u.in_new)
, flags(u.flags)
{
}

light::update & light::update::operator=(const light::update& u)
{
	this->light_type = u.light_type;
	this->pos = u.pos;
	this->dir = u.dir;
	this->in_old = u.in_old;
	this->in_new = u.in_new;
	this->flags = u.flags;
	return *this;
}

light::update & light::update::operator=(light::update && u)
{
	this->light_type = u.light_type;
	this->pos = u.pos;
	this->dir = u.dir;
	this->in_old = u.in_old;
	this->in_new = u.in_new;
	this->flags = u.flags;
	return *this;
}
