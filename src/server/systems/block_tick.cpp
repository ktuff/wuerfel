#include "block_tick.hpp"


server::block_tick::block_tick()
: block_tick(0)
{
}

server::block_tick::block_tick(server::block_tick_id id)
: id(id)
, type(e_block_update)
, dir(e_none)
, voxel_pos(0)
, block(0)
, num_ticks(0)
, repeat(false)
{
}

server::block_tick::block_tick(server::block_tick && u)
: id(u.id)
, type(u.type)
, dir(u.dir)
, voxel_pos(u.voxel_pos)
, block(u.block)
, num_ticks(u.num_ticks)
, repeat(u.repeat)
{
}

server::block_tick::block_tick(const server::block_tick& u)
: id(u.id)
, type(u.type)
, dir(u.dir)
, voxel_pos(u.voxel_pos)
, block(u.block)
, num_ticks(u.num_ticks)
, repeat(u.repeat)
{
}

server::block_tick & server::block_tick::operator=(server::block_tick && u)
{
	this->id = u.id;
	this->type = u.type;
	this->dir = u.dir;
	this->voxel_pos = u.voxel_pos;
	this->block = u.block;
	this->num_ticks = u.num_ticks;
	this->repeat = u.repeat;

	return *this;
}

server::block_tick & server::block_tick::operator=(const server::block_tick& u)
{
	this->id = u.id;
	this->type = u.type;
	this->dir = u.dir;
	this->voxel_pos = u.voxel_pos;
	this->block = u.block;
	this->num_ticks = u.num_ticks;
	this->repeat = u.repeat;

	return *this;
}
