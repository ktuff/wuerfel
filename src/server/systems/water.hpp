#pragma once

#include <common/types.hpp>


namespace game {

class level;

} // namespace game


namespace server {

struct node {
	pos_t const p;
	uint8_t const num_steps;
	uint8_t distance;
	node *xn, *xp, *zn, *zp;

	node(pos_t const& p, uint8_t steps);
	~node();
};


class water_flow {

	private:
		game::level* m_level;
		node* m_center;
		std::vector<node*> m_queue;
		std::unordered_map<pos_t, node*> m_nodes;
		std::vector<std::set<node*>> m_steps;
		block_t m_block;


	public:
		water_flow(game::level* level, coord_t x, coord_t y, coord_t z, block_t block);
		~water_flow();


	public:
		void apply();

	private:
		node* add_node(pos_t const& pos, uint8_t steps);
		uint8_t get_distance(pos_t const& pos) const;
		void forward();
		void backward();
		bool is_passable(pos_t const& pos) const;
};

} // namespace server
