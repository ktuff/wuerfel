#pragma once

#include <common/types.hpp>

namespace light {

struct update {
	enum {
		e_light_sky,
		e_light_block
	} light_type;
	pos_t pos;
	side_t dir;
	uint8_t in_old;
	uint8_t in_new;
	union {
		struct {
			uint8_t decrease_bit : 1;
			uint8_t request_bit : 1;
			uint8_t response_bit : 1;
			uint8_t : 0;
		} flags;
	};

	update();
	update(update const& u);
	update(update && u);

	update& operator=(update const& u);
	update& operator=(update && u);
};

} // namespace light
