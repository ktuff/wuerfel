#pragma once

#include <set>
#include <unordered_map>
#include <vector>

#include <common/systems.hpp>

#include "circuit_update.hpp"


namespace game {

class world;

} // namespace game


namespace circuit {

class scheduler {

	private:
		game::world *m_world;
		std::vector<update> m_updates;
		std::unordered_map<pos_t, std::set<uint64_t>> m_ids;
		std::set<uint64_t> m_ignore;
		uint64_t m_next_id;


	public:
		scheduler(game::world * p_world);
		~scheduler();


	public:
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t oblock, block_t nblock);
		void on_state_change(coord_t x, coord_t y, coord_t z, state_t ostate, state_t nstate);
		void tick();
		void reset();

	private:
		void process(update const& u);
		void process_wire(update const& u);
		void process_torch(update const& u);
		void process_repeater(update const& u);
		void process_comparator(update const& u);
		void process_solid(update const& u);
		void process_insolid(update const& u);

		void solid_set(update const& u);
		void solid_remove(update const& u);
		void solid_update(update const& u);
		void insolid_update(update const& u);
		void wire_set(update const& u);
		void wire_remove(update const& u);
		void wire_update(update const& u);
		void torch_set(update const& u);
		void torch_remove(update const& u);
		void torch_update(update const& u);
		void repeater_set(update const& u);
		void repeater_remove(update const& u);
		void repeater_update(update const& u);
		void comparator_set(update const& u);
		void comparator_remove(update const& u);
		void comparator_update(update const& u);

		uint64_t next_id();
		void add_update(update const& u, side_t side, bool fetch_dest = true);
		uint8_t get_delay(block_t block, uint8_t state) const;
};

} // namespace circuit
