#pragma once

#include <common/world/world_listener.hpp>

#include "block_tick.hpp"


namespace game {

class level;

} // namespace game


namespace server {

class tree_generation;


class block_tick_system : public world_listener {

	private:
		game::level* m_level;
		server::tree_generation* m_tree_generation;
		std::vector<block_tick> m_updates;
		std::unordered_map<pos_t, std::set<block_tick_id>> m_pos_id_map;
		std::set<block_tick_id> m_ignore;
		block_tick_id m_next_id;


	public:
		block_tick_system(game::level* level, server::tree_generation* tree_generation);
		~block_tick_system();


	public:
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block) override;
		void on_state_change(coord_t x, coord_t y, coord_t z, state_t value) override;
		void update();
		void reset();

	private:
		void process(block_tick const& bt);
		void process_sapling(block_tick const& bt);
		void process_bamboo(block_tick const& bt);
		void process_flower(block_tick const& bt);
		void process_water(block_tick const& bt);

		void add_update(block_tick const& bt, side_t dir);
		block_tick_id next_id();
		tick_delay_t get_delay(block_t block) const;
		bool eval_prob(unsigned int treshold, unsigned int interval) const;

};

} // namespace server
