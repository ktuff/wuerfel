#pragma once

#include <common/types.hpp>


namespace server {

typedef uint64_t block_tick_id;
typedef uint32_t tick_delay_t;


enum block_tick_type {
	e_block_set,
	e_block_update
};


struct block_tick {
	block_tick_id id;
	block_tick_type type;
	side_t dir;
	pos_t voxel_pos;
	block_t block;
	tick_delay_t num_ticks;
	bool repeat;

	block_tick();
	block_tick(block_tick_id id);
	block_tick(block_tick&& u);
	block_tick(block_tick const& u);
	block_tick& operator=(block_tick && u);
	block_tick& operator=(block_tick const& u);
};

} // namespace server
