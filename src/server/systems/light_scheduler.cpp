#include <common/world/voxel.hpp>
#include <common/world/world.hpp>

#include "light_scheduler.hpp"


light::scheduler::scheduler(game::world* world)
: m_world(world)
, m_changes(0)
{
}

light::scheduler::~scheduler()
{
}


void light::scheduler::reset()
{
}

void light::scheduler::tick()
{
	this->m_changes = 0;
	uint64_t index = 0;
	while (index < m_updates.size() && m_changes < 1000UL) {
		// no reference because u gets invalidated if the vector changes
		update u = m_updates[index];
		++index;
		this->process(u);
	}
	if (index > 0) {
		this->m_updates.erase(m_updates.begin(), m_updates.begin()+index);
		if (index == m_updates.size()) {
			this->m_updates = std::vector<update>();
		}
	}
}

// TODO fill subchunks on expansion

void light::scheduler::on_chunk_change(coord_t x, coord_t z)
{
	auto chunk = m_world->get_chunk(x, z);
	if (!chunk) {
		return;
	}
	if (chunk->get_state() != CHUNK_UNINITIALIZED) {
		return;
	}

	update u;
	u.light_type = update::e_light_sky;
	u.in_new = LIGHT_MAX_VALUE;
	coord_t y = (chunk->get_top() + 1) * CHUNK_SIZE;
	for (coord_t x=0; x<CHUNK_SIZE; x++) {
		for (coord_t z=0; z<CHUNK_SIZE; z++) {
			u.pos = pos_t(chunk->posw.x + x, y, chunk->posw.y + z);
			this->add_update(u, e_down);
		}
	}
	// TODO chunk->set_flag(light_init);
}

void light::scheduler::on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block)
{
	// TODO partial transparency

	if (def::block_defs[old_block].transparent && !def::block_defs[new_block].transparent) {
		if (def::block_defs[new_block].emission > 0) {
		}

		light_t const current_value = m_world->get_light(x, y, z);
		this->m_world->set_light(x, y, z, {{0, 0}});

		update u;
		u.flags.decrease_bit = 1;
		u.pos = pos_t(x, y, z);
		u.in_old = current_value.sky;

		u.light_type = update::e_light_sky;
		this->add_update(u, e_up);
		this->add_update(u, e_north);
		this->add_update(u, e_west);
		this->add_update(u, e_south);
		this->add_update(u, e_east);
		this->add_update(u, e_down);

		/*
		u.in_old = current_value.block;
		u.light_type = update::e_light_block;
		for (uint8_t i=0; i<side_count-1; i++) {
			this->add_update(u, all_sides[i]);
		}
		*/
	} else if (!def::block_defs[old_block].transparent && def::block_defs[new_block].transparent) {
		update u;
		u.pos = pos_t(x, y, z);
		u.flags.request_bit = 1;

		u.light_type = update::e_light_sky;
		this->add_update(u, e_up);
		this->add_update(u, e_north);
		this->add_update(u, e_west);
		this->add_update(u, e_south);
		this->add_update(u, e_east);
		this->add_update(u, e_down);

		/*
		u.light_type = update::e_light_block;
		for (uint8_t i=0; i<side_count-1; i++) {
			this->add_update(u, all_sides[i]);
		}
		*/
	}
}

void light::scheduler::on_state_change(coord_t, coord_t, coord_t)
{
}

void light::scheduler::on_power_change(coord_t, coord_t, coord_t)
{
}

void light::scheduler::init_chunk(game::chunk_ptr chunk)
{
	std::vector<update> down_updates;
	std::vector<update> other_updates;
	update u;
	u.light_type = update::e_light_sky;
	u.in_new = LIGHT_MAX_VALUE;
	coord_t y = (chunk->get_top() + 1) * CHUNK_SIZE;
	for (coord_t x=0; x<CHUNK_SIZE; x++) {
		for (coord_t z=0; z<CHUNK_SIZE; z++) {
			u.pos = pos_t(chunk->posw.x + x, y, chunk->posw.y + z);
			this->add_init_update(u, e_down, down_updates, other_updates);
		}
	}
	this->m_changes = 0;
	size_t index = 0;
	while (index < down_updates.size()) {
		update du = down_updates[index];
		++index;
		this->process_init_update(chunk, du, down_updates, other_updates);
	}
	for (auto const& update : other_updates) {
		this->m_updates.push_back(update);
	}
}

void light::scheduler::add_update(const update& u, side_t side)
{
	update nu(u);
	game::world::apply_side(nu.pos.x, nu.pos.y, nu.pos.z, side);
	if (!::in_bounds(nu.pos.x, nu.pos.y, nu.pos.z)) {
		return;
	}
	if (!m_world->chunk_exists_global(nu.pos.x, nu.pos.z)) {
		return;
	}
	nu.dir = switched_sides[side];
	this->m_updates.push_back(nu);
}

void light::scheduler::process(const update& u)
{
	switch (u.light_type) {
	case update::e_light_sky:
		this->process_sky(u);
		break;
	case update::e_light_block:
		this->process_block(u);
		break;
	}
}

void light::scheduler::process_sky(const update& u)
{
	light_t const current_value = m_world->get_light(u.pos.x, u.pos.y, u.pos.z);
	if (current_value.sky >= u.in_new && !u.flags.decrease_bit && !u.flags.request_bit) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value.sky;
	light_t new_value = current_value;

	// no value calculation if request
	if (u.flags.request_bit) {
		new_value.sky = current_value.sky;
		u1.in_new = new_value.sky;
		this->add_update(u1, u.dir);
		return;
	}

	// solid blocks are not transparent => light = 0, no updates
	block_t block = m_world->get_block(u.pos.x, u.pos.y, u.pos.z);
	if (!def::block_defs[block].transparent) {
		new_value.sky = 0;
		if (new_value.sky != current_value.sky) {
			this->m_world->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
			++m_changes;
		}
		return;
	}

	// calculate new value
	if (u.flags.decrease_bit) {
		// propagate "turn-off" or respond with increase
		uint8_t reduction = (0 < u.in_old) && (u.in_old < LIGHT_MAX_VALUE || u.dir != e_up);
		uint8_t old_value = u.in_old - reduction;
		if (old_value >= current_value.sky) {
			if (current_value.sky < LIGHT_MAX_VALUE || u.dir != e_down) {
				new_value.sky = 0;
				u1.flags.decrease_bit = 1;
			} else {
				return;
			}
		} else {
			new_value.sky = current_value.sky;
			u1.flags.response_bit = 1;
		}
	} else { // new_value.sky >= current_value.sky
		new_value.sky = u.in_new - (0 < u.in_new && (u.in_new < LIGHT_MAX_VALUE || u.dir != e_up));
	}

	if (new_value.sky != current_value.sky) {
		this->m_world->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
		++m_changes;
	}
	if (new_value.sky != current_value.sky || u1.flags.response_bit) {
		u1.in_new = new_value.sky;
		this->add_update(u1, e_up);
		this->add_update(u1, e_north);
		this->add_update(u1, e_west);
		this->add_update(u1, e_south);
		this->add_update(u1, e_east);
		this->add_update(u1, e_down);
	}
}

void light::scheduler::process_block(const update& u)
{
	light_t const current_value = m_world->get_light(u.pos.x, u.pos.y, u.pos.z);
	if (current_value.block >= u.in_new && !u.flags.decrease_bit && !u.flags.request_bit) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value.block;
	light_t new_value = current_value;

	// no value calculation if request
	if (u.flags.request_bit) {
		new_value.block = current_value.block;
		u1.in_new = new_value.block;
		this->add_update(u1, u.dir);
		return;
	}

	// solid blocks are not transparent => light = 0, no updates
	block_t block = m_world->get_block(u.pos.x, u.pos.y, u.pos.z);
	if (!def::block_defs[block].transparent) {
		new_value.block = 0;
		if (new_value.block != current_value.block) {
			this->m_world->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
			++m_changes;
		}
		return;
	}

	// calculate new value
	if (u.flags.decrease_bit) {
		// propagate "turn-off" or respond with increase
		uint8_t old_value = u.in_old - 1;
		if (old_value >= current_value.block) {
			new_value.block = 0;
			u1.flags.decrease_bit = 1;
		} else {
			new_value.block = current_value.block;
			u1.flags.response_bit = 1;
		}
	} else { // new_value.block >= current_value.block
		new_value.block = u.in_new - 1;
	}

	if (new_value.block != current_value.block) {
		this->m_world->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
		++m_changes;
	}
	if (new_value.block != current_value.block || u1.flags.response_bit) {
		u1.in_new = new_value.block;
		this->add_update(u1, e_up);
		this->add_update(u1, e_north);
		this->add_update(u1, e_west);
		this->add_update(u1, e_south);
		this->add_update(u1, e_east);
		this->add_update(u1, e_down);
	}
}

void light::scheduler::add_init_update(const light::update& u, side_t side, std::vector<update>& down_updates, std::vector<update>& other_updates)
{
	update nu(u);
	game::world::apply_side(nu.pos.x, nu.pos.y, nu.pos.z, side);
	if (!::in_bounds(nu.pos.x, nu.pos.y, nu.pos.z)) {
		return;
	}
	if (!m_world->chunk_exists_global(nu.pos.x, nu.pos.z)) {
		return;
	}
	nu.dir = switched_sides[side];
	if (side == e_down) {
		down_updates.push_back(nu);
	} else {
		other_updates.push_back(nu);
	}
}

void light::scheduler::process_init_update(game::chunk_ptr chunk, const light::update& u, std::vector<update>& down_updates, std::vector<update>& other_updates)
{
	light_t const current_value = chunk->get_light(u.pos.x, u.pos.y, u.pos.z);
	if (current_value.sky >= u.in_new && !u.flags.decrease_bit && !u.flags.request_bit) {
		return;
	}

	update u1;
	u1.pos = u.pos;
	u1.in_old = current_value.sky;
	light_t new_value = current_value;

	// no value calculation if request
	if (u.flags.request_bit) {
		//new_value.sky = current_value.sky;
		//u1.in_new = new_value.sky;
		//this->add_init_update(u1, u.dir, down_updates, other_updates);
		return;
	}

	// solid blocks are not transparent => light = 0, no updates
	block_t block = chunk->get_block(u.pos.x, u.pos.y, u.pos.z);
	if (!def::block_defs[block].transparent) {
		new_value.sky = 0;
		if (new_value.sky != current_value.sky) {
			chunk->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
			++m_changes;
		}
		return;
	}

	// calculate new value
	if (u.flags.decrease_bit) {
		// propagate "turn-off" or respond with increase
		uint8_t reduction = (0 < u.in_old) && (u.in_old < LIGHT_MAX_VALUE || u.dir != e_up);
		uint8_t old_value = u.in_old - reduction;
		if (old_value >= current_value.sky) {
			if (current_value.sky < LIGHT_MAX_VALUE || u.dir != e_down) {
				new_value.sky = 0;
				u1.flags.decrease_bit = 1;
			} else {
				return;
			}
		} else {
			new_value.sky = current_value.sky;
			u1.flags.response_bit = 1;
		}
	} else { // new_value.sky >= current_value.sky
		new_value.sky = u.in_new - (0 < u.in_new && (u.in_new < LIGHT_MAX_VALUE || u.dir != e_up));
	}

	if (new_value.sky != current_value.sky) {
		chunk->set_light(u.pos.x, u.pos.y, u.pos.z, new_value);
		++m_changes;
	}
	if (new_value.sky != current_value.sky || u1.flags.response_bit) {
		u1.in_new = new_value.sky;
		this->add_init_update(u1, e_up, down_updates, other_updates);
		this->add_init_update(u1, e_north, down_updates, other_updates);
		this->add_init_update(u1, e_west, down_updates, other_updates);
		this->add_init_update(u1, e_south, down_updates, other_updates);
		this->add_init_update(u1, e_east, down_updates, other_updates);
		this->add_init_update(u1, e_down, down_updates, other_updates);
	}
}
