#include "circuit_update.hpp"


circuit::update::update()
: id(0)
, type(circuit_update)
, delay(0)
, pos({0, 0, 0})
, dir(e_none)
, in_old(0)
, in_new(0)
, src_block(0)
, dest_block(0)
, flags({0x00})
{
}

circuit::update::update(uint64_t id)
: id(id)
, type(circuit_update)
, delay(0)
, pos({0, 0, 0})
, dir(e_none)
, in_old(0)
, in_new(0)
, src_block(0)
, dest_block(0)
, flags({0x00})
{
}

circuit::update::update(circuit::update && u)
: id(u.id)
, type(u.type)
, delay(u.delay)
, pos(u.pos)
, dir(u.dir)
, in_old(u.in_old)
, in_new(u.in_new)
, src_block(u.src_block)
, dest_block(u.dest_block)
, flags(u.flags)
{
}

circuit::update::update(const circuit::update& u)
: id(u.id)
, type(u.type)
, delay(u.delay)
, pos(u.pos)
, dir(u.dir)
, in_old(u.in_old)
, in_new(u.in_new)
, src_block(u.src_block)
, dest_block(u.dest_block)
, flags(u.flags)
{
}

circuit::update & circuit::update::operator=(circuit::update && u)
{
	this->id = u.id;
	this->type = u.type;
	this->delay = u.delay;
	this->pos = u.pos;
	this->dir = u.dir;
	this->in_old = u.in_old;
	this->in_new = u.in_new;
	this->src_block = u.src_block;
	this->dest_block = u.dest_block;
	this->flags = u.flags;
	return *this;
}

circuit::update & circuit::update::operator=(const circuit::update& u)
{
	this->id = u.id;
	this->type = u.type;
	this->delay = u.delay;
	this->pos = u.pos;
	this->dir = u.dir;
	this->in_old = u.in_old;
	this->in_new = u.in_new;
	this->src_block = u.src_block;
	this->dest_block = u.dest_block;
	this->flags = u.flags;
	return *this;
}
