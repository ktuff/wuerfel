#include "../core_system.hpp"
#include "module_listener.hpp"
#include "system_module.hpp"


entity::system_module::system_module(::entity::core_system* core, ::entity::system_module_id id)
: m_core(core)
, m_id(id)
{
	this->m_core->add_module(this);
}

entity::system_module::~system_module()
{
	this->m_core->remove_module(this);
}


entity::system_module_id entity::system_module::id() const
{
	return m_id;
}

void entity::system_module::on_entity_creation(::entity::entity*)
{
}

void entity::system_module::on_entity_destruction(::entity::entity*)
{
}

void entity::system_module::on_event(::entity::event*)
{
}

void entity::system_module::to_json(::entity::id, nlohmann::json&)
{
}

void entity::system_module::from_json(::entity::id, const nlohmann::json&)
{
}

void entity::system_module::add_listener(::entity::module_listener* listener)
{
	this->m_listeners.emplace(listener);
}

void entity::system_module::remove_listener(::entity::module_listener* listener)
{
	this->m_listeners.erase(listener);
}

void entity::system_module::emit_event(::entity::event* event)
{
	this->m_core->notify(event);
	for (auto* l : m_listeners) {
		l->on_event(event);
	}
}
