#pragma once

#include <set>

#include <nlohmann/json.hpp>

#include "../types.hpp"


namespace entity {

class core_system;
struct event;
class module_listener;


class system_module {

	protected:
		::entity::core_system* m_core;
		system_module_id const m_id;
		std::set<module_listener*> m_listeners;


	public:
		system_module(::entity::core_system* core, system_module_id id);
		virtual ~system_module();


	public:
		system_module_id id() const;
		virtual void on_entity_creation(::entity::entity* entity);
		virtual void on_entity_destruction(::entity::entity* entity);
		virtual void on_event(::entity::event* event);
		virtual void to_json(::entity::id id, nlohmann::json& json);
		virtual void from_json(::entity::id id, nlohmann::json const& json);
		void add_listener(module_listener* listener);
		void remove_listener(module_listener* listener);

	protected:
		void emit_event(::entity::event* event);

};

} // namespace entity
