#pragma once

#include <memory>
#include <unordered_map>

#include "observer.hpp"


namespace entity {

struct health;
class manager;


class health_system : ::entity::observer {

	private:
		::entity::manager* m_manager;
		std::unordered_map<entity_id, std::unique_ptr<health>> m_health;


	public:
		health_system(::entity::manager* manager);


	public:
		void on_entity_creation(entity_id id, entity_type type) override;
		void on_entity_destruction(entity_id id, entity_type type) override;
		void on_event(::entity::event * event) override;

	private:
		void damage_entity(entity_id id);

};

} // namespace entity

