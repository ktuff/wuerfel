#pragma once


namespace entity {

enum op_code : uint64_t {
	e_net_invalid,
	e_net_creation,
	e_net_destruction,
	e_net_pos,
	e_net_rot,
	e_net_item,
	e_net_item_selection,
	e_net_container
};

} // entity
