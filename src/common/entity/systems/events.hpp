#pragma once

#include <ktf/math/vec3.hpp>

#include "../types.hpp"
#include "inventory_types.hpp"
#include <common/item/item.hpp>


namespace entity {

class entity;
class system_module;


enum events {
	e_position,
	e_rotation,
	e_item_change,
	e_selection_change,
	e_hitbox,
	e_damage,
	e_container
};


struct event {
	::entity::system_module* origin;
	::entity::events const type;
	::entity::id entity_id;

	event(::entity::system_module* origin, ::entity::events type, ::entity::id id);
};

struct event_position : public event {
	ktf::vec3<double> position;

	event_position(::entity::system_module* origin, ::entity::id id, ktf::vec3<double> const& position);
};

struct event_rotation : public event {
	ktf::vec3<double> rotation;

	event_rotation(::entity::system_module* origin, ::entity::id id, ktf::vec3<double> const& rotation);
};

struct event_hitbox : public event {
	bool onground;

	event_hitbox(::entity::system_module* origin, ::entity::id id, bool onground);
};

struct event_item_change : public event {
	::entity::inventory_group target;
	int index;
	::item::item_stack old_item;
	::item::item_stack new_item;

	event_item_change(
		::entity::system_module* origin,
		::entity::id id,
		::entity::inventory_group target,
		int index,
		::item::item_stack old_item,
		::item::item_stack new_item
	);
};

struct event_selection_change : public event {
	int old_index;
	int new_index;

	event_selection_change(::entity::system_module* origin, ::entity::id id, int o_index, int n_index);
};

struct event_damage : public event {
	double health;

	event_damage(::entity::system_module* origin, ::entity::id id);
};

struct event_container : public event {
	::entity::id container;

	event_container(::entity::system_module* origin, ::entity::id id, ::entity::id container);
};

} // namespace entity
