#include "events.hpp"
#include "physics_system.hpp"


entity::system_module_id const entity::physics_system::s_id = 13678; // TODO


entity::physics_system::physics_system(::entity::core_system* core_system, game::world* world)
: system_module(core_system, s_id)
, m_world(world)
{
}

entity::physics_system::~physics_system()
{
}


void entity::physics_system::on_entity_creation(::entity::entity* entity)
{
}

void entity::physics_system::on_entity_destruction(::entity::entity* entity)
{
}

void entity::physics_system::on_event(::entity::event* event)
{
	switch (event->type) {
	case e_position:
		break;
	case e_rotation:
		break;
	default:
		break;
	}
}

void entity::physics_system::update()
{
}
