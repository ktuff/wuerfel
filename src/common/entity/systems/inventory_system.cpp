#include <common/entity/core_system.hpp>
#include <common/entity/entity.hpp>
#include <common/item/armor.hpp>
#include <common/item/crafting.hpp>
#include <common/item/inventory.hpp>

#include "inventory_system.hpp"


::entity::system_module_id const ::entity::inventory_system::s_id = 7;


entity::inventory_system::inventory_system(::entity::core_system* system)
: system_module(system, s_id)
{
}

entity::inventory_system::~inventory_system()
{
}


void entity::inventory_system::on_entity_creation(::entity::entity* entity)
{
	switch (entity->m_type) {
	case e_item:
		this->m_inventory_map.emplace(entity->m_id, std::make_unique<inventory>(1));
		break;
	case e_player:
		this->m_inventory_map.emplace(entity->m_id, std::make_unique<inventory>(36));
		this->m_crafting_map.emplace(entity->m_id, std::make_unique<crafting>(2, 2));
// 		this->m_armor_map.emplace(entity->m_id, std::make_unique<armor>());
		this->m_addon_map.emplace(entity->m_id, std::make_unique<container>(other_t, 1));
		break;
	case e_pig:
	case e_cow:
	case e_chicken:
		break;
	case e_chest:
		this->m_inventory_map.emplace(entity->m_id, std::make_unique<inventory>(27));
		break;
	case e_furnace:
		this->m_inventory_map.emplace(entity->m_id, std::make_unique<inventory>(3));
		break;
	case e_workbench:
		this->m_crafting_map.emplace(entity->m_id, std::make_unique<crafting>(3, 3));
		break;
	case e_max_num_entity_types:
		break;
	}
}

void entity::inventory_system::on_entity_destruction(::entity::entity* entity)
{
	switch (entity->m_type) {
	case e_item:
		this->m_inventory_map.erase(entity->m_id);
		break;
	case e_player:
		this->m_inventory_map.erase(entity->m_id);
		this->m_crafting_map.erase(entity->m_id);
// 		this->m_armor_map.erase(entity->m_id);
		this->m_addon_map.erase(entity->m_id);
		break;
	case e_pig:
	case e_cow:
	case e_chicken:
		break;
	case e_chest:
		this->m_inventory_map.erase(entity->m_id);
		break;
	case e_furnace:
		this->m_inventory_map.erase(entity->m_id);
		break;
	case e_workbench:
		this->m_crafting_map.erase(entity->m_id);
		break;
	case e_max_num_entity_types:
		break;
	}

	if (m_open_container_map.find(entity->m_id) != m_open_container_map.end()) {
		this->m_open_container_map.erase(entity->m_id);
	}
}

void entity::inventory_system::on_event(::entity::event* event)
{
	if (event->origin == this) {
		return;
	}

	switch (event->type) {
	case e_position:
	case e_rotation:
	case e_item_change:
	case e_selection_change:
	case e_hitbox:
	case e_damage:
	case e_container:
		break;
	}
}

void entity::inventory_system::to_json(::entity::id id, nlohmann::json& json)
{
	auto inventory = nlohmann::json::object();

	if (m_inventory_map.find(id) != m_inventory_map.end()) {
		nlohmann::json items = nlohmann::json::array();
		auto* inv = static_cast<::inventory*>(get_container(id, ::entity::e_items));
		for (size_t i=0; i<inv->size(); ++i) {
			nlohmann::json item_stack = nlohmann::json::object();
			auto const& item = inv->get_item(i);
			item_stack["type"] = item.type;
			item_stack["count"] = item.count;
			items += item_stack;
		}
		inventory["items"] = items;
	}

	if (m_crafting_map.find(id) != m_crafting_map.end()) {
		nlohmann::json items = nlohmann::json::array();
		auto* c = static_cast<crafting*>(get_container(id, ::entity::e_crafting));
		for (size_t i=0; i<c->size(); ++i) {
			nlohmann::json item_stack = nlohmann::json::object();
			auto const& item = c->get_item(i);
			item_stack["type"] = item.type;
			item_stack["count"] = item.count;
			items += item_stack;
		}
		inventory["crafting"] = items;
	}

	// TODO armor, temp

	json["inventory"] = inventory;
}

void entity::inventory_system::from_json(::entity::id id, const nlohmann::json& json)
{
	nlohmann::json const& inventory = json["inventory"];
	if (m_inventory_map.find(id) != m_inventory_map.end()) {
		nlohmann::json const& items = inventory["items"];
		auto* inv = static_cast<::inventory*>(get_container(id, ::entity::e_items));
		for (size_t i=0; i<inv->size(); ++i) {
			nlohmann::json item_stack = items[i];
			auto type = item_stack["type"].get<::item::item_type_id>();
			auto count = item_stack["count"].get<::item::stack_size_t>();
			::item::item_stack item(type, count);
			inv->set_item(i, item);
		}
	}

	if (m_crafting_map.find(id) != m_crafting_map.end()) {
		nlohmann::json items = inventory["crafting"];
		auto* c = static_cast<crafting*>(get_container(id, ::entity::e_crafting));
		for (size_t i=0; i<c->size(); ++i) {
			nlohmann::json item_stack = items[i];
			auto type = item_stack["type"].get<::item::item_type_id>();
			auto count = item_stack["count"].get<::item::stack_size_t>();
			::item::item_stack item(type, count);
			c->set_item(i, item);
		}
	}

	// TODO armor, temp
}

void entity::inventory_system::add_listener(::entity::id id, ::entity::inventory_group target, container_listener* listener)
{
	if (container* c = get_container(id, target)) {
		c->add_listener(listener);
	}
}

void entity::inventory_system::remove_listener(::entity::id id, ::entity::inventory_group target, container_listener* listener)
{
	if (container* c = get_container(id, target)) {
		c->remove_listener(listener);
	}
}

int entity::inventory_system::get_inventory_size(::entity::id id, ::entity::inventory_group target) const
{
	if (container* c = get_container(id, target)) {
		return c->m_size;
	} else {
		return 0;
	}
}

int entity::inventory_system::get_selected_index(::entity::id id) const
{
	auto it = m_inventory_map.find(id);
	if (it != m_inventory_map.end()) {
		return it->second->get_selected_index();
	} else {
		return 0;
	}
}

void entity::inventory_system::set_selected_index(::entity::id id, int index)
{
	auto it = m_inventory_map.find(id);
	if (it != m_inventory_map.end()) {
		int old_index = it->second->get_selected_index();
		if (old_index != index) {
			it->second->set_selected_index(index);
			int new_index = it->second->get_selected_index();

			::entity::event_selection_change event(this, id, old_index, new_index);
			this->emit_event(&event);
		}
	}
}

item::item_stack entity::inventory_system::get_item(::entity::id id, inventory_group target, int index)
{
	if (container* c = get_container(id, target)) {
		return c->get_item(index);
	} else {
		return ::item::item_stack::null;
	}
}

void entity::inventory_system::set_item(::entity::id id, inventory_group target, int index, ::item::item_stack const& item)
{
	if (container* c = get_container(id, target)) {
		::item::item_stack old = c->get_item(index);
		if (old == item) {
			return;
		} else if (c->set_item(index, item)) {
			::entity::event_item_change event(this, id, target, index, old, item);
			this->emit_event(&event);
		}
	}
}

void entity::inventory_system::swap_items(::entity::id id1, inventory_group target1, int index1, ::entity::id id2, inventory_group target2, int index2)
{
	container* container1 = get_container(id1, target1);
	container* container2 = get_container(id2, target2);
	if (container1 != nullptr && container2 != nullptr) {
		::item::item_stack i1 = container1->get_item(index1);
		::item::item_stack i2 = container2->get_item(index2);
		if (container::swap_item(container1, index1, container2, index2)) {
			::entity::event_item_change event1(this, id1, target1, index1, i1, i2);
			::entity::event_item_change event2(this, id2, target2, index2, i2, i1);
			this->emit_event(&event1);
			this->emit_event(&event2);
		}
	} else {
		// invalid
	}
}

::entity::id entity::inventory_system::get_container_id(::entity::id id) const
{
	auto it = m_open_container_map.find(id);
	if (it != m_open_container_map.end()) {
		return it->second;
	} else {
		return ::entity::id(0);
	}
}

void entity::inventory_system::set_container_id(::entity::id id, ::entity::id container)
{
	auto it = m_open_container_map.find(id);
	if (it != m_open_container_map.end()) {
		if (it->second == container) {
			return;
		}
		::entity::event_container ec(this, id, ::entity::id(0));
		this->emit_event(&ec);
		this->m_open_container_map.erase(id);
	}
	this->m_open_container_map.emplace(id, container);
	::entity::event_container ec(this, id, container);
	this->emit_event(&ec);
}

container * entity::inventory_system::get_container(::entity::id id, ::entity::inventory_group target) const
{
	switch (target) {
	case e_items:
		if (m_inventory_map.find(id) != m_inventory_map.end()) {
			return m_inventory_map.at(id).get();
		} else {
			return nullptr;
		}
	case e_crafting:
		if (m_crafting_map.find(id) != m_crafting_map.end()) {
			return m_crafting_map.at(id).get();
		} else {
			return nullptr;
		}
	case e_armor:
		if (m_armor_map.find(id) != m_armor_map.end()) {
			return m_armor_map.at(id).get();
		} else {
			return nullptr;
		}
	case e_other:
		if (m_addon_map.find(id) != m_addon_map.end()) {
			return m_addon_map.at(id).get();
		} else {
			return nullptr;
		}
	default:
		return nullptr;
	}
}
