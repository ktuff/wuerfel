#pragma once

#include <common/util/aabb.hpp>

#include "system_module.hpp"


namespace game {

class world;

} // namespace game


namespace entity {

struct physics {
	aabb hitbox;
	bool onground = true;
};


class physics_system : public ::entity::system_module {

	public:
		static system_module_id const s_id;

	private:
		game::world* m_world;
		std::unordered_map<::entity::id, std::unique_ptr<physics>> m_physics;


	public:
		physics_system(::entity::core_system* core_system, game::world* world);
		~physics_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void update();

};

} // namespace entity

