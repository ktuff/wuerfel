#pragma once

#include <common/types.hpp>

#include "../types.hpp"
#include "system_module.hpp"


namespace game {

class world;

} // namespace game


namespace entity {


enum target_type {
	e_target_none,
	e_target_block,
	e_target_entity
};


struct target {
	target_type type = e_target_none;
	side_t side = side_t::e_none;
	ktf::vec3<coord_t> position;
	::entity::id entity = 0;
};


class target_system : public ::entity::system_module {

	public:
		static system_module_id const s_id;

	private:
		game::world* m_world;
		std::unordered_map<::entity::id, ::entity::target> m_targets;
		std::set<::entity::id> m_changed;


	public:
		target_system(::entity::core_system* core, game::world* world);
		~target_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		target get_target(::entity::id id) const;
		void update();

};

} // namespace entity
