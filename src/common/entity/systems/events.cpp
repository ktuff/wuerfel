#include "../entity.hpp"
#include "events.hpp"


entity::event::event(::entity::system_module* origin, ::entity::events type, ::entity::id id)
: origin(origin)
, type(type)
, entity_id(id)
{
}

entity::event_position::event_position(::entity::system_module* origin, ::entity::id id, ktf::vec3<double> const& position)
: event(origin, e_position, id)
, position(position)
{
}

entity::event_rotation::event_rotation(::entity::system_module* origin, ::entity::id id, ktf::vec3<double> const& rotation)
: event(origin, e_rotation, id)
, rotation(rotation)
{
}

entity::event_hitbox::event_hitbox(::entity::system_module* origin, ::entity::id id, bool onground)
: event(origin, e_hitbox, id)
, onground(onground)
{
}

entity::event_item_change::event_item_change(
	::entity::system_module* origin,
	::entity::id id,
	::entity::inventory_group target,
	int index,
	::item::item_stack old_item,
	::item::item_stack new_item
)
: event(origin, e_item_change, id)
, target(target)
, index(index)
, old_item(old_item)
, new_item(new_item)
{
}

entity::event_selection_change::event_selection_change(::entity::system_module* origin, ::entity::id id, int o_index, int n_index)
: event(origin, e_selection_change, id)
, old_index(o_index)
, new_index(n_index)
{
}

entity::event_damage::event_damage(::entity::system_module* origin, ::entity::id id)
: event(origin, e_damage, id)
{
}

entity::event_container::event_container(::entity::system_module* origin, ::entity::id id, ::entity::id container)
: event(origin, e_container, id)
, container(container)
{
}
