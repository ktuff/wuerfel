#pragma once

#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <nlohmann/json.hpp>

#include <common/item/item.hpp>

#include "events.hpp"
#include "inventory_types.hpp"
#include "system_module.hpp"


class armor;
class container;
class container_listener;
class crafting;
class inventory;


namespace entity {

class inventory_system : public ::entity::system_module {

	public:
		static system_module_id const s_id;

	private:
		std::unordered_map<::entity::id, std::unique_ptr<::inventory>> m_inventory_map;
		std::unordered_map<::entity::id, std::unique_ptr<crafting>> m_crafting_map;
		std::unordered_map<::entity::id, std::unique_ptr<armor>> m_armor_map;
		std::unordered_map<::entity::id, std::unique_ptr<container>> m_addon_map;
		std::unordered_map<::entity::id, ::entity::id> m_open_container_map;
		std::unordered_set<::entity::system_module*> m_observer;


	public:
		inventory_system(core_system* system);
		~inventory_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;
		void to_json(::entity::id id, nlohmann::json& json) override;
		void from_json(::entity::id id, nlohmann::json const& json) override;

		void add_listener(::entity::id id, inventory_group target, container_listener* listener);
		void remove_listener(::entity::id id, inventory_group target, container_listener* listener);

		int get_inventory_size(::entity::id id, inventory_group target) const;
		int get_selected_index(::entity::id id) const;
		void set_selected_index(::entity::id id, int index);
		::item::item_stack get_item(::entity::id id, inventory_group target, int index);
		void set_item(::entity::id id, inventory_group target, int index, ::item::item_stack const& item);
		void swap_items(::entity::id id1, inventory_group target1, int index1, ::entity::id id2, inventory_group target2, int index2);
		::entity::id get_container_id(::entity::id id) const;
		void set_container_id(::entity::id id, ::entity::id container);

	private:
		container* get_container(::entity::id id, inventory_group target) const;
};

} // namespace entity

