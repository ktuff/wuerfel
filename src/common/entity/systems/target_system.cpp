#include <common/entity/systems/events.hpp>
#include <common/util/hitboxes.hpp>

#include "../core_system.hpp"
#include "../entities/living_entity.hpp"
#include "target_system.hpp"


::entity::system_module_id const entity::target_system::s_id = 578;


entity::target_system::target_system(::entity::core_system* core, game::world* world)
: system_module(core, s_id)
, m_world(world)
{
}

entity::target_system::~target_system()
{
}


void entity::target_system::on_entity_creation(::entity::entity* entity)
{
	if (auto* e = dynamic_cast<::entity::living_entity*>(entity)) {
		target t;
		t.type = e_target_none;
		this->m_targets.emplace(e->m_id, t);
		this->m_changed.emplace(e->m_id);
	}
}

void entity::target_system::on_entity_destruction(::entity::entity* entity)
{
	if (auto* e = dynamic_cast<::entity::living_entity*>(entity)) {
		this->m_targets.erase(e->m_id);
	}
}

void entity::target_system::on_event(::entity::event* event)
{
	switch (event->type) {
	case e_position:
		this->m_changed.emplace(event->entity_id);
		break;
	case e_rotation:
		this->m_changed.emplace(event->entity_id);
		break;
	default:
		break;
	}
}

entity::target entity::target_system::get_target(::entity::id id) const
{
	if (m_targets.find(id) != m_targets.end()) {
		return m_targets.at(id);
	} else {
		target t;
		t.type = e_target_none;
		return t;
	}
}

void entity::target_system::update()
{
	for (::entity::id id : m_changed) {
		auto* entity = m_core->get_entity(id);
		if (!entity) {
			continue;
		}

		ktf::vec3<double> pos;
		float reach = 4.0f;
		if (auto* le = dynamic_cast<::entity::living_entity*>(entity)) {
			pos = le->get_eye_position();
			reach = le->get_reach();
		} else {
			continue;
		}
		auto& target = m_targets.at(id);
		target.type = e_target_none;

		ktf::vec3<double> const& dir = entity->get_direction(); // TODO get_eye_rotation
		double dx = dir.x >= 0.0 ? reach : -reach;
		double dy = dir.y >= 0.0 ? reach : -reach;
		double dz = dir.z >= 0.0 ? reach : -reach;
		aabb const main_box(pos, pos);
		aabb const box = main_box.expand_directional(dx, dy, dz);
		std::vector<aabb> boxes;
		hitboxes::get_viewboxes(m_world, boxes, box);

		const ray r(pos, dir);
		double tmin = reach + 0.00001;
		for (aabb const& a : boxes) {
			if (a.intersect(r, 0.0, reach)) {
				double t = 0.0;
				side_t side = a.intersected_side(r, t);
				if (t > 0.0 && t < tmin) {
					tmin = t;
					target.type = e_target_block;
					target.position = a.origin;
					target.side = side;
				}
			}
		}
	}
}
