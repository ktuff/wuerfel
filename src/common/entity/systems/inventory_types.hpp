#pragma once


namespace entity {

enum inventory_group {
	e_items,
	e_crafting,
	e_armor,
	e_other
};

} // namespace entity
