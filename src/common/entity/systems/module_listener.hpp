#pragma once

#include "events.hpp"


namespace entity {

class module_listener {

	public:
		virtual void on_event(::entity::event* event) = 0;

};

} // namespace entity
