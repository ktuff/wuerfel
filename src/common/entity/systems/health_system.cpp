#include "events.hpp"
#include "health_system.hpp"


namespace entity {

struct health {
	bool immune;
	float max_health;
	float current_health;
	float dt_last_damage;
};

} // namespace entity


entity::health_system::health_system(::entity::manager* manager)
: m_manager(manager)
{
}


void entity::health_system::on_entity_creation(entity_id id, entity_type type)
{
}

void entity::health_system::on_entity_destruction(entity_id id, entity_type type)
{
}

void entity::health_system::on_event(::entity::event* event)
{
	switch (event->type) {
	case e_damage:
		this->damage_entity(event->entity);
		break;
	default:
		break;
	}
}

void entity::health_system::damage_entity(entity_id id)
{
	auto it = m_health.find(id);
	if (it == m_health.end()) {
		return;
	}
	if (it->second->immune) {
		return;
	}

	if (it->second->dt_last_damage) {
		// TODO
	}

	// TODO

	if (it->second->current_health <= 0.0f) {
		// kill / discard entity
		// TODO
	}
}

