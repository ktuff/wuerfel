#include "core_system.hpp"
#include "systems/events.hpp"
#include "systems/system_module.hpp"
#include "entities/chest.hpp"
#include "entities/chicken.hpp"
#include "entities/cow.hpp"
#include "entities/furnace.hpp"
#include "entities/item.hpp"
#include "entities/pig.hpp"
#include "entities/player.hpp"
#include "entities/workbench.hpp"


entity::core_system::core_system(game::world* world)
: m_next_id(1)
, m_world(world)
{
}

entity::core_system::~core_system()
{
}


void entity::core_system::add_module(::entity::system_module* sys_module)
{
	assert(m_modules.find(sys_module->id()) == m_modules.end());
	this->m_modules.emplace(sys_module->id(), sys_module);
}

void entity::core_system::remove_module(::entity::system_module* sys_module)
{
	this->m_modules.erase(sys_module->id());
}

entity::system_module * entity::core_system::get_module(::entity::system_module_id id) const
{
	auto it = m_modules.find(id);
	if (it != m_modules.end()) {
		return m_modules.at(id);
	} else {
		return nullptr;
	}
}

void entity::core_system::notify(::entity::event* event)
{
	for (auto& [id, sm] : m_modules) {
		sm->on_event(event);
	}
}

void entity::core_system::reset()
{
	while (!m_entity_map.empty()) {
		this->destroy_entity(m_entity_map.begin()->first);
	}
	this->m_entity_map.clear();
}

::entity::id entity::core_system::get_next_id() const
{
	return m_next_id;
}

void entity::core_system::set_next_id(::entity::id start)
{
	this->m_next_id = start;
}

bool entity::core_system::valid(::entity::id id) const
{
	return (m_entity_map.find(id) != m_entity_map.end());
}

entity::entity * entity::core_system::get_entity(::entity::id id) const
{
	auto it = m_entity_map.find(id);
	if (it != m_entity_map.end()) {
		return it->second.get();
	} else {
		return nullptr;
	}
}

void entity::core_system::destroy_entity(::entity::id id)
{
	auto it = m_entity_map.find(id);
	if (it != m_entity_map.end()) {
		for (auto& [id, mod] : m_modules) {
			mod->on_entity_destruction(it->second.get());
		}
		this->m_entity_map.erase(id);
	}
}

void entity::core_system::sweep_discarded()
{
	std::vector<::entity::id> discarded_ids;
	for (auto& [id, entity] : m_entity_map) {
		if (entity->is_discarded()) {
			discarded_ids.push_back(id);
		}
	}
	for (::entity::id id : discarded_ids) {
		this->destroy_entity(id);
	}
}

void entity::core_system::update(int64_t ns)
{
	for (auto& [id, entity] : m_entity_map) {
		entity->update(ns);
		entity->move(ns / 1.0e9);
	}
}

::entity::item* entity::core_system::create_entity_item(::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::item>(this, m_world, id);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::player* entity::core_system::create_entity_player(::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::player>(this, m_world, id);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::pig * entity::core_system::create_entity_pig(::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::pig>(this, m_world, id);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::cow * entity::core_system::create_entity_cow(::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::cow>(this, m_world, id);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::chicken * entity::core_system::create_entity_chicken(::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::chicken>(this, m_world, id);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::chest* entity::core_system::create_entity_chest(const ktf::vec3<coord_t>& pos, ::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::chest>(this, m_world, id, pos);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::furnace* entity::core_system::create_entity_furnace(const ktf::vec3<coord_t>& pos, ::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::furnace>(this, m_world, id, pos);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

::entity::workbench* entity::core_system::create_entity_workbench(const ktf::vec3<coord_t>& pos, ::entity::id id)
{
	id = get_entity_id(id);
	auto entity = std::make_unique<::entity::workbench>(this, m_world, id, pos);
	auto* ptr = entity.get();
	this->on_entity_creation(std::move(entity));
	return ptr;
}

bool entity::core_system::to_json(::entity::id entity_id, nlohmann::json& json) const
{
	auto const* entity = get_entity(entity_id);
	if (entity == nullptr) {
		return false;
	}

	json["id"] = entity->m_id;
	json["type"] = entity->m_type;
	switch (entity->m_type) {
	case e_chest:
	case e_furnace:
	case e_workbench: {
		auto* t = dynamic_cast<tile const*>(entity);
		auto const& vox_pos = t->get_voxel_position();
		auto pos = nlohmann::json::object();
		pos["x"] = vox_pos.x;
		pos["y"] = vox_pos.y;
		pos["z"] = vox_pos.z;
		json["voxelPos"] = pos;
	}	break;
	default:
		break;
	}

	for (auto mod : m_modules) {
		mod.second->to_json(entity_id, json);
	}
	return true;
}

::entity::id entity::core_system::from_json(const nlohmann::json& json)
{
	::entity::id entity_id = json["id"].get<::entity::id>();
	auto* entity = get_entity(entity_id);
	if (!entity) {
		::entity::type type = json["type"].get<::entity::type>();
		switch (type) {
		case e_player:
			this->create_entity_player(entity_id);
			break;
		case e_item:
			this->create_entity_item(entity_id);
			break;
		case e_pig:
			this->create_entity_pig(entity_id);
			break;
		case e_cow:
			this->create_entity_cow(entity_id);
			break;
		case e_chicken:
			this->create_entity_chicken(entity_id);
			break;
		case e_chest: {
			auto const& pos = json["voxelPos"];
			ktf::vec3<coord_t> vox_pos;
			pos["x"].get_to(vox_pos.x);
			pos["y"].get_to(vox_pos.y);
			pos["z"].get_to(vox_pos.z);
			this->create_entity_chest(vox_pos, entity_id);
		}	break;
		case e_furnace: {
			auto const& pos = json["voxelPos"];
			ktf::vec3<coord_t> vox_pos;
			pos["x"].get_to(vox_pos.x);
			pos["y"].get_to(vox_pos.y);
			pos["z"].get_to(vox_pos.z);
			this->create_entity_furnace(vox_pos, entity_id);
		}	break;
		case e_workbench: {
			auto const& pos = json["voxelPos"];
			ktf::vec3<coord_t> vox_pos;
			pos["x"].get_to(vox_pos.x);
			pos["y"].get_to(vox_pos.y);
			pos["z"].get_to(vox_pos.z);
			this->create_entity_workbench(vox_pos, entity_id);
		} break;
		case e_max_num_entity_types:
			// invalid
			break;
		}
		for (auto mod : m_modules) {
			mod.second->from_json(entity_id, json);
		}
	}
	return entity_id;
}

::entity::id entity::core_system::get_entity_id(::entity::id id)
{
	if (id == 0) {
		id = m_next_id;
		this->m_next_id = m_next_id + 1;
	}
	return id;
}

void entity::core_system::on_entity_creation(std::unique_ptr<::entity::entity> && entity)
{
	::entity::entity* entity_ptr = entity.get();
	this->m_entity_map.emplace(entity->m_id, std::move(entity));

	for (auto& [id, mod] : m_modules) {
		mod->on_entity_creation(entity_ptr);
	}
}

