#pragma once

#include <memory>

#include <nlohmann/json.hpp>

#include "entity.hpp"
#include "types.hpp"


namespace game {

class world;

} // namespace game


namespace entity {

class chest;
class chicken;
class cow;
class entity;
struct event;
class furnace;
class item;
class pig;
class player;
class system_module;
class workbench;


class core_system {

	private:
		std::unordered_map<system_module_id, system_module*> m_modules;

		::entity::id m_next_id;
		std::unordered_map<::entity::id, std::unique_ptr<::entity::entity>> m_entity_map;
		game::world* m_world;


	public:
		core_system(game::world* world);
		~core_system();


	public:
		void add_module(system_module* sys_module);
		void remove_module(system_module* sys_module);
		system_module* get_module(system_module_id id) const;
		void notify(::entity::event* event);

		void reset();
		::entity::id get_next_id() const;
		void set_next_id(::entity::id start);
		bool valid(::entity::id id) const;
		entity* get_entity(::entity::id id) const;
		void destroy_entity(::entity::id id);
		void sweep_discarded();
		void update(int64_t ns);

		::entity::item* create_entity_item(::entity::id id = 0);
		::entity::player* create_entity_player(::entity::id id = 0);
		::entity::pig* create_entity_pig(::entity::id id = 0);
		::entity::cow* create_entity_cow(::entity::id id = 0);
		::entity::chicken* create_entity_chicken(::entity::id id = 0);
		::entity::chest* create_entity_chest(ktf::vec3<coord_t> const& pos, ::entity::id id = 0);
		::entity::furnace* create_entity_furnace(ktf::vec3<coord_t> const& pos, ::entity::id id = 0);
		::entity::workbench* create_entity_workbench(ktf::vec3<coord_t> const& pos, ::entity::id id = 0);

		bool to_json(::entity::id entity_id, nlohmann::json& json) const;
		::entity::id from_json(nlohmann::json const& json);

	private:
		::entity::id get_entity_id(::entity::id id);
		void on_entity_creation(std::unique_ptr<::entity::entity>&& entity);

};

} // namespace entity
