#pragma once

#include <common/util/aabb.hpp>

#include "types.hpp"


namespace game {

class world;

} // namespace game


namespace entity {

class core_system;


class entity {

	public:
		::entity::type const m_type;
		::entity::id const m_id;

	protected:
		::entity::core_system* m_core_system;
		game::world* m_world;

	private:
		bool m_discarded;
		ktf::vec3<double> m_position;
		ktf::vec3<double> m_rotation;
		ktf::vec3<double> m_velocity;
		ktf::vec3<double> m_acceleration;
		ktf::vec3<double> m_direction; // TODO remove
	protected:
		bool m_collide;
		aabb m_hitbox;
		bool m_onground;


	public:
		entity(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id);
		virtual ~entity();


	public:
		bool is_discarded() const;
		void discard();
		ktf::vec3<double> get_position() const;
		ktf::vec3<double> get_rotation() const;
		ktf::vec3<double> get_direction() const;
		ktf::vec3<double> get_velocity();
		ktf::vec3<double> get_acceleration();
		virtual ktf::vec3<double> get_eye_position() const;
		virtual ktf::vec3<double> get_eye_rotation() const;
		aabb get_hitbox() const;
		bool onground() const;

		void set_position(const ktf::vec3<double>& pos);
		void set_rotation(const ktf::vec3<double>& rot);
		void set_velocity(const ktf::vec3<double>& vel);
		void set_acceleration(const ktf::vec3<double>& acc);

		virtual void update(int64_t ns);
		void move(double dt);

	private:
		virtual ktf::vec3<double> clip_velocity(ktf::vec3<double> const& init_velocity);
		virtual void update_velocity(double dt, ktf::vec3<double> const& velocity);

};

} // namespace entity
