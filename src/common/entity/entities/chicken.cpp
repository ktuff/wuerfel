#include <common/util/hitboxes.hpp>

#include "chicken.hpp"


entity::chicken::chicken(::entity::core_system* core_system, game::world* world, ::entity::id id)
: living_entity(core_system, world, e_chicken, id)
{
	this->set_eye_offset({0.00, 0.25, 0.00}); // TODO correct offset
	this->set_acceleration({0.0, -9.81 * 1.2, 0.0});
}

entity::chicken::~chicken()
{
}


void entity::chicken::update(int64_t ns)
{
}

ktf::vec3<double> entity::chicken::clip_velocity(const ktf::vec3<double>& init_velocity)
{
	ktf::vec3<double> velocity = init_velocity;
	aabb hitbox = m_hitbox;
	aabb const& movement_space = hitbox.expand_directional(velocity.x, velocity.y, velocity.z);
	std::vector<aabb> hitboxes;
	hitboxes::get_hitboxes(m_world, hitboxes, movement_space);
	// skip unnecessary calculations
	if (hitboxes.empty()) {
		return init_velocity;
	}

	for (const aabb& bounding_box : hitboxes) {
		velocity.y = bounding_box.clip_y_collision(hitbox, velocity.y);
	}
	hitbox.move(ktf::vec3<double>(0.0, velocity.y, 0.0));

	for (const aabb& bounding_box : hitboxes) {
		velocity.x = bounding_box.clip_x_collision(hitbox, velocity.x);
	}
	hitbox.move(ktf::vec3<double>(velocity.x, 0.0, 0.0));

	for (const aabb& bounding_box : hitboxes) {
		velocity.z = bounding_box.clip_z_collision(hitbox, velocity.z);
	}
	hitbox.move(ktf::vec3<double>(0.0, 0.0, velocity.z));

	return velocity;
}
