#include "chest.hpp"

entity::chest::chest(::entity::core_system* core_system, game::world* world, ::entity::id id, const ktf::vec3<coord_t>& pos)
: tile(core_system, world, ::entity::type::e_chest, id, pos)
{
}

entity::chest::~chest()
{
}


void entity::chest::connect()
{
}

void entity::chest::disconnect()
{
}

void entity::chest::update(int64_t ns)
{
}
