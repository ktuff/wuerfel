#pragma once

#include "tile.hpp"


namespace entity {

class workbench : public ::entity::tile {

	private:


	public:
		workbench(::entity::core_system* core_system, game::world* world, ::entity::id id, ktf::vec3<coord_t> const& pos);
		~workbench();


	public:
		void update(int64_t ns) override;

};

} // namespace entity
