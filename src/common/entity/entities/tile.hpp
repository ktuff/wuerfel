#pragma once

#include "../entity.hpp"


namespace entity {

class tile : public ::entity::entity {

	private:
		ktf::vec3<coord_t> m_block_pos;


	public:
		tile(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id, ktf::vec3<coord_t> const& pos);
		virtual ~tile();


	public:
		ktf::vec3<coord_t> get_voxel_position() const;
		void move(ktf::vec3<coord_t> const& new_pos);

};

} // namespace entity
