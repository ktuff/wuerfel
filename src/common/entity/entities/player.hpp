#pragma once

#include "living_entity.hpp"


namespace entity {

class player : public ::entity::living_entity {

	private:


	public:
		player(::entity::core_system* core_system, game::world* world, ::entity::id id);
		~player();


	public:
		void update();
		ktf::vec3<double> clip_velocity(ktf::vec3<double> const& init_velocity) override;
		void update_velocity(double dt, ktf::vec3<double> const& velocity) override;

};

} // namespace entity
