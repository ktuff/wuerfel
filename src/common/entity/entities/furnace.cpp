#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/logic/furnace_recipes.hpp>

#include "furnace.hpp"


entity::furnace::furnace(::entity::core_system* core_system, game::world* world, ::entity::id id, const ktf::vec3<coord_t>& pos)
: tile(core_system, world, ::entity::type::e_furnace, id, pos)
, m_fuel(0)
, m_progress(0)
{
}

entity::furnace::~furnace()
{
}


void entity::furnace::update(int64_t)
{
	auto* mod = m_core_system->get_module(::entity::inventory_system::s_id);
	auto* is = dynamic_cast<::entity::inventory_system*>(mod);

	::item::item_stack raw = is->get_item(m_id, e_items, 0);
	::item::item_stack fuel = is->get_item(m_id, e_items, 1);
	::item::item_stack cooked = is->get_item(m_id, e_items, 2);
	if (!furnace_recipes::get_result(raw.type)) {
		this->m_progress = 0;
		return;
	}

	if (m_fuel <= 0) {
		if (!::item::all_types[fuel.type]->features.fuel) {
			this->m_progress = 0;
			return;
		}
		float fuel_value = ::item::all_types[fuel.type]->burn_value;
		this->m_fuel += fuel_value;
		fuel -= 1;
		is->set_item(m_id, e_items, 1, fuel);
	}
	this->m_progress += 1;
	this->m_fuel -= 1;
	float cook_time = ::item::all_types[raw.type]->cook_time;
	if (m_progress >= cook_time) {
		this->m_progress = 0;
		if (cooked.type == ::item::e_none) {
			::item::item_type_id cooked_id = furnace_recipes::get_result(raw.type);
			cooked = ::item::item_stack(cooked_id, 1);
		} else {
			cooked += 1;
		}
		raw -= 1;
		is->set_item(m_id, e_items, 0, raw);
		is->set_item(m_id, e_items, 2, cooked);
	}
}
