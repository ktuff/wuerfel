#pragma once

#include "../entity.hpp"

namespace entity {

class item : public ::entity::entity {

	private:
		uint32_t m_time_to_live;


	public:
		item(::entity::core_system* core_system, game::world* world, ::entity::id id);
		~item();


	public:
		void update(int64_t ns) override;
		ktf::vec3<double> clip_velocity(const ktf::vec3<double>& init_velocity) override;

};

} // namespace entity
