#include "living_entity.hpp"


entity::living_entity::living_entity(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id)
: ::entity::entity(core_system, world, type, id)
, m_health(20.0)
, m_damage_cooldown(0)
{
}

entity::living_entity::~living_entity()
{
}


ktf::vec3<double> entity::living_entity::get_eye_position() const
{
	// apply transform
	return get_position() + m_eye_offset;
}

ktf::vec3<double> entity::living_entity::get_eye_rotation() const
{
	return m_eye_rotation;
}

void entity::living_entity::set_eye_offset(const ktf::vec3<double>& offset)
{
	this->m_eye_offset = offset;
}

void entity::living_entity::set_eye_rotation(const ktf::vec3<double>& rotation)
{
	this->m_eye_rotation = rotation;
}

float entity::living_entity::get_reach() const
{
	return 4.0f;
}

double entity::living_entity::get_health() const
{
// 	m_damage_cooldown;
// 	this->m_health = std::max(0.0, m_health - damage);
// 	if (m_health == 0.0) {
// 		this->kill();
// 	}
// 	this->m_velocity += direction * damage;
	return m_health;
}
