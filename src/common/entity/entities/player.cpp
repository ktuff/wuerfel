#include <common/util/hitboxes.hpp>

#include "player.hpp"


entity::player::player(::entity::core_system* core_system, game::world* world, ::entity::id id)
: living_entity(core_system, world, ::entity::type::e_player, id)
{
	this->set_eye_offset({0.00, 1.62, 0.00});
}

entity::player::~player()
{
}


void entity::player::update()
{
}

ktf::vec3<double> entity::player::clip_velocity(const ktf::vec3<double>& init_velocity)
{
	// skip collision and step mechanic if collision disabled
	if (m_collide) {
		bool edge_clipping = false; // TODO
		ktf::vec3<double> velocity = init_velocity;
		bool onground = m_onground;
		aabb hitbox = m_hitbox;
		aabb step_hitbox = hitbox;
		aabb const& movement_space = hitbox.expand_directional(velocity.x, velocity.y, velocity.z);
		std::vector<aabb> hitboxes;
		hitboxes::get_hitboxes(m_world, hitboxes, movement_space);
		// skip unnecessary calculations
		if (hitboxes.empty()) {
			return init_velocity;
		}

		double const clip_eps = 0.0001;
		double const step_size = 0.52;
		bool const x_first = std::abs(velocity.x) > std::abs(velocity.z);

		// move in y direction
		for (const aabb& bounding_box : hitboxes) {
			velocity.y = bounding_box.clip_y_collision(hitbox, velocity.y);
		}

		hitbox.move(ktf::vec3<double>(0.0, velocity.y, 0.0));
		onground = (init_velocity.y != velocity.y && init_velocity.y < 0.0);

		bool clipped = false;

		if (x_first) {
			goto collision_x_check;
		} else {
			goto collision_z_check;
		}

collision_x_check:
		// move in x direction
		if (velocity.x != 0.0) {
			double dclip = 0.0;
			for (const aabb& bounding_box : hitboxes) {
				double dy = hitbox.param[0].y - bounding_box.param[1].y;
				// ignore if not reachable
				bool ignore = ((bounding_box.param[0].x - hitbox.param[1].x - velocity.x) > 0) ||
							  ((bounding_box.param[1].x - hitbox.param[0].x - velocity.x) < 0);
				if (onground && dy >= -clip_eps && dy < step_size && !ignore) {
					double dx = bounding_box.clip_x_offset(hitbox, velocity.x, false, true);
					if (std::abs(dclip) < std::abs(dx)) {
						dclip = dx;
					}
				}
				velocity.x = bounding_box.clip_x_collision(hitbox, velocity.x);
			}

			// clip to edge (x)
			if (edge_clipping && onground) {
				if (std::abs(dclip) - clip_eps <= 0.0) {
					dclip = 0.0;
				} else {
					dclip -= ((dclip > 0.0) - (dclip < 0.0)) * clip_eps;
				}
				if (std::abs(dclip) < std::abs(velocity.x)) {
					velocity.x = dclip;
					clipped = true;
				}
			}
			hitbox.move(ktf::vec3<double>(velocity.x, 0.0, 0.0));
		}
		if (x_first) {
			goto collision_z_check;
		} else {
			goto after_collision_check;
		}

collision_z_check:
		// move in z direction
		if (velocity.z != 0.0) {
			double dclip = 0.0;
			for (const aabb& bounding_box : hitboxes) {
				double dy = hitbox.param[0].y - bounding_box.param[1].y;
				// ignore if not reachable
				bool ignore = ((bounding_box.param[0].z - hitbox.param[1].z - velocity.z) > 0) ||
							  ((bounding_box.param[1].z - hitbox.param[0].z - velocity.z) < 0);
				if (onground && dy >= -clip_eps && dy < step_size && !ignore) {
					double dz = bounding_box.clip_z_offset(hitbox, velocity.z, true, false);
					if (std::abs(dclip) < std::abs(dz)) {
						dclip = dz;
					}
				}
				velocity.z = bounding_box.clip_z_collision(hitbox, velocity.z);
			}

			// clip to edge (z)
			if (edge_clipping && onground) {
				if (std::abs(dclip) - clip_eps <= 0.0) {
					dclip = 0.0;
				} else {
					dclip -= ((dclip > 0.0) - (dclip < 0.0)) * clip_eps;
				}
				if (std::abs(dclip) < std::abs(velocity.z)) {
					velocity.z = dclip;
					clipped = true;
				}
			}
			hitbox.move(ktf::vec3<double>(0.0, 0.0, velocity.z));
		}
		if (x_first) {
			goto after_collision_check;
		} else {
			goto collision_x_check;
		}

after_collision_check:
		// step if collision with block on same height, ignore clipped
		if ((init_velocity.x != velocity.x || init_velocity.z != velocity.z) && !clipped && onground) {
			ktf::vec3<double> bvel = init_velocity;
			// step up
			bvel.y += step_size;
			for (const aabb& bounding_box : hitboxes) {
				bvel.y = bounding_box.clip_y_collision(step_hitbox, bvel.y);
			}
			step_hitbox.move(ktf::vec3<double>(0.0, bvel.y, 0.0));
			// try x direction
			if (bvel.x != 0.0) {
				for (const aabb& bounding_box : hitboxes) {
					bvel.x = bounding_box.clip_x_collision(step_hitbox, bvel.x);
				}
				step_hitbox.move(ktf::vec3<double>(bvel.x, 0.0, 0.0));
			}
			// try z direction
			if (bvel.z != 0.0) {
				for (const aabb& bounding_box : hitboxes) {
					bvel.z = bounding_box.clip_z_collision(step_hitbox, bvel.z);
				}
				step_hitbox.move(ktf::vec3<double>(0.0, 0.0, bvel.z));
			}
			// move down and save results if more movement possible than without up-step
			if ((velocity.x > 0 && bvel.x > velocity.x) || (velocity.x < 0 && bvel.x < velocity.x) || (velocity.z > 0 && bvel.z > velocity.z) || (velocity.z < 0 && bvel.z < velocity.z)) {
				double yoff = -step_size;
				for (const aabb& bounding_box : hitboxes) {
					yoff = bounding_box.clip_y_collision(step_hitbox, yoff);
				}
				step_hitbox.move(ktf::vec3<double>(0.0, yoff, 0.0));
				bvel.y += yoff;
				velocity = bvel;
				hitbox = step_hitbox;
			}
		}

		this->m_onground = onground;
		return velocity;
	} else {
		return init_velocity;
	}
}

void entity::player::update_velocity(double dt, ktf::vec3<double> const& velocity)
{
	// update velocity
	double xz_damp = 7.5;
	if (!m_onground) {
		// less velocity change in air
		xz_damp = 7.0;
	}
	ktf::vec3<double> a = get_acceleration();
	ktf::vec3<double> v = velocity;
	v.x = v.x - dt * xz_damp * v.x + dt * a.x;
	v.z = v.z - dt * xz_damp * v.z + dt * a.z;
	v.y = v.y - 0.15 * dt * v.y + dt * a.y;

	this->set_velocity(v);
	this->set_acceleration(ktf::vec3<double>());
}
