#pragma once

#include "living_entity.hpp"


namespace entity {

class cow : public living_entity {

	private:


	public:
		cow(::entity::core_system* core_system, game::world* world, ::entity::id id);
		~cow();


	public:
		void update(int64_t ns) override;
		ktf::vec3<double> clip_velocity(const ktf::vec3<double>& init_velocity) override;

};

} // namespace entity
