#include "tile.hpp"

entity::tile::tile(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id, ktf::vec3<coord_t> const& pos)
: entity(core_system, world, type, id)
, m_block_pos(pos)
{
}

entity::tile::~tile()
{
}


ktf::vec3<coord_t> entity::tile::get_voxel_position() const
{
	return m_block_pos;
}

void entity::tile::move(ktf::vec3<coord_t> const& new_pos)
{
	this->m_block_pos = new_pos;
	// TODO verify
}
