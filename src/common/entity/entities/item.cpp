#include <common/util/hitboxes.hpp>

#include "item.hpp"


constexpr uint32_t const initial_time_to_live = 5 * 60 * 20; // TODO define number of ticks per second in constant


entity::item::item(::entity::core_system* core_system, game::world* world, ::entity::id id)
: entity::entity(core_system, world, ::entity::type::e_item, id)
, m_time_to_live(initial_time_to_live)
{
	this->set_acceleration({0.0, -9.81 * 1.2, 0.0});
}

entity::item::~item()
{
}


void entity::item::update(int64_t)
{
	if (m_time_to_live == 0) {
		return;
	}

	this->m_time_to_live = m_time_to_live - 1;
	if (m_time_to_live == 0) {
		this->discard();
	}
}

ktf::vec3<double> entity::item::clip_velocity(const ktf::vec3<double>& init_velocity)
{
	ktf::vec3<double> velocity = init_velocity;
	aabb hitbox = m_hitbox;
	aabb const& movement_space = hitbox.expand_directional(velocity.x, velocity.y, velocity.z);
	std::vector<aabb> hitboxes;
	hitboxes::get_hitboxes(m_world, hitboxes, movement_space);
	// skip unnecessary calculations
	if (hitboxes.empty()) {
		return init_velocity;
	}

	for (const aabb& bounding_box : hitboxes) {
		velocity.y = bounding_box.clip_y_collision(hitbox, velocity.y);
	}
	hitbox.move(ktf::vec3<double>(0.0, velocity.y, 0.0));

	for (const aabb& bounding_box : hitboxes) {
		velocity.x = bounding_box.clip_x_collision(hitbox, velocity.x);
	}
	hitbox.move(ktf::vec3<double>(velocity.x, 0.0, 0.0));

	for (const aabb& bounding_box : hitboxes) {
		velocity.z = bounding_box.clip_z_collision(hitbox, velocity.z);
	}
	hitbox.move(ktf::vec3<double>(0.0, 0.0, velocity.z));

	return velocity;
}
