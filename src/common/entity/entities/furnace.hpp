#pragma once

#include "tile.hpp"


namespace entity {

class furnace : public tile {

	private:
		uint32_t m_fuel;
		uint32_t m_progress;


	public:
		furnace(::entity::core_system* core_system, game::world* world, ::entity::id id, ktf::vec3<coord_t> const& pos);
		~furnace();


	public:
		void update(int64_t ns) override;

};

} // namespace entity
