#pragma once

#include "../entity.hpp"


namespace entity {

class living_entity : public ::entity::entity {

	private:
		ktf::vec3<double> m_eye_offset;
		ktf::vec3<double> m_eye_rotation;
		double m_health;
		size_t m_damage_cooldown;


	public:
		living_entity(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id);
		~living_entity();


	public:
		ktf::vec3<double> get_eye_position() const override;
		ktf::vec3<double> get_eye_rotation() const override;
		void set_eye_offset(ktf::vec3<double> const& offset);
		void set_eye_rotation(ktf::vec3<double> const& rotation);
		float get_reach() const;
		double get_health() const;
};

} // namespace entity
