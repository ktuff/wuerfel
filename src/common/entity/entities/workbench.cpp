#include "workbench.hpp"


entity::workbench::workbench(::entity::core_system* core_system, game::world* world, ::entity::id id, const ktf::vec3<coord_t>& pos)
: tile(core_system, world, ::entity::type::e_workbench, id, pos)
{
}

entity::workbench::~workbench()
{
}


void entity::workbench::update(int64_t ns)
{
}
