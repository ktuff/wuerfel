#pragma once

#include "tile.hpp"


namespace entity {

class chest : public tile {

	private:
		::entity::id m_connected = ::entity::null_id;


	public:
		chest(::entity::core_system* core_system, game::world* world, ::entity::id id, ktf::vec3<coord_t> const& pos);
		~chest();


	public:
		void connect();
		void disconnect();

		void update(int64_t ns) override;

};

} // namespace entity
