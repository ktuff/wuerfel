#pragma once

#include <stdint.h>


namespace entity {

typedef uint64_t id;
typedef uint64_t system_module_id;

static id const null_id = id(-1);


class core_system;


enum type {
	e_player,
	e_item,
	e_pig,
	e_cow,
	e_chicken,
	e_chest,
	e_furnace,
	e_workbench,
	e_max_num_entity_types
};


class entity;

} // namespace entity
