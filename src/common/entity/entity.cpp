#include <ktf/math/radians.hpp>

#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/item/inventory.hpp>

#include "entity.hpp"


entity::entity::entity(::entity::core_system* core_system, game::world* world, ::entity::type type, ::entity::id id)
: m_type(type)
, m_id(id)
, m_core_system(core_system)
, m_world(world)
, m_discarded(false)
, m_collide(true)
, m_onground(false)
{
}

entity::entity::~entity()
{
}


bool entity::entity::is_discarded() const
{
	return m_discarded;
}

void entity::entity::discard()
{
	this->m_discarded = true;
}

ktf::vec3<double> entity::entity::get_position() const
{
	return m_position;
}

ktf::vec3<double> entity::entity::get_rotation() const
{
	return m_rotation;
}

ktf::vec3<double> entity::entity::get_direction() const
{
	return m_direction;
}

ktf::vec3<double> entity::entity::get_velocity()
{
	return m_velocity;
}

ktf::vec3<double> entity::entity::get_acceleration()
{
	return m_acceleration;
}

ktf::vec3<double> entity::entity::get_eye_position() const
{
	return m_position;
}

ktf::vec3<double> entity::entity::get_eye_rotation() const
{
	return m_rotation;
}

aabb entity::entity::get_hitbox() const
{
	return m_hitbox;
}

bool entity::entity::onground() const
{
	return m_onground;
}

void entity::entity::set_position(const ktf::vec3<double>& pos)
{
	this->m_position = pos;
	::entity::event_position event(nullptr, m_id, pos);
	this->m_core_system->notify(&event);

	switch (m_type) {
	case e_player:
		this->m_hitbox.set_bounds(pos + ktf::vec3<double>(-0.4, 0.0, -0.4), pos + ktf::vec3<double>(0.4, 1.8, 0.4));
		break;
	case e_item:
		this->m_hitbox.set_bounds(pos + ktf::vec3<double>(-0.15, -0.15, -0.15), pos + ktf::vec3<double>(0.15, 0.15, 0.15));
		break;
	default:
		break;
	}
}

void entity::entity::set_rotation(const ktf::vec3<double>& rot)
{
	double pitch = rot.x, yaw = rot.y, roll = rot.z;
	if (pitch >= 90.0) {
		pitch = 89.9;
	} else if (pitch <= -90.0) {
		pitch = -89.9;
	}
	while (yaw >= 180.0) {
		yaw -= 360.0;
	}
	while (yaw < -180.0) {
		yaw += 360.0;
	}
	while (roll >= 180.0) {
		roll -= 360.0;
	}
	while (roll < -180.0) {
		roll += 360.0;
	}

	this->m_rotation = ktf::vec3<double>(pitch, yaw, roll);

	double x = std::cos(ktf::radians(pitch)) * std::cos(ktf::radians(yaw));
	double y = std::sin(ktf::radians(pitch));
	double z = std::cos(ktf::radians(pitch)) * std::sin(ktf::radians(yaw));
	this->m_direction = ktf::vec3<double>::normalize({x, y, z});

	::entity::event_position event(nullptr, m_id, m_rotation);
	this->m_core_system->notify(&event);
}

void entity::entity::set_velocity(const ktf::vec3<double>& vel)
{
	this->m_velocity = vel;
}

void entity::entity::set_acceleration(const ktf::vec3<double>& acc)
{
	this->m_acceleration = acc;
}

void entity::entity::update(int64_t)
{
}

void entity::entity::move(double dt)
{
	ktf::vec3<double> velocity = get_velocity();
	ktf::vec3<double> orig_velocity = velocity * dt;
	ktf::vec3<double> clipped_velocity = clip_velocity(orig_velocity);
	ktf::vec3<double> position = get_position();
	this->set_position(position + clipped_velocity);

	// deal damage on collision
	if (double collision_factor = (orig_velocity - clipped_velocity).length()) {
		if (collision_factor > 0.1) {
// 			this->hit(collision_factor, ktf::vec3<double>(0.0, 1.0, 0.0));
		}
	}

	// stop movement in direction if collision occured
	if (orig_velocity.x != clipped_velocity.x) {
		velocity.x = 0.0;
	}
	if (orig_velocity.y != clipped_velocity.y) {
		velocity.y = 0.0;
	}
	if (orig_velocity.z != clipped_velocity.z) {
		velocity.z = 0.0;
	}

	this->update_velocity(dt, velocity);
}

ktf::vec3<double> entity::entity::clip_velocity(const ktf::vec3<double>& init_velocity)
{
	return init_velocity;
}

void entity::entity::update_velocity(double dt, ktf::vec3<double> const& velocity)
{
	ktf::vec3<double> acceleration = get_acceleration();
	ktf::vec3<double> new_velocity = velocity + acceleration * dt;

	this->set_velocity(new_velocity);
}
