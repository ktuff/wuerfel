#pragma once

#include <string>
#include <vector>

#include <openssl/rsa.h>


class rsa_key {

	public:
		static rsa_key load_from_file(std::string const& private_key_path, std::string const& public_key_path);
		static rsa_key load_from_buffer(std::vector<char> const& private_key, std::vector<char> const& public_key);
		static rsa_key generate(int bits);
		static rsa_key generate(int bits, std::string const& private_key_path, std::string const& public_key_path);

	private:
		static bool generate_key(int bits, BIO* bp_public, BIO* bp_private);

	private:
		RSA* m_private_key;
		RSA* m_public_key;


	private:
		rsa_key(RSA* private_key, RSA* public_key);
	public:
		rsa_key();
		rsa_key(rsa_key const& key_pair) = delete;
		rsa_key(rsa_key&& key_pair);
		~rsa_key();


	public:
		rsa_key& operator=(rsa_key const& key_pair) = delete;
		rsa_key& operator=(rsa_key&& key_pair);
		bool operator()() const;

		std::vector<char> get_public_key() const;
		bool decrypt(std::vector<unsigned char> const& cipher, std::vector<unsigned char>& message) const;
		bool encrypt(std::vector<unsigned char> const& message, std::vector<unsigned char>& cipher) const;

};
