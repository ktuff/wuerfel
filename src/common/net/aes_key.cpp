#include <fstream>

#include <openssl/rand.h>

#include "aes_key.hpp"


aes_key aes_key::read_from_buffer(std::vector<uint8_t> const& key, std::vector<uint8_t> const& iv)
{
	EVP_CIPHER const* cipher = ::EVP_aes_256_cbc();
	OPENSSL_assert(::EVP_CIPHER_key_length(cipher) == (int)key.size());
	OPENSSL_assert(::EVP_CIPHER_iv_length(cipher) == (int)iv.size());
	return aes_key(cipher, key, iv);
}

aes_key aes_key::read_from_file(std::string const& path)
{
	std::ifstream file;
	file.open(path, std::ios_base::binary);
	if (!file.is_open()) {
		return aes_key();
	}
	EVP_CIPHER const* cipher = ::EVP_aes_256_cbc();
	std::vector<uint8_t> key(::EVP_CIPHER_key_length(cipher));
	std::vector<uint8_t> iv(::EVP_CIPHER_iv_length(cipher));
	file.read((char*)key.data(), key.size());
	file.read((char*)iv.data(), iv.size());
	return aes_key(cipher, key, iv);
}

void aes_key::write_to_buffer(aes_key const& aes, std::vector<uint8_t>& key, std::vector<uint8_t>& iv)
{
	if (!aes.m_cipher) {
		return;
	}

	key.resize(aes.m_key.size());
	iv.resize(aes.m_iv.size());

	std::copy(aes.m_key.begin(), aes.m_key.end(), key.begin());
	std::copy(aes.m_iv.begin(), aes.m_iv.end(), iv.begin());
}

void aes_key::write_to_file(aes_key const& aes, const std::string& path)
{
	if (!aes.m_cipher) {
		return;
	}

	std::ofstream file;
	file.open(path, std::ios_base::binary);
	if (!file.is_open()) {
		return;
	}
	file.write((char*)aes.m_key.data(), aes.m_key.size());
	file.write((char*)aes.m_iv.data(), aes.m_iv.size());
	file.close();
}

aes_key aes_key::generate()
{
	EVP_CIPHER const* cipher = ::EVP_aes_256_cbc();
	std::vector<uint8_t> key(::EVP_CIPHER_key_length(cipher));
	std::vector<uint8_t> iv(::EVP_CIPHER_iv_length(cipher));

	int rc = 0;
	unsigned char salt[8];
	int key_length = ::EVP_CIPHER_key_length(cipher);
	std::vector<uint8_t> data(key_length * 32);
	rc = ::RAND_bytes(salt, sizeof(salt));
	rc = ::RAND_bytes(data.data(), data.size());
	int nrounds = 16;
	rc = ::EVP_BytesToKey(cipher, ::EVP_sha3_512(), salt, data.data(), data.size(), nrounds, key.data(), iv.data());
	if (rc != key_length) {
		::printf("Key size invalid\n");
		return aes_key();
	}

	return read_from_buffer(key, iv);
}


aes_key::aes_key()
: m_cipher(NULL)
, m_key(0)
, m_iv(0)
{
}

aes_key::aes_key(EVP_CIPHER const* cipher, const std::vector<uint8_t>& key, const std::vector<uint8_t>& iv)
: m_cipher(cipher)
, m_key(key)
, m_iv(iv)
{
}

aes_key::aes_key(const aes_key& key)
: m_cipher(key.m_cipher)
, m_key(key.m_key)
, m_iv(key.m_iv)
{
}

aes_key::aes_key(aes_key && key)
: m_cipher(key.m_cipher)
, m_key(key.m_key)
, m_iv(key.m_iv)
{
}

aes_key::~aes_key()
{
}


bool aes_key::decrypt(const std::vector<unsigned char>& cipher, std::vector<unsigned char>& message)
{
	if (!m_cipher) {
		return false;
	}

	int block_size = ::EVP_CIPHER_block_size(m_cipher);

	int rc = 0;
	int message_length;
	message.resize(cipher.size() + block_size);
	EVP_CIPHER_CTX* ctx_dec = ::EVP_CIPHER_CTX_new();
	if (ctx_dec == NULL) {
		return false;
	}
	EVP_CIPHER_CTX_init(ctx_dec);
	rc = ::EVP_DecryptInit_ex(ctx_dec, m_cipher, NULL, m_key.data(), m_iv.data());
	if (rc != 1) {
		goto free_ctx;
	}
	rc = ::EVP_DecryptUpdate(ctx_dec, message.data(), &message_length, cipher.data(), cipher.size());
	if (rc != 1) {
		goto free_ctx;
	}
	rc = ::EVP_DecryptFinal_ex(ctx_dec, message.data() + message_length, &message_length);
	if (rc != 1) {
		goto free_ctx;
	}

free_ctx:
	::EVP_CIPHER_CTX_free(ctx_dec);

	return true;
}

bool aes_key::encrypt(const std::vector<unsigned char>& message, std::vector<unsigned char>& cipher)
{
	if (!m_cipher) {
		return false;
	}

	int block_size = ::EVP_CIPHER_block_size(m_cipher);

	int rc = 0;
	int cipher_length;
	cipher.resize(message.size() + block_size);
	EVP_CIPHER_CTX* ctx_enc = ::EVP_CIPHER_CTX_new();
	if (ctx_enc == NULL) {
		return false;
	}
	EVP_CIPHER_CTX_init(ctx_enc);
	rc = ::EVP_EncryptInit_ex(ctx_enc, m_cipher, NULL, m_key.data(), m_iv.data());
	if (rc != 1) {
		goto free_ctx;
	}
	rc = ::EVP_EncryptUpdate(ctx_enc, cipher.data(), &cipher_length, message.data(), message.size());
	if (rc != 1) {
		goto free_ctx;
	}
	rc = ::EVP_EncryptFinal_ex(ctx_enc, cipher.data() + cipher_length, &cipher_length);
	if (rc != 1) {
		goto free_ctx;
	}

free_ctx:
	::EVP_CIPHER_CTX_free(ctx_enc);

	return true;
}
