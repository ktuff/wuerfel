#pragma once

#include <string>

#include <nlohmann/json.hpp>
#include <ktf/math/vec3.hpp>
#include <ktf/net/packet.hpp>

#include <common/entity/types.hpp>
#include <common/world/chunk.hpp>


namespace game {
	class level;
} // namespace game


namespace packets {

	typedef nlohmann::json packet_t;

	/*
	 * COMPRESS
	 */
	packet_t s_chunks_loaded();
	packet_t s_chunk_add(const std::shared_ptr<game::chunk>& chunk, bool& success);
	packet_t s_chunk_remove(coord_t x, coord_t z);

	packet_t s_set_block(coord_t x, coord_t y, coord_t z, block_t block, side_t side);
	packet_t s_set_rotation(coord_t x, coord_t y, coord_t z, side_t value);
	packet_t s_set_state(coord_t x, coord_t y, coord_t z, state_t value);
	packet_t s_set_power(coord_t x, coord_t y, coord_t z, power_t value);
	packet_t s_set_light(coord_t x, coord_t y, coord_t z, light_t value);

	packet_t s_player_pos(::entity::id id, const ktf::vec3<double>& pos);
	packet_t s_player_rot(::entity::id id, const ktf::vec3<double>& rot);

	/*
	 * DECOMPRESS
	 */
	void r_chunk_add(const packet_t& packet, game::world* world, std::shared_ptr<game::chunk>& chunk);
	void r_chunk_remove(const packet_t& packet, coord_t& x, coord_t& z);

	void r_set_block(const packet_t& packet, coord_t& x, coord_t& y, coord_t& z, block_t& block, side_t& side);
	void r_set_rotation(const packet_t& packet, coord_t& x, coord_t& y, coord_t& z, side_t& value);
	void r_set_state(const packet_t& packet, coord_t& x, coord_t& y, coord_t& z, state_t& value);
	void r_set_power(const packet_t& packet, coord_t& x, coord_t& y, coord_t& z, power_t& value);
	void r_set_light(const packet_t& packet, coord_t& x, coord_t& y, coord_t& z, light_t& value);

	void r_player_pos(const packet_t& packet, ::entity::id& id, ktf::vec3<double>& pos);
	void r_player_rot(const packet_t& packet, ::entity::id& id, ktf::vec3<double>& rot);

} // namespace packets
