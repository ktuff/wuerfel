#pragma once

#include <stdint.h>


enum packet_emitter_type : uint32_t {
	e_op_none,
	e_op_auth,
	e_op_general,
	e_op_entity
};


enum packet_type : uint64_t {

	packet_auth_login,
	packet_auth_challenge,
	packet_auth_response,
	packet_auth_result,

	packet_chunks_loaded,
	packet_chunk_add,
	packet_chunk_remove,
	packet_entity_batch,

	packet_block = 20,
	packet_rotation = 21,
	packet_state = 22,
	packet_power = 23,
	packet_light = 24,

	packet_player_pos = 40,
	packet_player_rot = 41,

	packet_batch = 128

};
