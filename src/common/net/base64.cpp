#include <cmath>

#include <openssl/evp.h>

#include "base64.hpp"


size_t base64::encode_bound(size_t in)
{
	return std::ceil(in / 48.0) * 65 + 1;
}

size_t base64::decode_bound(size_t in)
{
	return std::ceil(in / 4.0) * 3 + 1;
}

size_t base64::encode(unsigned char const* data, size_t in_length, unsigned char* encoded)
{
	int bytes = 0;
	size_t length = 0;
	EVP_ENCODE_CTX* ctx = ::EVP_ENCODE_CTX_new();
	::EVP_EncodeInit(ctx);
	int rc = ::EVP_EncodeUpdate(ctx, encoded, &bytes, data, in_length);
	if (rc == -1) {
		length = 0;
		goto free_ctx;
	}
	length = bytes;
	::EVP_EncodeFinal(ctx, encoded + bytes, &bytes);
	length += bytes;

free_ctx:
	::EVP_ENCODE_CTX_free(ctx);
	return length;
}

size_t base64::decode(unsigned char const* data, size_t in_length, unsigned char* decoded)
{
	int bytes = 0;
	size_t length = 0;
	EVP_ENCODE_CTX* ctx = ::EVP_ENCODE_CTX_new();
	::EVP_DecodeInit(ctx);
	int rc = ::EVP_DecodeUpdate(ctx, decoded, &bytes, data, in_length);
	if (rc == -1) {
		length = 0;
		goto free_ctx;
	}
	length = bytes;
	::EVP_DecodeFinal(ctx, decoded + bytes, &bytes);
	length += bytes;

free_ctx:
	::EVP_ENCODE_CTX_free(ctx);
	return length;
}
