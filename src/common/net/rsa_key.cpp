#include <openssl/pem.h>
#include <openssl/err.h>

#include "rsa_key.hpp"


rsa_key rsa_key::load_from_file(const std::string& private_key_path, const std::string& public_key_path)
{
	FILE * fp;
	RSA* p_rsa;
	RSA* pb_rsa;

	fp = ::fopen(private_key_path.c_str(), "rb");
	if (!fp) {
		return rsa_key();
	}
	p_rsa = ::PEM_read_RSAPrivateKey(fp, NULL, NULL, NULL);
	if (!p_rsa) {
		return rsa_key();
	}
	::fclose(fp);

	fp = ::fopen(public_key_path.c_str(), "rb");
	if (!fp) {
		goto error;
	}
	pb_rsa = ::PEM_read_RSAPublicKey(fp, NULL, NULL, NULL);
	if (!pb_rsa) {
		goto error;
	}
	::fclose(fp);

	return rsa_key(p_rsa, pb_rsa);

error:
	::RSA_free(p_rsa);
	return rsa_key();
}

rsa_key rsa_key::load_from_buffer(const std::vector<char>& private_key, const std::vector<char>& public_key)
{
	RSA* p_rsa = NULL;
	if (!private_key.empty()) {
		BIO* pkeybio = ::BIO_new_mem_buf((void*) private_key.data(), private_key.size());
		if (pkeybio != NULL) {
			p_rsa = ::PEM_read_bio_RSAPrivateKey(pkeybio, &p_rsa, NULL, NULL);
			if (p_rsa == NULL) {
				char buffer[128];
				::ERR_error_string(::ERR_get_error(), buffer);
				::printf("Error reading private key:%s\n", buffer);
			}
			::BIO_free(pkeybio);
		}
	}

	RSA* pb_rsa = NULL;
	if (!public_key.empty()) {
		BIO* pbkeybio = ::BIO_new_mem_buf((void*) public_key.data(), public_key.size());
		if (pbkeybio != NULL) {
			pb_rsa = ::PEM_read_bio_RSAPublicKey(pbkeybio, &pb_rsa, NULL, NULL);
			if (pb_rsa == NULL) {
				char buffer[128];
				::ERR_error_string(::ERR_get_error(), buffer);
				::printf("Error reading public key:%s\n", buffer);
			}
			::BIO_free(pbkeybio);
		}
	}

	return rsa_key(p_rsa, pb_rsa);
}

// should at least 2048 bits
rsa_key rsa_key::generate(int bits)
{
	int rc = 0;
	bool valid = false;
	size_t private_length;
	size_t public_length;
	std::vector<char> private_key;
	std::vector<char> public_key;
	BIO* bp_private = ::BIO_new(::BIO_s_mem());
	BIO* bp_public = ::BIO_new(::BIO_s_mem());
	if (bp_private == NULL) {
		goto error0;
	}
	if (bp_public == NULL) {
		goto error1;
	}

	valid = rsa_key::generate_key(bits, bp_public, bp_private);
	if (!valid) {
		goto error2;
	}

	private_length = BIO_pending(bp_private);
	public_length = BIO_pending(bp_public);
	private_key.resize(private_length + 1);
	public_key.resize(public_length + 1);

	rc = ::BIO_read(bp_private, private_key.data(), private_length);
	::ERR_print_errors(bp_private);
	if (rc != 1) {
		goto error1;
	}

	rc = ::BIO_read(bp_public, public_key.data(), public_length);
	::ERR_print_errors(bp_public);
	if (rc != 1) {
		goto error2;
	}

	// add NULL termination
	private_key[private_length] = '\0';
	public_key[public_length] = '\0';

	::BIO_free_all(bp_private);
	::BIO_free_all(bp_public);

	return load_from_buffer(private_key, public_key);

error2:
	::BIO_free_all(bp_public);
error1:
	::BIO_free_all(bp_private);
error0:
	return rsa_key();
}

rsa_key rsa_key::generate(int bits, std::string const& private_key_path, std::string const& public_key_path)
{
	BIO* bp_private = NULL;
	BIO* bp_public = NULL;
	bool valid = false;

	bp_private = ::BIO_new_file(private_key_path.c_str(), "w+");
	if (bp_private == NULL) {
		goto error0;
	}

	bp_public = ::BIO_new_file(public_key_path.c_str(), "w+");
	if (bp_public == NULL) {
		goto error1;
	}

	valid = rsa_key::generate_key(bits, bp_public, bp_private);
	if (!valid) {
		goto error2;
	}

	::BIO_free_all(bp_private);
	::BIO_free_all(bp_public);

	return load_from_file(private_key_path, public_key_path);

error2:
	::BIO_free_all(bp_public);
error1:
	::BIO_free_all(bp_private);
error0:
	return rsa_key();
}

bool rsa_key::generate_key(int bits, BIO* bp_public, BIO* bp_private)
{
	int rc = 0;
	bool valid = false;

	RSA* rsa = NULL;
	unsigned long e = RSA_F4;
	BIGNUM* bne = ::BN_new();
	rc = ::BN_set_word(bne, e);
	if (rc != 1) {
		goto free1;
	}

	// 1. generate rsa key
	rsa = ::RSA_new();
	rc = ::RSA_generate_key_ex(rsa, bits, bne, NULL);
	if (rc != 1) {
		goto free2;
	}

	// 2. save private key
	rc = ::PEM_write_bio_RSAPrivateKey(bp_private, rsa, NULL, NULL, 0, NULL, NULL);
	if (rc != 1) {
		goto free2;
	}

	// 3. save public key
	rc = ::PEM_write_bio_RSAPublicKey(bp_public, rsa);
	if (rc != 1) {
		goto free2;
	}

	valid = true;

free2:
	::RSA_free(rsa);
free1:
	::BN_free(bne);

	return valid;
}


rsa_key::rsa_key()
: m_private_key(NULL)
, m_public_key(NULL)
{
}

rsa_key::rsa_key(RSA* private_key, RSA* public_key)
: m_private_key(private_key)
, m_public_key(public_key)
{
}

rsa_key::rsa_key(rsa_key && key_pair)
: m_private_key(key_pair.m_private_key)
, m_public_key(key_pair.m_public_key)
{
	key_pair.m_private_key = NULL;
	key_pair.m_public_key = NULL;
}

rsa_key::~rsa_key()
{
	if (m_private_key) {
		::RSA_free(m_private_key);
	}
	if (m_public_key) {
		::RSA_free(m_public_key);
	}
}

rsa_key & rsa_key::operator=(rsa_key && key_pair)
{
	if (m_private_key) {
		::RSA_free(m_private_key);
	}
	if (m_public_key) {
		::RSA_free(m_public_key);
	}

	this->m_private_key = key_pair.m_private_key;
	this->m_public_key = key_pair.m_public_key;
	key_pair.m_private_key = NULL;
	key_pair.m_public_key = NULL;

	return *this;
}

bool rsa_key::operator()() const
{
	return m_private_key != NULL || m_public_key != NULL;
}

std::vector<char> rsa_key::get_public_key() const
{
	int rc = 0;
	BIO* bp_public = ::BIO_new(::BIO_s_mem());
	size_t public_length = 0;
	std::vector<char> public_key;
	rc = ::PEM_write_bio_RSAPublicKey(bp_public, m_public_key);
	if (rc != 1) {
		goto error;
	}
	public_length = BIO_pending(bp_public);
	public_key.resize(public_length + 1);
	rc = ::BIO_read(bp_public, public_key.data(), public_length);
	::ERR_print_errors(bp_public);
	if (rc <= 0) {
		public_key.resize(0);
		goto error;
	}
	// add NULL termination
	public_key[public_length] = '\0';

error:
	::BIO_free_all(bp_public);

	return public_key;
}

bool rsa_key::decrypt(const std::vector<unsigned char>& cipher, std::vector<unsigned char>& message) const
{
	if (m_private_key == NULL) {
		return false;
	}

	int block_size = ::RSA_size(m_private_key);
	if ((int)message.size() > block_size - 41) {
		return false;
	}

	message.resize(block_size);
	int length = ::RSA_private_decrypt(cipher.size(), cipher.data(), message.data(), m_private_key, RSA_PKCS1_OAEP_PADDING);
	if (length == -1) {
		return false;
	} else {
		message.resize(length);
		return true;
	}
}

bool rsa_key::encrypt(const std::vector<unsigned char>& message, std::vector<unsigned char>& cipher) const
{
	if (m_public_key == NULL) {
		return false;
	}

	int block_size = ::RSA_size(m_public_key);
	if ((int)message.size() > block_size - 41) {
		return false;
	}

	cipher.resize(block_size);
	int length = ::RSA_public_encrypt(message.size(), message.data(), cipher.data(), m_public_key, RSA_PKCS1_OAEP_PADDING);
	if (length == -1) {
		return false;
	} else {
		cipher.resize(length);
		return true;
	}
}
