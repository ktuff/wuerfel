#pragma once

#include <string>
#include <vector>

#include <openssl/aes.h>
#include <openssl/evp.h>


class aes_key {

	public:
		static aes_key read_from_buffer(std::vector<uint8_t> const& key, std::vector<uint8_t> const& iv);
		static aes_key read_from_file(std::string const& path);
		static void write_to_buffer(aes_key const& aes, std::vector<uint8_t>& key, std::vector<uint8_t>& iv);
		static void write_to_file(aes_key const& aes, std::string const& path);
		static aes_key generate();


	private:
		EVP_CIPHER const* const m_cipher;
		std::vector<uint8_t> m_key;
		std::vector<uint8_t> m_iv;


	private:
		aes_key(EVP_CIPHER const* cipher, std::vector<uint8_t> const& key, std::vector<uint8_t> const& iv);
	public:
		aes_key();
		aes_key(aes_key const& key);
		aes_key(aes_key && key);
		~aes_key();


	public:
		bool decrypt(std::vector<unsigned char> const& cipher, std::vector<unsigned char>& message);
		bool encrypt(std::vector<unsigned char> const& message, std::vector<unsigned char>& cipher);

};
