#include <assert.h>
#include <string.h>

#include <nlohmann/json.hpp>
#include <ktf/util/pointer.h>

#include <common/entity/entities/tile.hpp>
#include <common/level.hpp>
#include <common/util/chunk_compression.hpp>

#include "base64.hpp"
#include "packet_handler.hpp"
#include "types.hpp"


/*
 * COMPRESS
 */
packets::packet_t packets::s_chunks_loaded()
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_chunks_loaded;
	return req;
}

packets::packet_t packets::s_chunk_add(const std::shared_ptr<game::chunk>& chunk, bool& success)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_chunk_add;
	auto const& data = ::wfl_compress_chunk(chunk);
	if (data.first) {
		success = true;
		size_t bound = base64::encode_bound(data.second);
		std::vector<unsigned char> encoded(bound);
		size_t size = base64::encode((unsigned char const*)data.first.get(), data.second, encoded.data());
		req["chunk"] = std::string((char*)encoded.data(), size);
	} else {
		success = false;
	}
	return req;
}

packets::packet_t packets::s_chunk_remove(coord_t x, coord_t z)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_chunk_remove;
	req["x"] = x;
	req["z"] = z;
	return req;
}

packets::packet_t packets::s_set_block(coord_t x, coord_t y, coord_t z, block_t block, side_t side)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_block;
	req["x"] = x;
	req["y"] = y;
	req["z"] = z;
	req["block"] = block;
	req["orientation"] = side;
	return req;
}

packets::packet_t packets::s_set_rotation(coord_t x, coord_t y, coord_t z, side_t value)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_rotation;
	req["x"] = x;
	req["y"] = y;
	req["z"] = z;
	req["orientation"] = value;
	return req;
}

packets::packet_t packets::s_set_state(coord_t x, coord_t y, coord_t z, state_t value)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_state;
	req["x"] = x;
	req["y"] = y;
	req["z"] = z;
	req["state"] = value;
	return req;
}

packets::packet_t packets::s_set_power(coord_t x, coord_t y, coord_t z, power_t value)
{
    nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_power;
	req["x"] = x;
	req["y"] = y;
	req["z"] = z;
	req["power"] = value;
	return req;
}

packets::packet_t packets::s_set_light(coord_t x, coord_t y, coord_t z, light_t value)
{
    nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_light;
	req["x"] = x;
	req["y"] = y;
	req["z"] = z;
	req["light"] = value.value;
	return req;
}

packets::packet_t packets::s_player_pos(::entity::id id, const ktf::vec3<double>& pos)
{   
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_player_pos;
	req["id"] = id;
	req["x"] = pos.x;
	req["y"] = pos.y;
	req["z"] = pos.z;
	return req;
}

packets::packet_t packets::s_player_rot(::entity::id id, const ktf::vec3<double>& rot)
{
	nlohmann::json req = nlohmann::json::object();
	req["packetType"] = packet_player_rot;
	req["id"] = id;
	req["x"] = rot.x;
	req["y"] = rot.y;
	req["z"] = rot.z;
	return req;
}


/*
 * DECOMPRESS
 */
void packets::r_chunk_add(const packets::packet_t& packet, game::world* world, std::shared_ptr<game::chunk>& chunk)
{
	std::string chunk_data;
	packet["chunk"].get_to(chunk_data);
	size_t bound = base64::decode_bound(chunk_data.size());
	std::vector<unsigned char> decoded(bound);
	size_t size = base64::decode((unsigned char const*)chunk_data.data(), chunk_data.size(), decoded.data());
	chunk = ::wfl_decompress_chunk(world, (char*)decoded.data(), size);
}

void packets::r_chunk_remove(const packets::packet_t& packet, coord_t& x, coord_t& z)
{
	packet["x"].get_to(x);
	packet["z"].get_to(z);
}

void packets::r_set_block(const packets::packet_t& packet, coord_t& x, coord_t& y, coord_t& z, block_t& block, side_t& side)
{
	packet["x"].get_to(x);
	packet["y"].get_to(y);
	packet["z"].get_to(z);
	packet["block"].get_to(block);
	packet["orientation"].get_to(side);
}

void packets::r_set_rotation(const packets::packet_t& packet, coord_t& x, coord_t& y, coord_t& z, side_t& value)
{
	packet["x"].get_to(x);
	packet["y"].get_to(y);
	packet["z"].get_to(z);
	packet["orientation"].get_to(value);
}

void packets::r_set_state(const packets::packet_t& packet, coord_t& x, coord_t& y, coord_t& z, state_t& value)
{
	packet["x"].get_to(x);
	packet["y"].get_to(y);
	packet["z"].get_to(z);
	packet["state"].get_to(value);
}

void packets::r_set_power(const packets::packet_t& packet, coord_t& x, coord_t& y, coord_t& z, power_t& value)
{
	packet["x"].get_to(x);
	packet["y"].get_to(y);
	packet["z"].get_to(z);
	packet["power"].get_to(value);
}

void packets::r_set_light(const packets::packet_t& packet, coord_t& x, coord_t& y, coord_t& z, light_t& value)
{
	packet["x"].get_to(x);
	packet["y"].get_to(y);
	packet["z"].get_to(z);
	packet["light"].get_to(value.value);
}

void packets::r_player_pos(const packets::packet_t& packet, ::entity::id& id, ktf::vec3<double>& pos)
{
	packet["id"].get_to(id);
	packet["x"].get_to(pos.x);
	packet["y"].get_to(pos.y);
	packet["z"].get_to(pos.z);
}

void packets::r_player_rot(const packets::packet_t& packet, ::entity::id& id, ktf::vec3<double>& rot)
{
	packet["id"].get_to(id);
	packet["x"].get_to(rot.x);
	packet["y"].get_to(rot.y);
	packet["z"].get_to(rot.z);
}
