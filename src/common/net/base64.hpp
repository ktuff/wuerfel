#pragma once

#include <cstddef>
#include <vector>


namespace base64 {

size_t encode_bound(size_t in);
size_t decode_bound(size_t in);
size_t encode(unsigned char const* data, size_t in_length, unsigned char* encoded);
size_t decode(unsigned char const* data, size_t in_length, unsigned char* decoded);

template<typename T>
bool encode(std::vector<T> const& data, std::vector<T>& encoded)
{
	int encoded_length = base64::encode_bound(data.size());
	encoded.resize(encoded_length);
	size_t length = base64::encode((unsigned char*)data.data(), data.size(), (unsigned char*)encoded.data());
	encoded.resize(length);

	return true;
}

template<typename T>
bool decode(std::vector<T> const& data, std::vector<T>& decoded)
{
	int decoded_length = base64::decode_bound(data.size());
	decoded.resize(decoded_length);
	size_t length = base64::decode((unsigned char*)data.data(), data.size(), (unsigned char*)decoded.data());
	decoded.resize(length);

	return true;
}

} // namespace base64
