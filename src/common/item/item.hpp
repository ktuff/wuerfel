#pragma once

#include <string>
#include <vector>

#include <common/types.hpp>
#include <common/world/voxel.hpp>


namespace item {

typedef uint8_t stack_size_t;

struct item_features {
	uint64_t block : 1;
	uint64_t tool : 1;
	uint64_t armor : 1;
	uint64_t food : 1;
	uint64_t fuel : 1;
	uint64_t cookable : 1;
	uint64_t useable : 1;
	uint64_t _ : 57;
};

enum item_type_id : uint32_t {
	e_none = 0,
	e_block_stone,
	e_block_dirt,
	e_block_grass,
	e_block_cobblestone,
	e_block_torch,
	e_block_log_oak,
	e_block_planks_oak,
	e_block_leaves_oak,
	e_block_sapling_oak,
	e_block_glass,
	e_block_sand,
	e_block_gravel,
	e_block_flower_red,
	e_block_flower_yellow,
	e_pumpkin,
	e_melon,
	e_bamboo,
	e_block_water,
	e_block_workbench,
	e_block_furnace,
	e_block_chest,
	e_block_circuit_wire,
	e_block_circuit_torch,
	e_block_circuit_repeater,
	e_block_circuit_comparator,
	e_stick,
	e_coal,
	e_potato,
	e_baked_potato,
	e_carrot,
	e_seeds_wheat,
	e_seeds_melon,
	e_seeds_pumpkin,
	e_wheat,
	e_bread,
	e_melon_slice,
	e_egg,
	e_sugar,
	e_pumpkin_pie,
	e_arrow,
	e_bow,
	e_helmet_leather,
	e_helmet_iron,
	e_helmet_gold,
	e_helmet_diamond,
	e_chestplate_leather,
	e_chestplate_iron,
	e_chestplate_gold,
	e_chestplate_diamond,
	e_leggings_leather,
	e_leggings_iron,
	e_leggings_gold,
	e_leggings_diamond,
	e_boots_leather,
	e_boots_iron,
	e_boots_gold,
	e_boots_diamond,
	e_pickaxe_wood,
	e_pickaxe_stone,
	e_pickaxe_iron,
	e_pickaxe_gold,
	e_pickaxe_diamond,
	e_shovel_wood,
	e_shovel_stone,
	e_shovel_iron,
	e_shovel_gold,
	e_shovel_diamond,
	e_sword_wood,
	e_sword_stone,
	e_sword_iron,
	e_sword_gold,
	e_sword_diamond,
	e_axe_wood,
	e_axe_stone,
	e_axe_iron,
	e_axe_gold,
	e_axe_diamond,
	e_hoe_wood,
	e_hoe_stone,
	e_hoe_iron,
	e_hoe_gold,
	e_hoe_diamond,
	e_item_count
};

enum tool_material {
	e_tool_no_material,
	e_tool_wood,
	e_tool_stone,
	e_tool_iron,
	e_tool_gold,
	e_tool_diamond
};

enum armor_material {
	e_armor_no_material,
	e_armor_leather,
	e_armor_iron,
	e_armor_gold,
	e_armor_diamond
};


struct item_type {
	item::item_type_id const id;
	char const* const name;
	stack_size_t const max_stack_size;
	item::item_features features;

	block_t block_type;
	float food_value;
	float burn_value;
	float cook_time;
	tool_material tool_mat;
	armor_material armor_mat;
	uint16_t max_durability;


	constexpr item_type(stack_size_t max_stack_size, item::item_type_id id, char const* name)
	: id(id)
	, name(name)
	, max_stack_size(max_stack_size)
	, features{}
	, block_type(0)
	, food_value(0.0f)
	, burn_value(0.0f)
	, cook_time(0.0f)
	, tool_mat(e_tool_no_material)
	, armor_mat(e_armor_no_material)
	, max_durability(0)
	{
	}

	constexpr item_type(item_type const& type)
	: id(type.id)
	, name(type.name)
	, max_stack_size(type.max_stack_size)
	, features(type.features)
	, block_type(type.block_type)
	, food_value(type.food_value)
	, burn_value(type.burn_value)
	, cook_time(type.cook_time)
	, tool_mat(type.tool_mat)
	, armor_mat(type.armor_mat)
	, max_durability(type.max_durability)
	{
	}

	constexpr item_type block(block_t block_type)
	{
		if (id > std::numeric_limits<block_t>::max() || id != block_type) {
			// TODO error
		}

		item_type type (*this);
		type.features.block = 1;
		type.block_type = block_type;
		return type;
	}
	constexpr item_type tool(tool_material material)
	{
		item_type type (*this);
		type.features.tool = 1;
		type.tool_mat = material;
		return type;
	}
	constexpr item_type armor(armor_material material)
	{
		item_type type(*this);
		type.features.armor = 1;
		type.armor_mat = material;
		return type;
	}
	constexpr item_type food(float value)
	{
		item_type type (*this);
		type.features.food = 1;
		type.burn_value = value;
		return type;
	}
	constexpr item_type fuel(float value)
	{
		item_type type (*this);
		type.features.fuel = 1;
		type.burn_value = value;
		return type;
	}
	constexpr item_type cookable(float duration)
	{
		item_type type (*this);
		type.features.cookable = 1;
		type.cook_time = duration;
		return type;
	}
	constexpr item_type useable()
	{
		item_type type (*this);
		type.features.useable = 1;
		return type;
	}
};


class item_stack {

	public:
		static const item_stack null;

	public:
		item_type_id type;
		stack_size_t count;

	public:
		item_stack();
		item_stack(item_type_id type, stack_size_t count);
		virtual ~item_stack();

	public:
		bool operator==(item_stack const& o) const;
		bool operator!=(item_stack const& o) const;
		void operator+=(stack_size_t num);
		void operator-=(stack_size_t num);
		stack_size_t move(item_stack & o);
};


class tool_item_stack : public item_stack {
	uint32_t durability;

	void use() {
		this->durability -= 1;
	}
};


constexpr item_type type_none = item_type(0, e_none, "item.none");

constexpr item_type type_stone = item_type(64, item::e_block_stone, "item.block.stone").block(block::e_block_stone);
constexpr item_type type_dirt = item_type(64, item::e_block_dirt, "item.block.dirt").block(block::e_block_dirt);
constexpr item_type type_grass = item_type(64, item::e_block_grass, "item.block.grass").block(block::e_block_grass);
constexpr item_type type_cobblestone = item_type(64, item::e_block_cobblestone, "item.block.cobblestone").block(block::e_block_cobblestone).cookable(40);
constexpr item_type type_torch = item_type(64, item::e_block_torch, "item.block.torch").block(block::e_block_torch);
constexpr item_type type_log_oak = item_type(64, item::e_block_log_oak, "item.block.log.oak").block(block::e_block_log_oak).fuel(4.0f);
constexpr item_type type_planks_oak = item_type(64, item::e_block_planks_oak, "item.block.plank.oak").block(block::e_block_planks_oak).fuel(1.0f);
constexpr item_type type_leaves_oak = item_type(64, item::e_block_leaves_oak, "item.block.leaves.oak").block(block::e_block_leaves_oak).fuel(0.5f);
constexpr item_type type_sapling_oak = item_type(64, item::e_block_sapling_oak, "item.block.sapling.oak").block(block::e_block_sapling_oak).fuel(0.5f);
constexpr item_type type_glass = item_type(64, item::e_block_glass, "item.block.glass").block(block::e_block_glass);
constexpr item_type type_sand = item_type(64, item::e_block_sand, "item.block.sand").block(block::e_block_sand);
constexpr item_type type_gravel = item_type(64, item::e_block_gravel, "item.block.gravel").block(block::e_block_gravel);
constexpr item_type type_flower_red = item_type(64, item::e_block_flower_red, "item.block.flower_red").block(block::e_block_flower_red);
constexpr item_type type_flower_yellow = item_type(64, item::e_block_flower_yellow, "item.block.flower_yellow").block(block::e_block_flower_yellow);
constexpr item_type type_pumpkin = item_type(64, item::e_pumpkin, "item.block.pumpkin").block(block::e_block_pumpkin);
constexpr item_type type_melon = item_type(64, item::e_melon, "item.block.melon").block(block::e_block_melon);
constexpr item_type type_bamboo = item_type(64, item::e_bamboo, "item.block.bamboo").block(block::e_block_bamboo);;
constexpr item_type type_water = item_type(64, item::e_block_water, "item.block.water").block(block::e_block_water_source);
constexpr item_type type_workbench = item_type(64, item::e_block_workbench, "item.block.tile.workbench").block(block::e_block_workbench).fuel(0.5f);
constexpr item_type type_furnace = item_type(64, item::e_block_furnace, "item.block.tile.furnace").block(block::e_block_furnace);
constexpr item_type type_chest = item_type(64, item::e_block_chest, "item.block.tile.chest").block(block::e_block_chest);
constexpr item_type type_circuit_wire = item_type(64, item::e_block_circuit_wire, "item.block.circuit.wire").block(block::e_block_circuit_wire);
constexpr item_type type_circuit_torch = item_type(64, item::e_block_circuit_torch, "item.block.circuit.torch").block(block::e_block_circuit_torch);
constexpr item_type type_circuit_repeater = item_type(64, item::e_block_circuit_repeater, "item.block.circuit.repeater").block(block::e_block_circuit_repeater);
constexpr item_type type_circuit_comparator = item_type(64, item::e_block_circuit_comparator, "item.block.circuit.comparator").block(block::e_block_circuit_comparator);

constexpr item_type type_stick = item_type(64, item::e_stick, "item.stick").fuel(20);
constexpr item_type type_coal = item_type(64, item::e_coal, "item.coal").fuel(8.0f);
constexpr item_type type_potato = item_type(64, item::e_potato, "item.potato").food(1.0f).cookable(40);
constexpr item_type type_baked_potato = item_type(64, item::e_baked_potato, "item.potato.baked").food(4.0f);
constexpr item_type type_carrot = item_type(64, item::e_carrot, "item.carrot").food(1.0f).cookable(40);
constexpr item_type type_seeds_wheat = item_type(64, item::e_seeds_wheat, "item.seeds.wheat");
constexpr item_type type_seeds_melon = item_type(64, item::e_seeds_melon, "item.seeds.melon");
constexpr item_type type_seeds_pumpkin = item_type(64, item::e_seeds_pumpkin, "item.seeds.pumpkin");
constexpr item_type type_wheat = item_type(64, item::e_wheat, "item.wheat");
constexpr item_type type_bread = item_type(64, item::e_bread, "item.bread");
constexpr item_type type_melon_slice = item_type(64, item::e_melon_slice, "item.melon_slice");
constexpr item_type type_egg = item_type(64, item::e_egg, "item.egg");
constexpr item_type type_sugar = item_type(64, item::e_sugar, "item.sugar");
constexpr item_type type_pumpkin_pie = item_type(64, item::e_pumpkin_pie, "item.pumpkin_pie");
constexpr item_type type_arrow = item_type(64, item::e_arrow, "item.arrow");

constexpr item_type type_bow = item_type(1, item::e_bow, "item.bow").fuel(1.0f);
constexpr item_type type_helmet_leather = item_type(1, item::e_helmet_leather, "item.helmet.leather").armor(e_armor_leather);
constexpr item_type type_helmet_iron = item_type(1, item::e_helmet_iron, "item.helmet.wood").armor(e_armor_iron);
constexpr item_type type_helmet_gold = item_type(1, item::e_helmet_gold, "item.helmet.wood").armor(e_armor_gold);
constexpr item_type type_helmet_diamond = item_type(1, item::e_helmet_diamond, "item.helmet.wood").armor(e_armor_diamond);
constexpr item_type type_chestplate_leather = item_type(1, item::e_chestplate_leather, "item.chestplate.wood").armor(e_armor_leather);
constexpr item_type type_chestplate_iron = item_type(1, item::e_chestplate_iron, "item.chestplate.wood").armor(e_armor_iron);
constexpr item_type type_chestplate_gold = item_type(1, item::e_chestplate_gold, "item.chestplate.wood").armor(e_armor_gold);
constexpr item_type type_chestplate_diamond = item_type(1, item::e_chestplate_diamond, "item.chestplate.wood").armor(e_armor_diamond);
constexpr item_type type_leggings_leather = item_type(1, item::e_leggings_leather, "item.leggings.wood").armor(e_armor_leather);
constexpr item_type type_leggings_iron = item_type(1, item::e_leggings_iron, "item.leggings.wood").armor(e_armor_iron);
constexpr item_type type_leggings_gold = item_type(1, item::e_leggings_gold, "item.leggings.wood").armor(e_armor_gold);
constexpr item_type type_leggings_diamond = item_type(1, item::e_leggings_diamond, "item.leggings.wood").armor(e_armor_diamond);
constexpr item_type type_boots_leather = item_type(1, item::e_boots_leather, "item.boots.wood").armor(e_armor_leather);
constexpr item_type type_boots_iron = item_type(1, item::e_boots_iron, "item.boots.wood").armor(e_armor_iron);
constexpr item_type type_boots_gold = item_type(1, item::e_boots_gold, "item.boots.wood").armor(e_armor_gold);
constexpr item_type type_boots_diamond = item_type(1, item::e_boots_diamond, "item.boots.wood").armor(e_armor_diamond);
constexpr item_type type_pickaxe_wood = item_type(1, item::e_pickaxe_wood, "item.pickaxe.wood").tool(e_tool_wood).fuel(2.0f);
constexpr item_type type_pickaxe_stone = item_type(1, item::e_pickaxe_stone, "item.pickaxe.stone").tool(e_tool_stone).fuel(2.0f);
constexpr item_type type_pickaxe_iron = item_type(1, item::e_pickaxe_iron, "item.pickaxe.iron").tool(e_tool_iron).fuel(2.0f);
constexpr item_type type_pickaxe_gold = item_type(1, item::e_pickaxe_gold, "item.pickaxe.gold").tool(e_tool_gold).fuel(2.0f);
constexpr item_type type_pickaxe_diamond = item_type(1, item::e_pickaxe_diamond, "item.pickaxe.diamond").tool(e_tool_diamond).fuel(2.0f);
constexpr item_type type_shovel_wood = item_type(1, item::e_shovel_wood, "item.shovel.wood").tool(e_tool_wood).fuel(2.0f);
constexpr item_type type_shovel_stone = item_type(1, item::e_shovel_stone, "item.shovel.stone").tool(e_tool_stone).fuel(2.0f);
constexpr item_type type_shovel_iron = item_type(1, item::e_shovel_iron, "item.shovel.iron").tool(e_tool_iron).fuel(2.0f);
constexpr item_type type_shovel_gold = item_type(1, item::e_shovel_gold, "item.shovel.gold").tool(e_tool_gold).fuel(2.0f);
constexpr item_type type_shovel_diamond = item_type(1, item::e_shovel_diamond, "item.shovel.diamond").tool(e_tool_diamond).fuel(2.0f);
constexpr item_type type_sword_wood = item_type(1, item::e_sword_wood, "item.sword.wood").tool(e_tool_wood).fuel(2.0f);
constexpr item_type type_sword_stone = item_type(1, item::e_sword_stone, "item.sword.stone").tool(e_tool_stone).fuel(2.0f);
constexpr item_type type_sword_iron = item_type(1, item::e_sword_iron, "item.sword.iron").tool(e_tool_iron).fuel(2.0f);
constexpr item_type type_sword_gold = item_type(1, item::e_sword_gold, "item.sword.gold").tool(e_tool_gold).fuel(2.0f);
constexpr item_type type_sword_diamond = item_type(1, item::e_sword_diamond, "item.sword.diamond").tool(e_tool_diamond).fuel(2.0f);
constexpr item_type type_axe_wood = item_type(1, item::e_axe_wood, "item.axe.wood").tool(e_tool_wood).fuel(2.0f);
constexpr item_type type_axe_stone = item_type(1, item::e_axe_stone, "item.axe.stone").tool(e_tool_stone).fuel(2.0f);
constexpr item_type type_axe_iron = item_type(1, item::e_axe_iron, "item.axe.iron").tool(e_tool_iron).fuel(2.0f);
constexpr item_type type_axe_gold = item_type(1, item::e_axe_gold, "item.axe.gold").tool(e_tool_gold).fuel(2.0f);
constexpr item_type type_axe_diamond = item_type(1, item::e_axe_diamond, "item.axe.diamond").tool(e_tool_diamond).fuel(2.0f);
constexpr item_type type_hoe_wood = item_type(1, item::e_hoe_wood, "item.hoe.wood").tool(e_tool_wood).fuel(2.0f);
constexpr item_type type_hoe_stone = item_type(1, item::e_hoe_stone, "item.hoe.stone").tool(e_tool_stone).fuel(2.0f);
constexpr item_type type_hoe_iron = item_type(1, item::e_hoe_iron, "item.hoe.iron").tool(e_tool_iron).fuel(2.0f);
constexpr item_type type_hoe_gold = item_type(1, item::e_hoe_gold, "item.hoe.gold").tool(e_tool_gold).fuel(2.0f);
constexpr item_type type_hoe_diamond = item_type(1, item::e_hoe_diamond, "item.hoe.diamond").tool(e_tool_diamond).fuel(2.0f);


constexpr item_type const * all_types [] {
	&type_none,
	&type_stone,
	&type_dirt,
	&type_grass,
	&type_cobblestone,
	&type_torch,
	&type_log_oak,
	&type_planks_oak,
	&type_leaves_oak,
	&type_sapling_oak,
	&type_glass,
	&type_sand,
	&type_gravel,
	&type_flower_red,
	&type_flower_yellow,
	&type_pumpkin,
	&type_melon,
	&type_bamboo,
	&type_water,
	&type_workbench,
	&type_furnace,
	&type_chest,
	&type_circuit_wire,
	&type_circuit_torch,
	&type_circuit_repeater,
	&type_circuit_comparator,
	&type_stick,
	&type_coal,
	&type_potato,
	&type_baked_potato,
	&type_carrot,
	&type_seeds_wheat,
	&type_seeds_melon,
	&type_seeds_pumpkin,
	&type_wheat,
	&type_bread,
	&type_melon_slice,
	&type_egg,
	&type_sugar,
	&type_pumpkin_pie,
	&type_arrow,
	&type_bow,
	&type_helmet_leather,
	&type_helmet_iron,
	&type_helmet_gold,
	&type_helmet_diamond,
	&type_chestplate_leather,
	&type_chestplate_iron,
	&type_chestplate_gold,
	&type_chestplate_diamond,
	&type_leggings_leather,
	&type_leggings_iron,
	&type_leggings_gold,
	&type_leggings_diamond,
	&type_boots_leather,
	&type_boots_iron,
	&type_boots_gold,
	&type_boots_diamond,
	&type_pickaxe_wood,
	&type_pickaxe_stone,
	&type_pickaxe_iron,
	&type_pickaxe_gold,
	&type_pickaxe_diamond,
	&type_shovel_wood,
	&type_shovel_stone,
	&type_shovel_iron,
	&type_shovel_gold,
	&type_shovel_diamond,
	&type_sword_wood,
	&type_sword_stone,
	&type_sword_iron,
	&type_sword_gold,
	&type_sword_diamond,
	&type_axe_wood,
	&type_axe_stone,
	&type_axe_iron,
	&type_axe_gold,
	&type_axe_diamond,
	&type_hoe_wood,
	&type_hoe_stone,
	&type_hoe_iron,
	&type_hoe_gold,
	&type_hoe_diamond
};

size_t const num_item_types = sizeof(all_types) / sizeof(item_type const* const);

item::item_type_id get_type(block_t block);
block_t get_block(item::item_type_id type);

} // namespace item
