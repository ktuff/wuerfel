#pragma once

#define INVENTORY_GROUP_ITEMS   0
#define INVENTORY_GROUP_ARMOR   1
#define INVENTORY_GROUP_TEMP	2

class inventory_listener {
    public:
        virtual void on_item_change(int group, int index) = 0;
		virtual void on_selection_change(int prev, int curr) = 0;
};
