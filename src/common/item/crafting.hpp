#pragma once

#include "container.hpp"

extern int32_t const prefix_crafting_items;
extern int32_t const prefix_crafting_result;

class recipe;

class crafting final : public container {

	private:
		int m_w, m_h;
		recipe const * m_recipe;
		int m_dim[4];


	public:
		crafting(int p_w, int p_h);
		~crafting();


	public:
		item::item_stack get_item(int32_t p_index) const override;
		bool set_item(int32_t p_index, item::item_stack const& p_item) override;

		void craft(int p_count);
		item::item_stack get_preview() const;
		item::item_stack get_result();

};
