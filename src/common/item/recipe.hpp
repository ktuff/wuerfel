#pragma once

#include <vector>
#include "item.hpp"

#define MAX_RECIPE_SIZE 3

class recipe {

	private:
		int const m_w, m_h;
		std::vector<item::item_stack> const m_items;
		item::item_stack const m_result;


	public:
		recipe(int p_w, int p_h, std::vector<item::item_stack> const& p_items, item::item_stack const& p_result);


	public:
		bool compatible(std::vector<item::item_stack> const& p_items, int p_x, int p_y, int p_w, int p_h, int row_size) const;
		item::item_stack apply(std::vector<item::item_stack> & p_items, int count, int p_x, int p_y, int p_w, int p_h, int row_size) const;
		item::item_stack get_result() const;

};


class recipes {

	private:
		static std::vector<recipe> s_recipes[MAX_RECIPE_SIZE * MAX_RECIPE_SIZE];


	public:
		static void load();
		static recipe const* get_recipe(std::vector<item::item_stack> const& p_items, int x0, int y0, int w, int h, int row_size);
};
