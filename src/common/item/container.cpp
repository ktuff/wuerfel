#include "container.hpp"

container::container(container_type p_type, int p_size)
: m_type(p_type)
, m_size(p_size)
{
    this->m_items.resize(p_size);
}

container::~container()
{
}


size_t container::size() const
{
	return m_size;
}

void container::clear()
{
	this->m_items.clear();
	this->m_items.resize(m_size);
}

item::item_stack container::get_item(int32_t p_index) const
{
	if (0 <= p_index && p_index < m_size) {
		return m_items[p_index];
	} else {
		return item::item_stack::null;
	}
}

bool container::set_item(int32_t p_index, const item::item_stack& p_item)
{
	if (0 <= p_index && p_index < m_size && placeable(p_index, p_item)) {
		this->m_items[p_index] = p_item;
		this->notify(p_index);
		return true;
	} else {
		return false;
	}
}

bool container::placeable(int32_t, item::item_stack const&)
{
	return true;
}

void container::add_listener(container_listener* p_listener)
{
	this->m_listeners.push_back(p_listener);
}

void container::remove_listener(container_listener* p_listener)
{
	for (size_t i=0; i<m_listeners.size(); i++) {
		if (m_listeners[i] == p_listener) {
			this->m_listeners.erase(m_listeners.begin() + i);
			break;
		}
	}
}

void container::notify(int p_index)
{
	for (auto* listener : m_listeners) {
		listener->on_item_change(this, p_index);
	}
}

bool container::swap_item(container* p_container1, int32_t p_index1, container* p_container2, int32_t p_index2)
{
	if (p_container1 && p_container2) {
		item::item_stack it1 = p_container1->get_item(p_index1);
		item::item_stack it2 = p_container2->get_item(p_index2);
		int8_t moved = it2.move(it1);
		if (moved) {
			p_container1->set_item(p_index1, it1);
			p_container2->set_item(p_index2, it2);
		} else {
			p_container1->set_item(p_index1, it2);
			p_container2->set_item(p_index2, it1);
		}
		return true;
	} else {
		return false;
	}
}
