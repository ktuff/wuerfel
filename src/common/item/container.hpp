#pragma once

#include "container_listener.hpp"
#include "item.hpp"

enum container_type {
	inventory_t,
	crafting_t,
	armor_t,
	furnace_t,
	chest_t,
	other_t
};


class container {

	public:
		container_type const m_type;
		int const m_size;

	protected:
		std::vector<item::item_stack> m_items;
		std::vector<container_listener*> m_listeners;


	public:
		container(container_type p_type, int p_size);
		virtual ~container();


	public:
		size_t size() const;
		void clear();
		virtual item::item_stack get_item(int32_t p_index) const;
		virtual bool set_item(int32_t p_index, item::item_stack const& p_item);
		virtual bool placeable(int32_t p_index, item::item_stack const& p_item);

		void add_listener(container_listener* p_listener);
		void remove_listener(container_listener* p_listener);

	protected:
		void notify(int p_index);

	public:
		static bool swap_item(container* p_container1, int32_t p_index1, container* p_container2, int32_t p_index2);
};
