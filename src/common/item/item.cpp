#include <assert.h>
#include <string.h>

#include <common/world/voxel.hpp>

#include "item.hpp"

const item::item_stack item::item_stack::null(e_none, 0);


item::item_stack::item_stack()
: type(item::e_none)
, count(0)
{
}

item::item_stack::item_stack(item::item_type_id type, item::stack_size_t count)
: type(type)
, count((count > item::all_types[type]->max_stack_size) ? item::all_types[type]->max_stack_size : count)
{
}

item::item_stack::~item_stack()
{
}


bool item::item_stack::operator==(const item::item_stack& o) const
{
	return type == o.type && count == o.count;
}

bool item::item_stack::operator!=(const item::item_stack& o) const
{
	return type != o.type || count != o.count;
}

void item::item_stack::operator+=(stack_size_t num)
{
	if (type == item::e_none) {
		return;
	}
	int new_num = int(count) + num;
	stack_size_t max_size = item::all_types[type]->max_stack_size;
	if (new_num >= max_size) {
		this->count = max_size;
	} else {
		this->count += num;
	}
}

void item::item_stack::operator-=(stack_size_t num)
{
	if (type == item::e_none) {
		return;
	}
	if (count - num <= 0) {
		*this = item_stack::null;
	} else {
		this->count -= num;
	}
}

item::stack_size_t item::item_stack::move(item_stack& p_item)
{
	if (p_item.type != type) {
		return 0;
	} else {
		stack_size_t remaining = item::all_types[type]->max_stack_size - count;
		stack_size_t moved = std::min(remaining, p_item.count);
		*this += moved;
		p_item -= moved;
		return moved;
	}
}

namespace item {

item::item_type_id block_types_map[e_block_count];

} // namespace item


const bool valid_item_ids = [] {
	::memset(item::block_types_map, 0, sizeof(item::block_types_map));
	for (size_t i=0; i<item::num_item_types; ++i) {
		item::item_type const* const itype = item::all_types[i];
		assert(itype->id == i);
		if (itype->features.block) {
			item::block_types_map[itype->block_type] = itype->id;
		}
	}
	return true;
}();


item::item_type_id item::get_type(block_t block)
{
	assert(0 <= block && block < e_block_count);
	return item::block_types_map[block];
}

block_t item::get_block(item::item_type_id type)
{
	assert(0 <= type && type < item::num_item_types);
	item::item_type const* itype = item::all_types[type];
	if (itype->features.block) {
		return itype->block_type;
	} else {
		return e_block_air;
	}
}


constexpr bool item_defs_valid()
{
	for (item::item_type_id i=item::e_none; i<item::e_item_count-1; i=item::item_type_id(i+1)) {
		if (item::all_types[i]->id != i) {
			return false;
		}
	}
	return true;
}
static_assert(item_defs_valid(), "Item definitions are not in the right order.");
