#include "crafting.hpp"
#include "recipe.hpp"

int32_t const prefix_crafting_items = 0x01000000;
int32_t const prefix_crafting_result = 0x02000000;


crafting::crafting(int p_w, int p_h)
: container(crafting_t, p_w*p_h+1)
, m_w(p_w)
, m_h(p_h)
, m_recipe(nullptr)
{
	this->m_dim[0] = m_dim[1] = m_dim[2] = m_dim[3];
}

crafting::~crafting()
{
}

// TODO mirrored patterns

item::item_stack crafting::get_item(int32_t p_index) const
{
	int32_t group = p_index & 0xFF00000;
	int32_t index = p_index & 0x00FFFFF;
	if (group == prefix_crafting_items) {
		if (index < 0 || index >= m_w * m_h) {
			return item::item_stack::null;
		} else {
			return m_items[index];
		}
	} else if (group == prefix_crafting_result) {
		return m_items[m_size-1];
	} else {
		return item::item_stack::null;
	}
}

bool crafting::set_item(int32_t p_index, item::item_stack const& p_item)
{
	int32_t group = p_index & 0xFF00000;
	int32_t index = p_index & 0x00FFFFF;
	if (group == prefix_crafting_items) {
		if (index < 0 || index >= m_w * m_h) {
			return false;
		}
		this->m_items[index] = p_item;
		this->notify(prefix_crafting_items | index);

		int x0 = m_w, x1 = 0;
		int y0 = m_h, y1 = 0;
		for (int h=0; h<m_h; h++) {
			for (int w=0; w<m_w; w++) {
				item::item_stack* it = &m_items[h*m_w + w];
				if (it->type) {
					x0 = std::min(x0, w);
					y0 = std::min(y0, h);
					x1 = std::max(x1, w);
					y1 = std::max(y1, h);
				}
			}
		}

		recipe const* old = m_recipe;
		if(x0 == m_w || y0 == m_h) {
			this->m_recipe = nullptr;
		} else {
			this->m_dim[0] = x0;
			this->m_dim[1] = y0;
			this->m_dim[2] = x1;
			this->m_dim[3] = y1;

			this->m_recipe = recipes::get_recipe(m_items, x0, y0, x1-x0+1, y1-y0+1, m_w);
		}
		if (m_recipe != old) {
			this->notify(prefix_crafting_result | 0);
		}
		return true;
	} else if (group == prefix_crafting_result) {
		this->m_items[m_size-1] = item::item_stack::null;
		this->notify(prefix_crafting_result | 0);
		return true;
	} else {
		return false;
	}
}

void crafting::craft(int count)
{
	int x0 = m_dim[0];
	int y0 = m_dim[1];
	int x1 = m_dim[2];
	int y1 = m_dim[3];
	if (m_recipe) {
		this->m_items[m_size-1] = m_recipe->apply(m_items, count, x0, y0, x1-x0+1, y1-y0+1, m_w);
		for (int i=0; i<m_size-1; i++) {
			container::notify(prefix_crafting_items | i);
		}
		container::notify(prefix_crafting_result | 0);
	} else {
		this->m_items[m_size-1] = item::item_stack::null;
	}
}

item::item_stack crafting::get_preview() const
{
	if (m_recipe) {
		return m_recipe->get_result();
	} else {
		return item::item_stack::null;
	}
}

item::item_stack crafting::get_result()
{
	item::item_stack result = m_items[m_size-1];
	this->m_items[m_size-1] = item::item_stack::null;
	return result;
}
