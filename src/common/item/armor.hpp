#pragma once

#include "container.hpp"


class armor : public container {

	private:


	public:
		armor();
		~armor();


	public:
		bool placeable(int index, item::item_stack const& i) override;
		double get_armor_value() const;

};
