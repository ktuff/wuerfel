#include "inventory.hpp"

int32_t const prefix_inventory_items = 0x01000000;
int32_t const prefix_inventory_temp  = 0x03000000;


inventory::inventory(int p_size)
: container(inventory_t, p_size)
, m_selected_index(0)
{
	container::add_listener(this);
}

inventory::~inventory()
{
	container::remove_listener(this);
}


void inventory::clear()
{
	container::clear();
}

void inventory::on_close()
{
}

void inventory::add_listener(inventory_listener* p_listener)
{
	this->m_listeners.push_back(p_listener);
}

void inventory::remove_listener(inventory_listener* p_listener)
{
	for (size_t i=0; i<m_listeners.size(); i++) {
		if (m_listeners[i] == p_listener) {
			this->m_listeners.erase(m_listeners.begin() + i);
			break;
		}
	}
}

void inventory::on_item_change(container*, int p_index)
{
	this->notify(INVENTORY_GROUP_ITEMS, p_index);
}

void inventory::set_selected_index(int index)
{
	// TODO modulo
	int prev = m_selected_index;
	this->m_selected_index = index;
	for (auto* listener : m_listeners) {
		listener->on_selection_change(prev, index);
	}
}

int inventory::get_selected_index() const
{
	return m_selected_index;
}

void inventory::notify(int p_group, int p_index)
{
	for (auto* listener : m_listeners) {
		listener->on_item_change(p_group, p_index);
	}
}
