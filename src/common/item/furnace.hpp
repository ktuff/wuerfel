#pragma once

#include "container.hpp"

class furnace : public container {

	private:
		int32_t m_fuel;
		int32_t m_smelted;


	public:
		furnace();
		~furnace();


	public:
		bool placeable(int32_t p_index, item::item_stack const& p_item) override;
};
