#pragma once

#include "container.hpp"
#include "inventory_listener.hpp"

extern int32_t const prefix_inventory_items;
extern int32_t const prefix_inventory_temp;

class inventory final : public container, public container_listener {

	private:
		item::item_stack m_temp;
		int m_selected_index;
		std::vector<inventory_listener*> m_listeners;


	public:
		inventory(int p_size);
		~inventory();


	public:
		void clear();
		void on_close();

		void add_listener(inventory_listener* p_listener);
		void remove_listener(inventory_listener* p_listener);

		void on_item_change(container* p_container, int p_index) override;
		void set_selected_index(int index);
		int get_selected_index() const;

	private:
		void notify(int p_group, int p_index);
};
