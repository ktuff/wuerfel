#pragma once

class container;

class container_listener {
    public:
        virtual void on_item_change(container* p_container, int p_index) = 0;
};
