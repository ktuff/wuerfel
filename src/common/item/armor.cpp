#include "armor.hpp"


armor::armor()
: container(armor_t, 4)
{
}

armor::~armor()
{
}


bool armor::placeable(int index, item::item_stack const& i)
{
	/*
	return (index == 0 && (i.type == item::e_helmet || i.type == item::e_helmet || i.type == item::e_helmet || i.type == item::e_helmet))
		|| (index == 1 && (i.type == item::e_chestplate || i.type == item::e_chestplate || i.type == item::e_chestplate || i.type == item::e_chestplate))
		|| (index == 2 && (i.type == item::e_leggings || i.type == item::e_leggings || i.type == item::e_leggings || i.type == item::e_leggings))
		|| (index == 3 && (i.type == item::e_boots || i.type == item::e_boots || i.type == item::e_boots || i.type == item::e_boots));
	*/
	// TODO
	return true;
}

double armor::get_armor_value() const
{
	auto const& helmet = get_item(0);
	auto const& chestplate = get_item(1);
	auto const& leggings = get_item(2);
	auto const& boots = get_item(3);
	double armor_value = 0.0;
	if (helmet.type) {
		armor_value += 1;
	}
	if (chestplate.type) {
		armor_value += 1;
	}
	if (leggings.type) {
		armor_value += 1;
	}
	if (boots.type) {
		armor_value += 1;
	}
	return armor_value;
}
