#include <ktf/util/logger.hpp>
#include <pugixml.hpp>

#include "recipe.hpp"

recipe::recipe(int p_w, int p_h, const std::vector<item::item_stack>& p_items, const item::item_stack& p_result)
: m_w(p_w)
, m_h(p_h)
, m_items(p_items)
, m_result(p_result)
{
}


bool recipe::compatible(std::vector<item::item_stack> const& p_items, int p_x, int p_y, int p_w, int p_h, int row_size) const
{
	if (p_items.size() < m_items.size() || p_w != m_w || p_h != m_h) {
		return false;
	}
	for (int h=0; h<m_h; h++) {
		for (int w=0; w<m_w; w++) {
			item::item_stack const& it = p_items[(h+p_y)*row_size + w+p_x];
			if (it.type != m_items[h*m_w + w].type) {
				return false;
			}
		}
	}
	return true;
}

item::item_stack recipe::apply(std::vector<item::item_stack>& p_items, int count, int p_x, int p_y, int p_w, int p_h, int row_size) const
{
	if (count <= 0 || !compatible(p_items, p_x, p_y, p_w, p_h, row_size)) {
		return item::item_stack::null;
	}

	int num = count;
	for (int h=0; h<m_h; h++) {
		for (int w=0; w<m_w; w++) {
			item::item_stack const& ing = m_items[h*m_w+w];
			item::item_stack& it = p_items[(h+p_y)*row_size + w+p_x];
			if (it.type && ing.type) {
				num = std::min(num, it.count / ing.count);
			}
		}
	}

	for (int h=0; h<m_h; h++) {
		for (int w=0; w<m_w; w++) {
			item::item_stack const& ing = m_items[h*m_w+w];
			item::item_stack& it = p_items[(h+p_y)*row_size + w+p_x];
			if (it.type && ing.type) {
				it -= num * ing.count;
			}
		}
	}

	// TODO enforce max stack size
	item::item_stack result = m_result;
	result.count *= num;
	return result;
}

item::item_stack recipe::get_result() const
{
	return m_result;
}


std::vector<recipe> recipes::s_recipes[MAX_RECIPE_SIZE*MAX_RECIPE_SIZE];

void recipes::load()
{
	for (int i=0; i<MAX_RECIPE_SIZE*MAX_RECIPE_SIZE; i++) {
		recipes::s_recipes[i].clear();
	}

	ktf::logger::info("Loading recipes...");
	std::string const path("assets/recipes.xml");

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	if (!result) {
		ktf::logger::error("could not load: " + path);
		return;
	}

	auto find_item = [](std::string const& id) -> item::item_type const* {
		for (size_t x=0; x<item::num_item_types; ++x) {
			if (item::all_types[x]->name == id) {
				return item::all_types[x];
			}
		}
		return nullptr;
	};

	auto const& doc_root = doc.root();
	auto const& crafting = doc_root.child("crafting");
	auto const& recipes = crafting.child("recipes");
	for (auto const& r : recipes.children()) {
		if (r.name() != std::string("recipe")) {
			continue;
		}

		unsigned int r_amount = r.attribute("amount").as_uint();
		unsigned int r_width = r.attribute("width").as_uint();
		unsigned int r_height = r.attribute("height").as_uint();
		if (r_amount == 0 || r_width == 0 || r_height == 0 || r_width > MAX_RECIPE_SIZE || r_height > MAX_RECIPE_SIZE) {
			continue;
		}

		std::string r_id = r.attribute("id").as_string();
		item::item_stack result;
		if (item::item_type const * res = find_item(r_id)) {
			result = item::item_stack(res->id, r_amount);
		} else {
			ktf::logger::debug("Invalid recipe: " + r_id);
			continue;
		}

		std::vector<item::item_stack> ingredients;
		for (auto const& i : r.children()) {
			if (i.name() != std::string("ingredient")) {
				continue;
			}

			unsigned int i_amount = i.attribute("amount").as_uint();
			if (i_amount == 0) {
				continue;
			}
			std::string i_id = i.attribute("id").as_string();
			if (item::item_type const * res = find_item(i_id)) {
				ingredients.emplace_back(res->id, i_amount);
			} else {
				ktf::logger::debug("Invalid ingredient: " + i_id);
				continue;
			}

		}

		unsigned int num = r_width*r_height;
		if (ingredients.size() != num) {
			continue;
		}

		ktf::logger::info("Loaded recipe: " + r_id);
		unsigned int hash = (r_height-1)*MAX_RECIPE_SIZE + r_width-1;
		recipes::s_recipes[hash].emplace_back(r_width, r_height, ingredients, result);
	}
}

recipe const* recipes::get_recipe(const std::vector<item::item_stack>& p_items, int x0, int y0, int w, int h, int row_size)
{
	int type = (h-1)*MAX_RECIPE_SIZE + w-1;
	auto const& recipes = recipes::s_recipes[type];
	for (auto const& recipe : recipes) {
		if (recipe.compatible(p_items, x0, y0, w, h, row_size)) {
			return &recipe;
		}
	}
	return nullptr;
}
