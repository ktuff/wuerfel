#include <stdexcept>

#include "voxel.hpp"


constexpr bool block_defs_valid()
{
	for (block_t b=0; b<e_block_count-1; ++b) {
		if (def::block_defs[b].id != b) {
			return false;
		}
	}
	return true;
}
static_assert(block_defs_valid(), "Block definitions are not in the right order.");

static std::unordered_map<std::string, block_t> const block_ids = []() {
	std::unordered_map<std::string, block_t> map(e_block_count);
	for (size_t i=0; i<e_block_count; ++i) {
		if (map.find(def::block_defs[i].name) != map.end()) {
			throw std::logic_error("Duplicated block names.");
		}
		map.emplace(def::block_defs[i].name, def::block_defs[i].id);
	}
	return map;
}();

block_t def::get_block_id(std::string const& name)
{
	auto it = block_ids.find(name);
	if (it != block_ids.end()) {
		return it->second;
	} else {
		return e_block_air;
	}
}

char const * side_names[side_count] = {
	"north",
	"west",
	"south",
	"east",
	"down",
	"up",
	"none"
};

const char * def::get_side_name(side_t side)
{
	return side_names[(side < side_count) ? side : e_none];
}

side_t def::get_side(std::string const& name)
{
	if (name == "north") {
		return e_north;
	} else if (name == "west") {
		return e_west;
	} else if (name == "south") {
		return e_south;
	} else if (name == "east") {
		return e_east;
	} else if (name == "down") {
		return e_down;
	} else if (name == "up") {
		return e_up;
	} else {
		return e_none;
	}
}
