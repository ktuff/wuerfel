#include <string.h>

#include <common/config.hpp>
#include <common/world/voxel.hpp>
#include <common/world/chunk.hpp>
#include <common/world/world.hpp>


game::chunk::chunk(world* world, int64_t x, int64_t z)
: posl(x, z)
, posw(x*CHUNK_SIZE, z*CHUNK_SIZE)
, m_world(world)
, m_hmax(-1)
, m_top_subchunk(-1)
, m_state(CHUNK_INVALID)
, m_modified(chunk_unmodified)
{
	for (coord_t c=0; c<NUM_SUBCHUNKS; c++) {
		this->m_voxels[c] = nullptr;
		this->m_block_count[c] = 0;
	}
	::memset(m_height, '\0', sizeof(m_height));
	::memset(neighbors, 0, sizeof(neighbors));
}

game::chunk::~chunk()
{
	if (neighbors[e_neg_x_nil_z]) {
		this->neighbors[e_neg_x_nil_z]->link(nullptr, e_pos_x_nil_z, false);
	}
	if (neighbors[e_pos_x_nil_z]) {
		this->neighbors[e_pos_x_nil_z]->link(nullptr, e_neg_x_nil_z, false);
	}
	if (neighbors[e_nil_x_neg_z]) {
		this->neighbors[e_nil_x_neg_z]->link(nullptr, e_nil_x_pos_z, false);
	}
	if (neighbors[e_nil_x_pos_z]) {
		this->neighbors[e_nil_x_pos_z]->link(nullptr, e_nil_x_neg_z, false);
	}

	this->remove_subchunk(0);
}


void game::chunk::link(chunk* ch, chunk_link n, bool backlink)
{
	this->neighbors[n] = ch;
	if (backlink && ch) {
		ch->link(this, backlinks[n], false);
	}
}

void game::chunk::fill(voxel const * const* voxels, const uint64_t* bpsc)
{
	// clear chunk
	this->remove_subchunk(0);

	// get top subchunk
	coord_t n_chunks = -1;
	for (coord_t c = 0; c<NUM_SUBCHUNKS; ++c) {
		if (bpsc[c]) {
			n_chunks = c;
		}
		this->m_block_count[c] = bpsc[c];
	}

	// copy voxels
	for (coord_t c = 0; c<=n_chunks; ++c) {
		if (m_block_count[c]) {
			this->add_subchunk(c, false);
			::memcpy(m_voxels[c], voxels[c], BLOCKS_PER_SUBCHUNK * sizeof(voxel));
		} else {
			this->add_subchunk(c, true);
		}
	}

	// generate height map
	this->m_hmax = -1;
	if (m_top_subchunk >= 0) {
		for (int64_t z=posw.y; z<posw.y+CHUNK_SIZE; ++z) {
			for (int64_t x=posw.x; x<posw.x+CHUNK_SIZE; ++x) {
				int16_t height = -1;
				for (int16_t y=(m_top_subchunk+1)*CHUNK_SIZE-1; y >= 0; --y) {
					if (get_block(x, y, z) != e_block_air) {
						height = y;
						break;
					}
				}
				this->set_height(x, z, -1, height, true);
			}
		}
	} else {
		for (int64_t z=posw.y; z<posw.y+CHUNK_SIZE; ++z) {
			for (int64_t x=posw.x; x<posw.x+CHUNK_SIZE; ++x) {
				this->set_height(x, z, 0, -1);
			}
		}
	}
	this->m_state = CHUNK_BASE_GEN;
}

voxel const* const* game::chunk::get_voxels() const
{
	return m_voxels;
}

const uint64_t* game::chunk::get_bpsc() const
{
    return &(m_block_count[0]);
}

int8_t game::chunk::get_top() const
{
    return m_top_subchunk;
}

voxel game::chunk::get_voxel(coord_t x, coord_t y, coord_t z) const
{
	if (y < 0) {
		return empty_voxel;
	} else if (y >= CHUNK_HEIGHT) {
		return empty_voxel_sky;
	} else if (voxel* pp = m_voxels[y/CHUNK_SIZE]) {
		return pp[get_index(x, y, z)];
	} else {
		return empty_voxel_sky;
	}
}

block_t game::chunk::get_block(coord_t x, coord_t y, coord_t z) const
{
	return get_voxel(x, y, z).block_id;
}

side_t game::chunk::get_orientation(coord_t x, coord_t y, coord_t z) const
{
	return get_voxel(x, y, z).orientation;
}

state_t game::chunk::get_state(coord_t x, coord_t y, coord_t z) const
{
	return get_voxel(x, y, z).state;
}

power_t game::chunk::get_power(coord_t x, coord_t y, coord_t z) const
{
	return get_voxel(x, y, z).power_value;
}

light_t game::chunk::get_light(coord_t x, coord_t y, coord_t z) const
{
	return get_voxel(x, y, z).light_value;
}

int16_t game::chunk::get_height(coord_t x, coord_t z) const
{
	return m_height[get_xz_index(x, z)];
}

bool game::chunk::set_block(coord_t x, coord_t y, coord_t z, block_t val)
{
	if (y < 0 || y >= CHUNK_HEIGHT) {
		return false;
	}
	block_t old_block = get_block(x, y, z);
	if (old_block == val) {
		return false;
	}
	int8_t sub_chunk = y / CHUNK_SIZE;
	if (val == e_block_air && old_block != e_block_air) {
		this->m_block_count[sub_chunk]--;
	} else if (old_block == e_block_air && val != e_block_air) {
		this->m_block_count[sub_chunk]++;
	}

	// adjust height map
	int16_t h = get_height(x, z);
	if (y == h && val == e_block_air) {
		int16_t new_h = -1;
		for (int16_t yi=h; yi>=0; --yi) {
			if (get_block(x, yi, z) != e_block_air) {
				new_h = yi;
				break;
			}
		}
		this->set_height(x, z, h, new_h);
	} else if (y > h) {
		this->set_height(x, z, h, y);
	}

	// set new value
	uint64_t index = get_index(x, y, z);
	if (val != e_block_air) {
		this->m_voxels[sub_chunk][index].block_id = val;
	} else {
		if (y >= h) {
			this->m_voxels[sub_chunk][index] = empty_voxel_sky;
		} else {
			this->m_voxels[sub_chunk][index] = empty_voxel;
		}
	}

	this->m_modified = chunk_modified;

	this->m_world->on_block_change(x, y, z, old_block, val);
	return true;
}

bool game::chunk::set_orientation(coord_t x, coord_t y, coord_t z, side_t val)
{
	if (y < 0 || y >= CHUNK_HEIGHT) {
		return false;
	} else {
		uint64_t sc = y/CHUNK_SIZE;
		if (m_voxels[sc]) {
			this->m_voxels[sc][get_index(x, y, z)].orientation = val;
			this->m_modified = chunk_modified;
			this->m_world->on_orientation_change(x, y, z, val);
			return true;
		} else {
			return false;
		}
	}
}

bool game::chunk::set_state(coord_t x, coord_t y, coord_t z, state_t val)
{
	bool rc = false;
	if (y >= 0 && y < CHUNK_HEIGHT) {
		uint64_t sc = y/CHUNK_SIZE;
		if (m_voxels[sc] && val < def::block_defs[get_block(x, y, z)].states) {
			this->m_voxels[sc][get_index(x, y, z)].state = val;
			this->m_modified = chunk_modified;
			this->m_world->on_state_change(x, y, z, val);
			rc = true;
		}
	}
	return rc;
}

bool game::chunk::set_power(coord_t x, coord_t y, coord_t z, power_t val)
{
	if (y < 0 || y >= CHUNK_HEIGHT) {
		return false;
	} else {
		uint64_t sc = y/CHUNK_SIZE;
		if (m_voxels[sc]) {
			this->m_voxels[sc][get_index(x, y, z)].power_value = val;
			this->m_modified = chunk_modified;
			this->m_world->on_power_change(x, y, z, val);
			return true;
		} else {
			return false;
		}
	}
}

bool game::chunk::set_light(coord_t x, coord_t y, coord_t z, light_t val)
{
	if (y < 0 || y >= CHUNK_HEIGHT) {
		return false;
	} else {
		uint64_t sc = y/CHUNK_SIZE;
		if (m_voxels[sc]) {
			this->m_voxels[sc][get_index(x, y, z)].light_value = val;
			this->m_world->on_light_change(x, y, z, val);
			return true;
		} else {
			return false;
		}
	}
}

void game::chunk::set_height(coord_t x, coord_t z, int16_t prev_height, int16_t new_height, bool ignore)
{
	if (prev_height == new_height) {
		return;
	}

	this->m_height[get_xz_index(x, z)] = new_height;
	if (new_height > m_hmax) {
		this->m_hmax = new_height;
	} else if (prev_height == m_hmax) {
		int16_t h = -1;
		for (coord_t i=0; i<CHUNK_SIZE*CHUNK_SIZE && h < m_hmax; ++i) { // abort as soon as possible, it is not possible that a block is higher than the old one
			h = std::max(h, m_height[i]);
		}
		this->m_hmax = h;
	}

	int8_t new_top_subchunk = m_hmax / CHUNK_SIZE;
	if (new_top_subchunk < m_top_subchunk && !ignore) {
		this->remove_subchunk(new_top_subchunk + 1);
	} else if (new_top_subchunk > m_top_subchunk) {
		this->add_subchunk(new_top_subchunk, true);
	}
}

int8_t game::chunk::get_state() const
{
	return m_state;
}

bool game::chunk::is_modified() const
{
	return m_modified == chunk_modified;
}

void game::chunk::set_is_modified(bool value)
{
	if (value) {
		this->m_modified = chunk_modified;
	} else {
		this->m_modified = chunk_unmodified;
	}
}

transparency game::chunk::get_transparency(coord_t x, coord_t y, coord_t z) const
{
	coord_t bx = x & (CHUNK_SIZE-1);
	coord_t bz = z & (CHUNK_SIZE-1);

	transparency rc {{0, 0, 0, 0, 0, 0, 0}};
	if (bx == 0) {
		rc.east = def::block_defs[get_block(x+1, y, z)].transparent;
		if (neighbors[e_neg_x_nil_z]) {
			rc.west = def::block_defs[neighbors[e_neg_x_nil_z]->get_block(x-1, y, z)].transparent;
		} else {
			rc.west = 1;
		}
	} else if (bx == CHUNK_SIZE-1) {
		rc.west = def::block_defs[get_block(x-1, y, z)].transparent;
		if (neighbors[e_pos_x_nil_z]) {
			rc.east = def::block_defs[neighbors[e_pos_x_nil_z]->get_block(x+1, y, z)].transparent;
		} else {
			rc.east = 1;
		}
	} else {
		rc.west = def::block_defs[get_block(x-1, y, z)].transparent;
		rc.east = def::block_defs[get_block(x+1, y, z)].transparent;
	}
	if (bz == 0) {
		rc.south = def::block_defs[get_block(x, y, z+1)].transparent;
		if (neighbors[e_nil_x_neg_z]) {
			rc.north = def::block_defs[neighbors[e_nil_x_neg_z]->get_block(x, y, z-1)].transparent;
		} else {
			rc.north = 1;
		}
	} else if (bz == CHUNK_SIZE-1) {
		rc.north = def::block_defs[get_block(x, y, z-1)].transparent;
		if (neighbors[e_nil_x_pos_z]) {
			rc.south = def::block_defs[neighbors[e_nil_x_pos_z]->get_block(x, y, z+1)].transparent;
		} else {
			rc.south = 1;
		}
	} else {
		rc.north = def::block_defs[get_block(x, y, z-1)].transparent;
		rc.south = def::block_defs[get_block(x, y, z+1)].transparent;
	}
	if (y == WORLD_MIN_Y) {
		rc.down = 1;
	} else {
		rc.down = def::block_defs[get_block(x, y-1, z)].transparent;
	}
	if (y == WORLD_MAX_Y-1) {
		rc.up = 1;
	} else {
		rc.up = def::block_defs[get_block(x, y+1, z)].transparent;
	}

	return rc;
}

light_array game::chunk::get_light_array(coord_t x, coord_t y, coord_t z) const
{
	coord_t bx = x & (CHUNK_SIZE-1);
	coord_t bz = z & (CHUNK_SIZE-1);

	light_array rc {
		{{0,0}},
		{{0,0}},
		{{0,0}},
		{{0,0}},
		{{0,0}},
		{{0,0}}
	};
	if (bx == 0) {
		rc.east = get_light(x+1, y, z);
		if (neighbors[e_neg_x_nil_z]) {
			rc.west = neighbors[e_neg_x_nil_z]->get_light(x-1, y, z);
		} else {
			rc.west = {{0,0}};
		}
	} else if (bx == CHUNK_SIZE-1) {
		rc.west = get_light(x-1, y, z);
		if (neighbors[e_pos_x_nil_z]) {
			rc.east = neighbors[e_pos_x_nil_z]->get_light(x+1, y, z);
		} else {
			rc.east = {{0,0}};
		}
	} else {
		rc.west = get_light(x-1, y, z);
		rc.east = get_light(x+1, y, z);
	}
	if (bz == 0) {
		rc.south = get_light(x, y, z+1);
		if (neighbors[e_nil_x_neg_z]) {
			rc.north = neighbors[e_nil_x_neg_z]->get_light(x, y, z-1);
		} else {
			rc.north = {{0,0}};
		}
	} else if (bz == CHUNK_SIZE-1) {
		rc.north = get_light(x, y, z-1);
		if (neighbors[e_nil_x_pos_z]) {
			rc.south = neighbors[e_nil_x_pos_z]->get_light(x, y, z+1);
		} else {
			rc.south = {{0,0}};
		}
	} else {
		rc.north = get_light(x, y, z-1);
		rc.south = get_light(x, y, z+1);
	}
	if (y == WORLD_MIN_Y) {
		rc.down = {{0,0}};
	} else {
		rc.down = get_light(x, y-1, z);
	}
	if (y == WORLD_MAX_Y-1) {
		rc.up.sky = 15;
		rc.up.block = 0;
	} else {
		rc.up = get_light(x, y+1, z);
	}

	return rc;
}

void game::chunk::add_subchunk(int8_t sc_index, bool clear)
{
	for (int8_t i=sc_index; i>m_top_subchunk; --i) {
		if (!m_voxels[i]) {
			this->m_voxels[i] = new voxel[BLOCKS_PER_SUBCHUNK];
		}
		if (clear) {
			for (size_t j=0; j<BLOCKS_PER_SUBCHUNK; ++j) {
				this->m_voxels[i][j] = empty_voxel_sky;
			}
		}
	}
	this->m_top_subchunk = sc_index;
}

void game::chunk::remove_subchunk(int8_t sc_index)
{
	if (sc_index <= m_top_subchunk) {
		for (int8_t i=m_top_subchunk; i>=sc_index; --i) {
			if (m_voxels[i]) {
				delete[] m_voxels[i];
				this->m_voxels[i] = nullptr;
				this->m_block_count[i] = 0;
			}
		}
		this->m_top_subchunk = sc_index - 1;
	}
}

uint64_t game::chunk::get_index(coord_t x, coord_t y, coord_t z)
{
	y = y & CHUNK_MASK;
	if (x < 0) {
		x = ~x;
	}
	if (z < 0) {
		z = ~z;
	}
	x = x & CHUNK_MASK;
	z = z & CHUNK_MASK;
	return ((y * CHUNK_SIZE) + z)*CHUNK_SIZE + x;
}

uint64_t game::chunk::get_xz_index(coord_t x, coord_t z)
{
	if (x < 0) {
		x = ~x;
	}
	if (z < 0) {
		z = ~z;
	}
	x = x & CHUNK_MASK;
	z = z & CHUNK_MASK;
	return z * CHUNK_SIZE + x;
}
