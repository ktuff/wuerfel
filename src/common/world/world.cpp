#include "voxel.hpp"
#include "world.hpp"

game::world::world()
{    
}

game::world::~world() {
	this->reset();
	for (world_listener* lst : listeners) {
		lst->on_destruction();
	}
}


void game::world::reset()
{
	for (auto& it : chunk_map) {
		for (world_listener* lst : listeners) {
			lst->on_chunk_remove(it.first.x, it.first.y);
		}
	}
	this->chunk_map.clear();
}

void game::world::add_listener(world_listener* lst)
{
	for (unsigned int i=0; i<listeners.size(); i++) {
		if (listeners[i] == lst) {
			return;
		}
	}
	this->listeners.push_back(lst);
}

void game::world::remove_listener(world_listener* lst)
{
	for (unsigned int i=0; i<listeners.size(); i++) {
		if (listeners[i] == lst) {
			listeners.erase(listeners.begin()+i);
			break;
		}
	}
}

bool game::world::chunk_exists(coord_t x, coord_t z) const
{
	return chunk_map.find(ccoord_t(x, z)) != chunk_map.end();
}

bool game::world::chunk_exists_global(coord_t x, coord_t z) const
{
	x = ::floor((long double)x / CHUNK_SIZE);
	z = ::floor((long double)z / CHUNK_SIZE);
	return chunk_map.find(ccoord_t(x, z)) != chunk_map.end();
}

std::unordered_map<ccoord_t, game::chunk_ptr>& game::world::get_chunks()
{
	return chunk_map;
}

game::chunk_ptr game::world::get_chunk(coord_t x, coord_t z) const
{
	auto cp = chunk_map.find(ccoord_t(x, z));
	if (cp != chunk_map.end()) {
		return cp->second;
	} else {
		return nullptr;
	}
}

game::chunk_ptr game::world::get_chunk_global(coord_t x, coord_t z) const
{
	x = ::floor((long double)x / CHUNK_SIZE);
	z = ::floor((long double)z / CHUNK_SIZE);
	return get_chunk(x, z);
}

void game::world::set_chunk(coord_t x, coord_t z, const game::chunk_ptr& chunk)
{
	this->remove_chunk(x, z);
	this->chunk_map[ccoord_t(x, z)] = chunk;
	chunk->link(get_chunk(x-1, z).get(), e_neg_x_nil_z, true);
	chunk->link(get_chunk(x+1, z).get(), e_pos_x_nil_z, true);
	chunk->link(get_chunk(x, z-1).get(), e_nil_x_neg_z, true);
	chunk->link(get_chunk(x, z+1).get(), e_nil_x_pos_z, true);

	for (auto lst : listeners) {
		lst->on_chunk_add(x, z);
	}
}

void game::world::set_chunk_global(coord_t x, coord_t z, const std::shared_ptr<game::chunk>& ch)
{
	x = ::floor((long double)x / CHUNK_SIZE);
	z = ::floor((long double)z / CHUNK_SIZE);
	this->set_chunk(x, z, ch);
}

void game::world::remove_chunk(coord_t x, coord_t z)
{
	if (chunk_exists(x, z)) {
		this->chunk_map.erase(ccoord_t(x, z));

		for (world_listener* lst : listeners) {
			lst->on_chunk_remove(x, z);
		}
	}
}

int16_t game::world::get_height(coord_t x, coord_t z) const
{
	if (in_bounds(x, z)) {
		coord_t cx = ::floor((long double)x / CHUNK_SIZE);
		coord_t cz = ::floor((long double)z / CHUNK_SIZE);
		auto ccol = chunk_map.find({cx, cz});
		if (ccol != chunk_map.end()) {
			return ccol->second->get_height(x, z);
		} else {
			return -1;
		}
	} else {
		return -1;
	}
}

bool game::world::is_transparent(coord_t x, coord_t y, coord_t z, side_t side) const
{
	if (!in_bounds(x, y, z)) {
		return true;
	}

	block_t block = 0;
	switch (side) {
	case e_north:
		block = get_block(x, y, z-1);
		break;
	case e_west:
		block = get_block(x-1, y, z);
		break;
	case e_south:
		block = get_block(x, y, z+1);
		break;
	case e_east:
		block = get_block(x+1, y, z);
		break;
	case e_down:
		block = get_block(x, y-1, z);
		break;
	case e_up:
		block = get_block(x, y+1, z);
		break;
	case e_none:
		return true;
	}
	return def::block_defs[block].transparent;
}

bool game::world::is_placeable(coord_t x, coord_t y, coord_t z, block_t blck, side_t side) const
{
	switch (blck) {
	case e_block_rail:
		return !def::block_defs[get_block(x, y-1, z)].transparent;
	case e_block_circuit_wire:
		return !def::block_defs[get_block(x, y-1, z)].transparent;
	case e_block_circuit_torch:
		return !def::block_defs[get_block(x, y, z, side)].transparent;
	default:
		return true;
	}
}

block_t game::world::get_block(coord_t x, coord_t y, coord_t z) const
{
	if (!in_bounds(x, y, z)) {
		return e_block_air;
	}

	chunk_ptr ch = get_chunk_global(x, z);
	if (ch) {
		return ch->get_block(x, y, z);
	} else {
		return e_block_air;
	}
}

block_t game::world::get_block(coord_t x, coord_t y, coord_t z, side_t side) const
{
	this->apply_side(x, y, z, side);
	return get_block(x, y, z);
}

bool game::world::set_block(coord_t x, coord_t y, coord_t z, block_t block, side_t side)
{
	if (!in_bounds(x, y, z)) {
		return false;
	}

	chunk_ptr cp = get_chunk_global(x, z);
	if (!cp) {
		return false;
	}
	block_t old_block = cp->get_block(x, y, z);
	if (old_block == block) {
		return false;
	}

	if (cp) {
		cp->set_block(x, y, z, block);
		cp->set_orientation(x, y, z, side);
	}

	return true;
}

side_t game::world::get_orientation(coord_t x, coord_t y, coord_t z) const
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			return ch->get_orientation(x, y, z);
		}
	}
	return e_none;
}

side_t game::world::get_orientation(coord_t x, coord_t y, coord_t z, side_t side) const
{
	this->apply_side(x, y, z, side);
	return get_orientation(x, y, z);
}

bool game::world::set_orientation(coord_t x, coord_t y, coord_t z, side_t val)
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			bool rc = ch->set_orientation(x, y, z, val);
			return rc;
		}
	}
	return false;
}

state_t game::world::get_state(coord_t x, coord_t y, coord_t z) const
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			return ch->get_state(x, y, z);
		}
	}
	return 0;
}

state_t game::world::get_state(coord_t x, coord_t y, coord_t z, side_t side) const
{
	this->apply_side(x, y, z, side);
	return get_state(x, y, z);
}

bool game::world::set_state(coord_t x, coord_t y, coord_t z, state_t val)
{
	bool rc = false;
	if (in_bounds(x, z)) {
		if (chunk_ptr ch = get_chunk_global(x, z)) {
			rc = ch->set_state(x, y, z, val);
		}
	}
	return rc;
}

power_t game::world::get_power(coord_t x, coord_t y, coord_t z) const
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			return ch->get_power(x, y, z);
		}
	}
	return 0;
}

power_t game::world::get_power(coord_t x, coord_t y, coord_t z, side_t side) const
{
	this->apply_side(x, y, z, side);
	return get_power(x, y, z);
}

bool game::world::set_power(coord_t x, coord_t y, coord_t z, power_t val)
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			bool rc = ch->set_power(x, y, z, val);
			return rc;
		}
	}
	return false;
}

light_t game::world::get_light(coord_t x, coord_t y, coord_t z) const
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			return ch->get_light(x, y, z);
		} else {
			return empty_voxel.light_value;
		}
	} else if (y >= CHUNK_HEIGHT) {
		return empty_voxel_sky.light_value;
	} else {
		return empty_voxel.light_value;
	}
}

light_t game::world::get_light(coord_t x, coord_t y, coord_t z, side_t side) const
{
	this->apply_side(x, y, z, side);
	return get_light(x, y, z);
}

bool game::world::set_light(coord_t x, coord_t y, coord_t z, light_t val)
{
	if (in_bounds(x, z)) {
		chunk_ptr ch = get_chunk_global(x, z);
		if (ch) {
			bool rc = ch->set_light(x, y, z, val);
			return rc;
		}
	}
	return false;
}

void game::world::apply_side(coord_t& x, coord_t& y, coord_t& z, side_t side)
{
	switch (side) {
	case e_north:
		z--;
		break;
	case e_west:
		x--;
		break;
	case e_south:
		z++;
		break;
	case e_east:
		x++;
		break;
	case e_down:
		y--;
		break;
	case e_up:
		y++;
		break;
	default:
		break;
	}
}

void game::world::on_block_change(coord_t x, coord_t y, coord_t z, block_t before, block_t after)
{
	for (world_listener* lst : listeners) {
		lst->on_block_change(x, y, z, before, after);
	}
}

void game::world::on_orientation_change(coord_t x, coord_t y, coord_t z, side_t side)
{
	for (world_listener* lst : listeners) {
		lst->on_rotation_change(x, y, z, side);
	}
}

void game::world::on_state_change(coord_t x, coord_t y, coord_t z, state_t state)
{
	for (world_listener* lst : listeners) {
		lst->on_state_change(x, y, z, state);
	}
}

void game::world::on_power_change(coord_t x, coord_t y, coord_t z, power_t value)
{
	for (world_listener* lst : listeners) {
		lst->on_power_change(x, y, z, value);
	}
}

void game::world::on_light_change(coord_t x, coord_t y, coord_t z, light_t value)
{
	for (world_listener* lst : listeners) {
		lst->on_light_change(x, y, z, value);
	}
}
