#pragma once

#include <limits>

#include <common/types.hpp>


constexpr coord_t exp_base2(coord_t e)
{
	if(e < 0) {
		e = 1;
	}
	return (1 << e);
}

coord_t const WORLD_WIDTH          =  std::numeric_limits<coord_t>::max();
coord_t const WORLD_DEPTH          =  std::numeric_limits<coord_t>::max();
coord_t const WORLD_HEIGHT         =  exp_base2(8);
coord_t const WORLD_MIN_X          = -exp_base2(16)-1;
coord_t const WORLD_MIN_Y          =  0;
coord_t const WORLD_MIN_Z          = -exp_base2(16)-1;
coord_t const WORLD_MAX_X          =  exp_base2(16)-1;
coord_t const WORLD_MAX_Y          =  WORLD_HEIGHT-1;
coord_t const WORLD_MAX_Z          =  exp_base2(16)-1;
coord_t const CHUNK_SIZE           =  exp_base2(4);
coord_t const CHUNK_MASK           =  CHUNK_SIZE-1;
coord_t const CHUNK_HEIGHT         =  WORLD_HEIGHT;
coord_t const NUM_SUBCHUNKS        =  CHUNK_HEIGHT/CHUNK_SIZE;
coord_t const BLOCKS_PER_SUBCHUNK  =  CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE;
coord_t const BLOCKS_PER_CHUNK     =  BLOCKS_PER_SUBCHUNK*NUM_SUBCHUNKS;

bool in_bounds(coord_t x, coord_t z);
bool in_bounds(coord_t x, coord_t y, coord_t z);
