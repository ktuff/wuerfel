#include "config.hpp"


bool in_bounds(coord_t x, coord_t z)
{
	return  (x <= WORLD_MAX_X && x >= WORLD_MIN_X) &&
			(z <= WORLD_MAX_Z && z >= WORLD_MIN_Z);
}

bool in_bounds(coord_t x, coord_t y, coord_t z)
{
	return  (x <= WORLD_MAX_X && x >= WORLD_MIN_X) &&
			(y <= WORLD_MAX_Y && y >= WORLD_MIN_Y) &&
			(z <= WORLD_MAX_Z && z >= WORLD_MIN_Z);
}
