#include "world_listener.hpp"


void world_listener::on_chunk_add(coord_t, coord_t)
{
}

void world_listener::on_chunk_remove(coord_t, coord_t)
{
}

void world_listener::on_block_change(coord_t, coord_t, coord_t, block_t, block_t)
{
}

void world_listener::on_rotation_change(coord_t, coord_t, coord_t, side_t)
{
}

void world_listener::on_state_change(coord_t, coord_t, coord_t, state_t)
{
}

void world_listener::on_power_change(coord_t, coord_t, coord_t, power_t)
{
}

void world_listener::on_light_change(coord_t, coord_t, coord_t, light_t)
{
}

void world_listener::on_destruction()
{
}

