#pragma once

#include <string>

#include <common/world/chunk.hpp>
#include <common/world/world_listener.hpp>


namespace game {

class world {

	private:
		std::unordered_map<ccoord_t, chunk_ptr> chunk_map;
		std::vector<world_listener*> listeners;


	public:
		world();
		~world();


	public:
		void reset();

		void add_listener(world_listener* lst);
		void remove_listener(world_listener* lst);

		bool chunk_exists(coord_t x, coord_t z) const;
		bool chunk_exists_global(coord_t x, coord_t z) const;
		std::unordered_map<ccoord_t, chunk_ptr>& get_chunks();
		chunk_ptr get_chunk(coord_t x, coord_t z) const;
		chunk_ptr get_chunk_global(coord_t x, coord_t z) const;
		void set_chunk(coord_t x, coord_t z, const chunk_ptr& ch);
		void set_chunk_global(coord_t x, coord_t z, const chunk_ptr& ch);
		void remove_chunk(coord_t x, coord_t z);

		int16_t get_height(coord_t x, coord_t z) const;
		bool is_transparent(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool is_placeable(coord_t x, coord_t y, coord_t z, block_t blck, side_t side = e_none) const;

		block_t get_block(coord_t x, coord_t y, coord_t z) const;
		block_t get_block(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_block(coord_t x, coord_t y, coord_t z, block_t val, side_t side = e_north);

		side_t get_orientation(coord_t x, coord_t y, coord_t z) const;
		side_t get_orientation(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_orientation(coord_t x, coord_t y, coord_t z, side_t val);

		state_t get_state(coord_t x, coord_t y, coord_t z) const;
		state_t get_state(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_state(coord_t x, coord_t y, coord_t z, state_t val);

		power_t get_power(coord_t x, coord_t y, coord_t z) const;
		power_t get_power(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_power(coord_t x, coord_t y, coord_t z, power_t val);

		light_t get_light(coord_t x, coord_t y, coord_t z) const;
		light_t get_light(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_light(coord_t x, coord_t y, coord_t z, light_t val);

		static void apply_side(coord_t& x, coord_t& y, coord_t& z, side_t side);

	private:
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t before, block_t after);
		void on_orientation_change(coord_t x, coord_t y, coord_t z, side_t side);
		void on_state_change(coord_t x, coord_t y, coord_t z, state_t state);
		void on_power_change(coord_t x, coord_t y, coord_t z, power_t value);
		void on_light_change(coord_t x, coord_t y, coord_t z, light_t value);

		friend class chunk;
};

} // namespace game
