#pragma once

#include <memory>

#include <common/systems.hpp>
#include <common/world/config.hpp>
#include <common/world/voxel.hpp>


#define CHUNK_INVALID       0
#define CHUNK_BASE_GEN      1
#define CHUNK_DECORATED     2


constexpr voxel empty_voxel {e_block_air, {e_north, 0}, 0, {{0, 0}}};
constexpr voxel empty_voxel_sky {e_block_air, {e_north, 0}, 0, {{LIGHT_MAX_VALUE, 0}}};


enum chunk_link : uint8_t {
	e_neg_x_nil_z,
	e_nil_x_neg_z,
	e_pos_x_nil_z,
	e_nil_x_pos_z
};

chunk_link const backlinks[4] = {
	e_pos_x_nil_z,
	e_nil_x_pos_z,
	e_neg_x_nil_z,
	e_nil_x_neg_z
};


struct light_array {
	light_t north;
	light_t west;
	light_t south;
	light_t east;
	light_t down;
	light_t up;
};


class tile_entity;


namespace game {

class world;


class chunk {

	public:
		ccoord_t const posl;
		ccoord_t const posw;

	private:
		world* m_world;
		voxel* m_voxels[NUM_SUBCHUNKS];
		int16_t m_height[CHUNK_SIZE*CHUNK_SIZE];
		uint64_t m_block_count[NUM_SUBCHUNKS];

		coord_t m_hmax;
		int8_t m_top_subchunk;
		int8_t m_state;
		enum {
			chunk_modified,
			chunk_unmodified
		} m_modified;

		chunk* neighbors[4];


	public:
		chunk(world* world, coord_t x, coord_t z);
		~chunk();


	public:
		void link(chunk* ch, chunk_link n, bool backlink);
		void fill(voxel const * const* voxels, const uint64_t* bpsc);
		voxel const * const* get_voxels() const;
		const uint64_t* get_bpsc() const;
		int8_t get_top() const;

		voxel get_voxel(coord_t x, coord_t y, coord_t z) const;
		block_t get_block(coord_t x, coord_t y, coord_t z) const;
		side_t get_orientation(coord_t x, coord_t y, coord_t z) const;
		state_t get_state(coord_t x, coord_t y, coord_t z) const;
		power_t get_power(coord_t x, coord_t y, coord_t z) const;
		light_t get_light(coord_t x, coord_t y, coord_t z) const;
		int16_t get_height(coord_t x, coord_t z) const;

		bool set_block(coord_t x, coord_t y, coord_t z, block_t val);
		bool set_orientation(coord_t x, coord_t y, coord_t z, side_t val);
		bool set_state(coord_t x, coord_t y, coord_t z, state_t val);
		bool set_power(coord_t x, coord_t y, coord_t z, power_t val);
		bool set_light(coord_t x, coord_t y, coord_t z, light_t val);
		void set_height(coord_t x, coord_t z, int16_t prev_height, int16_t new_height, bool ignore = false);

		transparency get_transparency(coord_t x, coord_t y, coord_t z) const;
		light_array get_light_array(coord_t x, coord_t y, coord_t z) const;
		int8_t get_state() const;
		bool is_modified() const;
		void set_is_modified(bool value);

	private:
		void add_subchunk(int8_t sc_index, bool clear);
		void remove_subchunk(int8_t sc_index);

	public:
		static uint64_t get_index(coord_t x, coord_t y, coord_t z);
		static uint64_t get_xz_index(coord_t x, coord_t z);

};


typedef std::shared_ptr<chunk> chunk_ptr;

} // namespace game

