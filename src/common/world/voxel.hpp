#pragma once

#include <string>
#include <unordered_map>

#include <common/types.hpp>

#pragma pack(push,1)
struct voxel {
	block_t block_id;
	struct {
		side_t orientation : 3;
		state_t state : 5;
	};
	power_t power_value;
	light_t light_value;
};
#pragma pack(pop)


enum block : block_t {
	e_block_air = 0,
	e_block_stone,
	e_block_cobblestone,
	e_block_dirt,
	e_block_grass,
	e_block_slab_stone,
	e_block_glass,
	e_block_sand,
	e_block_gravel,
	e_block_leaves_oak,
	e_block_log_oak,
	e_block_planks_oak,
	e_block_slab_oak,
	e_block_sapling_oak,
	e_block_workbench,
	e_block_furnace,
	e_block_chest,
	e_block_rail,
	e_block_circuit_wire,
	e_block_circuit_torch,
	e_block_circuit_repeater,
	e_block_circuit_comparator,
	e_block_torch,
	e_block_obsidian,
	e_block_clay,
	e_block_ore_coal,
	e_block_ore_iron,
	e_block_ore_gold,
	e_block_ore_diamond,
	e_block_flower_red,
	e_block_flower_yellow,
	e_block_pumpkin,
	e_block_melon,
	e_block_bamboo,
	e_block_water_source,
	e_block_water_flowing,
	e_block_count
};


namespace def {

struct block_def {
	block_t id;
	uint8_t tile : 1;
	uint8_t transparent : 1;
	uint8_t usable : 1;
	uint8_t circuit : 1;
	uint8_t gravity : 1;
	uint8_t group : 3;
	uint8_t states : 4;
	uint8_t emission : 4;
	char const * name;
};

enum block_group : uint8_t {
	e_group_air,
	e_group_solid,
	e_group_insolid,
	e_group_tile,
	e_group_circuit,
	e_group_fluid
};

block_def constexpr block_defs[e_block_count] = {
	{e_block_air, 					false, true,  false, false, false, e_group_air,		1,  0, "wuerfel.block.air"},
	{e_block_stone, 				false, false, false, false, false, e_group_solid,	1,  0, "wuerfel.block.stone"},
	{e_block_cobblestone, 			false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.cobblestone"},
	{e_block_dirt, 					false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.dirt"},
	{e_block_grass, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.grass"},
	{e_block_slab_stone, 			false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.slab_stone"},
	{e_block_glass, 				false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.glass"},
	{e_block_sand, 					false, false, false, false, true,  e_group_solid, 	1,  0, "wuerfel.block.sand"},
	{e_block_gravel, 				false, false, false, false, true,  e_group_solid, 	1,  0, "wuerfel.block.gravel"},
	{e_block_leaves_oak, 			false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.leaves_oak"},
	{e_block_log_oak, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.log_oak"},
	{e_block_planks_oak, 			false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.planks_oak"},
	{e_block_slab_oak, 				false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.slab_oak"},
	{e_block_sapling_oak, 			false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.sapling_oak"},
	{e_block_workbench, 			true,  false, true,  false, false, e_group_tile, 	1,  0, "wuerfel.block.workbench"},
	{e_block_furnace, 				true,  false, true,  false, false, e_group_tile, 	1,  0, "wuerfel.block.furnace"},
	{e_block_chest, 				true,  true,  true,  false, false, e_group_tile, 	1,  0, "wuerfel.block.chest"},
	{e_block_rail, 					false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.rail"},
	{e_block_circuit_wire, 			false, true,  false, true,  false, e_group_circuit, 1,  0, "wuerfel.block.circuit_wire"},
	{e_block_circuit_torch, 		false, true,  false, true,  false, e_group_circuit, 1,  8, "wuerfel.block.circuit_torch"},
	{e_block_circuit_repeater, 		false, true,  true,  true,  false, e_group_circuit, 4,  0, "wuerfel.block.circuit_repeater"},
	{e_block_circuit_comparator, 	false, true,  true,  true,  false, e_group_circuit, 2,  0, "wuerfel.block.circuit_comparator"},
	{e_block_torch, 				false, true,  false, false, false, e_group_insolid, 1, 15, "wuerfel.block.torch"},
	{e_block_obsidian, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.obsidian"},
	{e_block_clay, 					false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.clay"},
	{e_block_ore_coal, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.ore_coal"},
	{e_block_ore_iron, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.ore_iron"},
	{e_block_ore_gold, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.ore_gold"},
	{e_block_ore_diamond, 			false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.ore_diamond"},
	{e_block_flower_red, 			false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.flower_red"},
	{e_block_flower_yellow, 		false, true,  false, false, false, e_group_insolid, 1,  0, "wuerfel.block.flower_yellow"},
	{e_block_pumpkin, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.pumpkin"},
	{e_block_melon, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.melon"},
	{e_block_bamboo, 				false, false, false, false, false, e_group_solid, 	1,  0, "wuerfel.block.bamboo"},
	{e_block_water_source, 			false, true,  false, false, false, e_group_fluid, 	1,  0, "wuerfel.block.water_source"},
	{e_block_water_flowing, 		false, true,  false, false, false, e_group_fluid, 	8,  0, "wuerfel.block.water_flowing"}
};

constexpr bool valid_block_id(block_t id)
{
	return e_block_air <= id && id < e_block_count;
}

block_t get_block_id(std::string const& name);
char const* get_side_name(side_t side);
side_t get_side(std::string const& name);

} // namespace def
