#pragma once

#include <common/types.hpp>


namespace game {

class world;

} // namespace game


class world_listener {

	public:
		virtual void on_chunk_add(coord_t x, coord_t z);
		virtual void on_chunk_remove(coord_t x, coord_t z);
		virtual void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block);
		virtual void on_rotation_change(coord_t x, coord_t y, coord_t z, side_t value);
		virtual void on_state_change(coord_t x, coord_t y, coord_t z, state_t value);
		virtual void on_power_change(coord_t x, coord_t y, coord_t z, power_t value);
		virtual void on_light_change(coord_t x, coord_t y, coord_t z, light_t value);
		virtual void on_destruction();

};
