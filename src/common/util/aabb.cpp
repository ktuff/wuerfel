#include "aabb.hpp"

const double aabb::eps = 1e-16;

aabb::aabb()
{
	ktf::vec3<double> p(0);
	this->set_bounds(p, p);
}

aabb::aabb(const ktf::vec3<double>& p0, const ktf::vec3<double>& p1, const ktf::vec3<double>& origin)
: origin(origin)
{
	assert(p0 <= p1);
	this->set_bounds(p0, p1);
}

aabb::aabb(const aabb& box)
{
	this->origin = box.origin;
	this->set_bounds(box.param[0], box.param[1]);
}

aabb::~aabb()
{
}


aabb& aabb::operator=(const aabb& box)
{
	this->origin = box.origin;
	this->set_bounds(box.param[0], box.param[1]);
	return *this;
}

/**
 * moves the object by the offset
 */
void aabb::move(const ktf::vec3<double>& offset)
{
	this->set_bounds(param[0] + offset, param[1] + offset);
}

/**
 * extends the AABB in all directions with the given radius
 */
void aabb::expand(double radius)
{
	ktf::vec3<double> expansion = ktf::vec3<double>(radius, radius, radius);
	this->param[0] -= expansion;
	this->param[1] += expansion;
}

/**
 * extends a copy of this AABB and returns it 
 * @return - the extended AABB
 */
aabb aabb::expand_directional(double x_offset, double y_offset, double z_offset) const
{
	double x0, y0, z0, x1, y1, z1;
	if (x_offset < 0) {
		x0 = param[0].x + x_offset;
		x1 = param[1].x;
	} else {
		x0 = param[0].x;
		x1 = param[1].x + x_offset;
	}
	if (y_offset < 0) {
		y0 = param[0].y + y_offset;
		y1 = param[1].y;
	} else {
		y0 = param[0].y;
		y1 = param[1].y + y_offset;
	}
	if (z_offset < 0) {
		z0 = param[0].z + z_offset;
		z1 = param[1].z;
	} else {
		z0 = param[0].z;
		z1 = param[1].z + z_offset;
	}
	return aabb({x0, y0, z0}, {x1, y1, z1});
}

/**
 * sets the bounds of this AABB to given corner-positions
 */
void aabb::set_bounds(const ktf::vec3<double>& p0, const ktf::vec3<double>& p1)
{
	this->param[0] = p0;
	this->param[1] = p1;
	this->center = (p1 + p0) / 2.0f;
}

/**
 * calculates the X offset to another AABB
 * @return - new X offset
 */
double aabb::clip_x_collision(const aabb& o, double offset) const
{
	if (o.param[1].y <= param[0].y || o.param[0].y >= param[1].y) {
		return offset;
	}
	if (o.param[1].z <= param[0].z || o.param[0].z >= param[1].z) {
		return offset;
	}
	double offset_x;
	if (offset > 0.0 && o.param[1].x <= param[0].x && (offset_x = param[0].x - o.param[1].x - eps) < offset) {
		offset = offset_x;
	}
	if (offset < 0.0 && o.param[0].x >= param[1].x && (offset_x = param[1].x - o.param[0].x + eps) > offset) {
		offset = offset_x;
	}
	return offset;
}

/**
 * calculates the Y offset to another AABB
 * @return - new Y offset
 */
double aabb::clip_y_collision(const aabb& o, double offset) const
{
	if (o.param[1].x <= param[0].x || o.param[0].x >= param[1].x) {
		return offset;
	}
	if (o.param[1].z <= param[0].z || o.param[0].z >= param[1].z) {
		return offset;
	}
	double offset_y;
	if (offset > 0.0 && o.param[1].y <= param[0].y && (offset_y = param[0].y - o.param[1].y - eps) < offset) {
		offset = offset_y;
	}
	if (offset < 0.0 && o.param[0].y >= param[1].y && (offset_y = param[1].y - o.param[0].y + eps) > offset) {
		offset = offset_y;
	}
	return offset;
}

/**
 * calculates the Z offset to another AABB
 * @return - new Z offset
 */
double aabb::clip_z_collision(const aabb& o, double offset) const
{
	if (o.param[1].x <= param[0].x || o.param[0].x >= param[1].x) {
		return offset;
	}
	if (o.param[1].y <= param[0].y || o.param[0].y >= param[1].y) {
		return offset;
	}
	double offset_z;
	if (offset > 0.0 && o.param[1].z <= param[0].z && (offset_z = param[0].z - o.param[1].z - eps) < offset) {
		offset = offset_z;
	}
	if (offset < 0.0 && o.param[0].z >= param[1].z && (offset_z = param[1].z - o.param[0].z + eps) > offset) {
		offset = offset_z;
	}
	return offset;
}

double aabb::clip_x_offset(const aabb& o, double dx, bool check_y, bool check_z) const
{
	if ((check_y && (o.param[1].y <= param[0].y || o.param[0].y >= param[1].y)) || (check_z && (o.param[1].z <= param[0].z || o.param[0].z >= param[1].z))) {
		return 0;
	}
	int d = dx < 0;
	return param[1-d].x - o.param[d].x;
}

double aabb::clip_y_offset(const aabb& o, double dy, bool check_x, bool check_z) const
{
	if ((check_x && (o.param[1].x <= param[0].x || o.param[0].x >= param[1].x)) || (check_z && (o.param[1].z <= param[0].z || o.param[0].z >= param[1].z))) {
		return 0;
	}
	int d = dy < 0;
	return param[1-d].y - o.param[d].y;
}

double aabb::clip_z_offset(const aabb& o, double dz, bool check_x, bool check_y) const
{
	if ((check_x && (o.param[1].x <= param[0].x || o.param[0].x >= param[1].x)) || (check_y && (o.param[1].y <= param[0].y || o.param[0].y >= param[1].y))) {
		return 0;
	}
	int d = dz < 0;
	return param[1-d].z - o.param[d].z;
}

bool aabb::intersect(const aabb& o) const
{
	// if equal border return false
	return  param[0].x < o.param[1].x && param[1].x > o.param[0].x &&
			param[0].y < o.param[1].y && param[1].y > o.param[0].y &&
			param[0].z < o.param[1].z && param[1].z > o.param[0].z;
}

bool aabb::intersect(const ray& r, double t0, double t1) const
{
	double tmin, tmax, tymin, tymax, tzmin, tzmax;

	tmin = (param[r.sign[0]].x - r.origin.x) * r.inv_dir.x;
	tmax = (param[1-r.sign[0]].x - r.origin.x) * r.inv_dir.x;
	tymin = (param[r.sign[1]].y - r.origin.y) * r.inv_dir.y;
	tymax = (param[1-r.sign[1]].y - r.origin.y) * r.inv_dir.y;
	if ((tmin > tymax) || (tymin > tmax)) {
		return false;
	}
	if (tymin > tmin) {
		tmin = tymin;
	}
	if (tymax < tmax) {
		tmax = tymax;
	}
	tzmin = (param[r.sign[2]].z - r.origin.z) * r.inv_dir.z;
	tzmax = (param[1-r.sign[2]].z - r.origin.z) * r.inv_dir.z;
	if ((tmin > tzmax) || (tzmin > tmax)) {
		return false;
	}
	if (tzmin > tmin) {
		tmin = tzmin;
	}
	if (tzmax < tmax) {
		tmax = tzmax;
	}
	return (tmin < t1) && (tmax > t0);
}

side_t aabb::intersected_side(const ray& r, double& f) const
{
	ktf::vec3<double> n0(1.0, 0.0, 0.0);
	ktf::vec3<double> n1(0.0, 1.0, 0.0);
	ktf::vec3<double> n2(0.0, 0.0, 1.0);
	double hx = center.x - param[0].x;
	double hy = center.y - param[0].y;
	double hz = center.z - param[0].z;
	if (r.plane_intersection(param[r.dir.x < 0], n0, f) && std::abs(r.origin.y + r.dir.y*f - center.y) <= hy && std::abs(r.origin.z + r.dir.z*f - center.z) <= hz) {
		return (r.dir.x < 0) ? e_east : e_west;
	}
	if (r.plane_intersection(param[r.dir.y < 0], n1, f) && std::abs(r.origin.x + r.dir.x*f - center.x) <= hx && std::abs(r.origin.z + r.dir.z*f - center.z) <= hz) {
		return (r.dir.y < 0) ? e_up: e_down;
	}
	if (r.plane_intersection(param[r.dir.z < 0], n2, f) && std::abs(r.origin.x + r.dir.x*f - center.x) <= hx && std::abs(r.origin.y + r.dir.y*f - center.y) <= hy) {
		return (r.dir.z < 0) ? e_south : e_north;
	}
	return e_none;
}
