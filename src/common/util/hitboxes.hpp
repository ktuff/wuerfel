#pragma once

#include <vector>

#include <common/util/aabb.hpp>


namespace game {

class world;

} // namespace game


class hitboxes {

	public:
		static void get_hitboxes(game::world* world, std::vector<aabb>& hitboxes, aabb const& a, block_t filter = 0);
		static void get_viewboxes(game::world* world, std::vector<aabb>& boxes, aabb const& a);

	private:
		static bool has_hitbox(block_t block);
		static aabb get_hitbox(block_t block, coord_t x, coord_t y, coord_t z);

		static bool has_viewbox(block_t block);
		static aabb get_viewbox(block_t block, coord_t x, coord_t y, coord_t z);

};
