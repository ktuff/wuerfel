#pragma once

#include <ktf/math/vec3.hpp>


class ray {

	public:
		ktf::vec3<double> origin;
		ktf::vec3<double> dir;
		ktf::vec3<double> inv_dir;
		int sign[3];


	public:
		ray();
		ray(const ktf::vec3<double>& origin, const ktf::vec3<double>& dir);


	public:
		bool plane_intersection(const ktf::vec3<double>& p0, const ktf::vec3<double>& n, double& fac) const;
		ktf::vec3<double> intersection_point(const ktf::vec3<double>& plane_p, const ktf::vec3<double>& plane_n) const;

};
