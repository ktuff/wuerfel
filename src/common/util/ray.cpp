#include "ray.hpp"

ray::ray()
{
}

ray::ray(const ktf::vec3<double>& origin, const ktf::vec3<double>& dir)
: origin(origin)
, dir(dir)
{
	this->inv_dir = ktf::vec3<double>(1/dir.x, 1/dir.y, 1/dir.z);

	this->sign[0] = (inv_dir.x < 0);
	this->sign[1] = (inv_dir.y < 0);
	this->sign[2] = (inv_dir.z < 0);
}


bool ray::plane_intersection(const ktf::vec3<double>& p0, const ktf::vec3<double>& n, double& fac) const
{
	double denom = n*dir;
	if (std::abs(denom) > 1e-8) {
		ktf::vec3<double> p0l0 = p0 - origin;
		fac = (p0l0 * n) / denom;
		return (fac >= 0);
	} else {
		return false;
	}
}

ktf::vec3<double> ray::intersection_point(const ktf::vec3<double>& plane_p, const ktf::vec3<double>& plane_n) const
{
	ktf::vec3<double> diff = origin - plane_p;
	double prod1 = diff * plane_n;
	double prod2 = dir * plane_n;
	double prod3 = prod1 / prod2;
	return origin - dir * prod3;
}
