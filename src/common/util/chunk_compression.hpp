#pragma once

#include <common/world/chunk.hpp>


std::pair<std::unique_ptr<char[]>, size_t> wfl_compress_chunk(game::chunk_ptr chunk);
game::chunk_ptr wfl_decompress_chunk(game::world* world, char const *p_data, size_t p_length);

std::pair<std::unique_ptr<char[]>, size_t> compress(char const* p_data, size_t p_size);
std::pair<std::unique_ptr<char[]>, size_t> decompress(char const* p_data, size_t p_size);
