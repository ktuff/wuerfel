#pragma once

#include <assert.h>

#include <common/types.hpp>
#include "ray.hpp"


class aabb {

	private:
		static const double eps;

	public:
		ktf::vec3<double> param[2];
		ktf::vec3<double> center;
		ktf::vec3<double> origin;


	public:
		aabb();
		aabb(const ktf::vec3<double>& p0, const ktf::vec3<double>& p1, const ktf::vec3<double>& origin = ktf::vec3<double>(0));
		aabb(const aabb& box);
		~aabb();


	public:
		aabb& operator=(const aabb& box);


	public:
		// values
		void move(const ktf::vec3<double>& offset);
		void expand(double radius);
		aabb expand_directional(double xOffset, double yOffset, double zOffset) const;
		void set_bounds(const ktf::vec3<double>& p0, const ktf::vec3<double>& p1);

		// collision
		double clip_x_collision(const aabb& o, double offset) const;
		double clip_y_collision(const aabb& o, double offset) const;
		double clip_z_collision(const aabb& o, double offset) const;
		double clip_x_offset(const aabb& o, double dx, bool check_y, bool check_z) const;
		double clip_y_offset(const aabb& o, double dy, bool check_x, bool check_z) const;
		double clip_z_offset(const aabb& o, double dz, bool check_x, bool check_y) const;
		bool intersect(const aabb& o) const;

		// raytracing
		bool intersect(const ray& r, double t0, double t1) const;
		side_t intersected_side(const ray& r, double& f) const;
};
