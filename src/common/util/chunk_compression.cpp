#include <string.h>
#include <zlib.h>

#include <ktf/util/pointer.h>

#include "chunk_compression.hpp"


size_t const wfl_chunk_buffersize = sizeof(coord_t) * 2
								  +	sizeof(int8_t)
								  + NUM_SUBCHUNKS * sizeof(uint64_t)
								  + NUM_SUBCHUNKS * BLOCKS_PER_SUBCHUNK * sizeof(voxel);



std::pair<std::unique_ptr<char []>, size_t> compress(const char* p_data, size_t p_size)
{
	uLongf compressed_size = ::compressBound(p_size);
	size_t maximum_size = sizeof(size_t) + compressed_size;
	std::unique_ptr<char[]> compressed_data = std::unique_ptr<char[]>(new char[maximum_size]);
	char* data_ptr = compressed_data.get();
	((size_t*)data_ptr)[0] = p_size;
	int rc = ::compress((Bytef*)::ptr(data_ptr, sizeof(size_t)), &compressed_size, (Bytef*)p_data, p_size);
	if (rc == Z_OK) {
		return std::make_pair(std::move(compressed_data), compressed_size + sizeof(size_t));
	} else {
		return std::make_pair(nullptr, 0);
	}
}

std::pair<std::unique_ptr<char []>, size_t> decompress(const char* p_data, size_t p_size)
{
	uLongf uncompressed_size = ((size_t*)p_data)[0];
	std::unique_ptr<char[]> uncompressed_data = std::unique_ptr<char[]>(new char[uncompressed_size]);
	uLong compressed_size = p_size - sizeof(size_t);
	int rc = ::uncompress((Bytef*)uncompressed_data.get(), &uncompressed_size, (Bytef*)::cptr(p_data, sizeof(size_t)), compressed_size);
	if (rc == Z_OK) {
		return std::make_pair(std::move(uncompressed_data), uncompressed_size);
	} else {
		return std::make_pair(nullptr, 0);
	}
}

std::pair<std::unique_ptr<char[]>, size_t> wfl_compress_chunk(std::shared_ptr<game::chunk> p_chunk)
{
	std::unique_ptr<char[]> pbuffer = std::unique_ptr<char[]>(new char[wfl_chunk_buffersize]);
	char* buffer = pbuffer.get();

	size_t offset = 0;
	coord_t pos[2] = {
		p_chunk->posl.x,
		p_chunk->posl.y
	};
	::memcpy(::ptr(buffer, offset), pos, sizeof(coord_t) * 2);
	offset += sizeof(coord_t) * 2;
	int8_t num_sc = p_chunk->get_top() + 1;
	((int8_t*)::ptr(buffer, offset))[0] = num_sc;
	offset += sizeof(int8_t);
	uint64_t const * const p_bpsc = p_chunk->get_bpsc();
	for (int8_t sc=0; sc<num_sc; sc++) {
		((uint64_t*)::ptr(buffer, offset))[sc] = p_bpsc[sc];
	}
	offset += sizeof(uint64_t) * num_sc;

	voxel const * const * const p_voxels = p_chunk->get_voxels();
	for (int8_t sc=0; sc<num_sc; sc++) {
		::memcpy(::ptr(buffer, offset), p_voxels[sc], sizeof(voxel) * BLOCKS_PER_SUBCHUNK);
		offset += sizeof(voxel) * BLOCKS_PER_SUBCHUNK;
	}

	return ::compress(buffer, offset);
}

std::shared_ptr<game::chunk> wfl_decompress_chunk(game::world* world, char const *p_data, size_t p_length)
{
	auto uncompressed_data = ::decompress(p_data, p_length);
	char const* data = uncompressed_data.first.get();

	size_t offset = 0;
	coord_t pos[2];
	::memcpy(pos, data, 2 * sizeof(coord_t));
	offset += 2 * sizeof(coord_t);
	int8_t num_sc = ((int8_t*)::cptr(data, offset))[0];
	offset += sizeof(int8_t);
	uint64_t bpsc[NUM_SUBCHUNKS];
	voxel * p_voxels[NUM_SUBCHUNKS];
	std::unique_ptr<voxel[]> mem_voxel[NUM_SUBCHUNKS];
	for (int8_t sc=0; sc<num_sc; sc++) {
		mem_voxel[sc] = std::unique_ptr<voxel[]>(new voxel[BLOCKS_PER_SUBCHUNK]);
		p_voxels[sc] = mem_voxel[sc].get();
		bpsc[sc] = ((uint64_t*)::cptr(data, offset))[sc];
	}
	offset += sizeof(uint64_t)*num_sc;
	for (int8_t sc=num_sc; sc<NUM_SUBCHUNKS; sc++) {
		bpsc[sc] = 0;
		p_voxels[sc] = nullptr;
	}
	for (int8_t sc=0; sc<num_sc; sc++) {
		::memcpy(p_voxels[sc], ::cptr(data, offset), sizeof(voxel) * BLOCKS_PER_SUBCHUNK);
		offset += sizeof(voxel) * BLOCKS_PER_SUBCHUNK;
	}

	std::shared_ptr<game::chunk> chunk = std::make_shared<game::chunk>(world, pos[0], pos[1]);
	chunk->fill(p_voxels, bpsc);
	return chunk;
}
