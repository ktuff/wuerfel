#include <common/world/voxel.hpp>
#include <common/world/world.hpp>

#include "hitboxes.hpp"


void hitboxes::get_hitboxes(game::world* world, std::vector<aabb>& hitboxes, const aabb& a, block_t filter)
{
	hitboxes.clear();
	coord_t x0 = ::floor(a.param[0].x);
	coord_t y0 = ::floor(a.param[0].y);
	coord_t z0 = ::floor(a.param[0].z);
	coord_t x1 = ::ceil(a.param[1].x);
	coord_t y1 = ::ceil(a.param[1].y);
	coord_t z1 = ::ceil(a.param[1].z);

	for (coord_t x=x0; x<=x1; x++) {
		for (coord_t y=y0; y<=y1; y++) {
			for (coord_t z=z0; z<=z1; z++) {
				block_t block = world->get_block(x, y, z);
				if ((!filter || block == filter) && has_hitbox(block)) {
					hitboxes.push_back(get_hitbox(block, x, y, z));
				}
			}
		}
	}
}

void hitboxes::get_viewboxes(game::world* world, std::vector<aabb>& boxes, const aabb& a)
{
	boxes.clear();
	coord_t x0 = ::floor(a.param[0].x);
	coord_t y0 = ::floor(a.param[0].y);
	coord_t z0 = ::floor(a.param[0].z);
	coord_t x1 = ::ceil(a.param[1].x);
	coord_t y1 = ::ceil(a.param[1].y);
	coord_t z1 = ::ceil(a.param[1].z);

	for (coord_t x=x0; x<=x1; x++) {
		for (coord_t y=y0; y<=y1; y++) {
			for (coord_t z=z0; z<=z1; z++) {
				block_t block = world->get_block(x, y, z);
				if (has_viewbox(block)) {
					boxes.push_back(get_viewbox(block, x, y, z));
				}
			}
		}
	}
}

bool hitboxes::has_hitbox(block_t block)
{
	switch(block) {
	case e_block_air:
	case e_block_circuit_wire:
	case e_block_circuit_torch:
	case e_block_flower_red:
	case e_block_flower_yellow:
	case e_block_sapling_oak:
		return false;
	case e_block_water_source:
	case e_block_circuit_repeater:
	case e_block_circuit_comparator:
	case e_block_rail:
		return true;
	default:
		return true;
	}
}

aabb hitboxes::get_hitbox(block_t block, coord_t x, coord_t y, coord_t z)
{
	switch(block) {
	case e_block_air:
		return aabb();
	case e_block_circuit_repeater:
	case e_block_circuit_comparator:
	case e_block_rail:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y-0.375, z+0.5}, ktf::vec3<double>(x, y, z));
	case e_block_water_source:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y+0.4375, z+0.5}, ktf::vec3<double>(x, y, z));
	default:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y+0.5, z+0.5}, ktf::vec3<double>(x, y, z));
	}
}

bool hitboxes::has_viewbox(block_t block)
{
	switch(block) {
	case e_block_air:
		return false;
	default:
		return true;
	}
}

aabb hitboxes::get_viewbox(block_t block, coord_t x, coord_t y, coord_t z)
{
	switch(block) {
	case e_block_air:
		return aabb();
	case e_block_circuit_wire:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y-0.4, z+0.5}, ktf::vec3<double>(x, y, z));
	case e_block_circuit_torch:
	case e_block_torch:
		return aabb({x-0.1, y-0.5, z-0.1}, {x+0.1, y+0.2, z+0.1}, ktf::vec3<double>(x, y, z));
	case e_block_circuit_repeater:
	case e_block_circuit_comparator:
	case e_block_rail:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y-0.375, z+0.5}, ktf::vec3<double>(x, y, z));
	default:
		return aabb({x-0.5, y-0.5, z-0.5}, {x+0.5, y+0.5, z+0.5}, ktf::vec3<double>(x, y, z));
	}
}
