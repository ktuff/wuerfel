#pragma once

#include "tile_entity_map.hpp"
#include "entity/core_system.hpp"
#include "world/world.hpp"


namespace game {
    
class level {

	private:
		bool m_open;
		std::string m_name;
		uint64_t m_seed;
		uint64_t m_ticks;
		bool m_is_server;
		game::world world;
		entity::core_system m_entities;
		tile_entity_map m_tile_entities;


	public:
		level(bool is_server);
		~level();


	public:
		bool operator()() const;

		void init(std::string const& name, uint64_t seed, uint64_t ticks, ::entity::id start_id);
		void reset();
		std::string get_name() const;
		uint64_t get_seed() const;
		uint64_t get_ticks() const;
		void next_tick();

		game::world* get_world();

		entity::core_system* get_entity_system();
		entity::entity* get_entity(::entity::id id);
		::entity::id get_tile_entity(coord_t x, coord_t y, coord_t z) const;
		bool is_entity_in_water(::entity::id id);

		// access functions
		std::shared_ptr<game::chunk> get_chunk(coord_t x, coord_t z) const;
		void put_chunk(coord_t x, coord_t z, const std::shared_ptr<game::chunk>& chunk);
		void remove_chunk(coord_t x, coord_t z);

		block_t get_block(coord_t x, coord_t y, coord_t z) const;
		block_t get_block(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_block(coord_t x, coord_t y, coord_t z, block_t val, side_t side = e_north);

		side_t get_orientation(coord_t x, coord_t y, coord_t z) const;
		side_t get_orientation(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_orientation(coord_t x, coord_t y, coord_t z, side_t val);

		state_t get_state(coord_t x, coord_t y, coord_t z) const;
		state_t get_state(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_state(coord_t x, coord_t y, coord_t z, state_t val);

		power_t get_power(coord_t x, coord_t y, coord_t z) const;
		power_t get_power(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_power(coord_t x, coord_t y, coord_t z, power_t val);

		light_t get_light(coord_t x, coord_t y, coord_t z) const;
		light_t get_light(coord_t x, coord_t y, coord_t z, side_t side) const;
		bool set_light(coord_t x, coord_t y, coord_t z, light_t val);

};

} // namespace game
