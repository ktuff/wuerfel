#include "tile_entity_map.hpp"
#include "entity/core_system.hpp"
#include "entity/entities/tile.hpp"
#include "world/world.hpp"


::entity::system_module_id const tile_entity_map::s_id = 8;


tile_entity_map::tile_entity_map(::game::world* world, ::entity::core_system* entity_system, bool is_server)
: system_module(entity_system, s_id)
, m_world(world)
, m_is_server(is_server)
{
	this->m_world->add_listener(this);
}

tile_entity_map::~tile_entity_map()
{
	this->m_world->remove_listener(this);
}


void tile_entity_map::on_entity_creation(::entity::entity* e)
{
	if (auto* ptile = dynamic_cast<::entity::tile*>(e)) {
		if (m_tile_entities.find(ptile->get_voxel_position()) == m_tile_entities.end()) {
			this->m_tile_entities.emplace(ptile->get_voxel_position(), ptile->m_id);
		}
	}
}

void tile_entity_map::on_entity_destruction(::entity::entity* e)
{
	if (auto* ptile = dynamic_cast<::entity::tile*>(e)) {
		if (m_tile_entities.find(ptile->get_voxel_position()) != m_tile_entities.end()) {
			this->m_tile_entities.erase(ptile->get_voxel_position());
		}
	}
}

void tile_entity_map::on_event(::entity::event*)
{
}

void tile_entity_map::on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block)
{
	if (!m_is_server) {
		return;
	}

	if (def::block_defs[old_block].tile) {
		auto it = m_tile_entities.find(ktf::vec3<coord_t>(x, y, z));
		if (it != m_tile_entities.end()) {
			this->m_core->destroy_entity(it->second);
		}
	}

	if (def::block_defs[new_block].tile) {
		ktf::vec3<coord_t> pos(x, y, z);
		switch (new_block) {
		case e_block_chest:
			this->m_core->create_entity_chest(pos);
			break;
		case e_block_furnace:
			this->m_core->create_entity_furnace(pos);
			break;
		case e_block_workbench:
			this->m_core->create_entity_workbench(pos);
			break;
		default:
			break;
		}
	}
}

::entity::id tile_entity_map::get(coord_t x, coord_t y, coord_t z) const
{
	if (m_tile_entities.find(ktf::vec3<coord_t>(x, y, z)) != m_tile_entities.end()) {
		return m_tile_entities.at(ktf::vec3<coord_t>(x, y, z));
	} else {
		return 0;
	}
}

