#pragma once

#include <string>
#include <unordered_map>


namespace common {

class config {

	private:
		std::string const m_config_path;
		std::unordered_map<std::string, bool> m_bool_values;
		std::unordered_map<std::string, int> m_int_values;
		std::unordered_map<std::string, float> m_float_values;
		std::unordered_map<std::string, std::string> m_string_values;


	public:
		config(
			std::string const& path,
			std::unordered_map<std::string, bool> const& defaults_bool,
			std::unordered_map<std::string, int> const& defaults_int,
			std::unordered_map<std::string, float> const& defaults_float,
			std::unordered_map<std::string, std::string> const& defaults_string
		);
		~config();


	public:
		bool get_bool(std::string const& id) const;
		int get_int(std::string const& id) const;
		float get_float(std::string const& id) const;
		std::string get_string(std::string const& id) const;
    
		void set_bool(std::string const& id, bool value);
		void set_int(std::string const& id, int value);
		void set_float(std::string const& id, float value);
		void set_string(std::string const& id, const std::string& value);

	private:
		void load();
		void save();

};

} // namespace common
