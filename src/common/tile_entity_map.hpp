#pragma once

#include <unordered_map>

#include "types.hpp"
#include "entity/systems/system_module.hpp"
#include "world/world_listener.hpp"


class tile_entity_map : public ::entity::system_module, world_listener {

	public:
		static ::entity::system_module_id const s_id;

	private:
		::game::world* m_world;
		std::unordered_map<ktf::vec3<coord_t>, ::entity::id> m_tile_entities;
		bool m_is_server;


	public:
		tile_entity_map(::game::world* world, ::entity::core_system* entity_system, bool is_server);
		~tile_entity_map();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block) override;

		::entity::id get(coord_t x, coord_t y, coord_t z) const;

};
