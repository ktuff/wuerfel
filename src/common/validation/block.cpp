#include <common/level.hpp>
#include "block.hpp"


namespace common {

namespace block {

bool is_placeable_torch(game::level* plevel, side_t orientation, coord_t x, coord_t y, coord_t z);
bool is_placeable_ontop(game::level* plevel, coord_t x, coord_t y, coord_t z);
bool is_placeable_plant(game::level* plevel, coord_t x, coord_t y, coord_t z);

} // namespace block

} // namespace common


bool common::block::is_placeable(game::level* plevel, block_t block, side_t orientation, coord_t x, coord_t y, coord_t z)
{
	switch (block) {
	case e_block_torch:
	case e_block_circuit_torch:
		return is_placeable_torch(plevel, orientation, x, y, z);
	case e_block_circuit_wire:
	case e_block_circuit_repeater:
	case e_block_circuit_comparator:
	case e_block_flower_red:
	case e_block_flower_yellow:
	case e_block_rail:
		return is_placeable_ontop(plevel, x, y, z);
	case e_block_sapling_oak:
		return is_placeable_plant(plevel, x, y, z);
	default:
		return true;
	}
}

bool common::block::is_placeable_torch(game::level* plevel, side_t orientation, coord_t x, coord_t y, coord_t z)
{
	switch (orientation) {
	case e_north:
	case e_south:
	case e_east:
	case e_west:
	case e_down: {
		block_t b = plevel->get_world()->get_block(x, y, z, orientation);
		return !def::block_defs[b].transparent;
	}
	default:
		return false;
	}
}

bool common::block::is_placeable_ontop(game::level* plevel, coord_t x, coord_t y, coord_t z)
{
	block_t b = plevel->get_world()->get_block(x, y, z, e_down);
	return !def::block_defs[b].transparent;
}

bool common::block::is_placeable_plant(game::level* plevel, coord_t x, coord_t y, coord_t z)
{
	block_t b = plevel->get_world()->get_block(x, y, z, e_down);
	return b == e_block_grass || b == e_block_dirt;
}

