#pragma once

#include <common/types.hpp>


namespace game {

class level;

} // namespace game


namespace common {

namespace block {

bool is_placeable(game::level* plevel, block_t block, side_t orientation, coord_t x, coord_t y, coord_t z);

} // namespace block

} // namespace common
