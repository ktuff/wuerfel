#pragma once

#include <common/types.hpp>


namespace game {

class level;

} // namespace game


namespace common {

namespace state {

bool is_valid_state(game::level* plevel, state_t state, coord_t x, coord_t y, coord_t z);

} // namespace state

} // namespace common
