#include <common/level.hpp>
#include "state.hpp"


namespace common {

namespace state {

bool is_valid_state_circuit_repeater(state_t state);
bool is_valid_state_circuit_comparator(state_t state);
bool is_valid_state_water(state_t state);

} // namespace state

} // namespace common


bool common::state::is_valid_state(game::level* plevel, state_t state, coord_t x, coord_t y, coord_t z)
{
	block_t block = plevel->get_block(x, y, z);
	switch (block) {
	case e_block_circuit_repeater:
		return is_valid_state_circuit_repeater(state);
	case e_block_circuit_comparator:
		return is_valid_state_circuit_comparator(state);
	case e_block_water_flowing:
		return is_valid_state_water(state);
	default:
		return state == 0;
	}
	return true;
}

bool common::state::is_valid_state_circuit_repeater(state_t state)
{
	return state < 4;
}

bool common::state::is_valid_state_circuit_comparator(state_t state)
{
	return state < 2;
}

bool common::state::is_valid_state_water(state_t state)
{
	return state < 8;
}
