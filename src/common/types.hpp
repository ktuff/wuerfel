#pragma once

#include <stdint.h>
#include <functional>

#include <ktf/math/vec.hpp>


typedef int64_t coord_t;
typedef uint16_t block_t;
typedef uint8_t state_t;
typedef uint8_t power_t;

union light_t {
	struct {
		uint8_t sky : 4;
		uint8_t block : 4;
	};
	uint8_t value;
};


uint64_t const side_count = 7;

enum side_t : uint8_t {
	e_north = 0,
	e_west = 1,
	e_south = 2,
	e_east = 3,
	e_down = 4,
	e_up = 5,
	e_none = 6
};

/*************************\
 *      DIRECTIONS       *
 *************************
 * NORTH =>	( 0,  0, -1) *
 * WEST =>	(-1,  0,  0) *
 * SOUTH =>	( 0,  0,  1) *
 * EAST =>	( 1,  0,  0) *
 * DOWN =>	( 0, -1,  0) *
 * UP =>	( 0,  1,  0) *
 * NONE =>	( 0,  0,  0) *
\*************************/

side_t const all_sides[side_count] = {
	e_north,
	e_west,
	e_south,
	e_east,
	e_down,
	e_up,
	e_none
};

side_t const switched_sides[side_count] = {
	e_south,
	e_east,
	e_north,
	e_west,
	e_up,
	e_down,
	e_none
};

side_t const left_sides[side_count] = {
	e_west,
	e_south,
	e_east,
	e_north,
	e_down,
	e_up,
	e_none
};

side_t const right_sides[side_count] = {
	e_east,
	e_north,
	e_west,
	e_south,
	e_down,
	e_up,
	e_none
};

union transparency {
	struct {
		uint8_t north : 1;
		uint8_t west : 1;
		uint8_t south : 1;
		uint8_t east : 1;
		uint8_t down : 1;
		uint8_t up : 1;
		uint8_t _ : 2;
	};
	uint8_t value = 0x00;
};

typedef ktf::vec3<coord_t> pos_t;
typedef ktf::vec2<coord_t> ccoord_t;
