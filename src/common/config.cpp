#include <fstream>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "config.hpp"


common::config::config(
	const std::string& path,
	std::unordered_map<std::string, bool> const& defaults_bool,
	std::unordered_map<std::string, int> const& defaults_int,
	std::unordered_map<std::string, float> const& defaults_float,
	std::unordered_map<std::string, std::string> const& defaults_string
) : m_config_path(path)
{
	this->load();

	for (auto [key, value] : defaults_bool) {
		if (m_bool_values.find(key) == m_bool_values.end()) {
			this->m_bool_values[key] = value;
		}
	}
	for (auto [key, value] : defaults_int) {
		if (m_int_values.find(key) == m_int_values.end()) {
			this->m_int_values[key] = value;
		}
	}
	for (auto [key, value] : defaults_float) {
		if (m_float_values.find(key) == m_float_values.end()) {
			this->m_float_values[key] = value;
		}
	}
	for (auto [key, value] : defaults_string) {
		if (m_string_values.find(key) == m_string_values.end()) {
			this->m_string_values[key] = value;
		}
	}
}

common::config::~config()
{
	this->save();
}


bool common::config::get_bool(const std::string& id) const
{
	auto it = m_bool_values.find(id);
	if (it != m_bool_values.end()) {
		return it->second;
	} else {
		return false;
	}
}

int common::config::get_int(const std::string& id) const
{
	auto it = m_int_values.find(id);
	if (it != m_int_values.end()) {
		return it->second;
	} else {
		return 0;
	}
}

float common::config::get_float(const std::string& id) const
{
	auto it = m_float_values.find(id);
	if (it != m_float_values.end()) {
		return it->second;
	} else {
		return 0.0f;
	}
}

std::string common::config::get_string(const std::string& id) const
{
	auto it = m_string_values.find(id);
	if (it != m_string_values.end()) {
		return it->second;
	} else {
		return "";
	}
}

void common::config::set_bool(const std::string& id, bool value)
{
	this->m_bool_values[id] = value;
}

void common::config::set_int(const std::string& id, int value)
{
	this->m_int_values[id] = value;
}

void common::config::set_float(const std::string& id, float value)
{
	this->m_float_values[id] = value;
}

void common::config::set_string(const std::string& id, const std::string& value)
{
	this->m_string_values[id] = value;
}

void common::config::load()
{
	std::ifstream file;
	file.open(m_config_path);
	if (!file.is_open()) {
		return;
	}
	nlohmann::json data;
	file >> data;

	for (auto const& [key, value] : data.items()) {
		if (value.is_boolean()) {
			this->set_bool(key, value.get<bool>());
		} else if (value.is_number_integer()) {
			this->set_int(key, value.get<int>());
		} else if (value.is_number_float()) {
			this->set_float(key, value.get<float>());
		} else if (value.is_string()) {
			this->set_string(key, value.get<std::string>());
		} else {
			// invalid / not supported
		}
	}
}

void common::config::save()
{
	std::ofstream file;
	file.open(m_config_path);
	if (!file.is_open()) {
		return;
	}

	nlohmann::json data;
	for (auto it : m_bool_values) {
		data[it.first] = it.second;
	}
	for (auto it : m_int_values) {
		data[it.first] = it.second;
	}
	for (auto it : m_float_values) {
		data[it.first] = it.second;
	}
	for (auto it : m_string_values) {
		data[it.first] = it.second;
	}

	file << data;
	file.close();
}
