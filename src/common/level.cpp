#include "level.hpp"
#include "util/hitboxes.hpp"


game::level::level(bool is_server)
: m_open(false)
, m_seed(0)
, m_ticks(0)
, m_is_server(is_server)
, m_entities(&world)
, m_tile_entities(&world, &m_entities, is_server)
{
}

game::level::~level()
{
}


bool game::level::operator()() const
{
	return m_open;
}

void game::level::init(const std::string& name, uint64_t seed, uint64_t ticks, ::entity::id start_id)
{
	this->m_name = name;
	this->m_seed = seed;
	this->m_ticks = ticks;
	this->m_entities.set_next_id(start_id);
	this->m_open = true;
}

void game::level::reset()
{
	this->m_open = false;
    this->world.reset();
    this->m_entities.reset();
}

std::string game::level::get_name() const
{
	return m_name;
}

uint64_t game::level::get_seed() const
{
	return m_seed;
}

uint64_t game::level::get_ticks() const
{
	return m_ticks;
}

void game::level::next_tick()
{
	this->m_ticks += 1;
}

game::world* game::level::get_world() {
    return &world;
}

entity::core_system * game::level::get_entity_system()
{
	return &m_entities;
}

entity::entity * game::level::get_entity(::entity::id id)
{
    return m_entities.get_entity(id);
}

::entity::id game::level::get_tile_entity(coord_t x, coord_t y, coord_t z) const
{
	return m_tile_entities.get(x, y, z);
}

bool game::level::is_entity_in_water(::entity::id id)
{
	 auto* entity = m_entities.get_entity(id);

	 aabb entity_hitbox = entity->get_hitbox();
	 std::vector<aabb> boxes;
	 hitboxes::get_hitboxes(&world, boxes, entity_hitbox, e_block_water_source);
	 for (auto const& box : boxes) {
		 if (box.intersect(entity_hitbox) || entity_hitbox.intersect(box)) {
			 return true;
		 }
	 }
	 return false;
}

std::shared_ptr<game::chunk> game::level::get_chunk(coord_t x, coord_t z) const
{
    return world.get_chunk(x, z);
}

void game::level::put_chunk(coord_t x, coord_t z, const std::shared_ptr<game::chunk>& chunk)
{
    world.set_chunk(x, z, chunk);
}

void game::level::remove_chunk(coord_t x, coord_t z)
{
    this->world.remove_chunk(x, z);
}


uint16_t game::level::get_block(coord_t x, coord_t y, coord_t z) const
{
    return world.get_block(x, y, z);
}

uint16_t game::level::get_block(coord_t x, coord_t y, coord_t z, side_t side) const
{
    return world.get_block(x, y, z, side);
}

bool game::level::set_block(coord_t x, coord_t y, coord_t z, block_t val, side_t side)
{
    /*
    const aabb& block_aabb = lut::get_hitbox(x, y, z, block);
    for(auto& it : players) {
        // TODO check only entities in chunk(s)
        if(it->get_hitbox().intersect(block_aabb)) {
            return false;
        }
    }
    */
    return world.set_block(x, y, z, val, side);
}

side_t game::level::get_orientation(coord_t x, coord_t y, coord_t z) const
{
    return world.get_orientation(x, y, z);
}

side_t game::level::get_orientation(coord_t x, coord_t y, coord_t z, side_t side) const
{
    return world.get_orientation(x, y, z, side);
}

bool game::level::set_orientation(coord_t x, coord_t y, coord_t z, side_t val)
{
    return world.set_orientation(x, y, z, val);
}

state_t game::level::get_state(coord_t x, coord_t y, coord_t z) const
{
	return world.get_state(x, y, z);
}

state_t game::level::get_state(coord_t x, coord_t y, coord_t z, side_t side) const
{
	return world.get_state(x, y, z, side);
}

bool game::level::set_state(coord_t x, coord_t y, coord_t z, state_t val)
{
	return world.set_state(x, y, z, val);
}

power_t game::level::get_power(coord_t x, coord_t y, coord_t z) const
{
    return world.get_power(x, y, z);
}

power_t game::level::get_power(coord_t x, coord_t y, coord_t z, side_t side) const
{
    return world.get_power(x, y, z, side);
}

bool game::level::set_power(coord_t x, coord_t y, coord_t z, power_t val)
{
    return world.set_power(x, y, z, val);
}

light_t game::level::get_light(coord_t x, coord_t y, coord_t z) const
{
    return world.get_light(x, y, z);
}

light_t game::level::get_light(coord_t x, coord_t y, coord_t z, side_t side) const
{
    return world.get_light(x, y, z, side);
}

bool game::level::set_light(coord_t x, coord_t y, coord_t z, light_t val)
{
    return world.set_light(x, y, z, val);
}
