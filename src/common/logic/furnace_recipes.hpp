#pragma once


class furnace_recipes {

	private:
		static item::item_type_id s_results[item::num_item_types];
		static size_t s_cook_time[item::num_item_types];

	public:
		static void load();
		static item::item_type_id get_result(item::item_type_id raw);
		static size_t get_cook_time(item::item_type_id raw);
};
