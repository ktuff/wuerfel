#include <ktf/util/logger.hpp>
#include <pugixml.hpp>

#include <common/item/item.hpp>

#include "furnace_recipes.hpp"


item::item_type_id furnace_recipes::s_results[item::num_item_types];
size_t furnace_recipes::s_cook_time[item::num_item_types];

#include <iostream>
void furnace_recipes::load()
{
	for (size_t i=0; i<item::num_item_types; ++i) {
		furnace_recipes::s_results[i] = item::e_none;
		furnace_recipes::s_cook_time[i] = 0;
	}

	ktf::logger::info("Loading recipes...");
	std::string const path("assets/recipes.xml");

	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	if (!result) {
		ktf::logger::error("could not load: " + path);
		return;
	}

	auto find_item = [](std::string const& id) -> item::item_type const* {
		for (size_t x=0; x<item::num_item_types; ++x) {
			if (item::all_types[x]->name == id) {
				return item::all_types[x];
			}
		}
		return nullptr;
	};

	auto const& doc_root = doc.root();
	auto const& crafting = doc_root.child("furnace");
	auto const& recipes = crafting.child("recipes");
	for (auto const& recipe : recipes.children()) {
		if (recipe.name() != std::string("recipe")) {
			continue;
		}

		std::string recipe_result = recipe.attribute("id").as_string();
		unsigned int time = recipe.attribute("time").as_uint();
		auto const& ing = recipe.child("ingredient");
		std::string recipe_base = ing.attribute("id").as_string();
		item::item_type const * i1 = find_item(recipe_result);
		item::item_type const * i2 = find_item(recipe_base);
		if (i1 && i2) {
			std::cout << recipe_base << " -> " << recipe_result << std::endl;
			furnace_recipes::s_results[i2->id] = i1->id;
			furnace_recipes::s_cook_time[i2->id] = time;
		} else {
			ktf::logger::error("Invalid furnace recipe: " + recipe_base + " -> " + recipe_result);
		}
	}
}

item::item_type_id furnace_recipes::get_result(item::item_type_id raw)
{
	return s_results[raw];
}

size_t furnace_recipes::get_cook_time(item::item_type_id raw)
{
	return s_cook_time[raw];
}

