#include <filesystem>
#include <ktf/opengl/framebuffer.hpp>
#include <ktf/opengl/gl.hpp>
#include <ktf/util/logger.hpp>

#include "constants.hpp"
#include "gui/game.hpp"
#include "gui/loading.hpp"
#include "gui/multiplayer.hpp"
#include "gui/paused.hpp"
#include "gui/singleplayer.hpp"
#include "gui/title.hpp"
#include "program.hpp"
#include "rendering/renderer.hpp"
#include "soundsystem.hpp"
#include "util.hpp"


program::program(int width, int height, bool fullscreen)
: state(state_menu)
, ipt(this, &m_resources, &m_renderer, &m_game)
, m_resources(window.get_composer())
, m_game()
, m_renderer(&window, &m_resources, m_game.get_level())
, server(false)
, remote(false)
{
	this->init(width, height, fullscreen);
	this->run();
}

program::~program()
{
	this->destroy();
}


void program::run()
{
	while (state != state_quit) {
		switch (state) {
		case state_waiting:
			this->loop_waiting();
			break;
		case state_menu:
			this->loop_menu();
			break;
		case state_loading:
			this->loop_loading();
			break;
		case state_game:
			this->loop_game();
			break;
		case state_paused:
			this->loop_paused();
			break;
		case state_quit:
			break;
		}
	}
}

void program::set_state(program_states state)
{
	this->state = state;
}

void program::set_screen(std::shared_ptr<client::gui> screen)
{
	std::shared_ptr<client::gui> prev_screen = m_gui;

	int width, height;
	this->m_gui = screen;
	this->m_gui->load();
	ktf::ui::gui* ui = m_gui->get_node_tree();
	ktf::gl::get_viewport_size(&width, &height);
	ui->resize(width, height);
	this->ipt.set_gui(screen);
	this->m_renderer.set_gui(screen);

	if (dynamic_cast<client::game_gui*>(screen.get())) {
		this->window.disable_cursor();
		this->ipt.reset_keys();
	} else {
		this->window.enable_cursor();
		if (dynamic_cast<client::game_gui*>(prev_screen.get())) {
			this->window.reset_cursor();
		}
	}
}

void program::toggle_fullscreen()
{
	this->window.toggle_fullscreen();
}

void program::screenshot()
{
	std::string folder = "screenshots/";
	if (!std::filesystem::exists(folder)) {
		std::filesystem::create_directory(folder);
	}
	std::string filepath = folder + std::string("screenshot_") + client::get_date() + std::string(".png");

	int w, h, c;
	std::vector<uint8_t> pixel_data;
	ktf::framebuffer framebuffer(0); // use default framebuffer
	framebuffer.read(pixel_data, w, h, c);
	ktf::png texture(w, h, c, pixel_data.data());
	texture.write(filepath);

	ktf::logger::info("Saved Screenshot: \"" + filepath + "\"");
}

void program::reload()
{
	client::soundsystem::stop();
	client::soundsystem::clear();
	client::soundsystem::start();

	this->m_resources.load();
	this->m_renderer.reload();
	int width, height;
	ktf::gl::get_viewport_size(&width, &height);
	if (m_gui) {
		this->m_gui->load(); // TODO not working
		m_gui->get_node_tree()->resize(width, height);
	}
	this->m_renderer.on_resize(width, height);
}

void program::start_local_game(std::string const& level_name)
{
	this->remote = false;
	this->server.halt();
	this->server.start(level_name, 0, [&](int port) {
		this->connect("127.0.0.1", port);
	});
}

void program::start_remote_game(std::string const& ip, int port)
{
	this->remote = true;

	this->connect(ip, port);
}

void program::leave_game()
{
	this->set_state(state_menu);
}

void program::init(int width, int height, bool fullscreen)
{
	bool success = window.create(width, height, fullscreen, 3, 3);
	if (!success) {
		ktf::logger::error("An error occured during window creation");
		return;
	}
	window.set_title(constants::window_title);
	window.set_icon(constants::icon_path);

	// register events
	this->window.on<ktf::event_size>([&](ktf::event* e) {
		auto* we = static_cast<ktf::window_event*>(e);
		ktf::gl::set_viewport_size(we->width, we->height);
		ktf::ui::gui* ui = m_gui->get_node_tree();
		ui->resize(we->width, we->height);
		this->m_renderer.on_resize(we->width, we->height);
	});
	this->window.on<ktf::event_focus>([&](ktf::event* e) {
		if (m_gui) {
			this->m_gui->get_node_tree()->on_focus_event(e);
		}
	});
	this->window.on<ktf::event_drop>([&](ktf::event* e) {
		auto* de = static_cast<ktf::drop_event*>(e);
		ktf::logger::info("Dropped: ");
		for (std::string const& path : de->paths) {
			ktf::logger::info(path);
		}
		if (m_gui) {
			this->m_gui->get_node_tree()->on_drop_event(e);
		}
	});
	this->window.on<ktf::event_key>([&](ktf::event* e) {
		auto* ke = static_cast<ktf::key_event*>(e);
		this->ipt.process_key(ke->key, ke->scancode, ke->action, ke->mods);
		if (m_gui) {
			this->m_gui->get_node_tree()->on_key_event(e);
		}
	});
	this->window.on<ktf::event_char>([&](ktf::event* e) {
		auto* ke = static_cast<ktf::key_event*>(e);
		this->ipt.process_char(ke->codepoint);
		if (m_gui) {
			this->m_gui->get_node_tree()->on_char_event(e);
		}
	});
	this->window.on<ktf::event_button>([&](ktf::event* e) {
		auto* me = static_cast<ktf::mouse_event*>(e);
		this->ipt.process_mouse_button(me->button, me->action, me->mods);
		if (m_gui) {
			this->m_gui->get_node_tree()->on_click_event(e);
		}
	});
	this->window.on<ktf::event_position>([&](ktf::event* e) {
		auto* me = static_cast<ktf::mouse_event*>(e);
		this->ipt.process_mouse_position(me->x, me->y);
		if (m_gui) {
			this->m_gui->get_node_tree()->on_cursor_pos_event(e);
		}
	});
	this->window.on<ktf::event_scroll>([&](ktf::event* e) {
		auto* me = static_cast<ktf::mouse_event*>(e);
		this->ipt.process_mouse_scroll(me->offset_x, me->offset_y);
		if (m_gui) {
			this->m_gui->get_node_tree()->on_scroll_event(e);
		}
	});

	// link program
	this->m_renderer.init();
	client::soundsystem::create();
	client::soundsystem::start();

	this->reload();

	this->set_screen(std::make_shared<client::title_screen>(this, &m_resources));
}

void program::destroy()
{
	ktf::logger::info("Closing program.");
	client::soundsystem::stop();
	client::soundsystem::destroy();
	this->m_resources.unload();
	this->m_renderer.destroy();
	this->window.destroy();
	ktf::logger::info("Quit.");
}

void program::events(double dt)
{
	this->window.poll();
	this->ipt.process(dt);
	this->m_game.tick(dt);
	if (window.should_close()) {
		this->state = state_quit;
	}

	static int frames = 0;
	frames++;

	static std::chrono::milliseconds last = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::milliseconds now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
	if ((now - last).count() >= 1000) {
		float cpu = info.get_cpu_usage();
		long long mem = info.get_proc_phys_mem();
		ktf::logger::info("CPU: " + std::to_string(cpu) + "% Mem: " + std::to_string(mem / 1024) + "MB FPS: " + std::to_string(frames));
		last = now;
		frames = 0;
	}
}

void program::loop_waiting()
{
	// init state
	std::chrono::microseconds prev = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	while (state == state_waiting) {
		std::chrono::microseconds now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		double dt = (now - prev).count() / 1.0e6;
		this->events(dt);
		this->m_renderer.render(dt);
	}
	// end state

	// TODO remove / change
}
    
void program::loop_menu()
{
	// init state

	std::chrono::microseconds prev = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	while (state == state_menu) {
		std::chrono::microseconds now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		double dt = (now - prev).count() / 1.0e6;
		this->events(dt);
		this->m_renderer.render(dt);
	}
	// end state
}

void program::loop_loading()
{
	// show loading screen
	this->set_screen(std::make_shared<client::loading_screen>(this, &m_resources));

	std::chrono::milliseconds timeout(remote ? 30000 : 60000);
	std::chrono::microseconds now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::microseconds start = now;
	std::chrono::microseconds prev = now;
	while (state == state_loading) {
		now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		double dt = (now - prev).count() / 1.0e6;
		prev = now;
		this->events(dt);
		this->m_renderer.render(dt);

		// TODO track progress

		if (m_game.is_open()) {
			this->set_state(state_game);
			return;
		} else if ((now - start) > timeout) {
			break;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	ktf::logger::error("could not start game");
	if (remote) {
		this->set_screen(std::make_shared<client::multiplayer_gui>(this, &m_resources));
	} else {
		this->server.halt();
		this->set_screen(std::make_shared<client::singleplayer_gui>(this, &m_resources));
	}
	this->set_state(state_menu);
}
    
void program::loop_game()
{
	// init
	{
		::entity::id target = m_game.get_target();
		auto* level = m_game.get_level();
		this->m_renderer.make_context(target);
		this->set_screen(std::make_shared<client::game_gui>(this, &m_resources, level->get_entity_system(), target));

		this->ipt.init_game();
		this->m_renderer.display_level(true);
	}

	// main loop
	std::chrono::microseconds start = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	std::chrono::microseconds prev = start;
	std::chrono::microseconds tick_duration(16666);
	while (state == state_game) {
		std::chrono::microseconds now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		double dt = (now - prev).count() / 1.0e6;

		this->events(dt);
		this->m_renderer.update_camera();
		this->m_renderer.render(dt);

		prev = now;
		now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		std::this_thread::sleep_for(tick_duration - (now - prev));
	}

	// end state
	this->m_renderer.display_level(false);
	this->m_game.stop();
	this->ipt.close_game();
	this->m_renderer.reset();
	if (!remote) {
		this->server.halt();
	}

	this->m_renderer.make_context(::entity::id(0));
}
    
void program::loop_paused()
{
	// init state
	std::chrono::microseconds prev = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
	while (state == state_paused) {
		std::chrono::microseconds now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
		double dt = (now - prev).count() / 1.0e6;

		this->events(dt);
		this->m_renderer.render(dt);
	}

	// TODO remove / change
	// currently not used
	// end state
}

void program::connect(std::string const& ip, int port)
{
	ktf::logger::info("Connecting to " + ip + ":" + std::to_string(port));
	this->m_game.start(ip.c_str(), port, 60000);
	this->set_state(state_loading);
}
