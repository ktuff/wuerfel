#pragma once

#include <server/server.hpp>
#include <ktf/opengl/window.hpp>
#include <ktf/util/proc_info.hpp>

#include "input.hpp"
#include "game/game.hpp"
#include "rendering/renderer.hpp"

namespace client {

class gui;

} // namespace client


enum program_states {
	state_waiting,
	state_menu,
	state_loading,
	state_game,
	state_paused,
	state_quit
};


class program {

	private:
		ktf::window window;
		program_states state;
		input ipt;
		std::shared_ptr<client::gui> m_gui;
		resources m_resources;
		game::game m_game;
		renderer m_renderer;
		server::server_instance server;
		bool remote;

		ktf::proc_info info;



	public:
		program(int width, int height, bool fullscreen);
		~program();


	public:
		void run();
		void set_state(program_states state);
		void set_screen(std::shared_ptr<client::gui> screen);
		void toggle_fullscreen();
		void screenshot();
		void reload();
		void start_local_game(std::string const& level_name);
		void start_remote_game(std::string const& ip, int port);
		void leave_game();

	private:
		void init(int width, int height, bool fullscreen);
		void destroy();

		void events(double dt);
		void loop_waiting();
		void loop_menu();
		void loop_loading();
		void loop_game();
		void loop_paused();

		void connect(std::string const& ip, int port);
};
