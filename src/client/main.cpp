#include <omp.h>

#include <ktf/util/logger.hpp>

#include <client/config.hpp>

#include "program.hpp"


int main(int argc, char **argv)
{
	// start logging
	ktf::logger::open("/tmp/wuerfel-log.txt");

	int width = 1080;
	int height = width * 9.0f/16.0f;
	bool fullscreen = false;
	int num_threads = -1;

	{
		std::vector<std::string> arguments;
		for (int i=1; i<argc; i++) {
			arguments.push_back(argv[i]);
		}

		for (unsigned int i=0; i<arguments.size(); i++) {
			std::string& arg = arguments[i];
			if (arg == "-j") {
				try {
					num_threads = std::stoi(arguments.at(++i));
				} catch (std::exception const& e) {
					ktf::logger::error("Invalid parameter after '-j'");
					::exit(1);
				}
			} else if (arg == "--width") {
				try {
					width = std::stoi(arguments.at(++i));
				} catch (std::exception const& e) {
					ktf::logger::error("Invalid parameter after '-j'");
					::exit(1);
				}
			} else if (arg == "--height") {
				try {
					height = std::stoi(arguments.at(++i));
				} catch (std::exception const& e) {
					ktf::logger::error("Invalid parameter after '-j'");
					::exit(1);
				}
			} else if (arg == "--fullscreen") {
				fullscreen = true;
			}
		}
	}

	int av_threads = omp_get_max_threads();
	if (num_threads <= 0 || av_threads < num_threads) {
		num_threads = av_threads;
	}
	client::config.set_int("num_threads", num_threads);
	ktf::logger::info("Thread count: " + std::to_string(num_threads));

	program instance(width, height, fullscreen);

	// stop logging
	ktf::logger::close();

	return 0;
}
