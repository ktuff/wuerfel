#include "constants.hpp"

namespace constants {
    const char* working_directory = "";
    const char* window_title = "Untitled block game";
    const char* icon_path = "assets/icon.png";
    const long network_timeout = 30000;
};
