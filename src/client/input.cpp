#include <GLFW/glfw3.h>
#include <ktf/math/radians.hpp>
#include <ktf/opengl/gl.hpp>
#include <ktf/util/logger.hpp>

#include <common/config.hpp>
#include <common/entity/systems/target_system.hpp>
#include <common/item/inventory.hpp>
#include <common/world/world.hpp>

#include "game/game.hpp"
#include "gui/furnace.hpp"
#include "gui/game.hpp"
#include "gui/workbench.hpp"
#include "rendering/renderer.hpp"
#include "input.hpp"
#include "program.hpp"


input::input(program* prog, resources* res, renderer* r, game::game* game)
: m_program(prog)
, m_resources(res)
, m_renderer(r)
, game(game)
, m_level(game->get_level())
, m_target(0)
, cursor_x(0)
, cursor_y(0)
, wireframe_mode(false)
, m_speed(1.0)
{
	// register global key events
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F1, GLFW_RELEASE, GLFW_MOD_ALT).value, [this]() {
		this->m_renderer->toggle_ui();
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F4, GLFW_RELEASE, GLFW_MOD_ALT).value, [this]() {
		this->m_program->set_state(state_quit);
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F5, GLFW_RELEASE, 0).value, [this]() {
		this->m_program->reload();
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F6, GLFW_RELEASE, 0).value, [this]() {
		this->m_renderer->toggle_viewer_mode();
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F7, GLFW_RELEASE, 0).value, [this]() {
		this->wireframe_mode = !wireframe_mode;
			if (wireframe_mode) {
				ktf::gl::use_polygon_lines();
			} else {
				ktf::gl::use_polygon_fill();
			}
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F8, GLFW_RELEASE, 0).value, [this]() {
		this->m_program->screenshot();
	});
	this->m_key_map.emplace(ktf::ui::keyt(GLFW_KEY_F11, GLFW_RELEASE, 0).value, [this]() {
		this->m_program->toggle_fullscreen();
	});

	this->reset_keys();
}

input::~input()
{
}


void input::init_game()
{
	this->m_target = game->get_target();
}

void input::close_game()
{
	this->m_target = 0;
}

void input::set_gui(std::weak_ptr<client::gui> ui)
{
	this->m_gui = ui;
}

void input::process_key(int key, int, int action, int mods)
{
	if (action == GLFW_RELEASE) {
		uint64_t key_event = ktf::ui::keyt(key, action, mods).value;
		if (m_key_map.find(key_event) != m_key_map.end()) {
			this->m_key_map.at(key_event)();
		}
	}

	if (action == GLFW_PRESS) {
		switch(key) {
		case GLFW_KEY_W:
			this->keys[0] = true;
			break;
		case GLFW_KEY_S:
			this->keys[1] = true;
			break;
		case GLFW_KEY_A:
			this->keys[2] = true;
			break;
		case GLFW_KEY_D:
			this->keys[3] = true;
			break;
		case GLFW_KEY_SPACE:
			this->keys[4] = true;
			break;
		case GLFW_KEY_LEFT_SHIFT:
			this->keys[5] = true;
			break;
		case GLFW_KEY_LEFT_CONTROL:
			this->keys[6] = true;
			break;
		case GLFW_KEY_E:
			this->reset_keys();
			break;
		case GLFW_KEY_ESCAPE:
			this->reset_keys();
			break;
		case GLFW_KEY_KP_SUBTRACT:
			this->m_speed -= 0.1;
			break;
		case GLFW_KEY_KP_ADD:
			this->m_speed += 0.1;
			break;
		}
	} else if (action == GLFW_RELEASE) {
		switch(key) {
		case GLFW_KEY_W:
			this->keys[0] = false;
			break;
		case GLFW_KEY_S:
			this->keys[1] = false;
			break;
		case GLFW_KEY_A:
			this->keys[2] = false;
			break;
		case GLFW_KEY_D:
			this->keys[3] = false;
			break;
		case GLFW_KEY_SPACE:
			this->keys[4] = false;
			break;
		case GLFW_KEY_LEFT_SHIFT:
			this->keys[5] = false;
			break;
		case GLFW_KEY_LEFT_CONTROL:
			this->keys[6] = false;
			break;
		}
	}
}

void input::process_char(unsigned int)
{
}

void input::process_mouse_button(int button, int action, int mods)
{
	auto ui = m_gui.lock();
	if (!dynamic_cast<client::game_gui*>(ui.get())) {
		return;
	}
	auto* entity = m_level->get_entity(m_target);
	if (!entity) {
		return;
	}

	switch (button) {
	case GLFW_MOUSE_BUTTON_LEFT: {
		if (action == GLFW_PRESS) {
			this->start_destroying();
		} else if (action == GLFW_RELEASE) {
			this->stop_destroying();
		}
	}	break;
	case GLFW_MOUSE_BUTTON_RIGHT: {
		if (action == GLFW_PRESS) {
			this->start_interact((mods & GLFW_MOD_SHIFT));
		} else if (action == GLFW_RELEASE) {
			this->stop_interact();
		}
	}	break;
	case GLFW_MOUSE_BUTTON_MIDDLE: {
		if (action == GLFW_PRESS) {
		} else if (action == GLFW_RELEASE) {
			this->pick_block();
		}
	}	break;
	}
}

void input::process_mouse_position(double x_pos, double y_pos)
{
	// cursor position is in [-0.5, 0.5] space
	double x = x_pos;
	double y = y_pos;
	auto ui = m_gui.lock();
	if (dynamic_cast<client::game_gui*>(ui.get())) {
		double sensitivity = 0.05;
		double diff_x = (x - cursor_x) * sensitivity;
		double diff_y = (cursor_y - y) * sensitivity;

		auto* entity = m_level->get_entity(m_target);
		if (entity) {
			auto rotation = entity->get_rotation();
			rotation.x += diff_y;
			rotation.y += diff_x;
			entity->set_rotation(rotation);
		}
	}
	this->cursor_x = x;
	this->cursor_y = y;
}

void input::process_mouse_scroll(double, double y_offset)
{
	auto ui = m_gui.lock();
	if (dynamic_cast<client::game_gui*>(ui.get())) {
		auto* entity = m_level->get_entity(m_target);
		if (!entity) {
			return;
		}
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_level->get_entity_system()->get_module(::entity::inventory_system::s_id));
		int index = inv->get_selected_index(m_target);
		index -= (int)y_offset;
		while (index < 0) {
			index += 9;
		}
		while (index >= 9) {
			index -= 9;
		}
		inv->set_selected_index(m_target, index);
	}
}

void input::process(double)
{
	auto ui = m_gui.lock();
	if (dynamic_cast<client::game_gui*>(ui.get())) {
		auto* entity = m_level->get_entity(m_target);
		if (!entity) {
			return;
		}

		ktf::vec3<double> acc = ktf::vec3<double>(0.0);
		ktf::vec3<double> vel = entity->get_velocity();
		ktf::vec3<double> rot = entity->get_rotation();

		double const base_speed = 40.0;
		if (keys[0]) {
			acc.x += base_speed;
		}
		if (keys[1]) {
			acc.x -= base_speed;
		}
		if (keys[2]) {
			acc.z -= base_speed;
		}
		if (keys[3]) {
			acc.z += base_speed;
		}

		double speed;
		if (keys[5]) {
			speed = m_speed * 0.5;
		} else if (keys[6]) {
			speed = m_speed * 1.4;
		} else {
			speed = m_speed;
		}

		acc.y += -9.81 * 3;
		if (keys[4]) {
			if (entity->onground()) {
				vel.y = acc.y = 8.5;
			}
		}

		double sin = std::sin(ktf::radians(rot.y));
		double cos = std::cos(ktf::radians(rot.y));
		double xo = (acc.x * cos - acc.z * sin) * speed;
		double zo = (acc.z * cos + acc.x * sin) * speed;
		acc.x = xo;
		acc.z = zo;

		entity->set_acceleration(acc);
		entity->set_velocity(vel);
	}
}

void input::reset_keys()
{
	for (int k=0; k<NUM_KEYS; k++) {
		this->keys[k] = false;
	}
}

void input::start_destroying()
{
	auto* client = game->get_client();
	auto* entity_system = m_level->get_entity_system();
	auto* targets = dynamic_cast<::entity::target_system*>(entity_system->get_module(::entity::target_system::s_id));
	auto target_block = targets->get_target(m_target);
	if (target_block.type != ::entity::e_target_none) {
		coord_t x = target_block.position.x;
		coord_t y = target_block.position.y;
		coord_t z = target_block.position.z;
		client->send_set_block(x, y, z, e_block_air, e_none);
	}
}

void input::stop_destroying()
{
	// TODO
}

void input::start_interact(bool alt)
{
	auto* entity_system = m_level->get_entity_system();
	auto* targets = dynamic_cast<::entity::target_system*>(entity_system->get_module(::entity::target_system::s_id));
	auto target_block = targets->get_target(m_target);
	if (target_block.type == ::entity::e_target_none) {
		return;
	}

	auto* client = game->get_client();
	coord_t x = target_block.position.x;
	coord_t y = target_block.position.y;
	coord_t z = target_block.position.z;

	auto block = m_level->get_block(x, y, z);
	if (def::block_defs[block].tile && !alt) {
		::entity::id tile_entity = m_level->get_tile_entity(x, y, z);
		if (tile_entity != 0) {
			auto* entities = m_level->get_entity_system();
			auto* entity = entities->get_entity(tile_entity);
			auto* inv = dynamic_cast<::entity::inventory_system*>(m_level->get_entity_system()->get_module(::entity::inventory_system::s_id));
			switch (entity->m_type) {
			case ::entity::type::e_chest: {
				// TODO send over extra request?
				inv->set_container_id(m_target, tile_entity);
				// TODO add chest gui
				// this->m_program->set_screen(std::make_shared<client::chest_gui>(m_program, m_resources, entity_system, target, tile_entity));
				break;
			}
			case ::entity::type::e_furnace: {
				// TODO send over extra request?
				inv->set_container_id(m_target, tile_entity);
				this->m_program->set_screen(std::make_shared<client::furnace_gui>(m_program, m_resources, entity_system, m_target, tile_entity));
				break;
			}
			case ::entity::type::e_workbench: {
				// TODO send over extra request?
				inv->set_container_id(m_target, tile_entity);
				this->m_program->set_screen(std::make_shared<client::workbench_gui>(m_program, m_resources, entity_system, m_target, tile_entity));
				break;
			}
			default:
				break;
			}
		}
	} else if (def::block_defs[block].usable && !alt) {
		state_t state = m_level->get_state(x, y, z);
		state = (state + 1) % def::block_defs[block].states;
		client->send_set_state(x, y, z, state);
	} else {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_level->get_entity_system()->get_module(::entity::inventory_system::s_id));
		item::item_stack const& i = inv->get_item(m_target, ::entity::inventory_group::e_items, inv->get_selected_index(m_target));
		block_t block = item::get_block(i.type);
		game::world::apply_side(x, y, z, target_block.side);
		side_t side = switched_sides[target_block.side];
		client->send_set_block(x, y, z, block, side);
	}
}

void input::stop_interact()
{
	// TODO
	// if (item.consumeable) {
	// 	item.consume();
	// } else {
	// 	// interact with block / entity
	// }
}

void input::pick_block()
{
	auto* entity_system = m_level->get_entity_system();
	auto* entity = entity_system->get_entity(m_target);
	if (entity == nullptr) {
		return;
	}

	auto* targets = dynamic_cast<::entity::target_system*>(entity_system->get_module(::entity::target_system::s_id));
	auto target_block = targets->get_target(m_target);
	if (target_block.type != ::entity::e_target_none) {
		coord_t x = target_block.position.x;
		coord_t y = target_block.position.y;
		coord_t z = target_block.position.z;
		auto* world = m_level->get_world();
		block_t block = world->get_block(x, y, z);
		item::item_type_id type = item::get_type(block);
		item::item_stack block_item(type, 1);
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_level->get_entity_system()->get_module(::entity::inventory_system::s_id));
		inv->set_item(entity->m_id, ::entity::inventory_group::e_items, inv->get_selected_index(entity->m_id), block_item);
	}
}
