#pragma once

#include <ktf/sound/soundsystem.hpp>

#include <common/world/world_listener.hpp>
#include <ktf/math/vec3.hpp>

namespace game {

class world;

} // namespace game


namespace client {

class soundsystem final : public world_listener {

	private:
		static soundsystem instance;

	private:
		ktf::soundsystem system;
		game::world* world;


	public:
		static void create();
		static void destroy();
		static void start();
		static void stop();
		static void clear();

		static void register_world(game::world* world);
		static void unregister_world(game::world* world);
		static void update_listener(const ktf::vec3<double>& pos, const ktf::vec3<double>& dir, const ktf::vec3<double>& up);

		static void play_sound();
		static void stop_sound();


	public:
		void on_destruction() override;
		void on_chunk_add(coord_t x, coord_t z) override;
		void on_chunk_remove(coord_t x, coord_t z) override;
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block) override;
		void on_rotation_change(coord_t x, coord_t y, coord_t z, side_t value) override;
		void on_power_change(coord_t x, coord_t y, coord_t z, power_t value) override;
		void on_light_change(coord_t x, coord_t y, coord_t z, light_t value) override;

};

} // namespace client
