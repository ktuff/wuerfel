#pragma once

#include <chrono>
#include <queue>

#include <client/entity/network.hpp>
#include <common/level.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/entity/systems/target_system.hpp>

#include "packager.hpp"


namespace game {

class game {

	private:
		::game::level m_level;
		client::packager m_packager;
		bool m_open;
		::entity::id target;
		ktf::vec2<int64_t> old_pos;

		::entity::inventory_system m_inventory_system;
		client::entity::network_system m_entity_network;
		::entity::target_system m_target_system;


	public:
		game();
		~game();


    public:
		::game::level* get_level();
		client::packager* get_client();
		::entity::id get_target() const;
		bool is_open() const;
		void start(const char* ip, int port, int timeout);
		void leave();
		void stop();
		void tick(double dt);

		void on_ready(::entity::id id);
		void on_leave();

};

} // namespace game
