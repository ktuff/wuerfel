#pragma once

#include <vector>

#include <common/net/rsa_key.hpp>


namespace client {

class packager;


class authentication {

	private:
		client::packager* m_network;
		rsa_key m_rsa_client;
		rsa_key m_rsa_server;


	public:
		authentication(client::packager* network);
		~authentication();


	public:
		void login(std::string const& username);
		void solve_challenge(std::string const& public_key_b64, std::string const& challenge_b64);

};

} // namespace client
