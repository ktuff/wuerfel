#include <common/net/base64.hpp>

#include "authentication.hpp"
#include "packager.hpp"


client::authentication::authentication(client::packager* network)
: m_network(network)
{
	std::string private_key_path("client_pk.pem");
	std::string public_key_path("client_pbk.pem");
	this->m_rsa_client = rsa_key::load_from_file(private_key_path, public_key_path);
	if (!m_rsa_client()) {
		rsa_key key = rsa_key::generate(4096, private_key_path, public_key_path);
		if (key()) {
			this->m_rsa_client = std::move(key);
		} else {
			throw std::runtime_error("Could not create RSA keys for client!");
		}
	}
}

client::authentication::~authentication()
{
}


void client::authentication::login(std::string const& username)
{
	std::vector<char> public_key = m_rsa_client.get_public_key();
	std::vector<char> encoded_key;
	base64::encode(public_key, encoded_key);
	this->m_network->send_auth_init(username, std::string(encoded_key.data(), encoded_key.size()));
}

void client::authentication::solve_challenge(std::string const& public_key_b64, std::string const& challenge_b64)
{
	size_t length = 0;
	std::vector<char> server_pb_key(base64::decode_bound(public_key_b64.size()));
	length = base64::decode((unsigned char*)public_key_b64.data(), public_key_b64.size(), (unsigned char*)server_pb_key.data());
	server_pb_key.resize(length);

	this->m_rsa_server = rsa_key::load_from_buffer({}, server_pb_key);
	if (!m_rsa_server()) {
		// TODO abort login
		return;
	}

	std::vector<unsigned char> challenge(base64::decode_bound(challenge_b64.size()));
	length = base64::decode((unsigned char*)challenge_b64.data(), challenge_b64.size(), challenge.data());
	challenge.resize(length);

	std::vector<unsigned char> message;
	if (!m_rsa_client.decrypt(challenge, message)) {
		// TODO abort login
		return;
	}
	std::vector<unsigned char> reply;
	if (!m_rsa_server.encrypt(message, reply)) {
		// TODO abort login
		return;
	}

	std::vector<unsigned char> encoded_reply;
	base64::encode(reply, encoded_reply);
	this->m_network->send_auth_reply(std::string((char*)encoded_reply.data(), encoded_reply.size()));
}
