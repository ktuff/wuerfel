#include <ktf/util/logger.hpp>

#include <client/constants.hpp>
#include <client/entity/network.hpp>
#include <common/level.hpp>
#include <common/net/packet_handler.hpp>
#include <common/net/types.hpp>
#include <common/util/chunk_compression.hpp>

#include "packager.hpp"
#include "game.hpp"


client::packager::packager(game::game* observer, game::level* level)
: m_auth(this)
, m_observer(observer)
, m_level(level)
, m_target(0)
{
	this->m_packet_batch = nlohmann::json::array();
	this->m_socket.set_timeout_limit(60000);
	this->m_socket.set_callbacks(
		[this]() {
			ktf::logger::info("Connected");
			this->m_auth.login("user"); // TODO get username from config / other input
		},
		[this](tcp_connection_close_status code) {
			ktf::logger::info("Disconnected with code " + std::to_string(code));
			if (this->m_observer) {
				this->m_target = ::entity::id(0);
				this->m_observer->on_leave();
			}
		}
	);
}

client::packager::~packager()
{
}


void client::packager::connect(const char* ip, int port, int timeout)
{
	this->m_socket.set_timeout_limit(timeout);
	this->m_socket.connect(ip, port);
}

void client::packager::disconnect()
{
	this->m_socket.disconnect(tcp_closed_normal);
}

bool client::packager::is_connected()
{
    return m_socket.is_connected();
}

void client::packager::tick()
{
	if (!is_connected()) {
		return;
	}

	this->m_socket.poll([this](tcp_packet const& packet) {
		auto* m = m_level->get_entity_system()->get_module(client::entity::network_system::s_id);
		auto* network_system = dynamic_cast<client::entity::network_system*>(m);

		auto decompressed_data = ::decompress(packet.m_data.get(), packet.m_size);
		nlohmann::json packet_batch = nlohmann::json::parse(std::string(decompressed_data.first.get(), decompressed_data.second));
		for (nlohmann::json const& p : packet_batch) {
			packet_emitter_type op_code = p["emitter"].get<packet_emitter_type>();
			auto const& packet_data = p["data"];

			switch (op_code) {
			case e_op_auth:
				this->process_auth_packet(packet_data);
				break;
			case e_op_general:
				this->process_general_packet(packet_data);
				break;
			case e_op_entity: {
				network_system->process_packet(packet_data);
				break;
			}
			case e_op_none:
				break;
			}
		}
	});

	std::string data = m_packet_batch.dump();
	auto compressed_data = ::compress(data.data(), data.size());
	tcp_packet packet(compressed_data.first, compressed_data.second);
	this->m_socket.send(packet);
	this->m_packet_batch = nlohmann::json::array();
}

void client::packager::send_auth_init(const std::string& username, const std::string& pbk_b64)
{
	packets::packet_t data;
	data["packetType"] = packet_type::packet_auth_login;
	data["user"] = username;
	data["key"] = pbk_b64;

	this->push_auth_packet(data);
}

void client::packager::send_auth_reply(const std::string& challenge_b64)
{
	packets::packet_t data;
	data["packetType"] = packet_type::packet_auth_response;
	data["reply"] = challenge_b64;

	this->push_auth_packet(data);
}

void client::packager::send_set_block(coord_t x, coord_t y, coord_t z, block_t block, side_t side)
{
    auto data = packets::s_set_block(x, y, z, block, side);
	this->push_general_packet(data);
}

void client::packager::send_set_rotation(coord_t x, coord_t y, coord_t z, side_t value)
{
    auto data = packets::s_set_rotation(x, y, z, value);
	this->push_general_packet(data);
}

void client::packager::send_set_state(coord_t x, coord_t y, coord_t z, state_t value)
{
	auto data = packets::s_set_state(x, y, z, value);
	this->push_general_packet(data);
}

void client::packager::send_set_power(coord_t x, coord_t y, coord_t z, power_t value)
{
	auto data = packets::s_set_power(x, y, z, value);
	this->push_general_packet(data);
}

void client::packager::send_player_pos(const ktf::vec3<double>& pos)
{
	auto data = packets::s_player_pos(m_target, pos);
	this->push_general_packet(data);
}

void client::packager::send_player_rot(const ktf::vec3<double>& rot)
{
	auto data = packets::s_player_rot(m_target, rot);
	this->push_general_packet(data);
}

void client::packager::push_auth_packet(const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_auth;
	packet["data"] = data;
	this->enqueue_packet(packet);
}

void client::packager::push_general_packet(const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_general;
	packet["data"] = data;
	this->enqueue_packet(packet);
}

void client::packager::push_entity_packet(const packets::packet_t& data)
{
	packets::packet_t packet = nlohmann::json::object();
	packet["emitter"] = packet_emitter_type::e_op_entity;
	packet["data"] = data;
	this->enqueue_packet(packet);
}

void client::packager::enqueue_packet(const packets::packet_t& packet)
{
	this->m_packet_batch += packet;
}

void client::packager::process_auth_packet(const packets::packet_t& data)
{
	packet_type type = data["packetType"].get<packet_type>();
	switch (type) {
	case packet_auth_challenge:
		this->recv_auth_challenge(data);
		break;
	case packet_auth_result:
		this->recv_auth_result(data);
		break;
	default:
		// TODO error
		break;
	}
}


void client::packager::process_general_packet(const packets::packet_t& data)
{
	packet_type type = data["packetType"].get<packet_type>();
	switch (type) {
	case packet_chunks_loaded:
		this->recv_chunks_loaded(data);
		break;
	case packet_chunk_add:
		this->recv_chunk_add(data);
		break;
	case packet_chunk_remove:
		this->recv_chunk_remove(data);
		break;
	case packet_entity_batch:
		this->recv_entity_batch(data);
		break;
	case packet_block:
		this->recv_set_block(data);
		break;
	case packet_rotation:
		this->recv_set_rotation(data);
		break;
	case packet_state:
		this->recv_set_state(data);
		break;
	case packet_power:
		this->recv_set_power(data);
		break;
	case packet_light:
		this->recv_set_light(data);
		break;
	case packet_player_pos:
		this->recv_player_pos(data);
		break;
	case packet_player_rot:
		this->recv_player_rot(data);
		break;
	default:
		break;
	}
}

void client::packager::recv_auth_challenge(const packets::packet_t& packet)
{
	auto const& pbk_b64 = packet["key"].get<std::string>();
	auto const& challenge_b64 = packet["challenge"].get<std::string>();
	this->m_auth.solve_challenge(pbk_b64, challenge_b64);
}

void client::packager::recv_auth_result(const packets::packet_t& packet)
{
	bool success = packet["success"].get<bool>();
	if (success) {
		::entity::id id = packet["target"].get<::entity::id>();
		uint64_t ticks = 0;
		this->m_level->init("%WORLD%", 0, ticks, ::entity::id(1));
		this->m_target = id;
	} else {
		// TODO
	}
}

void client::packager::recv_chunks_loaded(const packets::packet_t&)
{
	this->m_observer->on_ready(m_target);
}

void client::packager::recv_chunk_add(const packets::packet_t& packet)
{
	std::shared_ptr<game::chunk> chunk;
	auto* world = m_level->get_world();
	packets::r_chunk_add(packet, world, chunk);
	if (chunk) {
		int64_t x = chunk->posl.x, z = chunk->posl.y;
		world->set_chunk(x, z, chunk);
	}
}

void client::packager::recv_chunk_remove(const packets::packet_t& packet)
{
	coord_t x, z;
	packets::r_chunk_remove(packet, x, z);
	auto* world = m_level->get_world();
	world->remove_chunk(x, z);
}

void client::packager::recv_entity_batch(const packets::packet_t& packet)
{
	auto const& entities = packet["entities"];
	auto* entity_core = m_level->get_entity_system();
	for (auto const& p : entities) {
		entity_core->from_json(p);
	}
}

void client::packager::recv_set_block(packets::packet_t const& packet)
{
	coord_t x, y, z;
	block_t block;
	side_t side;
	packets::r_set_block(packet, x, y, z, block, side);
	auto* world = m_level->get_world();
	world->set_block(x, y, z, block, side);
}

void client::packager::recv_set_rotation(packets::packet_t const& packet)
{
	coord_t x, y, z;
	side_t value;
	packets::r_set_rotation(packet, x, y, z, value);
	auto* world = m_level->get_world();
	world->set_orientation(x, y, z, value);
}

void client::packager::recv_set_state(packets::packet_t const& packet)
{
	coord_t x, y, z;
	state_t value;
	packets::r_set_state(packet, x, y, z, value);
	auto* world = m_level->get_world();
	world->set_state(x, y, z, value);
}

void client::packager::recv_set_power(packets::packet_t const& packet)
{
	coord_t x, y, z;
	power_t value;
	packets::r_set_power(packet, x, y, z, value);
	auto* world = m_level->get_world();
	world->set_power(x, y, z, value);
}

void client::packager::recv_set_light(packets::packet_t const& packet)
{
	coord_t x, y, z;
	light_t value;
	packets::r_set_light(packet, x, y, z, value);
	auto* world = m_level->get_world();
	world->set_light(x, y, z, value);
}

void client::packager::recv_player_pos(packets::packet_t const& packet)
{
    ::entity::id pid;
    ktf::vec3<double> pos;
    packets::r_player_pos(packet, pid, pos);
    auto* entity = m_level->get_entity(pid);
    if (entity) {
        entity->set_position(pos);
    }
}

void client::packager::recv_player_rot(packets::packet_t const& packet)
{
    ::entity::id pid;
    ktf::vec3<double> rot;
    packets::r_player_rot(packet, pid, rot);
    auto* entity = m_level->get_entity(pid);
    if (entity) {
        entity->set_rotation(rot);
    }
}
