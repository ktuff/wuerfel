#include <memory>

#include <ktf/util/logger.hpp>

#include <client/config.hpp>
#include <client/soundsystem.hpp>
#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/item/inventory.hpp>
#include <common/world/chunk.hpp>

#include "game.hpp"


game::game::game()
: m_level(false)
, m_packager(this, &m_level)
, m_open(false)
, target(0)
, old_pos(20, 0)
, m_inventory_system(m_level.get_entity_system())
, m_entity_network(m_level.get_entity_system(), &m_packager)
, m_target_system(m_level.get_entity_system(), m_level.get_world())
{
}

game::game::~game()
{
}


game::level * game::game::get_level()
{
	return &m_level;
}

client::packager* game::game::get_client()
{
	return &m_packager;
}

::entity::id game::game::get_target() const
{
	return target;
}

bool game::game::is_open() const
{
	return m_open;
}

void game::game::start(const char* ip, int port, int timeout)
{
	this->m_packager.connect(ip, port, timeout);
	if (m_packager.is_connected()) {
		ktf::logger::info("logging in as: " + client::config.get_string("username"));
	} else {
		ktf::logger::info("failed to connect");
	}
}

void game::game::leave()
{
	this->m_packager.disconnect();
}

void game::game::stop()
{
	if (m_open) {
		this->m_open = false;
		this->target = 0;
		client::soundsystem::unregister_world(m_level.get_world());
		this->m_level.reset();
		this->leave();
	}
}

void game::game::tick(double dt)
{
	this->m_packager.tick();

	if (!m_open) {
		return;
	}

	auto* player = m_level.get_entity(target);
	const auto& pos = player->get_position();
	this->m_packager.send_player_pos(pos);

	player->move(dt); // TODO move to elsewhere

	this->m_target_system.update();

	// TODO
	// move to renderer? => link with camera
	ktf::vec3<double> up(0.0, 1.0, 0.0);
	auto const& eye_pos = player->get_eye_position();
	auto const& dir = player->get_direction(); // TODO eye_direction
	auto const& lisright = ktf::vec3<double>::normalize(ktf::vec3<double>::cross(up, dir));
	auto const& lisup = ktf::vec3<double>::cross(dir, lisright);
	client::soundsystem::update_listener(eye_pos, dir, lisup);
}

void game::game::on_ready(::entity::id id)
{
	client::soundsystem::register_world(m_level.get_world());

	// reset player
	entity::entity* player = m_level.get_entity(id);
	this->m_entity_network.set_target(player->m_id);
	this->target = player->m_id;
	player->set_position(ktf::vec3<double>(0, 256, 0));

	auto* inv = dynamic_cast<::entity::inventory_system*>(m_level.get_entity_system()->get_module(::entity::inventory_system::s_id));
	item::item_stack dust(item::e_block_circuit_wire, 64);
	item::item_stack torch(item::e_block_circuit_torch, 64);
	item::item_stack repeater(item::e_block_circuit_repeater, 64);
	item::item_stack comparator(item::e_block_circuit_comparator, 64);
	item::item_stack furnace(item::e_block_furnace, 64);
	item::item_stack workbench(item::e_block_workbench, 64);
	item::item_stack water(item::e_block_water, 64);
	item::item_stack stick(item::e_stick, 64);
	item::item_stack cobblestone(item::e_block_cobblestone, 64);
	item::item_stack sapling(item::e_block_sapling_oak, 64);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 0, dust);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 1, torch);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 2, repeater);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 3, comparator);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 4, furnace);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 5, workbench);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 6, water);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 7, stick);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 8, cobblestone);
	inv->set_item(player->m_id, ::entity::inventory_group::e_items, 9, sapling);

	this->m_open = true;
}

void game::game::on_leave()
{
	this->stop();
	ktf::logger::info("Closed game");
}
