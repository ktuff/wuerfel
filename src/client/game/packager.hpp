#pragma once

#include <ktf/math/vec3.hpp>
#include <ktf/net/tcp_client.hpp>

#include <common/net/packet_handler.hpp>

#include "authentication.hpp"


namespace game {
	class game;
	class level;
}


namespace client {

class packager {

	private:
		tcp_client m_socket;
		client::authentication m_auth;
		game::game* m_observer;
		game::level* m_level;
		::entity::id m_target;
		nlohmann::json m_packet_batch;


	public:
		packager(game::game* observer, game::level* wld);
		~packager();


	public:
		// control
		void connect(const char* ip, int port, int timeout);
		void disconnect();
		bool is_connected();
		void tick();

		// data
		void send_auth_init(std::string const& username, std::string const& pbk_b64);
		void send_auth_reply(std::string const& challenge_b64);

		void send_set_block(coord_t x, coord_t y, coord_t z, block_t block, side_t side);
		void send_set_rotation(coord_t x, coord_t y, coord_t z, side_t value);
		void send_set_state(coord_t x, coord_t y, coord_t z, state_t value);
		void send_set_power(coord_t x, coord_t y, coord_t z, power_t value);
		void send_player_pos(const ktf::vec3<double>& pos);
		void send_player_rot(const ktf::vec3<double>& rot);

		void push_auth_packet(packets::packet_t const& data);
		void push_general_packet(packets::packet_t const& data);
		void push_entity_packet(packets::packet_t const& data);

	private:
		void enqueue_packet(packets::packet_t const& packet);

		void process_auth_packet(packets::packet_t const& data);
		void process_general_packet(packets::packet_t const& data);

		void recv_auth_challenge(packets::packet_t const& packet);
		void recv_auth_result(packets::packet_t const& packet);
		void recv_chunks_loaded(packets::packet_t const& packet);
		void recv_chunk_add(packets::packet_t const& packet);
		void recv_chunk_remove(packets::packet_t const& packet);
		void recv_entity_batch(packets::packet_t const& packet);
		void recv_set_block(packets::packet_t const& packet);
		void recv_set_rotation(packets::packet_t const& packet);
		void recv_set_state(packets::packet_t const& packet);
		void recv_set_power(packets::packet_t const& packet);
		void recv_set_light(packets::packet_t const& packet);
		void recv_player_pos(packets::packet_t const& packet);
		void recv_player_rot(packets::packet_t const& packet);

};

} // namespace client
