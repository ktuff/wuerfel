#include <common/world/voxel.hpp>
#include <common/world/world.hpp>

#include "soundsystem.hpp"


client::soundsystem client::soundsystem::instance;

void client::soundsystem::create()
{
	soundsystem::instance.system.create();
}

void client::soundsystem::destroy()
{
	soundsystem::instance.system.destroy();
}

void client::soundsystem::start()
{
	soundsystem::instance.system.start();
}

void client::soundsystem::stop()
{
	soundsystem::instance.system.stop();
}

void client::soundsystem::clear()
{
	soundsystem::instance.system.clear();
}

void client::soundsystem::register_world(game::world* world)
{
	soundsystem::instance.world = world;
	world->add_listener(&soundsystem::instance);
}

void client::soundsystem::unregister_world(game::world* world)
{
	soundsystem::instance.world = nullptr;
	world->remove_listener(&soundsystem::instance);
}

void client::soundsystem::update_listener(const ktf::vec3<double>& pos, const ktf::vec3<double>& dir, const ktf::vec3<double>& up)
{
	soundsystem::instance.system.set_listener_position(pos.x, pos.y, pos.z);
	soundsystem::instance.system.set_listener_orientation(dir.x, dir.y, dir.z, up.x, up.y, up.z);
}

void client::soundsystem::play_sound()
{
}

void client::soundsystem::stop_sound()
{
}

void client::soundsystem::on_destruction()
{
	this->world = nullptr;
}

void client::soundsystem::on_chunk_add(coord_t, coord_t)
{
}

void client::soundsystem::on_chunk_remove(coord_t, coord_t)
{
}

void client::soundsystem::on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block)
{
	int64_t id;
	if (new_block == e_block_air) {
		if (old_block != e_block_air) {
			soundsystem::instance.system.play(id, "assets/sounds/set_block.ogg", 1.0f, 1.0f, false, x, y, z);
		}
	} else {
		soundsystem::instance.system.play(id, "assets/sounds/set_block.ogg", 1.0f, 1.0f, false, x, y, z);
	}
}

void client::soundsystem::on_rotation_change(coord_t, coord_t, coord_t, side_t)
{
}

void client::soundsystem::on_power_change(coord_t, coord_t, coord_t, power_t)
{
}

void client::soundsystem::on_light_change(coord_t, coord_t, coord_t, light_t)
{
}
