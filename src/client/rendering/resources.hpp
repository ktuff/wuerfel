#pragma once

#include <ktf/ui/text.hpp>
#include <ktf/util/assets_index.hpp>

#include <common/types.hpp>
#include <common/item/item.hpp>
#include <common/world/voxel.hpp>

#include "animation_map.hpp"

#define NUM_ORIENTATIONS side_count


class resources : public ktf::assets_index {

	public:
		std::function<size_t(std::string const&)> const loadf;
		ktf::ui::font_alloc const font_alloc;

	private:
		typedef std::array<ktf::mesh3d, side_count> mesh_data_t;
		std::string m_cache_dir;
		std::vector<mesh_data_t> m_block_models[e_block_count][NUM_ORIENTATIONS];
		animation_map m_animation_map;
		bool m_model_use_block_texture[item::num_item_types];


	public:
		resources(ktf::composer* composer);
		~resources();


	public:
		void load();
		void unload();
		void bind_animation_map();
		void update_animation_map();
		mesh_data_t const* get_block_model(block_t block, side_t orientation, state_t state) const;
		bool uses_block_texture(::item::item_type_id id) const;

	private:
		void load_post_processing_quad();
		void load_shaders();
		void load_textures();
		void load_materials();
		void load_fonts();
		void load_skybox();
		void load_blocks();
		void load_items();
		void load_sprites();
		std::vector<ktf::atlas_pair> generate_item_sprites();
		void build_texture_model(ktf::mesh3d& mesh, std::string const& path);

		std::vector<side_t> get_orientations(std::string const& type) const;

};
