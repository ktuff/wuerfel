#pragma once

#include <atomic>
#include <queue>
#include <thread>
#include <unordered_map>
#include <unordered_set>

#include <ktf/opengl/mesh.hpp>

#include <client/entity/render_system.hpp>
#include <common/world/world_listener.hpp>

#include "resources.hpp"


namespace client {

class gui;

} // namespace client


namespace game {

class chunk;
class level;

} // namespace game


namespace ktf {

class window;
class composer;

} // namespace ktf


struct chunk_mesh_info {
	std::vector<size_t> mesh_id;
	int8_t top;
};


struct chunk_job_flags {
	uint8_t block : 1;
	uint8_t power : 1;
	uint8_t state : 1;
	uint8_t light : 1;
	uint8_t _ : 4;
};


struct chunk_job {

private:
	static size_t next_id;

public:
	size_t id;
	chunk_job_flags flags;
	ccoord_t position;
	double distance;

	chunk_job();
	bool operator<(chunk_job const& rhs) const;
};


struct chunk_task {
	std::atomic<bool> done;
	std::thread slave;
	std::vector<ktf::mesh3d> meshes;
	game::chunk_ptr chunk;
	chunk_job_flags flags;

	chunk_task();
	chunk_task(chunk_task&& job);
};


class renderer : public world_listener {

	private:
		ktf::window* m_window;
		resources* m_resources;
		ktf::composer* composer;
		std::weak_ptr<client::gui> m_gui;
		game::level* level;
		bool ui_visible, level_visible;
		bool viewer_mode;

		::entity::id viewer;
		::entity::render_system m_entity_rendering;

		std::unordered_map<ccoord_t, chunk_mesh_info> m_chunk_meshes;
		std::unordered_map<ccoord_t, chunk_job> m_dirty_chunks;

		std::unordered_set<size_t> m_free_task_slots;
		std::vector<chunk_task> m_chunk_tasks;
		size_t m_wireframe_framebuffers[2];
		size_t m_wireframe_attachments[4];


	public:
		renderer(ktf::window* window, resources* res, game::level* level);
		~renderer();


	public:
		void init();
		void destroy();
		void reset();
		void reload();
		void make_context(::entity::id target);
		void set_gui(std::weak_ptr<client::gui> gui);
		void toggle_viewer_mode();
		void toggle_ui();
		void display_level(bool mode);
		void update_camera();
		void render(double dt);
		void on_resize(uint32_t width, uint32_t height);

		// listener functions
		void on_destruction() override;
		void on_chunk_add(coord_t x, coord_t z) override final;
		void on_chunk_remove(coord_t x, coord_t z) override final;
		void on_block_change(coord_t x, coord_t y, coord_t z, block_t old_block, block_t new_block) override final;
		void on_rotation_change(coord_t x, coord_t y, coord_t z, side_t value) override final;
		void on_state_change(coord_t x, coord_t y, coord_t z, state_t value) override final;
		void on_power_change(coord_t x, coord_t y, coord_t z, power_t value) override final;
		void on_light_change(coord_t x, coord_t y, coord_t z, light_t value) override final;

	private:
		void render_level(double dt);
		void render_blocks(double dt);
		void render_entities(double dt);
		void render_ui(double dt);
		void render_target_wireframe(double dt);
		void update_level();
		void update_ui();

		void add_dirty_chunk(ccoord_t const& chunk_position, chunk_job_flags const& flags, ktf::vec2<double> const& viewer_position);
		void check_chunk_update(coord_t x, coord_t z, chunk_job_flags const& flags);
		void create_chunk_mesh(game::chunk_ptr chunk, chunk_job_flags const& flags, std::vector<ktf::mesh3d>& meshes) const;
		void upload_mesh(game::chunk_ptr chunk, std::vector<ktf::mesh3d> const& mesh, chunk_job_flags const& flags);
		void create_framebuffers(uint32_t width, uint32_t height);
		void remove_framebuffers();
};
