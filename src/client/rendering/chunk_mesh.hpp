#pragma once

#include <common/world/chunk.hpp>
#include <ktf/opengl/mesh.hpp>


class resources;


class mesh_generator {

	private:
		game::chunk_ptr const& m_chunk;
		std::vector<ktf::mesh3d>* m_meshes;
		resources* m_resources;
		bool m_combined;
		enum {
			compute_full,
			compute_light
		};


	public:
		mesh_generator(game::chunk_ptr const& chunk, std::vector<ktf::mesh3d>* meshes, resources *res, bool combined);


	public:
		void compute_full_mesh();
		void compute_light_values();
		void compute_single_block(const ktf::vec3<coord_t>& pos);

	private:
		void add_block(ktf::vec3<coord_t> const& pos, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);
		void add_block_default(ktf::mesh3d* mesh, ktf::vec3<coord_t> const& pos, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);
		void add_block_circuit(ktf::mesh3d* mesh, ktf::vec3<coord_t> const& pos, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);

		void add_light(block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);
		void add_light_default(ktf::mesh3d* mesh, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);
		void add_light_circuit(ktf::mesh3d* mesh, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d);

		ktf::mesh3d* mesh_group(block_t block);
		bool is_transparent(transparency const& t, side_t side) const;
		light_t get_light(light_array const& la, side_t side) const;

};
