#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include <ktf/opengl/buffer_texture.hpp>


struct texture_anim {
	std::string name;
	size_t frames;
	size_t delay;
	float offset;
};

struct animation_state {
	size_t index;
	size_t frame;
	size_t time;
};


typedef float f32;
static_assert(sizeof(float) == 4); // TODO bits not bytes, move to elsewhere


class animation_map {

	private:
		ktf::buffer_texture m_buffer;
		std::unordered_map<std::string, animation_state> m_states;
		std::vector<texture_anim> m_textures;
		std::vector<f32> m_data;


	public:
		void init(std::vector<texture_anim> const& textures);
		size_t get_index(std::string const& name);
		void update(size_t dt);
		void reset();
		void bind();
};

