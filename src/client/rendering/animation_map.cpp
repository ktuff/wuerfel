#include "animation_map.hpp"


void animation_map::init(std::vector<texture_anim> const& textures)
{
	this->m_textures = textures;
	this->m_buffer = ktf::buffer_texture(textures.size() * sizeof(f32));
	this->m_data.resize(textures.size());
	for (size_t i=0; i<textures.size(); i++) {
		animation_state state;
		state.index = i;
		state.frame = 0;
		state.time = 0;
		this->m_states.emplace(textures[i].name, state);
		this->m_data[i] = 0.0f;
	}
	this->m_buffer.fill(reinterpret_cast<char const*>(m_data.data()));
}

size_t animation_map::get_index(const std::string& name)
{
	if (m_states.find(name) != m_states.end()) {
		return m_states.at(name).index;
	} else {
		return 0;
	}
}

void animation_map::update(size_t dt)
{
	bool data_changed = false;
	for (auto const& t : m_textures) {
		auto& state = m_states.at(t.name);
		if (t.frames > 1) {
			state.time += dt;
			if (state.time >= t.delay) {
				state.frame = (state.frame + 1) % t.frames;
				state.time -= t.delay;
				this->m_data[state.index] = t.offset * state.frame;
				data_changed = true;
			}
		}
	}
	if (data_changed) {
		this->m_buffer.fill(reinterpret_cast<char const*>(m_data.data()));
	}
}

void animation_map::reset()
{
	this->m_buffer = ktf::buffer_texture();
	this->m_states.clear();
	this->m_textures.clear();
	this->m_data.clear();
}

void animation_map::bind()
{
	this->m_buffer.bind();
}
