#include <assert.h>

#include <iostream>

#include <ktf/math/transform.hpp>
#include <ktf/opengl/composer.hpp>
#include <ktf/opengl/gl.hpp>
#include <ktf/opengl/window.hpp>

#include <client/config.hpp>
#include <client/gui/gui.hpp>
#include <common/entity/systems/target_system.hpp>
#include <common/item/inventory.hpp>
#include <common/level.hpp>

#include "chunk_mesh.hpp"
#include "renderer.hpp"
#include "resources.hpp"


ktf::buffer_attrib const a_pos = {
	0,
	0,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};
ktf::buffer_attrib const a_tex = {
	1,
	1,
	2,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec2<float>),
	0,
	0
};
ktf::buffer_attrib const a_clr = {
	2,
	2,
	4,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec4<float>),
	0,
	0
};
ktf::buffer_attrib const a_nrm = {
	3,
	3,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};
ktf::buffer_attrib const a_off = {
	4,
	4,
	1,
	ktf::composer::data_type_uint,
	ktf::composer::bool_false,
	sizeof(unsigned int),
	0,
	0
};


size_t chunk_job::next_id = 1;

chunk_job::chunk_job()
: id(next_id)
, flags({0, 0, 0, 0, 0})
, distance(1.0)
{
	++chunk_job::next_id;
}

bool chunk_job::operator<(const chunk_job& rhs) const
{
	if (distance == rhs.distance) {
		return id < rhs.id;
	} else {
		return distance < rhs.distance;
	}
}


chunk_task::chunk_task()
: done(false)
, meshes(2)
{
}

chunk_task::chunk_task(chunk_task && job)
{
	this->done = job.done.load();
	this->slave = std::move(job.slave);
	this->meshes = std::move(job.meshes);
}


renderer::renderer(ktf::window* window, resources* res, game::level* level)
: m_window(window)
, m_resources(res)
, composer(nullptr)
, level(level)
, ui_visible(true)
, level_visible(false)
, viewer_mode(false)
, viewer(0)
, m_entity_rendering(level->get_entity_system(), res)
, m_wireframe_framebuffers{size_t(-1), size_t(-1)}
, m_wireframe_attachments{size_t(-1), size_t(-1), size_t(-1), size_t(-1)}
{
}

renderer::~renderer()
{
	this->destroy();
}


void renderer::init()
{
	this->composer = m_window->get_composer();
	assert(composer != nullptr);

	this->composer->get_camera()->set_relative(true);
	this->level->get_world()->add_listener(this);
	this->m_entity_rendering.init(composer);

	size_t thread_count = client::config.get_int("num_threads");
	this->m_chunk_tasks.resize(thread_count * 2);
	for (size_t index=0; index<m_chunk_tasks.size(); ++index) {
		this->m_free_task_slots.emplace(index);
	}
}

void renderer::destroy()
{
	if (level) {
		this->level->get_world()->remove_listener(this);
	}
	this->reset();
}

void renderer::reset()
{
	for (size_t index=0; index<m_chunk_tasks.size(); ++index) {
		auto& job = m_chunk_tasks[index];
		if (job.slave.joinable()) {
			job.slave.join();
		}
		this->m_free_task_slots.emplace(index);
	}

	if (composer) {
		for (auto& m : m_chunk_meshes) {
			for (size_t i=0; i<m.second.mesh_id.size(); ++i) {
				this->composer->deregister_mesh(&m.second.mesh_id[i]);
			}
		}
		this->m_chunk_meshes.clear();
		this->m_dirty_chunks.clear();
	}
	this->m_entity_rendering.clear();
}

void renderer::reload()
{
	this->m_entity_rendering.reload();
}

void renderer::make_context(::entity::id target)
{
	this->viewer = target;
}

void renderer::set_gui(std::weak_ptr<client::gui> gui)
{
	this->m_gui = gui;
}

void renderer::toggle_viewer_mode()
{
	this->viewer_mode = !viewer_mode;
}

void renderer::toggle_ui()
{
    this->ui_visible = !ui_visible;
}

void renderer::display_level(bool mode)
{
    this->level_visible = mode;
}

void renderer::update_camera()
{
	auto const* entities = level->get_entity_system();
	auto const* entity = entities->get_entity(viewer);

	// update view matrix
	if (entity) {
		auto camera = composer->get_camera();
		ktf::vec3<double> pos = entity->get_eye_position();
		if (viewer_mode) {
			pos -= entity->get_direction() * 3.0;
		}
		const auto& rot = entity->get_rotation();
		camera->set_position(pos);
		camera->set_rotation(rot);
		camera->set_fov(70.0f);
	}
}

void renderer::render(double dt)
{
	// start iteration
	this->composer->begin_frame();

	// display level
	if (this->level_visible) {
		this->render_level(dt);
	}

	// display ui
	if (this->ui_visible) {
		this->render_ui(dt);
	}

	this->composer->end_frame();

	// end iteration
	this->m_window->swap_buffers();
}

void renderer::on_resize(uint32_t width, uint32_t height)
{
	this->remove_framebuffers();
	this->create_framebuffers(width, height);
}

void renderer::on_destruction()
{
	this->level = nullptr;
}

void renderer::on_chunk_add(coord_t x, coord_t z)
{
	ccoord_t pos_chunk(x, z);
	ccoord_t pos_nxnz(x-1, z-1);
	ccoord_t pos_nxpz(x, z+1);
	ccoord_t pos_pxnz(x+1, z);
	ccoord_t pos_pxpz(x+1, z+1);

	chunk_job_flags flags;
	flags.block = flags.power = flags.state = flags.light = 1;
	flags._ = 0;

	ktf::vec3<double> viewer_pos;
	auto const* entities = level->get_entity_system();
	auto const* entity = entities->get_entity(viewer);
	if (entity) {
		viewer_pos = entity->get_position();
	} else {
		viewer_pos = ktf::vec3<double>(0.0, 0.0, 0.0);
	}
	ktf::vec2<double> viewer_chunk_pos(viewer_pos.x / CHUNK_SIZE, viewer_pos.z / CHUNK_SIZE);
	this->add_dirty_chunk(pos_chunk, flags, viewer_chunk_pos);
	this->add_dirty_chunk(pos_nxnz, flags, viewer_chunk_pos);
	this->add_dirty_chunk(pos_nxpz, flags, viewer_chunk_pos);
	this->add_dirty_chunk(pos_pxnz, flags, viewer_chunk_pos);
	this->add_dirty_chunk(pos_pxpz, flags, viewer_chunk_pos);
}

void renderer::on_chunk_remove(coord_t x, coord_t z)
{
	ccoord_t coordinate(x, z);
	if (m_chunk_meshes.find(coordinate) != m_chunk_meshes.end()) {
		auto& mesh = m_chunk_meshes.at(coordinate);
		for (size_t i=0; i<mesh.mesh_id.size(); ++i) {
			this->composer->deregister_mesh(&mesh.mesh_id[i]);
		}
		this->m_chunk_meshes.erase(coordinate);
	}
	if (m_dirty_chunks.find(coordinate) != m_dirty_chunks.end()) {
		this->m_dirty_chunks.erase(coordinate);
	}
}

void renderer::on_block_change(coord_t x, coord_t, coord_t z, block_t old_block, block_t new_block)
{
	if (old_block == new_block) {
		return;
	}
	chunk_job_flags flags;
	flags.block = 1;
	this->check_chunk_update(x, z, flags);
}

void renderer::on_rotation_change(coord_t x, coord_t, coord_t z, side_t)
{
	chunk_job_flags flags;
	flags.block = 1;
	this->check_chunk_update(x, z, flags);
}

void renderer::on_state_change(coord_t x, coord_t, coord_t z, state_t)
{
	chunk_job_flags flags;
	flags.state = 1;
	this->check_chunk_update(x, z, flags);
}

void renderer::on_power_change(coord_t x, coord_t, coord_t z, power_t)
{
	chunk_job_flags flags;
	flags.power = 1;
	this->check_chunk_update(x, z, flags);
}

void renderer::on_light_change(coord_t x, coord_t, coord_t z, light_t)
{
	chunk_job_flags flags;
	flags.light = 1;
	this->check_chunk_update(x, z, flags);
}

void renderer::render_level(double dt)
{
	if (!viewer) {
		return;
	}

	ktf::gl::set_clear_color(1.0f, 1.0f, 1.0f, 1.0f);
	ktf::gl::enable_depth_testing();
	ktf::gl::enable_culling();

	// render skybox
	ktf::camera * cam = composer->get_camera();
	ktf::vec3<double> old_position = cam->get_position();
	cam->set_position({0.0, 0.0, 0.0});
	cam->update_matrices();
	ktf::gl::disable_depth_mask();
	this->m_resources->use_shader("game::shader.skybox");
	this->composer->use_perspective_projection();
	this->m_resources->use_texture("game::texture.skybox");
	size_t skybox_mesh = m_resources->get_mesh("game::mesh.skybox");
	this->composer->render_mesh(skybox_mesh, ktf::matrix<float>(1.0f));
	ktf::gl::enable_blending();
	this->m_resources->use_texture("game::texture.sky_deco");
	this->composer->render_mesh(skybox_mesh, ktf::matrix<float>(1.0f));
	ktf::gl::disable_blending();
	ktf::gl::enable_depth_mask();
	cam->set_position(old_position);
	cam->update_matrices();

	this->update_level();

	this->render_blocks(dt);
	this->render_entities(dt);

	// NOTE last step, alternatively copy depth buffer
	this->render_target_wireframe(dt);
}

void renderer::render_blocks(double)
{
	ktf::camera* camera = composer->get_camera();
	ktf::vec3<double> camera_position = camera->get_position();
	ktf::gl::disable_blending();

	this->m_resources->use_texture("texture::blocks");
	this->m_resources->use_shader("game::shader.solid");
	this->m_resources->use_material("block");
	this->composer->set_uniform_3f("ambient_color", ktf::vec3<float>(0.2));
	this->composer->set_uniform_3f("view_pos", ktf::vec3<float>(0.0f));
	this->composer->set_uniform_3f("light_dir", ktf::vec3<float>(0.5f, 1.0f, 1.0f));
	this->composer->use_perspective_projection();
	this->m_resources->bind_animation_map();
	this->m_resources->update_animation_map();

	for (size_t group=0; group<2; ++group) {
		for (const auto& mesh : m_chunk_meshes) {
			bool in_frustum = false;
			for (int y=0; y<=mesh.second.top && !in_frustum; y++) {
				const ktf::vec3<int64_t>& p = ktf::vec3<int64_t>(mesh.first.x, y, mesh.first.y)*CHUNK_SIZE - camera_position;
				in_frustum |= camera->cube_in_view(p, CHUNK_SIZE);
			}
			if (in_frustum) {
				auto cp = mesh.first * CHUNK_SIZE;
				auto matrix = ktf::translate(ktf::matrix<float>(1.0f), ktf::vec3<float>(cp.x-camera_position.x, -camera_position.y, cp.y-camera_position.z));
				this->composer->render_mesh(mesh.second.mesh_id[group], matrix);
			}
		}
		ktf::gl::enable_blending();
	}
}

void renderer::render_entities(double dt)
{
	this->m_entity_rendering.update(dt);
	this->m_entity_rendering.render();
}

void renderer::render_ui(double)
{
	this->update_ui();

	if (auto gui = m_gui.lock()) {
		auto* ui = gui->get_node_tree();
		auto gsprites = ui->get_group("widgets_sprite");
		auto gtexts = ui->get_group("widgets_text");

		ktf::gl::enable_blending();
		ktf::gl::set_blending_factors(ktf::gl::e_src_alpha, ktf::gl::e_one_minus_src_alpha);
		ktf::gl::enable_culling();
		ktf::gl::enable_depth_testing();

		this->m_resources->use_shader("ui::shader.gui");
		this->m_resources->use_texture("ui::texture.gui");
		composer->use_orthogonal_projection();
		for (ktf::ui::widget* m : gsprites->get_members()) {
			if (ktf::ui::renderable* r = dynamic_cast<ktf::ui::renderable*>(m)) {
				if (r->visible()) {
					composer->set_uniform_4f("base_color", r->color());
					composer->render_mesh(r->id(), r->model());
				}
			}
		}
		this->m_resources->use_shader("ui::shader.font");
		this->m_resources->use_texture("ui::texture.font");
		composer->use_orthogonal_projection();
		for (ktf::ui::widget* m : gtexts->get_members()) {
			if (ktf::ui::renderable* r = dynamic_cast<ktf::ui::renderable*>(m)) {
				if (r->visible()) {
					composer->set_uniform_4f("text_color", r->color());
					composer->render_mesh(r->id(), r->model());
				}
			}
		}
	}
}

void renderer::render_target_wireframe(double)
{
	auto* entity_system = level->get_entity_system();
	auto* targets = dynamic_cast<::entity::target_system*>(entity_system->get_module(::entity::target_system::s_id));
	auto target = targets->get_target(viewer);
	if (target.type == entity::e_target_none) {
		return;
	}

	std::vector<ktf::mesh3d> mesh(2);
	auto chunk = level->get_world()->get_chunk_global(target.position.x, target.position.z);
	mesh_generator gen(chunk, &mesh, m_resources, true);
	gen.compute_single_block(target.position);
	if (mesh[0].indices.size() == 0) {
		return;
	}

	float extent_factor = 1.0f;
	for (size_t i=0; i<mesh[0].pos.size(); ++i) {
		extent_factor = std::max({extent_factor, std::abs(mesh[0].pos[i].x), std::abs(mesh[0].pos[i].y), std::abs(mesh[0].pos[i].z)});
	}
	extent_factor = 1.0f / extent_factor;

	std::vector<ktf::buffer_data> data = {
		{
			mesh[0].pos.size() * sizeof(ktf::vec3<float>),
			mesh[0].pos.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh[0].tex.size() * sizeof(ktf::vec2<float>),
			mesh[0].tex.data(),
			ktf::composer::draw_hint_static
		},
		{
			mesh[0].nrm.size() * sizeof(ktf::vec3<float>),
			mesh[0].nrm.data(),
			ktf::composer::draw_hint_static
		}
	};
	auto at_nrm = a_nrm;
	at_nrm.data_index = 2;
	at_nrm.attrib_index = 2;
	std::vector<ktf::buffer_attrib> attribs = {
		a_pos,
		a_tex,
		at_nrm
	};

	size_t block_mesh;
	this->composer->register_mesh(&block_mesh, data, attribs, mesh[0].indices);

	size_t normals_shader = m_resources->get_shader("shader:normal_map");
	size_t median_filter_shader = m_resources->get_shader("shader:median");
	size_t sobel_filter_shader = m_resources->get_shader("shader:sobel");
	size_t invert_shader = m_resources->get_shader("shader:invert");
	size_t block_texture = m_resources->get_texture("texture::blocks");
	size_t quad_mesh = m_resources->get_mesh("internal:quad");

	int width, height;
	ktf::gl::get_viewport_size(&width, &height);
	ktf::gl::enable_depth_testing();
	ktf::gl::enable_blending();
	ktf::gl::set_blending_factors(ktf::gl::e_one, ktf::gl::e_zero);
	ktf::gl::enable_culling();
	ktf::gl::set_clear_color(0.0f, 0.0f, 0.0f, 0.0f);

	ktf::camera* camera = composer->get_camera();
	auto const& camera_position = camera->get_position();
	ktf::vec3<float> block_position = ktf::vec3<double>(target.position) - camera_position;
	ktf::matrix<float> model_matrix(1.0f);
	model_matrix = ktf::scale(model_matrix, ktf::vec3<float>(1.001f));
	model_matrix = ktf::translate(model_matrix, block_position);
	auto screen_matrix = ktf::scale(ktf::matrix<float>(1.0f), ktf::vec3<float>(width, height, 1.0f));
	ktf::vec2<float> texel_size(1.0f / width, 1.0f / height);

	// save original depth
	this->composer->bind_framebuffer_texture(m_wireframe_framebuffers[1], m_wireframe_attachments[3], ktf::e_fb_depth);
	this->composer->copy_framebuffer(0, m_wireframe_framebuffers[1], ktf::e_fb_depth, width, height, width, height);
	this->composer->unbind_framebuffer_texture(m_wireframe_framebuffers[1], ktf::e_fb_depth);

	// render selected block to framebuffer
	this->composer->bind_framebuffer_texture(m_wireframe_framebuffers[0], m_wireframe_attachments[1], ktf::e_fb_depth);
	this->composer->bind_framebuffer(m_wireframe_framebuffers[0]);
	ktf::gl::set_viewport_size(width, height);
	ktf::gl::clear();
	camera->set_relative(true);
	this->composer->bind_shader(normals_shader);
	this->composer->use_perspective_projection();
	this->composer->bind_texture(block_texture);
	this->composer->set_uniform_1f("extent_factor", extent_factor);
	this->composer->render_mesh(block_mesh, model_matrix);
	this->composer->unbind_framebuffer_texture(m_wireframe_framebuffers[0], ktf::e_fb_depth);

	// median filter
	this->composer->bind_framebuffer(m_wireframe_framebuffers[1]);
	ktf::gl::set_viewport_size(width, height);
	ktf::gl::clear();
	ktf::gl::disable_depth_testing();
	this->composer->bind_shader(median_filter_shader);
	this->composer->use_orthogonal_projection();
	this->composer->bind_texture(m_wireframe_attachments[0]);
	this->composer->set_uniform_2f("texel", texel_size);
	this->composer->render_mesh(quad_mesh, screen_matrix);

	// sobel + curve + black/white
	this->composer->bind_framebuffer(m_wireframe_framebuffers[0]);
	ktf::gl::set_viewport_size(width, height);
	ktf::gl::clear();
	this->composer->bind_shader(sobel_filter_shader);
	this->composer->use_orthogonal_projection();
	this->composer->bind_texture(m_wireframe_attachments[2]);
	this->composer->set_uniform_2f("texel", texel_size);
	this->composer->render_mesh(quad_mesh, screen_matrix);

	// combine
	this->composer->copy_framebuffer(0, m_wireframe_framebuffers[1], ktf::e_fb_color, width, height, width, height);
	this->composer->bind_framebuffer(0);
	ktf::gl::set_viewport_size(width, height);
	ktf::gl::clear();
	this->composer->bind_shader(invert_shader);
	this->composer->set_uniform_1i("texture0", 0);
	this->composer->set_uniform_1i("texture1", 1);
	this->composer->set_uniform_1i("texture2", 2);
	this->composer->set_uniform_1i("texture3", 3);
	this->composer->bind_texture(m_wireframe_attachments[2], 0);
	this->composer->bind_texture(m_wireframe_attachments[0], 1);
	this->composer->bind_texture(m_wireframe_attachments[3], 2);
	this->composer->bind_texture(m_wireframe_attachments[1], 3);
	this->composer->set_uniform_2f("texel", texel_size);
	this->composer->use_orthogonal_projection();
	this->composer->render_mesh(quad_mesh, screen_matrix);

	// restore original depth
	this->composer->bind_framebuffer_texture(m_wireframe_framebuffers[1], m_wireframe_attachments[3], ktf::e_fb_depth);
	this->composer->copy_framebuffer(m_wireframe_framebuffers[1], 0, ktf::e_fb_depth, width, height, width, height);

	// clean up
	this->composer->deregister_mesh(&block_mesh);

	ktf::gl::enable_depth_mask();
	ktf::gl::set_blending_factors(ktf::gl::e_src_alpha, ktf::gl::e_one_minus_src_alpha);
}

void renderer::update_level()
{
	// upload finished meshes to GPU
	for (size_t index=0; index<m_chunk_tasks.size(); ++index) {
		auto& job = m_chunk_tasks[index];
		if (job.done) {
			this->upload_mesh(job.chunk, job.meshes, job.flags);
			this->m_free_task_slots.emplace(index);
			job.done = false;
			job.meshes = {};
		}
	}

	// enqueue new tasks
	while (!m_dirty_chunks.empty() && !m_free_task_slots.empty()) {
		auto dirty_chunk = *m_dirty_chunks.begin();
		this->m_dirty_chunks.erase(m_dirty_chunks.begin());

		ccoord_t const& pos = dirty_chunk.second.position;
		game::chunk_ptr chunk = level->get_chunk(pos.x, pos.y);
		if (!chunk) {
			continue;
		}

		uint32_t index = *m_free_task_slots.begin();
		this->m_free_task_slots.erase(index);

		// init job
		if (m_chunk_tasks[index].slave.joinable()) {
			this->m_chunk_tasks[index].slave.join();
		}
		this->m_chunk_tasks[index].done = false;
		this->m_chunk_tasks[index].flags = dirty_chunk.second.flags;
		this->m_chunk_tasks[index].chunk = chunk;
		this->m_chunk_tasks[index].meshes.resize(2);
		this->m_chunk_tasks[index].slave = std::thread([this, index]() {
			this->create_chunk_mesh(m_chunk_tasks[index].chunk, m_chunk_tasks[index].flags, m_chunk_tasks[index].meshes);
			this->m_chunk_tasks[index].done = true;
		});
	}
}

void renderer::update_ui()
{
	if (auto gui = m_gui.lock()) {
		gui->update();
	}
}

void renderer::add_dirty_chunk(ccoord_t const& chunk_position, chunk_job_flags const& flags, ktf::vec2<double> const& viewer_position)
{
	if (m_dirty_chunks.find(chunk_position) == m_dirty_chunks.end()) {
		chunk_job job;
		job.flags = flags;
		job.position = chunk_position;
		job.distance = (viewer_position - chunk_position).length();
		this->m_dirty_chunks[chunk_position] = job;
	} else {
		chunk_job_flags& job_flags = m_dirty_chunks[chunk_position].flags;
		job_flags.block |= flags.block;
		job_flags.power |= flags.power;
		job_flags.state |= flags.state;
		job_flags.light |= flags.light;
	}
}


void renderer::check_chunk_update(coord_t x, coord_t z, chunk_job_flags const& flags)
{
	auto const* entities = level->get_entity_system();
	auto const* entity = entities->get_entity(viewer);
	if (!entity) {
		return;
	}

	ktf::vec3<double> const& viewer_pos = entity->get_position();
	ktf::vec2<double> chunk_pos_viewer(viewer_pos.x / CHUNK_SIZE, viewer_pos.z / CHUNK_SIZE);
	coord_t chunk_x = ::floor((long double)x / CHUNK_SIZE);
	coord_t chunk_z = ::floor((long double)z / CHUNK_SIZE);
	this->add_dirty_chunk(ccoord_t(chunk_x, chunk_z), flags, chunk_pos_viewer);

	auto const * const world = level->get_world();
	uint64_t bx = x & (CHUNK_SIZE - 1);
	if (bx == 0) {
		coord_t xi = ::floor((long double)(x - 1) / CHUNK_SIZE);
		coord_t zi = ::floor((long double)z / CHUNK_SIZE);
		if (world->chunk_exists(xi, zi)) {
			this->add_dirty_chunk(ccoord_t(xi, zi), flags, chunk_pos_viewer);
		}
	} else if (bx == CHUNK_SIZE - 1) {
		coord_t xi = ::floor((long double)(x + 1) / CHUNK_SIZE);
		coord_t zi = ::floor((long double)z / CHUNK_SIZE);
		if (world->chunk_exists(xi, zi)) {
			this->add_dirty_chunk(ccoord_t(xi, zi), flags, chunk_pos_viewer);
		}
	}

	uint64_t bz = z & (CHUNK_SIZE - 1);
	if (bz == 0) {
		coord_t xi = floor((long double)x / CHUNK_SIZE);
		coord_t zi = floor((long double)(z - 1) / CHUNK_SIZE);
		if (world->chunk_exists(xi, zi)) {
			this->add_dirty_chunk(ccoord_t(xi, zi), flags, chunk_pos_viewer);
		}
	} else if (bz == CHUNK_SIZE - 1) {
		coord_t xi = floor((long double)x / CHUNK_SIZE);
		coord_t zi = floor((long double)(z + 1) / CHUNK_SIZE);
		if (world->chunk_exists(xi, zi)) {
			this->add_dirty_chunk(ccoord_t(xi, zi), flags, chunk_pos_viewer);
		}
	}
}

void renderer::create_chunk_mesh(game::chunk_ptr chunk, chunk_job_flags const& flags, std::vector<ktf::mesh3d>& meshes) const
{
	mesh_generator gen(chunk, &meshes, m_resources, false);
	if (flags.block) {
		gen.compute_full_mesh();
	} else if (flags.light) {
		gen.compute_light_values();
	} else if (flags.power) {
		gen.compute_light_values();
	} else if (flags.state) {
		gen.compute_full_mesh();
	} else {
		return;
	}
}

void renderer::upload_mesh(game::chunk_ptr chunk, std::vector<ktf::mesh3d> const& mesh, chunk_job_flags const& flags)
{
	if (flags.block || flags.state) {
		std::vector<ktf::buffer_attrib> const attribs = {
			a_pos,
			a_tex,
			a_clr,
			a_nrm,
			a_off
		};

		if (m_chunk_meshes.find(chunk->posl) != m_chunk_meshes.end()) {
			auto oldmesh = m_chunk_meshes.at(chunk->posl);
			for (size_t j=0; j<oldmesh.mesh_id.size(); ++j) {
				this->composer->deregister_mesh(&oldmesh.mesh_id[j]);
			}
			this->m_chunk_meshes.erase(chunk->posl);
		}

		chunk_mesh_info mesh_info;
		mesh_info.mesh_id.resize(2);
		for (size_t i=0; i<mesh.size(); ++i) {
			std::vector<ktf::buffer_data> const data = {
				{
					mesh[i].pos.size() * sizeof(ktf::vec3<float>),
					mesh[i].pos.data(),
					ktf::composer::draw_hint_static
				},
				{
					mesh[i].tex.size() * sizeof(ktf::vec2<float>),
					mesh[i].tex.data(),
					ktf::composer::draw_hint_static
				},
				{
					mesh[i].clr.size() * sizeof(ktf::vec4<float>),
					mesh[i].clr.data(),
					ktf::composer::draw_hint_static
				},
				{
					mesh[i].nrm.size() * sizeof(ktf::vec3<float>),
					mesh[i].nrm.data(),
					ktf::composer::draw_hint_static
				},
				{
					mesh[i].offsets.size() * sizeof(unsigned int),
					mesh[i].offsets.data(),
					ktf::composer::draw_hint_static
				}
			};
			this->composer->register_mesh(&mesh_info.mesh_id[i], data, attribs, mesh[i].indices);
		}
		mesh_info.top = chunk->get_top();
		this->m_chunk_meshes[chunk->posl] = mesh_info;
	} else if (flags.light || flags.power) {
		if (m_chunk_meshes.find(chunk->posl) != m_chunk_meshes.end()) {
			auto& info = m_chunk_meshes.at(chunk->posl);
			for (size_t i=0; i<info.mesh_id.size(); ++i) {
				ktf::buffer_data data = {
					mesh[i].clr.size() * sizeof(ktf::vec4<float>),
					mesh[i].clr.data(),
					ktf::composer::draw_hint_static
				};
				this->composer->update_mesh_buffer(info.mesh_id[i], data, 2);
			}
		}
	}
}

void renderer::create_framebuffers(uint32_t width, uint32_t height)
{
	this->composer->register_framebuffer(&m_wireframe_framebuffers[0]);
	this->composer->register_framebuffer(&m_wireframe_framebuffers[1]);
	this->composer->register_texture(&m_wireframe_attachments[0], width, height, 4, nullptr, ktf::composer::data_type_ubyte, ktf::e_fb_color);
	this->composer->register_texture(&m_wireframe_attachments[1], width, height, 1, nullptr, ktf::composer::data_type_float, ktf::e_fb_depth);
	this->composer->register_texture(&m_wireframe_attachments[2], width, height, 4, nullptr, ktf::composer::data_type_ubyte, ktf::e_fb_color);
	this->composer->register_texture(&m_wireframe_attachments[3], width, height, 1, nullptr, ktf::composer::data_type_float, ktf::e_fb_depth);

	this->composer->bind_framebuffer_texture(m_wireframe_framebuffers[0], m_wireframe_attachments[0], ktf::e_fb_color);
	this->composer->bind_framebuffer_texture(m_wireframe_framebuffers[1], m_wireframe_attachments[2], ktf::e_fb_color);
}

void renderer::remove_framebuffers()
{
	this->composer->deregister_texture(&m_wireframe_attachments[3]);
	this->composer->deregister_texture(&m_wireframe_attachments[2]);
	this->composer->deregister_texture(&m_wireframe_attachments[1]);
	this->composer->deregister_texture(&m_wireframe_attachments[0]);
	this->composer->deregister_framebuffer(&m_wireframe_framebuffers[1]);
	this->composer->deregister_framebuffer(&m_wireframe_framebuffers[0]);
}
