#include <assert.h>
#include <filesystem>
#include <fstream>
#include <set>

#include <nlohmann/json.hpp>

#include <ktf/math/radians.hpp>
#include <ktf/math/transform.hpp>
#include <ktf/opengl/composer.hpp>
#include <ktf/util/logger.hpp>
#include <ktf/util/obj_loader.hpp>
#include <ktf/util/string.hpp>

#include <common/item/recipe.hpp>
#include <common/logic/furnace_recipes.hpp>

#include "resources.hpp"


ktf::buffer_attrib const pos_attrib = {
	0,
	0,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};
ktf::buffer_attrib const tex_attrib = {
	1,
	1,
	2,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec2<float>),
	0,
	0
};
ktf::buffer_attrib const clr_attrib = {
	2,
	2,
	4,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec4<float>),
	0,
	0
};
ktf::buffer_attrib const nrm_attrib = {
	3,
	3,
	3,
	ktf::composer::data_type_float,
	ktf::composer::bool_false,
	sizeof(ktf::vec3<float>),
	0,
	0
};


const ktf::vec3<float> skybox_vertices[24] = {
	// positions
	{-1.0f,  1.0f, -1.0f},
	{-1.0f, -1.0f, -1.0f},
	{ 1.0f, -1.0f, -1.0f},
	{ 1.0f,  1.0f, -1.0f},

	{-1.0f, -1.0f,  1.0f},
	{-1.0f, -1.0f, -1.0f},
	{-1.0f,  1.0f, -1.0f},
	{-1.0f,  1.0f,  1.0f},

	{ 1.0f, -1.0f, -1.0f},
	{ 1.0f, -1.0f,  1.0f},
	{ 1.0f,  1.0f,  1.0f},
	{ 1.0f,  1.0f, -1.0f},

	{-1.0f, -1.0f,  1.0f},
	{-1.0f,  1.0f,  1.0f},
	{ 1.0f,  1.0f,  1.0f},
	{ 1.0f, -1.0f,  1.0f},

	{-1.0f,  1.0f, -1.0f},
	{ 1.0f,  1.0f, -1.0f},
	{ 1.0f,  1.0f,  1.0f},
	{-1.0f,  1.0f,  1.0f},

	{-1.0f, -1.0f, -1.0f},
	{-1.0f, -1.0f,  1.0f},
	{ 1.0f, -1.0f, -1.0f},
	{ 1.0f, -1.0f,  1.0f}
};

const unsigned int skybox_indices[36] = {
	 0,  1,  2,  2,  3,  0,
	 4,  5,  6,  6,  7,  4,
	 8,  9, 10, 10, 11,  8,
	12, 13, 14, 14, 15, 12,
	16, 17, 18, 18, 19, 16,
	20, 21, 22, 22, 21, 23
};


struct texture_t {
	std::string name;
	std::string path;
	size_t frames;
	size_t delay;

	texture_t();
};

texture_t::texture_t()
: frames(1)
, delay(0)
{
}


struct object_t {
	std::string object_name;
	std::string texture;
};


struct model_t {
	unsigned int index;
	std::string file;
	std::vector<object_t> objects;
};


struct block_model_group_t {
	std::string name;
	std::vector<model_t> models;
};


enum axis_e : uint8_t {
	x_axis = 0,
	y_axis = 1,
	z_axis = 2,
	no_axis = 3
};

#define NUM_ROTATION_AXIS 3
#define NUM_ROTATIONS 4

/*
 * 7 => affected side
 * 3 => rotation axis
 * 4 => 0°, 90°, 180°, 270°
 */
const int model_rotations[side_count][NUM_ROTATION_AXIS][NUM_ROTATIONS] = {
	{ // NORTH
		{e_north, e_down,  e_south, e_up},
		{e_north, e_west,  e_south, e_east},
		{e_north, e_north, e_north, e_north}
	},

	{ // WEST
		{e_west, e_west,  e_west, e_west},
		{e_west, e_south, e_east, e_north},
		{e_west, e_up,    e_east, e_down}
	},

	{ // SOUTH
		{e_south, e_up,    e_north, e_down},
		{e_south, e_east,  e_north, e_west},
		{e_south, e_south, e_south, e_south}
	},

	{ // EAST
		{e_east, e_east,  e_east, e_east},
		{e_east, e_north, e_west, e_south},
		{e_east, e_down,  e_west, e_up}
	},

	{ // DOWN
		{e_down, e_south, e_up,   e_north},
		{e_down, e_down,  e_down, e_down},
		{e_down, e_west,  e_up,   e_east}
	},

	{ // UP
		{e_up, e_north, e_down, e_south},
		{e_up, e_up, 	e_up, 	e_up},
		{e_up, e_east, 	e_down, e_west}
	},

	{ // NONE
		{e_none, e_none, e_none, e_none},
		{e_none, e_none, e_none, e_none},
		{e_none, e_none, e_none, e_none}
	}
};


resources::resources(ktf::composer* composer)
: ktf::assets_index("assets/", composer)
, loadf([this](std::string const& id) {
		return get_sprite(id).id;
	})
, font_alloc {
		[this](size_t* id, std::string const& content, ktf::vec2<float>& size, size_t& num_lines, const std::vector<ktf::vec4<float>>& letter_colors) {
			*id = create_string(content, size, num_lines, letter_colors);
		},
		[this](size_t* id) {
			this->remove_string(id);
		}
	}
{
	std::string home_dir;
#ifdef _WIN32
	home_dir += ::getenv("HOMEDRIVE");
	home_dir += ::getenv("HOMEPATH");
#else
	home_dir += ::getenv("HOME");
#endif
	this->m_cache_dir = home_dir + "/.cache/wuerfel";
	if (!std::filesystem::exists(m_cache_dir)) {
		if (!std::filesystem::create_directories(m_cache_dir)) {
			throw std::runtime_error("Could not create cache directory. Please check permissions or contact the developer!");
		}
	}

	for (::item::item_type_id id=::item::e_none; id<::item::num_item_types;) {
		this->m_model_use_block_texture[id] = true;
		id = (::item::item_type_id)(id + 1);
	}
}

resources::~resources()
{
	this->unload();
}


// PUBLIC FUNCTIONS

void resources::load()
{
	this->unload();
    
	if(!std::filesystem::exists("assets/cache")) {
		std::filesystem::create_directory("assets/cache");
	}

	this->load_post_processing_quad();
	this->load_shaders();
	this->load_fonts();
	this->load_textures();
	this->load_materials();
	this->load_skybox();
	this->load_blocks();
	this->load_items();
	this->load_sprites();

	recipes::load();
	furnace_recipes::load(); // TODO move to common initialization
}

void resources::unload()
{
	this->clear();
	this->m_animation_map.reset();
}

void resources::bind_animation_map()
{
	this->m_animation_map.bind();
}

void resources::update_animation_map()
{
	this->m_animation_map.update(1);	// TODO time
}

resources::mesh_data_t const* resources::get_block_model(block_t block, side_t orientation, state_t state) const
{
	// set to 0 if invalid
	block = block * (block < e_block_count);
	orientation = (side_t)(orientation * (orientation < side_count));

	std::vector<mesh_data_t> const& vec = m_block_models[block][orientation];
	if (vec.size() > state) {
		return &vec[state];
	} else {
		return nullptr;
	}
}

bool resources::uses_block_texture(::item::item_type_id id) const
{
	if (id < 0 || id >= ::item::num_item_types) {
		return false;
	} else {
		return m_model_use_block_texture[id];
	}
}


// PRIVATE FUNCTIONS
void resources::load_post_processing_quad()
{

	ktf::mesh2d mesh;
	for (auto v : sprite_vertices) {
		v.w = 1.0f - v.w;
		mesh.pos.push(v);
	}
	for (const auto& i : sprite_indices) {
		mesh.indices.push(i);
	}
	std::vector<ktf::buffer_data> data = {
		{
			mesh.pos.size() * sizeof(ktf::vec4<float>),
			mesh.pos.data(),
			ktf::composer::draw_hint_static
		}
	};
	ktf::buffer_attrib const sprite_attrib = {
		0,
		0,
		4,
		ktf::composer::data_type_float,
		ktf::composer::bool_false,
		sizeof(ktf::vec4<float>),
		0,
		0
	};
	std::vector<ktf::buffer_attrib> attribs = {
		sprite_attrib
	};
	size_t id;
	this->composer->register_mesh(&id, data, attribs, mesh.indices);
	std::string const name("internal:quad");
	this->mesh_map.emplace(name, id);
	sprite sp = {id, 1.0f};
	this->sprites.emplace(name, sp);
}

void resources::load_shaders()
{
	this->load_shader("ui::shader.font", "shaders/font.vert", "shaders/font.frag");
	this->load_shader("ui::shader.gui", "shaders/gui.vert", "shaders/gui.frag");
	this->load_shader("gui::shader.buffer", "shaders/buffer.vert", "shaders/buffer.frag");
	this->load_shader("game::shader.solid", "shaders/phong.vert", "shaders/phong.frag");
	this->load_shader("game::shader.transparent", "shaders/transparent.vert", "shaders/transparent.frag");
	this->load_shader("game::shader.entity", "shaders/entity.vert", "shaders/entity.frag");
	this->load_shader("game::shader.skybox", "shaders/skybox.vert", "shaders/skybox.frag");
	this->load_shader("game::shader.animated", "shaders/anim.vert", "shaders/anim.frag");
	this->load_shader("shader::flat_textured", "shaders/flat_textured.vert", "shaders/flat_textured.frag");
	this->load_shader("shader:normal_map", "shaders/normal_map.vert", "shaders/normal_map.frag");
	this->load_shader("shader:median", "shaders/median.vert", "shaders/median.frag");
	this->load_shader("shader:sobel", "shaders/sobel.vert", "shaders/sobel.frag");
	this->load_shader("shader:invert", "shaders/invert.vert", "shaders/invert.frag");
}

void resources::load_textures()
{
	this->load_texture("ui::texture.font", "cache/glyph_map.png");
	this->load_texture("entity::texture.player", "textures/entity/skin.png");
	this->load_texture("entity::texture.pig", "textures/entity/pig.png");

	std::string skybox_folder = "textures/skybox/";
	std::array<std::string, 6> skybox_paths = {
		skybox_folder + "skybox_right.png",
		skybox_folder + "skybox_left.png",
		skybox_folder + "skybox_top.png",
		skybox_folder + "skybox_bottom.png",
		skybox_folder + "skybox_front.png",
		skybox_folder + "skybox_back.png"
	};
	this->load_cube_texture("game::texture.skybox", skybox_paths);

	// load sky decoration (sun + moon)
	ktf::png sun_texture;
	ktf::png moon_texture;
	sun_texture.load("assets/" + skybox_folder + "sun.png");
	moon_texture.load("assets/" + skybox_folder + "moon.png");
	if (!sun_texture.valid() || !moon_texture.valid()) {
		ktf::logger::error("Could not load sky decoration textures!");
	}
	if (sun_texture.get_width() != moon_texture.get_width() || sun_texture.get_height() != moon_texture.get_height()) {
		ktf::logger::error("Sky decoration textures are not the same size!");
	} else {
		std::vector<uint8_t> empty_data;
		empty_data.resize(sun_texture.get_width() * sun_texture.get_height() * 4);
		::memset(empty_data.data(), 0, empty_data.size());
		ktf::png empty_image(sun_texture.get_width(), sun_texture.get_height(), 4, empty_data.data());
		std::array<ktf::png, 6> sky_decoration_textures = {
			empty_image,
			empty_image,
			std::move(sun_texture),
			std::move(moon_texture),
			empty_image,
			empty_image,
		};
		this->load_cube_texture("game::texture.sky_deco", sky_decoration_textures);
	}
}

void resources::load_fonts()
{
	std::string chars = u8"ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜẞabcdefghijklmnopqrstuvwxyzäöüß0123456789,;.:-_#'+*~!\"§$%&/()=?'`{[]}\\@€<>|^°¹²³⁴⁵⁶⁷⁸⁹⁰";
	this->load_font("fonts/FuntaunaRegular.otf", "assets/cache/", ktf::to_utf32(chars));
}

void resources::load_materials()
{
	this->load_material("materials/block.mtl");
}

void resources::load_skybox()
{
	// TODO move
	// entity models
	this->load_mesh("models/player.obj");
	this->load_mesh("models/pig.obj");

	// skybox model
	ktf::mesh3d skybox_mesh;
	for (int i=0; i<24; i++) {
		skybox_mesh.pos.push(skybox_vertices[i]);
	}
	for (int i=0; i<36; i++) {
		skybox_mesh.indices.push(skybox_indices[i]);
	}
	size_t mesh;

	std::vector<ktf::buffer_data> data = {
		{
			skybox_mesh.pos.size() * sizeof(ktf::vec3<float>),
			skybox_mesh.pos.data(),
			ktf::composer::draw_hint_static
		}
	};
	std::vector<ktf::buffer_attrib> attribs = {
		pos_attrib
	};
	this->composer->register_mesh(&mesh, data, attribs, skybox_mesh.indices);

	if (mesh == (size_t)-1) {
		// error
	} else {
		this->mesh_map.emplace("game::mesh.skybox", mesh);
	}
}

void resources::load_blocks()
{
	auto const get_axis = [](std::string const& axis) {
		if (axis == "x") {
			return x_axis;
		} else if (axis == "y") {
			return y_axis;
		} else if (axis == "z") {
			return z_axis;
		} else {
			return no_axis;
		}
	};

	std::unordered_map<std::string, texture_t> texture_list;
	std::unordered_map<std::string, std::unique_ptr<ktf::obj>> objects;
	std::vector<block_model_group_t> model_list;
	std::unordered_map<std::string, std::vector<mesh_data_t>> mod_groups;

	// init
	std::ifstream file("assets/models/models-blocks.json");
	nlohmann::json json;
	file >> json;
	if (!json.is_object()) {
		throw std::runtime_error("json: no object");
	}

	// parse texture list and generate texture atlas
	nlohmann::json textures = json["textures"];
	if (!textures.is_array()) {
		throw std::runtime_error("textures: no array");
	}
	for (auto const& texture : textures) {
		if (!texture.is_object()) {
			throw std::runtime_error("texture: no object");
		}
		texture_t tex;
		texture["name"].get_to(tex.name);
		texture["file"].get_to(tex.path);
		nlohmann::json animation = texture["animation"];
		if (animation.is_object()) {
			animation["frames"].get_to(tex.frames);
			animation["delay"].get_to(tex.delay);
			if (tex.delay <= 0) {
				tex.delay = 1;
			}
			if (tex.frames <= 0) {
				tex.frames = 1;
			}
		}
		texture_list.emplace(tex.name, tex);
	}


	// load obj-files
	nlohmann::json object_files = json["objFiles"];
	if (!object_files.is_array()) {
		throw std::runtime_error("objFiles: no array");
	}
	for (auto const& file : object_files) {
		std::string filename;
		file.get_to(filename);
		objects.emplace(filename, std::make_unique<ktf::obj>("assets/models/" + filename));
	}

	std::set<std::string> unused_textures;
	for (auto const& [key, value] : texture_list) {
		unused_textures.emplace(key);
	}

	// load models
	nlohmann::json model_groups = json["modelGroups"];
	if (!model_groups.is_array()) {
		throw std::runtime_error("modelGroups: no array");
	}
	for (auto const& block : model_groups) {
		if (!block.is_object()) {
			ktf::logger::error("ERROR BLOCK OBJECT");
			continue;
		}

		block_model_group_t block_model_vector;
		block["name"].get_to(block_model_vector.name);

		nlohmann::json models = block["models"];
		if (!models.is_array()) {
			ktf::logger::error("ERROR MODELS ARRAY");
			continue;
		}
		unsigned int num_models;
		block["numModels"].get_to(num_models);
		if (num_models > (uint8_t)-1) {
			ktf::logger::error("ERROR NUM MODELS");
			continue;
		}
		for (auto const& model : models) {
			model_t single_model;

			model["index"].get_to(single_model.index);
			model["file"].get_to(single_model.file);
			auto const& objects = model["objects"];
			if (objects.is_array()) {
				for (auto const& object : objects) {
					object_t o;
					object["name"].get_to(o.object_name);
					object["texture"].get_to(o.texture);
					single_model.objects.push_back(o);
					if (unused_textures.find(o.texture) != unused_textures.end()) {
						unused_textures.erase(o.texture);
					}
				}
			}
			if (single_model.index < num_models) {
				block_model_vector.models.push_back(single_model);
			}
		}
		if (block_model_vector.models.size() == num_models) {
			model_list.push_back(block_model_vector);
		}
	}

	// remove duplicates or unused
	std::set<std::string> filtered_textures;
	for (auto const& [key, value] : texture_list) {
		if (unused_textures.find(key) == unused_textures.end()) {
			filtered_textures.emplace(value.path);
		}
	}

	// generate texture atlas
	std::vector<ktf::atlas_pair> textures_map;
	for (auto& path : filtered_textures) {
		textures_map.push_back({path, ("assets/textures/blocks/" + path)});
	}
	ktf::texture_atlas::generate(textures_map, "assets/cache/blocks", 4, 0, [](int size) {
		int dim = 1;
		while (dim < size) {
			dim *= 2;
		}
		return dim;
	});

	// load generated atlas
	std::string const texatlas_id("blocks");
	this->load_atlas(texatlas_id, "cache/blocks.tm");
	this->load_texture("texture::blocks", "cache/blocks.png");
	const auto& texture_atlas = atlas_map.at(texatlas_id);

	// generate animation map
	std::vector<texture_anim> animations;
	for (auto const& [key, value] : texture_list) {
		if (unused_textures.find(key) == unused_textures.end()) {
			texture_anim a;
			a.name = key;
			a.frames = value.frames;
			a.delay = value.delay;
			auto const* entry = texture_atlas.get_entry(value.path);
			if (entry) {
				a.offset = (entry->uv[1].y - entry->uv[0].y) / value.frames;
			} else {
				a.offset = 0.0f;
			}
			animations.push_back(a);
		}
	}
	this->m_animation_map.init(animations);

	// apply texture atlas
	for (block_model_group_t const& vec : model_list) {
		mod_groups.emplace(vec.name, std::vector<mesh_data_t>());
		std::vector<mesh_data_t>& model_group = mod_groups.at(vec.name);
		model_group.resize(vec.models.size());
		for (model_t const& model : vec.models) {
			auto object_it = objects.find(model.file);
			if (object_it == objects.end()) {
				ktf::logger::error("ERROR OBJECT");
				continue;
			}

			mesh_data_t& meshes = model_group[model.index];
			for (object_t const& object : model.objects) {
				auto tex_it = texture_list.find(object.texture);
				if (tex_it == texture_list.end()) {
					ktf::logger::error("ERROR TEXTURE");
					continue;
				}

				if (ktf::obj_object* obj_o = object_it->second->get_object(object.object_name)) {
					side_t side = def::get_side(object.object_name);

					size_t vertex_pointer = meshes[side].pos.size();
					meshes[side].pos = obj_o->pos;
					meshes[side].nrm = obj_o->nrm;

					for (size_t j=0; j<meshes[side].pos.size(); j++) {
						meshes[side].clr.push(ktf::vec4<float>(1.0f));
					}
					const ktf::entry* entry = texture_atlas.get_entry(tex_it->second.path);
					size_t offset_index = m_animation_map.get_index(tex_it->second.name);
					ktf::vec2<double> dim(1.0f);
					ktf::vec2<double> uv0(0.0f);
					if (entry) {
						uv0 = entry->uv[0];
						dim = entry->uv[1] - entry->uv[0];
						dim.y /= tex_it->second.frames;
					}
					for (size_t j=0; j<obj_o->tex.size(); j++) {
						ktf::vec2<double> tex = obj_o->tex[j];
						tex = (ktf::vec2<double>(tex.x, 1.0 - tex.y) ^ dim) + uv0;
						meshes[side].tex.push(tex);
						meshes[side].offsets.push(offset_index);
					}
					for (size_t j=0; j<obj_o->indices.size(); j++) {
						meshes[side].indices.push(obj_o->indices[j] + vertex_pointer);
					}
				} else {
					ktf::logger::error("ERROR OBJ: " + object.object_name + " " + vec.name);
				}
			}
		}
	}


	// reset model list
	for (size_t b=0; b<e_block_count; b++) {
		for (size_t o=0; o<NUM_ORIENTATIONS; o++) {
			this->m_block_models[b][o].clear();
		}
	}


	// generate adjusted block models
	nlohmann::json block_configs = json["blocks"];
	if (!block_configs.is_object()) {
		throw std::exception();
	}
	for (block_t block_id=1; block_id<e_block_count; ++block_id) {
		std::string name = def::block_defs[block_id].name;
		auto const& config = block_configs[name];
		if (config.is_null()) {
			ktf::logger::error("ERROR CONFIG: " + name);
			continue;
		}
		for (auto const& face : config) {
			std::string type, model, axis;
			int degree;
			face["type"].get_to(type);
			face["model"].get_to(model);
			if (face.contains("axis") && face.contains("degree")) {
				face["axis"].get_to(axis);
				face["degree"].get_to(degree);
			}

			auto const& it = mod_groups.find(model);
			if (it == mod_groups.end()) {
				ktf::logger::error("ERROR MODEL GROUP");
				continue;
			}

			axis_e rotation_axis = get_axis(axis);
			std::vector<side_t> orientations = get_orientations(type);
			double rad = ktf::radians((double)degree);
			uint8_t rstep = (degree / 90) % 4;
			for (side_t orientation : orientations) {
				this->m_block_models[block_id][orientation].resize(it->second.size());

				for (size_t i=0; i<it->second.size(); ++i) {
					auto const& mod = it->second[i];

					for (uint8_t side=0; side<side_count; ++side) {
						uint8_t curr_side = side;
						switch (rotation_axis) {
						case x_axis:
							curr_side = model_rotations[side][x_axis][rstep];
							for (size_t j=0; j<mod[side].pos.size(); j++) {
								ktf::vec3<double> pos = mod[side].pos[j];
								double py = std::cos(rad)*pos.y + std::sin(rad)*pos.z;
								double pz = std::cos(rad)*pos.z - std::sin(rad)*pos.y;
								pos = ktf::vec3<double>(pos.x, py, pz);
								this->m_block_models[block_id][orientation][i][curr_side].pos.push(pos);
							}
							for (size_t j=0; j<mod[side].nrm.size(); j++) {
								ktf::vec3<double> nrm = mod[side].nrm[j];
								double ny = std::cos(rad)*nrm.y + std::sin(rad)*nrm.z;
								double nz = std::cos(rad)*nrm.z - std::sin(rad)*nrm.y;
								nrm = ktf::vec3<double>(nrm.x, ny, nz);
								this->m_block_models[block_id][orientation][i][curr_side].nrm.push(nrm);
							}
							break;
						case y_axis:
							curr_side = model_rotations[side][y_axis][rstep];
							for (size_t j=0; j<mod[side].pos.size(); j++) {
								const ktf::vec3<double>& pos = mod[side].pos[j];
								double px = std::cos(rad)*pos.x + std::sin(rad)*pos.z;
								double pz = std::cos(rad)*pos.z - std::sin(rad)*pos.x;
								this->m_block_models[block_id][orientation][i][curr_side].pos.push(ktf::vec3<double>(px, pos.y, pz));
							}
							for (size_t j=0; j<mod[side].nrm.size(); j++) {
								const ktf::vec3<double>& nrm = mod[side].nrm[j];
								double nx = std::cos(rad)*nrm.x + std::sin(rad)*nrm.z;
								double nz = std::cos(rad)*nrm.z - std::sin(rad)*nrm.x;
								this->m_block_models[block_id][orientation][i][curr_side].nrm.push(ktf::vec3<double>(nx, nrm.y, nz));
							}
							break;
						case z_axis:
							curr_side = model_rotations[side][z_axis][rstep];
							for (size_t j=0; j<mod[side].pos.size(); j++) {
								const ktf::vec3<double>& pos = mod[side].pos[j];
								double px = std::cos(rad)*pos.x + std::sin(rad)*pos.y;
								double py = std::cos(rad)*pos.y - std::sin(rad)*pos.x;
								this->m_block_models[block_id][orientation][i][curr_side].pos.push(ktf::vec3<double>(px, py, pos.z));
							}
							for (size_t j=0; j<mod[side].nrm.size(); j++) {
								const ktf::vec3<double>& nrm = mod[side].nrm[j];
								double nx = std::cos(rad)*nrm.x + std::sin(rad)*nrm.y;
								double ny = std::cos(rad)*nrm.y - std::sin(rad)*nrm.x;
								this->m_block_models[block_id][orientation][i][curr_side].nrm.push(ktf::vec3<double>(nx, ny, nrm.z));
							}
							break;
						case no_axis:
							this->m_block_models[block_id][orientation][i][curr_side].pos = mod[side].pos;
							this->m_block_models[block_id][orientation][i][curr_side].nrm = mod[side].nrm;
							break;
						}

						this->m_block_models[block_id][orientation][i][curr_side].tex = mod[side].tex;
						this->m_block_models[block_id][orientation][i][curr_side].clr = mod[side].clr;
						this->m_block_models[block_id][orientation][i][curr_side].offsets = mod[side].offsets;
						this->m_block_models[block_id][orientation][i][curr_side].indices = mod[side].indices;
					}
				}
			}
		}
	}
}

void resources::load_items()
{
	struct item_model {
		bool valid = true;
		bool use_standard_model = true;
		std::string texture;
		std::string model_file;
	};

	std::vector<item_model> models(::item::num_item_types);

	std::ifstream file("assets/models/models-items.json");
	nlohmann::json json;
	file >> json;
	if (!json.is_object()) {
		throw std::runtime_error("json: no object");
	}
	auto const& items = json["items"];
	if (!json.is_object()) {
		throw std::runtime_error("items: no object");
	}
	for (::item::item_type_id id=::item::item_type_id(::item::e_none + 1); id<::item::num_item_types; id = (::item::item_type_id)(id + 1)) {
		::item::item_type const * type = item::all_types[id];
		if (items.find(type->name) == items.end()) {
			models[id].valid = false;
			continue;
			//throw std::runtime_error("item: missing model");
		}
		auto const& item = items[type->name];
		if (!item.is_object()) {
			throw std::runtime_error("item: no object");
		}
		auto const& model = item["model"];
		if (!model.is_object()) {
			if (type->features.block) {
				models[id].use_standard_model = true;
			} else {
				models[id].valid = false;
				throw std::runtime_error("missing item model");
			}
		} else {
			auto const& file = model["file"];
			model["texture"].get_to(models[id].texture);
			if (file.is_null()) {
				models[id].use_standard_model = true;
			} else {
				models[id].use_standard_model = false;
				file.get_to(models[id].model_file);
			}
		}
	}

	// generate atlas
	std::vector<ktf::atlas_pair> textures_map;
	for (auto const& model : models) {
		if (model.valid && model.texture != "") {
			textures_map.push_back({model.texture, ("assets/textures/items/" + model.texture)});
		}
	}
	ktf::texture_atlas::generate(textures_map, "assets/cache/items", 4, 0, [](int size) {
		int dim = 1;
		while (dim < size) {
			dim *= 2;
		}
		return dim;
	});

	std::string const texatlas_id("items");
	this->load_atlas(texatlas_id, "cache/items.tm");
	this->load_texture("texture::items", "cache/items.png");
	const auto& texture_atlas = atlas_map.at(texatlas_id);

	// generate meshes
	for (::item::item_type_id id=::item::item_type_id(::item::e_none + 1); id<::item::num_item_types; id = (::item::item_type_id)(id + 1)) {
		if (!models[id].valid) {
			this->m_model_use_block_texture[id] = false;
			continue;
		}
		ktf::mesh3d mesh;
		item::item_type const * type = item::all_types[id];
		if (models[id].use_standard_model) {
			if (type->features.block) {
				this->m_model_use_block_texture[id] = true;
				// load from block model
				block_t block = ::item::get_block(type->id);
				for (uint64_t i=0; i<side_count; i++) {
					if (m_block_models[block][e_down].empty()) {
						continue;
					}
					int vertex_pointer = mesh.pos.size();
					auto& p = m_block_models[block][e_down][0].at(i);
					for (unsigned int j=0; j<p.pos.size(); j++) {
						mesh.pos.push(p.pos[j]);
					}
					for (unsigned int j=0; j<p.nrm.size(); j++) {
						mesh.nrm.push(p.nrm[j]);
					}
					for (unsigned int j=0; j<p.tex.size(); j++) {
						mesh.tex.push(p.tex[j]);
					}
					for (unsigned int j=0; j<p.indices.size(); j++) {
						mesh.indices.push(p.indices[j] + vertex_pointer);
					}
				}
			} else {
				this->m_model_use_block_texture[id] = false;
				this->build_texture_model(mesh, "assets/textures/items/" + models[id].texture);

				const ktf::entry* entry = texture_atlas.get_entry(models[id].texture);
				ktf::vec2<double> dim(1.0f);
				ktf::vec2<double> uv0(0.0f);
				if (entry) {
					uv0 = entry->uv[0];
					dim = entry->uv[1] - entry->uv[0];
				}
				for (size_t j=0; j<mesh.tex.size(); j++) {
					ktf::vec2<float>& tex = mesh.tex[j];
					tex = uv0 + (ktf::vec2<float>(tex.x, 1.0f - tex.y) ^ dim);
				}
				for (size_t i=0; i<mesh.pos.size(); ++i) {
					mesh.nrm.push(ktf::vec3<float>(1.0f));
				}
			}
			for (size_t i=0; i<mesh.pos.size(); ++i) {
				mesh.clr.push(ktf::vec4<float>(1.0f));
			}
		} else {
			// load obj file
			this->m_model_use_block_texture[id] = false;
			ktf::obj obj("assets/models/items/" + models[id].model_file);
			for (size_t i=0; i<obj.num_objects(); ++i) {
				int vertex_pointer = mesh.pos.size();
				auto const& p = obj.get_object(i);
				for (unsigned int j=0; j<p.pos.size(); j++) {
					mesh.pos.push(p.pos[j]);
					mesh.clr.push(ktf::vec4<float>(1.0f));
				}
				for (unsigned int j=0; j<p.nrm.size(); j++) {
					mesh.nrm.push(p.nrm[j]);
				}
				for (unsigned int j=0; j<p.tex.size(); j++) {
					mesh.tex.push(p.tex[j]);
				}
				for (unsigned int j=0; j<p.indices.size(); j++) {
					mesh.indices.push(p.indices[j] + vertex_pointer);
				}
			}

			const ktf::entry* entry = texture_atlas.get_entry(models[id].texture);
			ktf::vec2<double> dim(1.0f);
			ktf::vec2<double> uv0(0.0f);
			if (entry) {
				uv0 = entry->uv[0];
				dim = entry->uv[1] - entry->uv[0];
			}
			for (size_t j=0; j<mesh.tex.size(); j++) {
				ktf::vec2<float>& tex = mesh.tex[j];
				tex = uv0 + (ktf::vec2<float>(tex.x, 1.0 - tex.y) ^ dim);
			}
		}

		size_t mesh_id;
		std::vector<ktf::buffer_data> data = {
			{
				mesh.pos.size() * sizeof(ktf::vec3<float>),
				mesh.pos.data(),
				ktf::composer::draw_hint_static
			},
			{
				mesh.tex.size() * sizeof(ktf::vec2<float>),
				mesh.tex.data(),
				ktf::composer::draw_hint_static
			},
			{
				mesh.clr.size() * sizeof(ktf::vec4<float>),
				mesh.clr.data(),
				ktf::composer::draw_hint_static
			},
			{
				mesh.nrm.size() * sizeof(ktf::vec3<float>),
				mesh.nrm.data(),
				ktf::composer::draw_hint_static
			}
		};
		std::vector<ktf::buffer_attrib> attribs = {
			pos_attrib,
			tex_attrib,
			clr_attrib,
			nrm_attrib
		};
		this->composer->register_mesh(&mesh_id, data, attribs, mesh.indices);
		this->mesh_map.emplace("item-3d:" + std::string(type->name), mesh_id);
	}
}

void resources::load_sprites()
{
	std::ifstream file;
	std::set<std::string> ids;
	try {
		file.open("assets/ui/sprites.txt");
		if (!file.is_open()) {
			ktf::logger::error("Could not load sprites.txt");
			return;
		}
		std::string line;
		while (std::getline(file, line)) {
			if (line != "") {
				ids.emplace(line);
			}
		}
	} catch (std::exception const& e) {
	}

	std::vector<ktf::atlas_pair> textures = generate_item_sprites();
	for (auto& id : ids) {
		textures.push_back({id, "assets/textures/gui/" + id});
	}

	ktf::texture_atlas::generate(textures, "assets/cache/atlas-gui", 4, 1);
	this->load_atlas("atlas-gui", "cache/atlas-gui.tm");
	this->load_texture("ui::texture.gui", "cache/atlas-gui.png");
	// create texture map
	for (auto& tex : textures) {
		if (tex.id != "") {
			this->load_sprite(tex.id, "atlas-gui");
		}
	}
}

std::vector<ktf::atlas_pair> resources::generate_item_sprites()
{
	const std::string cache_path("assets/cache/items/");
	const std::string assets_path("assets/textures/items/");
	if (!std::filesystem::exists(cache_path)) {
		std::filesystem::create_directory(cache_path);
	}
	size_t shader_id = get_shader("gui::shader.buffer");

	float const size = 64.0f;
	std::unordered_map<std::string, std::string> sprites_map;
	for (::item::item_type_id i=::item::item_type_id(::item::e_none + 1); i<::item::num_item_types;) {
		::item::item_type const* type = item::all_types[i];
		size_t mesh_id = get_mesh("item-3d:" + std::string(type->name));
		size_t texture_id;
		ktf::matrix<float> mat(1.0f);
		mat = ktf::scale(mat, ktf::vec3<float>(0.6f * size, -0.6f * size, 0.6f * size));
		if (m_model_use_block_texture[i]) {
			texture_id = get_texture("texture::blocks");
			mat = ktf::rotate(mat, (float)ktf::radians(45.0f), ktf::vec3<float>(0.0f, 1.0f, 0.0f));
			mat = ktf::rotate(mat, (float)ktf::radians(-25.0f), ktf::vec3<float>(1.0f, 0.0f, 0.0f));
		} else {
			texture_id = get_texture("texture::items");
		}
		const std::string path = cache_path + type->name + ".png";
		this->composer->generate_texture(path, size, size, 4, shader_id, texture_id, mesh_id, mat);
		sprites_map[type->name] = path;
		i = ::item::item_type_id(i + 1);
	}

	std::vector<ktf::atlas_pair> paths;
	for (const auto& it : sprites_map) {
		paths.push_back({"sprite:" + it.first, it.second});
	}
	return paths;
}

void resources::build_texture_model(ktf::mesh3d& mesh, const std::string& path)
{
	ktf::png texture;
	texture.load(path);
	int channels = texture.get_channels();
	int width = texture.get_width();
	int height = texture.get_height();
	if (channels < 4 || width != height) {
		mesh.pos.push({ 0.5f,  0.5f, 0.0f});
		mesh.pos.push({-0.5f, -0.5f, 0.0f});
		mesh.pos.push({ 0.5f, -0.5f, 0.0f});
		mesh.pos.push({-0.5f,  0.5f, 0.0f});
		mesh.tex.push({1.0f, 1.0f});
		mesh.tex.push({0.0f, 0.0f});
		mesh.tex.push({1.0f, 0.0f});
		mesh.tex.push({0.0f, 1.0f});
		mesh.indices.push(0);
		mesh.indices.push(1);
		mesh.indices.push(2);
		mesh.indices.push(0);
		mesh.indices.push(3);
		mesh.indices.push(1);
	} else {
		uint8_t* data = texture.get_data();

		// generate model sides
		std::vector<bool> transparent(width);
		for (int i=0; i<width; i++) {
			transparent[i] = true;
		}

		float pixel_size = 1.0f / width;
		float pd = pixel_size * 0.5f;
		unsigned int vertex_pointer = 0;
		for (int y=0; y<height; ++y) {
			for (int x=0; x<width; ++x) {
				bool t = (data[(y * width + x) * channels + 3] == 0);

				vertex_pointer = mesh.pos.size();
				bool xp = (x == 0 ? true : transparent[x-1]);
				if (xp && !t) {
					// left quad
					float px0 = pixel_size * x;
					float px1 = px0 + pixel_size;
					float py1 = 1.0f - (pixel_size * y);
					float py0 = py1 - pixel_size;
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f,  pd});
					mesh.pos.push({px0 - 0.5f, py0 - 0.5f, -pd});
					mesh.pos.push({px0 - 0.5f, py0 - 0.5f,  pd});
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f, -pd});
					mesh.tex.push({px1, py1});
					mesh.tex.push({px0, py0});
					mesh.tex.push({px1, py0});
					mesh.tex.push({px0, py1});
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 1);
					mesh.indices.push(vertex_pointer + 2);
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 3);
					mesh.indices.push(vertex_pointer + 1);
				} else if (!xp && t) {
					// right quad
					float px0 = pixel_size * x;
					float py1 = 1.0f - (pixel_size * y);
					float py0 = py1 - pixel_size;
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f, -pd});
					mesh.pos.push({px0 - 0.5f, py0 - 0.5f,  pd});
					mesh.pos.push({px0 - 0.5f, py0 - 0.5f, -pd});
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f,  pd});
					mesh.tex.push({px0, py1});
					mesh.tex.push({px0 - pixel_size, py0});
					mesh.tex.push({px0, py0});
					mesh.tex.push({px0 - pixel_size, py1});
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 1);
					mesh.indices.push(vertex_pointer + 2);
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 3);
					mesh.indices.push(vertex_pointer + 1);
				}

				vertex_pointer = mesh.pos.size();
				bool yp = transparent[x];
				if (yp && !t) {
					// up quad
					float px0 = pixel_size * x;
					float px1 = px0 + pixel_size;
					float py1 = 1.0f - (pixel_size * y);
					float py0 = py1 - pixel_size;
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f, -pd});
					mesh.pos.push({px1 - 0.5f, py1 - 0.5f,  pd});
					mesh.pos.push({px1 - 0.5f, py1 - 0.5f, -pd});
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f,  pd});
					mesh.tex.push({px0, py1});
					mesh.tex.push({px1, py0});
					mesh.tex.push({px1, py1});
					mesh.tex.push({px0, py0});
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 1);
					mesh.indices.push(vertex_pointer + 2);
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 3);
					mesh.indices.push(vertex_pointer + 1);
				} else if (!yp && t) {
					// down quad
					float px0 = pixel_size * x;
					float px1 = px0 + pixel_size;
					float py1 = 1.0f - (pixel_size * y);
					mesh.pos.push({px1 - 0.5f, py1 - 0.5f, -pd});
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f,  pd});
					mesh.pos.push({px0 - 0.5f, py1 - 0.5f, -pd});
					mesh.pos.push({px1 - 0.5f, py1 - 0.5f,  pd});
					mesh.tex.push({px1, py1 + pixel_size});
					mesh.tex.push({px0, py1});
					mesh.tex.push({px0, py1 + pixel_size});
					mesh.tex.push({px1, py1});
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 1);
					mesh.indices.push(vertex_pointer + 2);
					mesh.indices.push(vertex_pointer + 0);
					mesh.indices.push(vertex_pointer + 3);
					mesh.indices.push(vertex_pointer + 1);
				}
				transparent[x] = t;
			}
			// right border
			if (!transparent[width-1]) {
				// right quad
				float px0 = pixel_size * (width - 1);
				float px1 = px0 + pixel_size;
				float py1 = 1.0f - (pixel_size * y);
				float py0 = py1 - pixel_size;
				mesh.pos.push({px0 - 0.5f, py1 - 0.5f, -pd});
				mesh.pos.push({px0 - 0.5f, py0 - 0.5f,  pd});
				mesh.pos.push({px0 - 0.5f, py0 - 0.5f, -pd});
				mesh.pos.push({px0 - 0.5f, py1 - 0.5f,  pd});
				mesh.tex.push({px1, py1});
				mesh.tex.push({px0, py0});
				mesh.tex.push({px1, py0});
				mesh.tex.push({px0, py1});
				mesh.indices.push(vertex_pointer + 0);
				mesh.indices.push(vertex_pointer + 1);
				mesh.indices.push(vertex_pointer + 2);
				mesh.indices.push(vertex_pointer + 0);
				mesh.indices.push(vertex_pointer + 3);
				mesh.indices.push(vertex_pointer + 1);
			}
		}
		// upper border
		for (int x=0; x<width; ++x) {
			if (!transparent[x]) {
				vertex_pointer = mesh.pos.size();
				float px0 = pixel_size * x;
				float px1 = px0 + pixel_size;
				float py1 = 1.0f - (pixel_size * (height-1));
				float py0 = py1 - pixel_size;
				mesh.pos.push({px1 - 0.5f, py0 - 0.5f, -pd});
				mesh.pos.push({px0 - 0.5f, py0 - 0.5f,  pd});
				mesh.pos.push({px0 - 0.5f, py0 - 0.5f, -pd});
				mesh.pos.push({px1 - 0.5f, py0 - 0.5f,  pd});
				mesh.tex.push({px1, py1});
				mesh.tex.push({px0, py0});
				mesh.tex.push({px0, py1});
				mesh.tex.push({px1, py0});
				mesh.indices.push(vertex_pointer + 0);
				mesh.indices.push(vertex_pointer + 1);
				mesh.indices.push(vertex_pointer + 2);
				mesh.indices.push(vertex_pointer + 0);
				mesh.indices.push(vertex_pointer + 3);
				mesh.indices.push(vertex_pointer + 1);
			}
		}

		// add sandwich layers
		// front
		vertex_pointer = mesh.pos.size();
		mesh.pos.push({ 0.5f,  0.5f,  pd});
		mesh.pos.push({-0.5f, -0.5f,  pd});
		mesh.pos.push({ 0.5f, -0.5f,  pd});
		mesh.pos.push({-0.5f,  0.5f,  pd});
		mesh.tex.push({1.0f, 1.0f});
		mesh.tex.push({0.0f, 0.0f});
		mesh.tex.push({1.0f, 0.0f});
		mesh.tex.push({0.0f, 1.0f});
		mesh.indices.push(vertex_pointer + 0);
		mesh.indices.push(vertex_pointer + 1);
		mesh.indices.push(vertex_pointer + 2);
		mesh.indices.push(vertex_pointer + 0);
		mesh.indices.push(vertex_pointer + 3);
		mesh.indices.push(vertex_pointer + 1);

		// back
		vertex_pointer = mesh.pos.size();
		mesh.pos.push({-0.5f,  0.5f, -pd});
		mesh.pos.push({ 0.5f, -0.5f, -pd});
		mesh.pos.push({-0.5f, -0.5f, -pd});
		mesh.pos.push({ 0.5f,  0.5f, -pd});
		mesh.tex.push({0.0f, 1.0f});
		mesh.tex.push({1.0f, 0.0f});
		mesh.tex.push({0.0f, 0.0f});
		mesh.tex.push({1.0f, 1.0f});
		mesh.indices.push(vertex_pointer + 0);
		mesh.indices.push(vertex_pointer + 1);
		mesh.indices.push(vertex_pointer + 2);
		mesh.indices.push(vertex_pointer + 0);
		mesh.indices.push(vertex_pointer + 3);
		mesh.indices.push(vertex_pointer + 1);
	}
}

std::vector<side_t> resources::get_orientations(const std::string& type) const
{
	if(type == "xyz") {
		return {e_north, e_south, e_west, e_east, e_down, e_up};
	} else if(type == "x") {
		return {e_north, e_south};
	} else if(type == "y") {
		return {e_down, e_up};
	} else if(type == "z") {
		return {e_west, e_east};
	} else if(type == "up") {
		return {e_up};
	} else if(type == "down") {
		return {e_down};
	} else if(type == "north") {
		return {e_north};
	} else if(type == "south") {
		return {e_south};
	} else if(type == "west") {
		return {e_west};
	} else if(type == "east") {
		return {e_east};
	} else {
		return {e_none};
	}
}
