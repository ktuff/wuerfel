#include <client/rendering/chunk_mesh.hpp>
#include <client/rendering/resources.hpp>
#include <common/systems.hpp>


mesh_generator::mesh_generator(const game::chunk_ptr& chunk, std::vector<ktf::mesh3d>* meshes, resources* res, bool combined)
: m_chunk(chunk)
, m_meshes(meshes)
, m_resources(res)
, m_combined(combined)
{
}


void mesh_generator::compute_full_mesh()
{
	const ccoord_t& p = m_chunk->posw;
	int8_t topsc = m_chunk->get_top();
	for (coord_t h=0; h<(topsc+1)*CHUNK_SIZE; h++) {
		for (coord_t d=p.y; d<p.y+CHUNK_SIZE; d++) {
			for (coord_t w=p.x; w<p.x+CHUNK_SIZE; w++) {
				block_t block = m_chunk->get_block(w, h, d);
				if (block != e_block_air) {
					transparency tval = m_chunk->get_transparency(w, h, d);
					if (tval.value) {
						light_array lval = m_chunk->get_light_array(w, h, d);
						this->add_block({w-p.x, h, d-p.y}, block, tval, lval, w, h, d);
					}
				}
			}
		}
	}
}

void mesh_generator::compute_light_values()
{
	const ccoord_t& p = m_chunk->posw;
	int8_t topsc = m_chunk->get_top();
	for (coord_t h=0; h<(topsc+1)*CHUNK_SIZE; h++) {
		for (coord_t d=p.y; d<p.y+CHUNK_SIZE; d++) {
			for (coord_t w=p.x; w<p.x+CHUNK_SIZE; w++) {
				block_t block = m_chunk->get_block(w, h, d);
				if (block != e_block_air) {
					transparency tval = m_chunk->get_transparency(w, h, d);
					if (tval.value) {
						light_array lval = m_chunk->get_light_array(w, h, d);
						this->add_light(block, tval, lval, w, h, d);
					}
				}
			}
		}
	}
}

void mesh_generator::compute_single_block(ktf::vec3<coord_t> const& pos)
{
	block_t block = m_chunk->get_block(pos.x, pos.y, pos.z);
	if (block != e_block_air) {
		transparency tval = m_chunk->get_transparency(pos.x, pos.y, pos.z);
		if (tval.value) {
			light_array lval = m_chunk->get_light_array(pos.x, pos.y, pos.z);
			this->add_block({0, 0, 0}, block, tval, lval, pos.x, pos.y, pos.z);
		}
	}
}

void mesh_generator::add_block(ktf::vec3<coord_t> const& pos, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	auto* mesh = mesh_group(block);
	switch (block) {
	case e_block_air:
		return;
	case e_block_circuit_wire:
		this->add_block_circuit(mesh, pos, t, l, w, h, d);
		break;
	default:
		this->add_block_default(mesh, pos, block, t, l, w, h, d);
		break;
	}
}

void mesh_generator::add_block_default(ktf::mesh3d* mesh, ktf::vec3<coord_t> const& pos, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	std::array<ktf::mesh3d, side_count> const* model = nullptr;
	for (uint64_t side=0; side<side_count; side++) {
		if (is_transparent(t, all_sides[side])) {
			if (!model) {
				side_t orientation = m_chunk->get_orientation(w, h, d);
				state_t state = m_chunk->get_state(w, h, d);
				if (!(model = m_resources->get_block_model(block, orientation, state))) {
					return;
				}
			}
			const auto& p = (*model)[all_sides[side]];
			if (!p.indices.size()) {
				continue;
			}

			light_t lv = get_light(l, all_sides[side]);
			float light_value = std::max(lv.sky, lv.block) / float(LIGHT_MAX_VALUE);
			ktf::vec4<float> clr(light_value, light_value, light_value, 1.0f);

			size_t vertex_ptr = mesh->pos.size();
			for (size_t i=0; i<p.pos.size(); i++) {
				mesh->pos.push(p.pos[i] + pos);
				mesh->clr.push(clr);
			}
			for (size_t i=0; i<p.tex.size(); i++) {
				mesh->tex.push(p.tex[i]);
			}
			for (size_t i=0; i<p.nrm.size(); i++) {
				mesh->nrm.push(p.nrm[i]);
			}
			for (size_t i=0; i<p.offsets.size(); i++) {
				mesh->offsets.push(p.offsets[i]);
			}
			for (size_t i=0; i<p.indices.size(); i++) {
				mesh->indices.push(vertex_ptr + p.indices[i]);
			}
		}
	}
}

void mesh_generator::add_block_circuit(ktf::mesh3d* mesh, ktf::vec3<coord_t> const& pos, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	float const base = 0.3f;

	float value = 0.0f;
	std::array<ktf::mesh3d, side_count> const* model = nullptr;
	for (uint64_t side=0; side<side_count; side++) {
		if (is_transparent(t, all_sides[side])) {
			if (!model) {
				side_t orientation = m_chunk->get_orientation(w, h, d);
				if (!(model = m_resources->get_block_model(e_block_circuit_wire, orientation, 0))) {
					return;
				}
				value = m_chunk->get_power(w, h, d);
				value = base + value * ((1.0f - base) / CIRCUIT_MAX_VALUE);
			}
			const auto& p = (*model)[all_sides[side]];
			if (!p.indices.size()) {
				continue;
			}

			light_t lv = get_light(l, all_sides[side]);
			float light_value = std::max(lv.sky, lv.block) / float(LIGHT_MAX_VALUE);
			size_t vertex_ptr = mesh->pos.size();
			for (size_t i=0; i<p.pos.size(); i++) {
				mesh->pos.push(p.pos[i]+pos);
				mesh->clr.push({value * light_value, 0.0f, 0.0f, 1.0f});
			}
			for (size_t i=0; i<p.tex.size(); i++) {
				mesh->tex.push(p.tex[i]);
			}
			for (size_t i=0; i<p.nrm.size(); i++) {
				mesh->nrm.push(p.nrm[i]);
			}
			for (size_t i=0; i<p.offsets.size(); i++) {
				mesh->offsets.push(p.offsets[i]);
			}
			for (size_t i=0; i<p.indices.size(); i++) {
				mesh->indices.push(vertex_ptr + p.indices[i]);
			}
		}
	}
}

void mesh_generator::add_light(block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	auto* mesh = mesh_group(block);
	switch (block) {
	case e_block_air:
		return;
	case e_block_circuit_wire:
		this->add_light_circuit(mesh, t, l, w, h, d);
		break;
	default:
		this->add_light_default(mesh, block, t, l, w, h, d);
		break;
	}
}

void mesh_generator::add_light_default(ktf::mesh3d* mesh, block_t block, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	std::array<ktf::mesh3d, side_count> const* model = nullptr;
	for (uint64_t side=0; side<side_count; side++) {
		if (is_transparent(t, all_sides[side])) {
			if (!model) {
				side_t orientation = m_chunk->get_orientation(w, h, d);
				if (!(model = m_resources->get_block_model(block, orientation, 0))) {	// TODO state
					return;
				}
			}
			const auto& p = (*model)[all_sides[side]];
			if (!p.indices.size()) {
				continue;
			}

			light_t lv = get_light(l, all_sides[side]);
			float light_value = std::max(lv.sky, lv.block) / float(LIGHT_MAX_VALUE);
			ktf::vec4<float> color(light_value, light_value, light_value, 1.0f);
			for (size_t i=0; i<p.pos.size(); i++) {
				mesh->clr.push(color);
			}
		}
	}
}

void mesh_generator::add_light_circuit(ktf::mesh3d* mesh, transparency const& t, light_array const& l, coord_t w, coord_t h, coord_t d)
{
	float const base = 0.3f;

	float value = 0.0f;
	std::array<ktf::mesh3d, side_count> const* model = nullptr;
	for (uint64_t side=0; side<side_count; side++) {
		if (is_transparent(t, all_sides[side])) {
			if (!model) {
				side_t orientation = m_chunk->get_orientation(w, h, d);
				if (!(model = m_resources->get_block_model(e_block_circuit_wire, orientation, 0))) {
					return;
				}
				value = m_chunk->get_power(w, h, d);
				value = base + value * ((1.0f - base) / CIRCUIT_MAX_VALUE);
			}
			const auto& p = (*model)[all_sides[side]];
			if (!p.indices.size()) {
				continue;
			}

			light_t lv = get_light(l, all_sides[side]);
			float light_value = std::max(lv.sky, lv.block) / float(LIGHT_MAX_VALUE);
			for (size_t i=0; i<p.pos.size(); i++) {
				mesh->clr.push({value * light_value, 0.0f, 0.0f, 1.0f});
			}
		}
	}
}

ktf::mesh3d * mesh_generator::mesh_group(block_t block)
{
	if (m_combined) {
		return &(*m_meshes)[0];
	}

	switch (def::block_defs[block].group) {
	case def::e_group_solid:
		return &(*m_meshes)[0];
	default:
		return &(*m_meshes)[1];
	}
}

bool mesh_generator::is_transparent(const transparency& t, side_t side) const
{
	switch (side) {
	case e_west:
		return t.west;
	case e_east:
		return t.east;
	case e_north:
		return t.north;
	case e_south:
		return t.south;
	case e_down:
		return t.down;
	case e_up:
		return t.up;
	default:
		return true;
	}
}

light_t mesh_generator::get_light(const light_array& la, side_t side) const
{
	switch (side) {
	case e_north:
		return la.north;
	case e_west:
		return la.west;
	case e_south:
		return la.south;
	case e_east:
		return la.east;
	case e_down:
		return la.down;
	case e_up:
		return la.up;
	default: {
		light_t value;
		value.sky = std::max(la.north.sky, std::max(la.west.sky, std::max(la.south.sky, std::max(la.east.sky, std::max(la.down.sky, la.up.sky)))));
		value.block = std::max(la.north.block, std::max(la.west.block, std::max(la.south.block, std::max(la.east.block, std::max(la.down.block, la.up.block)))));
		return value;
	}
	}
}
