#pragma once

#include <ktf/ui/interactable.hpp>

#include <common/entity/types.hpp>


class program;
class renderer;
class resources;


namespace client {

class gui;

} // namespace client


namespace game {

class game;
class level;

} // namespace game


#define NUM_KEYS 7
    
class input {

	private:
		program* m_program;
		resources* m_resources;
		renderer* m_renderer;
		game::game* game;
		game::level* m_level;
		::entity::id m_target;
		double cursor_x, cursor_y;
		bool wireframe_mode;
		bool keys[NUM_KEYS];
		float m_speed;
		std::unordered_map<uint64_t, std::function<void(void)>> m_key_map;
		std::weak_ptr<client::gui> m_gui;


	public:
		input(program* prog, resources* res, renderer* r, game::game* game);
		~input();


	public:
		void init_game();
		void close_game();
		void set_gui(std::weak_ptr<client::gui> ui);
		void process_key(int key, int scancode, int action, int mods);
		void process_char(unsigned int codepoint);
		void process_mouse_button(int button, int action, int mods);
		void process_mouse_position(double x_pos, double y_pos);
		void process_mouse_scroll(double x_offset, double y_offset);
		void process(double dt);
		void reset_keys();

	private:
		void start_destroying();
		void stop_destroying();
		void start_interact(bool alt);
		void stop_interact();
		void pick_block();
};
