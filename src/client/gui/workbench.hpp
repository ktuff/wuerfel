#pragma once

#include "crafting_view.hpp"

namespace client {

class workbench_gui : public crafting_view {

	public:
		workbench_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id workbench);
		~workbench_gui() = default;


	public:
		void load() override;
		void on_item_change(int group, int index) override;
// 		void on_item_change(container * p_container, int p_index) override;

};

} // namespace client

