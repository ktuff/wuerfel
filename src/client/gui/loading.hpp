#pragma once

#include "gui.hpp"

namespace client {

class loading_screen : public gui {

	public:
		loading_screen(program* prog, resources* res);
		~loading_screen() = default;


	public:
		void load() override;
};

} // namespace client
