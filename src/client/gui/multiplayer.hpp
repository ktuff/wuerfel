#pragma once

#include "gui.hpp"

namespace client {

class multiplayer_gui : public gui {

	public:
		multiplayer_gui(program *prog, resources* res);
		~multiplayer_gui() = default;


	public:
		void load() override;
};

}

