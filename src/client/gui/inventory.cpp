#include <GLFW/glfw3.h>
#include <ktf/ui/sprite.hpp>
#include <ktf/ui/text.hpp>

#include <common/item/inventory.hpp>
#include "inventory.hpp"
#include "game.hpp"
#include "../program.hpp"

client::inventory_gui::inventory_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: crafting_view(prog, res, entity_system, id, id)
{
}


void client::inventory_gui::load()
{
	gui::load("assets/ui/layout_game_inventory.xml");

	// global shortcuts
	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_E, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});

	// common slots
	this->load_inventory();
	this->init_crafting(4);

	// armor slots
	auto swap_logic_armor = [this](int index) {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
		inv->swap_items(m_entity_id, ::entity::inventory_group::e_other, 0, m_entity_id, ::entity::inventory_group::e_armor, index);
	};
	for (int index=0; index<4; index++) {
		std::string slot_id = "armor-slot-" + std::to_string(index);
		if (auto widget = m_gui.find(slot_id + ":button")) {
			if (auto* slot = dynamic_cast<ktf::ui::interactable*>(widget.get())) {
				this->set_hover_action(slot, slot_id);
				this->set_click_action(slot, slot_id, index, swap_logic_armor);
			}
		}
		inventory_gui::on_item_change(INVENTORY_GROUP_ARMOR, index);
	}
}

void client::inventory_gui::on_item_change(int group, int index)
{
	auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));

	if (group == INVENTORY_GROUP_ITEMS || group == INVENTORY_GROUP_TEMP) {
		container_gui::on_item_change(group, index);
	} else if (group == INVENTORY_GROUP_ARMOR) {
		item::item_stack it = inv->get_item(m_entity_id, ::entity::inventory_group::e_armor, index);
		std::string slot_id = "armor-slot-" + std::to_string(index);
		this->update_item_sprite(slot_id, &it);
	}
}
