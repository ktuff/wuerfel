#include <ktf/ui/sprite.hpp>

#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/item/inventory.hpp>

#include "container.hpp"
#include "../rendering/resources.hpp"


client::container_gui::container_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: gui(prog, res)
, m_entity_system(entity_system)
, m_entity_id(id)
{
	auto* mod = entity_system->get_module(::entity::inventory_system::s_id);
	if (mod) {
		mod->add_listener(this);
	}

	// TODO set sprites to normal / default hide
}

client::container_gui::~container_gui()
{
	auto* mod = m_entity_system->get_module(::entity::inventory_system::s_id);
	if (mod) {
		mod->remove_listener(this);
	}
}


void client::container_gui::on_event(::entity::event* event)
{
	if (event->type == entity::e_item_change) {
		auto* e = static_cast<::entity::event_item_change*>(event);
		this->on_item_change(INVENTORY_GROUP_ITEMS, e->index);
	}
	if (event->type == entity::e_selection_change) {
		auto* e = static_cast<::entity::event_selection_change*>(event);
		this->on_selection_change(e->old_index, e->new_index);
	}
}

void client::container_gui::on_item_change(int group, int index)
{
	auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
	if (group == INVENTORY_GROUP_ITEMS) {
		item::item_stack it = inv->get_item(m_entity_id, ::entity::inventory_group::e_items, index);
		std::string slot_id = "item-slot-" + std::to_string(index);
		this->update_item_sprite(slot_id, &it);
	} else if (group == INVENTORY_GROUP_TEMP) {
		item::item_stack it = inv->get_item(m_entity_id, ::entity::inventory_group::e_other, 0);
		std::string slot_id = "temp-slot-" + std::to_string(index);
		this->update_item_sprite(slot_id, &it);
	} else {
		return;
	}
}

void client::container_gui::on_selection_change(int, int)
{
}

void client::container_gui::load_inventory()
{
	auto swap_logic = [this](int index) {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
		inv->swap_items(m_entity_id, ::entity::inventory_group::e_other, 0, m_entity_id, ::entity::inventory_group::e_items, index);
	};

	auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
	for (int index=0; index<inv->get_inventory_size(m_entity_id, ::entity::inventory_group::e_items); index++) {
		std::string slot_id = "item-slot-" + std::to_string(index);
		if (auto widget = m_gui.find(slot_id + ":button")) {
			if (auto* slot = dynamic_cast<ktf::ui::interactable*>(widget.get())) {
				this->set_hover_action(slot, slot_id);
				this->set_click_action(slot, slot_id, index, swap_logic);
			}
		}
		this->on_item_change(INVENTORY_GROUP_ITEMS, index);
	}
}

void client::container_gui::update_item_sprite(const std::string& slot_id, const item::item_stack* it)
{
	const std::string item_name = item::all_types[it->type]->name;
	auto slot = m_gui.find(slot_id + ":button");
	if (slot) {
		std::string sprite_id = slot_id + ":sprite";
		std::string text_id = slot_id + ":text";
		auto sprite = m_gui.find(sprite_id);
		auto text = m_gui.find(text_id);
		auto * sprite_ptr = dynamic_cast<ktf::ui::sprite*>(sprite.get());
		auto * text_ptr = dynamic_cast<ktf::ui::text*>(text.get());
		if (!sprite_ptr) {
			auto sw = std::make_shared<ktf::ui::sprite>(&m_gui, slot.get(), sprite_id, m_resources->loadf);
			sw->set_policy({ktf::ui::widget::size_policy::FIXED_RATIO, 1.0f});
			sw->set_scale(0.8f);
			sw->set_layer_offset(2);
			this->m_gui.add_widget(sw);
			sw->resize();
			sprite_ptr = sw.get();
		}
		if (!text_ptr) {
			auto tw = std::make_shared<ktf::ui::text>(&m_gui, slot.get(), text_id, m_resources->font_alloc);
			tw->set_offset({ktf::ui::widget::offset::PERCENT, 0.8f, 0.8f});
			tw->set_scale(0.4f);
			tw->set_layer_offset(2);
			this->m_gui.add_widget(tw);
			tw->resize();
			text_ptr = tw.get();
		}

		if (it->count) {
			sprite_ptr->set_content("sprite:" + item_name);
			text_ptr->set_content(std::to_string(it->count));
		} else {
			sprite_ptr->set_content("");
			text_ptr->set_content("");
		}
	}
}

void client::container_gui::set_click_action(ktf::ui::interactable* slot, const std::string & slot_id, int index, std::function<void(int)> swap_logic)
{
	slot->set_action(ktf::event_button, [slot, slot_id, index, swap_logic](ktf::event* e) {
		uint64_t state = slot->state();
		if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(slot->find(slot_id + ":sprite:normal"))) {
			sp->hide(state != 0);
		}
		if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(slot->find(slot_id + ":sprite:hover"))) {
			sp->hide(state != 1);
		}
		if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(slot->find(slot_id + ":sprite:click"))) {
			sp->hide(state != 2);
		}
		if (state == 1) {
			e->handled = true;
			swap_logic(index);
		}
	});
}
