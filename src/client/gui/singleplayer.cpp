#include <GLFW/glfw3.h>
#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>

#include "singleplayer.hpp"
#include "title.hpp"
#include "../program.hpp"

client::singleplayer_gui::singleplayer_gui(program* prog, resources* res)
: gui(prog, res)
{
}


void client::singleplayer_gui::load()
{
	gui::load("assets/ui/layout_menu_single_player.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
	});

	auto w1 = m_gui.find("start:button");
	auto w2 = m_gui.find("back:button");
	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->start_local_game("world1");
				// TODO get world name from list
				// TODO deactivate buttons
			}
		});

		this->set_hover_action(b1, "start");
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(w2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
			}
		});

		this->set_hover_action(b2, "back");
	}


	this->default_hide("start");
	this->default_hide("back");
}
