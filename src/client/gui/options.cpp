#include <GLFW/glfw3.h>
#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>

#include "audio_options.hpp"
#include "options.hpp"
#include "paused.hpp"
#include "title.hpp"
#include "../program.hpp"


client::options_gui::options_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: gui(prog, res)
, m_entity_system(entity_system)
, m_entity_id(id)
{
}


void client::options_gui::load()
{
	gui::load("assets/ui/layout_menu_options.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
	});

	auto w1 = m_gui.find("graphics:button");
	auto w2 = m_gui.find("audio:button");
	auto w3 = m_gui.find("controls:button");
	auto w4 = m_gui.find("back:button");

	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("graphics:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("graphics:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("graphics:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				//this->m_program->set_screen(std::make_shared<graphics_options_screen>(m_program));
			}
		});

		this->set_hover_action(b1, "graphics");
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(w2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("audio:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("audio:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("audio:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<audio_options_screen>(m_program, m_resources, m_entity_system, m_entity_id));
			}
		});

		this->set_hover_action(b2, "audio");
	}
	if (auto* b3 = dynamic_cast<ktf::ui::button*>(w3.get())) {
		b3->set_action(ktf::event_button, [this, b3](ktf::event* e) {
			uint64_t state = b3->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("controls:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("controls:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("controls:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				//this->m_program->set_screen(std::make_shared<controls_options_screen>(m_program));
			}
		});

		this->set_hover_action(b3, "controls");
	}
	if (auto* b4 = dynamic_cast<ktf::ui::button*>(w4.get())) {
		b4->set_action(ktf::event_button, [this, b4](ktf::event* e) {
			uint64_t state = b4->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("back:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("back:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("back:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				if (m_entity_id != 0) {
					this->m_program->set_screen(std::make_shared<paused_screen>(m_program, m_resources, m_entity_system, m_entity_id));
				} else {
					this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
				}
				e->handled = true;
			}
		});

		this->set_hover_action(b4, "back");
	}

	this->default_hide("graphics");
	this->default_hide("audio");
	this->default_hide("controls");
	this->default_hide("back");
}
