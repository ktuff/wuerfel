#pragma once

#include "crafting_view.hpp"

namespace client {

class inventory_gui : public crafting_view {

	public:
		inventory_gui(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		~inventory_gui() = default;


	public:
		void load() override;
		void on_item_change(int group, int index) override;

};

} // namespace client

