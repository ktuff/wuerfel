#include <GLFW/glfw3.h>
#include <ktf/ui/sprite.hpp>

#include <common/item/inventory.hpp>
#include "game.hpp"
#include "workbench.hpp"
#include "../program.hpp"


client::workbench_gui::workbench_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id workbench)
: crafting_view(prog, res, entity_system, id, workbench)
{
}


void client::workbench_gui::load()
{
	gui::load("assets/ui/layout_game_workbench.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_E, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});

	this->load_inventory();
	this->init_crafting(9);
}

void client::workbench_gui::on_item_change(int group, int index)
{
	if (group == INVENTORY_GROUP_ITEMS || group == INVENTORY_GROUP_TEMP) {
		container_gui::on_item_change(group, index);
	}
}

// void client::workbench_gui::on_item_change(container* p_container, int p_index)
// {
// 	int32_t group = p_index & 0xFF00000;
// 	int32_t index = p_index & 0x00FFFFF;
// 	if (group == prefix_crafting_items) {
// 		item it = p_container->get_item(prefix_crafting_items | p_index);
// 		std::string slot_id = "crafting-slot-" + std::to_string(index);
// 		this->update_item_sprite(slot_id, &it);
// 	} else if (group == prefix_crafting_result) {
// 		auto c = m_container.lock();
// 		if (c) {
// 			crafting * craft = static_cast<crafting*>(c.get());
// 			item it = craft->get_preview();
// 			std::string slot_id = "crafting-slot-result";
// 			this->update_item_sprite(slot_id, &it);
// 		}
// 	}
// }
