#pragma once

#include "gui.hpp"

namespace client {

class title_screen : public gui {

	public:
		title_screen(program* prog, resources* res);
		~title_screen() = default;


	public:
		void load() override;
};

} // namespace client
