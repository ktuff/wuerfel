#include <GLFW/glfw3.h>
#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>
#include <ktf/ui/text_field.hpp>
#include <ktf/util/logger.hpp>

#include "loading.hpp"
#include "multiplayer.hpp"
#include "title.hpp"
#include "../program.hpp"

client::multiplayer_gui::multiplayer_gui(program* prog, resources* res)
: gui(prog, res)
{
}


void client::multiplayer_gui::load()
{
	gui::load("assets/ui/layout_menu_multi_player.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
	});

	auto w1 = m_gui.find("start:button");
	auto w2 = m_gui.find("back:button");
	auto w3 = m_gui.find("ip:textfield");
	auto w4 = m_gui.find("port:textfield");
	auto* field_ip = dynamic_cast<ktf::ui::text_field*>(w3.get());
	auto* field_port = dynamic_cast<ktf::ui::text_field*>(w4.get());
	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1, field_ip, field_port](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("start:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				if (field_ip && field_port) {
					try {
						std::string ip = field_ip->get_text();
						int port = std::stoi(field_port->get_text());
						this->m_program->start_remote_game(ip, port);
					} catch (std::exception const& e) {
						ktf::logger::error("Invalid input");
					}
					// TODO deactivate buttons
				}
			}
		});

		this->set_hover_action(b1, "start");
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(w2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
			}
		});

		this->set_hover_action(b2, "back");
	}

	if (field_ip) {
		field_ip->set_next(std::dynamic_pointer_cast<ktf::ui::text_field>(w4));
	}
	if (field_port) {
		field_port->set_next(std::dynamic_pointer_cast<ktf::ui::text_field>(w3));
	}

	this->default_hide("start");
	this->default_hide("back");
}
