#pragma once

#include <common/entity/types.hpp>

#include "gui.hpp"


namespace client {

class audio_options_screen : public gui {

	private:
		::entity::core_system* m_entity_system;
		::entity::id m_entity_id;


	public:
		audio_options_screen(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		~audio_options_screen() = default;


	public:
		void load() override;

};

} //namespace client

