#include <GLFW/glfw3.h>
#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>

#include "game.hpp"
#include "options.hpp"
#include "paused.hpp"
#include "title.hpp"
#include "../program.hpp"

client::paused_screen::paused_screen(program * prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: gui(prog, res)
, m_entity_system(entity_system)
, m_entity_id(id)
{
}


void client::paused_screen::load()
{
	gui::load("assets/ui/layout_game_paused.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});

	auto w1 = m_gui.find("resume:button");
	auto w2 = m_gui.find("options:button");
	auto w3 = m_gui.find("title:button");
	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("resume:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("resume:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("resume:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
			}
		});

		this->set_hover_action(b1, "resume");
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(w2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("options:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("options:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("options:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<options_gui>(m_program, m_resources, m_entity_system, m_entity_id));
			}
		});

		this->set_hover_action(b2, "options");
	}
	if (auto* b3 = dynamic_cast<ktf::ui::button*>(w3.get())) {
		b3->set_action(ktf::event_button, [this, b3](ktf::event* e) {
			uint64_t state = b3->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("title:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("title:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("title:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->leave_game();
				this->m_program->set_screen(std::make_shared<title_screen>(m_program, m_resources));
			}
		});

		this->set_hover_action(b3, "title");
	}

	this->default_hide("resume");
	this->default_hide("options");
	this->default_hide("title");
}
