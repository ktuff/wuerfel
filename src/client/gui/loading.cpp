#include <GLFW/glfw3.h>

#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>

#include <client/program.hpp>

#include "loading.hpp"
#include "multiplayer.hpp"


client::loading_screen::loading_screen(program* prog, resources* res)
: gui(prog, res)
{
}


void client::loading_screen::load()
{
	gui::load("assets/ui/layout_menu_loading.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->leave_game();
	});

	auto w1 = m_gui.find("back:button");

	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("back:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("back:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("back:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->leave_game();
			}
		});

		this->set_hover_action(b1, "back");
	}

	this->default_hide("back");
}
