#pragma once

#include "container.hpp"


namespace client {

class game_gui : public container_gui {

	private:
		int m_last_health;
		int m_last_armor;


	public:
		game_gui(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		~game_gui() = default;


	public:
		void load() override;
		void update() override;
		void on_selection_change(int prev, int curr) override;

};

} // namespace client

