#pragma once

#include <functional>

#include <ktf/ui/gui.hpp>

class program;
class resources;

namespace client {

class gui {

	protected:
		ktf::ui::gui m_gui;
		program *m_program;
		resources* m_resources;


	public:
		gui(program * prog, resources* res);
		virtual ~gui() = default;


	public:
		ktf::ui::gui * get_node_tree();
		virtual void update();
		virtual void load() = 0;

	protected:
		void load(std::string const& path);
		void set_hover_action(ktf::ui::interactable* w, std::string const& id) const;
		void default_hide(std::string const& name);
		void build_debug() const;
};

} // namespace client
