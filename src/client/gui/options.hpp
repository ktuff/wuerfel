#pragma once

#include "gui.hpp"

#include <common/entity/types.hpp>


namespace client {

class options_gui : public gui {

	private:
		::entity::core_system* m_entity_system;
		::entity::id m_entity_id;


	public:
		options_gui(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		~options_gui() = default;


	public:
		void load() override;
};

} //namespace client

