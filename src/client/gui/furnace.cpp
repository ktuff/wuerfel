#include <GLFW/glfw3.h>
#include <ktf/ui/sprite.hpp>

#include <common/item/inventory.hpp>
#include "furnace.hpp"
#include "game.hpp"
#include "../program.hpp"


char const * const slot_ids[3] = {
	"raw-slot",
	"fuel-slot",
	"cooked-slot"
};


client::furnace_gui::furnace_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id furnace)
: container_gui(prog, res, entity_system, id)
, m_furnace(furnace)
{
}


void client::furnace_gui::load()
{
	gui::load("assets/ui/layout_game_furnace.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_E, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<game_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});

	this->load_inventory();

	auto swap_logic = [this](int index) {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
		inv->swap_items(m_entity_id, ::entity::inventory_group::e_other, 0, m_furnace, ::entity::inventory_group::e_items, index);
	};

	for (size_t index=0; index<sizeof(slot_ids)/sizeof(char const* const); index++) {
		std::string const& slot_id = slot_ids[index];
		if (auto widget = m_gui.find(slot_id + ":button")) {
			if (auto* slot = dynamic_cast<ktf::ui::interactable*>(widget.get())) {
				this->set_hover_action(slot, slot_id);
				this->set_click_action(slot, slot_id, index, swap_logic);
			}
		}

		furnace_gui::on_item_change(INVENTORY_GROUP_ITEMS, index);
	}
}

void client::furnace_gui::on_event(::entity::event* event)
{
	switch (event->type) {
	case ::entity::e_item_change: {
		auto* e = static_cast<::entity::event_item_change*>(event);
		if (event->entity_id == m_entity_id) {
			this->on_item_change(INVENTORY_GROUP_ITEMS, e->index);
		} else if (event->entity_id == m_furnace) {
			auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
			item::item_stack it = inv->get_item(m_furnace, ::entity::e_items, e->index);
			this->update_item_sprite(std::string(slot_ids[e->index]), &it);
		} else {
			// ignore
		}
	} 	break;
	case ::entity::e_selection_change: {
		auto* e = static_cast<::entity::event_selection_change*>(event);
		this->on_selection_change(e->old_index, e->new_index);
	} 	break;
	default:
		break;
	}
}

void client::furnace_gui::on_item_change(int group, int index)
{
	if (group == INVENTORY_GROUP_ITEMS || group == INVENTORY_GROUP_TEMP) {
		container_gui::on_item_change(group, index);
	}
}
