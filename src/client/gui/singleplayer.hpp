#pragma once

#include "gui.hpp"

namespace client {

class singleplayer_gui : public gui {

	public:
		singleplayer_gui(program *prog, resources* res);
		~singleplayer_gui() = default;


	public:
		void load() override;
};

} // namespace client
