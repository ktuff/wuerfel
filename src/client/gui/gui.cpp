#include <pugixml.hpp>
#include <ktf/ui/button.hpp>
#include <ktf/ui/slider.hpp>
#include <ktf/ui/sprite.hpp>
#include <ktf/ui/text.hpp>
#include <ktf/ui/text_field.hpp>
#include "gui.hpp"
#include "../rendering/resources.hpp"


client::gui::gui(program* prog, resources* res)
: m_program(prog)
, m_resources(res)
{
}


ktf::ui::gui * client::gui::get_node_tree()
{
	return &m_gui;
}

void client::gui::update()
{
}

void client::gui::load(const std::string& path)
{
	std::function<void(const pugi::xml_node&, ktf::ui::widget*, std::vector<std::pair<std::string, int>>&)> build_tree = [&](const pugi::xml_node& node, ktf::ui::widget* parent, std::vector<std::pair<std::string, int>>& array) {
		for (const auto& n : node.children()) {
			if (n.type() == pugi::xml_node_type::node_pcdata) {
				// ignore
			} else {
				std::shared_ptr<ktf::ui::widget> child_node;

				std::string id = n.attribute("id").as_string();
				for (auto const& a : array) {
					auto i1 = id.find(a.first);
					auto i2 = id.find("]");
					if (i1 != id.npos && i2 != id.npos && i1 < i2) {
						id = id.replace(i1, i2-i1+1, std::to_string(a.second));
					}
				}

				if (n.name() == std::string("div")) {
					child_node = std::make_shared<ktf::ui::widget>(&m_gui, parent, id);
				} else if (n.name() == std::string("button")) {
					child_node = std::make_shared<ktf::ui::button>(&m_gui, parent, id);
				} else if (n.name() == std::string("slider")) {
					auto slider = std::make_shared<ktf::ui::slider>(&m_gui, parent, id);
					int steps = n.attribute("steps").as_int(0);
					slider->set_steps(steps);
					child_node = slider;
				} else if (n.name() == std::string("sprite")) {
					std::string content = n.attribute("content").as_string("");
					child_node = std::make_shared<ktf::ui::sprite>(&m_gui, parent, id, m_resources->loadf, content);
				} else if (n.name() == std::string("text")) {
					auto text = std::make_shared<ktf::ui::text>(&m_gui, parent, id, m_resources->font_alloc);
					std::string content = n.attribute("content").as_string("");
					text->set_content(content);
					child_node = text;
				} else if (n.name() == std::string("textfield")) {
					auto text = std::make_shared<ktf::ui::text_field>(&m_gui, parent, id, m_resources->font_alloc);
					std::string placeholder = n.attribute("placeholder").as_string("");
					text->set_text(placeholder);
					child_node = text;
				} else if (n.name() == std::string("array")) {
					int base_index = n.attribute("base-index").as_uint();
					int offset_index = n.attribute("offset").as_uint();
					std::string array_name = n.attribute("id").as_string();
					// do not create new node, copy-paste children into parent
					int num_instances = n.attribute("instances").as_uint();
					if (array_name != "" && num_instances > 0) {
						for (int i=0; i<num_instances; ++i) {
							std::pair<std::string, int> a("[" + array_name + "]", base_index + offset_index*i);
							array.push_back(a);
							build_tree(n, parent, array);
							array.pop_back();
						}
					}
					// ignored, build in recursive call
					continue;
				} else {
					// ignore node
					continue;
				}

				// parse size constraints
				float width = n.attribute("width").as_float();
				float height = n.attribute("height").as_float();
				width = (width > 0.0f && width < 100.0f) ? width : 100.0f;
				height = (height > 0.0f && height < 100.0f) ? height : 100.0f;
				child_node->set_frame(ktf::vec2<float>(width / 100.0f, height / 100.0f));
				float oversize = n.attribute("oversize").as_float();
				float ext_ratio = n.attribute("ext-ratio").as_float();
				float ratio = n.attribute("ratio").as_float();
				float w_ratio = n.attribute("w-ratio").as_float();
				float h_ratio = n.attribute("h-ratio").as_float();
				if (oversize > 0.0f) {
					child_node->set_policy({ktf::ui::widget::size_policy::OVERSIZE, oversize});
				} else if (ext_ratio > 0.0f) {
					child_node->set_policy({ktf::ui::widget::size_policy::EXPANDING, ext_ratio});
				} else if (ratio > 0.0f) {
					child_node->set_policy({ktf::ui::widget::size_policy::FIXED_RATIO, ratio});
				} else if (w_ratio > 0.0f) {
					child_node->set_policy({ktf::ui::widget::size_policy::EXPANDING_HEIGHT, w_ratio});
				} else if (h_ratio > 0.0f) {
					child_node->set_policy({ktf::ui::widget::size_policy::EXPANDING_WIDTH, h_ratio});
				}
				// parse flow if given
				std::string flow = n.attribute("flow").as_string();
				if (flow == "vertical") {
					child_node->set_layout(ktf::ui::widget::layout(ktf::ui::widget::layout::VERTICAL));
				} else if (flow == "horizontal") {
					child_node->set_layout(ktf::ui::widget::layout(ktf::ui::widget::layout::HORIZONTAL));
				} else {
					child_node->set_layout(ktf::ui::widget::layout(ktf::ui::widget::layout::FREE));
				}
				float size = n.attribute("size").as_float(100.0f);
				size = (size < 0.0f) ? 100.0f : size;
				child_node->set_scale(size / 100.0f);
				// parse margin / offset
				float px = n.attribute("x").as_float();
				float py = n.attribute("y").as_float();
				px = (-100.0f <= px && px <= 100.0f) ? px : 0.0f;
				py = (-100.0f <= py && py <= 100.0f) ? py : 0.0f;
				child_node->set_offset({ktf::ui::widget::offset::PERCENT, px / 100.0f, py / 100.0f});

				// parse rotation
				float rotation = n.attribute("rotation").as_float();
				child_node->set_rotation(rotation);

				int depth = n.attribute("depth").as_int(0);
				child_node->set_layer_offset(depth);

				bool free = n.attribute("free").as_bool(false);
				std::string copy = n.attribute("copy").as_string("");
				child_node->set_copy(copy);

				this->m_gui.add_widget(child_node, free);

				build_tree(n, child_node.get(), array);

				if (n.name() == std::string("slider")) {
					ktf::ui::slider* slider_base = dynamic_cast<ktf::ui::slider*>(child_node.get());
					if (auto* slider = slider_base->find(id + "-knob")) {
						slider_base->set_slider(slider);
					}
				}
			}
		}
	};


	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(path.c_str());
	if (!result) {
		::printf("error: %s\n", path.c_str());
		return;
	}

	auto const& doc_root = doc.root();
	auto const& node = doc_root.child("document").find_child_by_attribute("div", "id", "root");
	this->m_gui.remove_all();
	std::vector<std::pair<std::string, int>> array;
	build_tree(node, m_gui.get_root(), array);

	// load all sprites / texts
	auto group = m_gui.get_group("widgets_all");
	for (auto* m : group->get_members()) {
		if (ktf::ui::sprite* r = dynamic_cast<ktf::ui::sprite*>(m)) {
			r->load();
		}
	}
}

void client::gui::set_hover_action(ktf::ui::interactable* w, const std::string& id) const
{
	w->set_action(ktf::event_position, [w, id](ktf::event*) {
		int64_t state = w->state();
		if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(w->find(id + ":sprite:normal"))) {
			sp->hide(state != 0);
		}
		if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(w->find(id + ":sprite:hover"))) {
			sp->hide(state != 1);
		}
	});
}

void client::gui::default_hide(const std::string& name)
{
	auto wh = m_gui.find(name + ":sprite:hover");
	auto wc = m_gui.find(name + ":sprite:click");
	if (auto* sp = dynamic_cast<ktf::ui::sprite*>(wh.get())) {
		sp->hide(true);
	}
	if (auto* sp = dynamic_cast<ktf::ui::sprite*>(wc.get())) {
		sp->hide(true);
	}
}

void client::gui::build_debug() const
{
	// TODO
	/*
	std::string xml_linebreak = "&#xA;";
	std::string cpu_info;
	std::string mem_info;
	std::string pos_info;
	std::string info = cpu_info + "\n" + mem_info + "\n" + pos_info;
	*/
}
