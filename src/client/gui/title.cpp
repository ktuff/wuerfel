#include <ktf/ui/button.hpp>
#include <ktf/ui/sprite.hpp>

#include "multiplayer.hpp"
#include "options.hpp"
#include "singleplayer.hpp"
#include "title.hpp"
#include "../program.hpp"


client::title_screen::title_screen(program* prog, resources* res)
: gui(prog, res)
{
}


void client::title_screen::load()
{
	gui::load("assets/ui/layout_menu_title_screen.xml");

	auto w1 = m_gui.find("button:singleplayer");
	auto w2 = m_gui.find("button:multiplayer");
	auto w3 = m_gui.find("button:options");
	auto w4 = m_gui.find("button:quit");
	if (auto* b1 = dynamic_cast<ktf::ui::button*>(w1.get())) {
		b1->set_action(ktf::event_button, [this, b1](ktf::event* e) {
			uint64_t state = b1->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("singleplayer:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("singleplayer:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b1->find("singleplayer:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<singleplayer_gui>(m_program, m_resources));
			}
		});

		this->set_hover_action(b1, "singleplayer");
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(w2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("multiplayer:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("multiplayer:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("multiplayer:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<multiplayer_gui>(m_program, m_resources));
			}
		});

		this->set_hover_action(b2, "multiplayer");
	}
	if (auto* b3 = dynamic_cast<ktf::ui::button*>(w3.get())) {
		b3->set_action(ktf::event_button, [this, b3](ktf::event* e) {
			uint64_t state = b3->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("options:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("options:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b3->find("options:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_screen(std::make_shared<options_gui>(m_program, m_resources, nullptr, 0));
			}
		});

		this->set_hover_action(b3, "options");
	}
	if (auto* b4 = dynamic_cast<ktf::ui::button*>(w4.get())) {
		b4->set_action(ktf::event_button, [this, b4](ktf::event* e) {
			uint64_t state = b4->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("quit:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("quit:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b4->find("quit:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				e->handled = true;
				this->m_program->set_state(state_quit);
			}
		});

		this->set_hover_action(b4, "quit");
	}

	this->default_hide("singleplayer");
	this->default_hide("multiplayer");
	this->default_hide("options");
	this->default_hide("quit");
}
