#include <GLFW/glfw3.h>
#include <iomanip>
#include <sstream>

#include <ktf/ui/button.hpp>
#include <ktf/ui/slider.hpp>
#include <ktf/ui/sprite.hpp>

#include "../program.hpp"
#include "audio_options.hpp"
#include "options.hpp"


client::audio_options_screen::audio_options_screen(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: gui(prog, res)
, m_entity_system(entity_system)
, m_entity_id(id)
{
}


void client::audio_options_screen::load()
{
	gui::load("assets/ui/layout_menu_audio.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<client::options_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});

	auto s1 = m_gui.find("volume:slider");
	auto s2 = m_gui.find("back:button");

	if (auto* w1 = dynamic_cast<ktf::ui::slider*>(s1.get())) {
		w1->set_action(ktf::event_position, [w1](ktf::event*) {
			float volume = w1->get_value();
			if (ktf::ui::text* tx = dynamic_cast<ktf::ui::text*>(w1->find("volume:text"))) {
				int64_t state = w1->state();
				if (state == 0) {
					tx->color(ktf::vec4<float>(1.0f, 1.0f, 1.0f, 1.0f));
				} else {
					tx->color(ktf::vec4<float>(1.0f, 1.0f, 0.6f, 1.0f));
				}
				std::stringstream content;
				content << "Volume: " << std::fixed << std::setprecision(1) << volume * 100 << "%";
				tx->set_content(content.str());
			}
			// TODO set volume in soundsystem
		});
	}
	if (auto* b2 = dynamic_cast<ktf::ui::button*>(s2.get())) {
		b2->set_action(ktf::event_button, [this, b2](ktf::event* e) {
			uint64_t state = b2->state();
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:normal"))) {
				sp->hide(state != 0);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:hover"))) {
				sp->hide(state != 1);
			}
			if (ktf::ui::sprite* sp = dynamic_cast<ktf::ui::sprite*>(b2->find("back:sprite:click"))) {
				sp->hide(state != 2);
			}
			if (state == 1) {
				this->m_program->set_screen(std::make_shared<options_gui>(m_program, m_resources, m_entity_system, m_entity_id));
				e->handled = true;
			}
		});

		this->set_hover_action(b2, "back");
	}

	this->default_hide("back");
}
