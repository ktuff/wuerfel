#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/item/crafting.hpp>
#include <common/item/inventory.hpp>

#include "crafting_view.hpp"


client::crafting_view::crafting_view(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id crafting)
: container_gui(prog, res, entity_system, id)
, m_crafting_id(crafting)
{
}


void client::crafting_view::on_event(::entity::event* event)
{
	switch (event->type) {
	case ::entity::e_item_change: {
		auto* e = static_cast<::entity::event_item_change*>(event);
		if (event->entity_id == m_entity_id) {
			this->on_item_change(INVENTORY_GROUP_ITEMS, e->index);
		} else if (event->entity_id == m_crafting_id) {
// 			auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
// 			item::item_stack it = inv->get_item(m_crafting_id, ::entity::e_crafting, e->index);
// 			this->update_item_sprite(std::string(slot_ids[e->index]), &it);
		} else {
			// ignore
		}
	} 	break;
	case ::entity::e_selection_change: {
		auto* e = static_cast<::entity::event_selection_change*>(event);
		this->on_selection_change(e->old_index, e->new_index);
	} 	break;
	default:
		break;
	}
}

void client::crafting_view::init_crafting(int count)
{
	auto swap_logic_crafting_field = [this](int index) {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
		inv->swap_items(m_entity_id, ::entity::inventory_group::e_other, 0, m_entity_id, ::entity::inventory_group::e_crafting, index);
	};
	auto swap_logic_crafting_result = [this](int) {
// 		crafting * craft = dynamic_cast<crafting*>(con.get());
// 		if (craft) {
// 			auto i1 = craft->get_preview();
// 			auto i2 = inv->get_item(prefix_inventory_temp | 0);
// 			if (i2.count + i1.count > items::item_types[i1.type]->max_stack_size) {
// 				return;
// 			}
// 			if (i1.type != i2.type && i2.type != ITEM_TYPE_NONE) {
// 				return;
// 			}
// 			craft->craft(1);
// 			container::swap_item(con.get(), prefix_crafting_result | 0, inv.get(), prefix_inventory_temp | 0);
// 		}
	};

	for (int index=0; index<count; index++) {
		std::string slot_id = "crafting-slot-" + std::to_string(index);
		if (auto widget = m_gui.find(slot_id + ":button")) {
			if (auto* slot = dynamic_cast<ktf::ui::interactable*>(widget.get())) {
				this->set_hover_action(slot, slot_id);
				this->set_click_action(slot, slot_id, index, swap_logic_crafting_field);
			}
		}
	}
	std::string slot_id = "crafting-slot-result";
	if (auto widget = m_gui.find(slot_id + ":button")) {
		if (auto* slot = dynamic_cast<ktf::ui::interactable*>(widget.get())) {
			this->set_hover_action(slot, slot_id);
			this->set_click_action(slot, slot_id, prefix_crafting_result | 0, swap_logic_crafting_result);
		}
	}
}
/*
void client::crafting_view::on_item_change(container* p_container, int p_index)
{
	if (crafting * craft = dynamic_cast<crafting*>(p_container)) {
		int32_t group = p_index & 0xFF00000;
		int32_t index = p_index & 0x00FFFFF;

		if (group == prefix_crafting_items) {
			item it = p_container->get_item(prefix_crafting_items | p_index);
			std::string slot_id = "crafting-slot-" + std::to_string(index);
			this->update_item_sprite(slot_id, &it);
		} else if (group == prefix_crafting_result) {
			item it = craft->get_preview();
			std::string slot_id = "crafting-slot-result";
			this->update_item_sprite(slot_id, &it);
		}
	}
}*/
