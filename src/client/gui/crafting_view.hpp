#pragma once

#include "container.hpp"

class crafting;

namespace client {

class crafting_view : public container_gui {

	private:
		::entity::id m_crafting_id;


	public:
		crafting_view(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id crafting);
		~crafting_view() = default;


	public:
		void on_event(::entity::event* event) override;

	protected:
		void init_crafting(int count);
};

} // namespace client
