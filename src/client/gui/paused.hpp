#pragma once

#include "gui.hpp"


namespace client {

class paused_screen : public gui {

	private:
		::entity::core_system* m_entity_system;
		::entity::id m_entity_id;


	public:
		paused_screen(program * prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		~paused_screen() = default;


	public:
		void load() override;
};

} // namespace client
