#include <GLFW/glfw3.h>
#include <ktf/ui/sprite.hpp>
#include <ktf/util/logger.hpp>

#include <client/config.hpp>
#include <client/program.hpp>
#include <common/entity/entities/living_entity.hpp>
#include <common/item/inventory.hpp>
#include "game.hpp"
#include "inventory.hpp"
#include "paused.hpp"


client::game_gui::game_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id)
: container_gui(prog,res, entity_system, id)
, m_last_health(-1)
, m_last_armor(-1)
{
}


void client::game_gui::load()
{
	gui::load("assets/ui/layout_game_view.xml");

	auto* root = dynamic_cast<ktf::ui::interactable*>(m_gui.get_root());
	root->add_key(ktf::ui::keyt(GLFW_KEY_ESCAPE, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<paused_screen>(m_program, m_resources, m_entity_system, m_entity_id));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_E, GLFW_PRESS, 0).value, [this]() {
		this->m_program->set_screen(std::make_shared<inventory_gui>(m_program, m_resources, m_entity_system, m_entity_id));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_PAGE_DOWN, GLFW_PRESS, 0).value, []() {
		client::config.set_int("distance", std::max(1, client::config.get_int("distance") - 1));
		ktf::logger::info("changed distance to: " + std::to_string(client::config.get_int("distance")));
	});
	root->add_key(ktf::ui::keyt(GLFW_KEY_PAGE_UP, GLFW_PRESS, 0).value, []() {
		client::config.set_int("distance", client::config.get_int("distance") + 1);
		ktf::logger::info("changed distance to: " + std::to_string(client::config.get_int("distance")));
	});
	root->set_action(ktf::event_focus, [this](ktf::event* e) {
		ktf::window_event* event = static_cast<ktf::window_event*>(e);
		if (!event->focused) {
			e->handled = true;
			this->m_program->set_screen(std::make_shared<paused_screen>(m_program, m_resources, m_entity_system, m_entity_id));
		}
	});

	auto* inv = dynamic_cast<::entity::inventory_system*>(m_entity_system->get_module(::entity::inventory_system::s_id));
	int selected = inv->get_selected_index(m_entity_id);
	for (int index=0; index<9; index++) {
		if (index != selected) {
			auto slot_prev = m_gui.find("item-slot-" + std::to_string(index) + "-selection:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(slot_prev.get())) {
				sp->hide(true);
			}
		}
	}
	for (int index=0; index<9; index++) {
		container_gui::on_item_change(INVENTORY_GROUP_ITEMS, index);
	}

	game_gui::update();

	this->m_last_armor = -1;
	this->m_last_health = -1;
}

void client::game_gui::update()
{
	auto* entity = m_entity_system->get_entity(m_entity_id);
	if (entity == nullptr) {
		return;
	}
	auto* e = dynamic_cast<::entity::living_entity*>(entity);
	if (e == nullptr) {
		return;
	}

	// update health status
	double health = e->get_health();
	int const MAX_HEALTH = 10 * 2;
	int const ihealth = int(health) % (MAX_HEALTH + 1);
	if (ihealth != m_last_health) {
		// display full health sprite
		for (int index=0; index<MAX_HEALTH/2; ++index) {
			auto full = m_gui.find("health-slot-" + std::to_string(index) + "-full:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(full.get())) {
				sp->hide(index >= ihealth / 2);
			}
			auto half = m_gui.find("health-slot-" + std::to_string(index) + "-half:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(half.get())) {
				sp->hide(true);
			}
		}
		// display half health sprite
		if (ihealth % 2 == 1) {
			auto half = m_gui.find("health-slot-" + std::to_string(ihealth / 2) + "-half:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(half.get())) {
				sp->hide(false);
			}
		}
		this->m_last_health = ihealth;
	}

	// update armor status
	double const current_armor = 0;
	int const MAX_ARMOR = 10 * 2;
	int const iarmor = int(current_armor) % (MAX_ARMOR + 1);
	if (iarmor != m_last_armor) {
		// toggle frames if (no) armor
		if ((iarmor != 0 && m_last_armor == 0) || (iarmor == 0 && m_last_armor != 0)) {
			for (int index=0; index<MAX_ARMOR/2; ++index) {
				auto frame = m_gui.find("armor-slot-" + std::to_string(index) + "-bg:sprite");
				if (auto* sp = dynamic_cast<ktf::ui::sprite*>(frame.get())) {
					sp->hide(true);
				}
			}
		}

		// display full armor sprite
		for (int index=0; index<MAX_ARMOR/2; ++index) {
			auto full = m_gui.find("armor-slot-" + std::to_string(index) + "-full:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(full.get())) {
				sp->hide(index >= iarmor / 2);
			}
			auto half = m_gui.find("armor-slot-" + std::to_string(index) + "-half:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(half.get())) {
				sp->hide(true);
			}
		}

		// display half armor sprite
		if (iarmor % 2 == 1) {
			auto half = m_gui.find("armor-slot-" + std::to_string(iarmor / 2) + "-half:sprite");
			if (auto* sp = dynamic_cast<ktf::ui::sprite*>(half.get())) {
				sp->hide(false);
			}
		}
		this->m_last_armor = iarmor;
	}
}

void client::game_gui::on_selection_change(int prev, int curr)
{
	auto slot_prev = m_gui.find("item-slot-" + std::to_string(prev) + "-selection:sprite");
	if (auto* sp = dynamic_cast<ktf::ui::sprite*>(slot_prev.get())) {
		sp->hide(true);
	}
	auto slot_curr = m_gui.find("item-slot-" + std::to_string(curr) + "-selection:sprite");
	if (auto* sp = dynamic_cast<ktf::ui::sprite*>(slot_curr.get())) {
		sp->hide(false);
	}
}
