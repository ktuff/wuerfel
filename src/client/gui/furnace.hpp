#pragma once

#include "container.hpp"

namespace client {

class furnace_gui : public container_gui {

	private:
		::entity::id m_furnace;


	public:
		furnace_gui(program *prog, resources* res, ::entity::core_system* entity_system, ::entity::id id, ::entity::id furnace);
		~furnace_gui() = default;


	public:
		void load() override;
		void on_event(::entity::event* event) override;
		void on_item_change(int group, int index) override;
// 		void on_item_change(container * p_container, int p_index) override;

};

} // namespace client
