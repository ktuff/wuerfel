#pragma once

#include <common/entity/types.hpp>
#include <common/entity/systems/module_listener.hpp>
#include <common/item/container_listener.hpp>
#include <common/item/inventory_listener.hpp>

#include "gui.hpp"


class container;
class inventory;


namespace entity {

class core_system;

} // namespace entity


namespace client {

class container_gui : public gui, public ::entity::module_listener {

	protected:
		::entity::core_system* m_entity_system;
		::entity::id m_entity_id;


	public:
		container_gui(program* prog, resources* res, ::entity::core_system* entity_system, ::entity::id id);
		virtual ~container_gui();


	public:
		virtual void on_event(::entity::event* event) override;
		virtual void on_item_change(int group, int index);
		virtual void on_selection_change(int prev, int curr);

	protected:
		void load_inventory();
		void update_item_sprite(std::string const& slot_id, item::item_stack const * it);
		void set_click_action(ktf::ui::interactable* slot, const std::string & slot_id, int index, std::function<void(int)> swap_logic);

};

} // namespace client
