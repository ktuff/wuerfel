#include <GLFW/glfw3.h>

#include "config.hpp"


common::config client::config(
	"config.json",
	{ // bool
	},
	{ // int
		{"distance", 8},
		{"num_threads", 12},

		{"keyForwards", GLFW_KEY_W},
		{"keyBackwards", GLFW_KEY_S},
		{"keyLeft", GLFW_KEY_A},
		{"keyRight", GLFW_KEY_D},
		{"keyJump", GLFW_KEY_SPACE},
		{"keySneak", GLFW_KEY_LEFT_SHIFT},
		{"keySprint", GLFW_KEY_LEFT_CONTROL}
	},
	{ // float
		{"settingAudioVolumeMaster", 1.0},
		{"settingAudioVolumeMusic", 0.8},
		{"settingAudioVolumeSound", 1.0}
	},
	{ // string
	}
);
