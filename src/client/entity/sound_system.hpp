#pragma once

#include <common/entity/systems/system_module.hpp>


namespace entity {

class sound_system : public ::entity::system_module {

	private:



	public:
		sound_system(::entity::core_system* core_system);
		~sound_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void update();

};

} // namespace entity
