#pragma once

#include <nlohmann/json.hpp>

#include <common/entity/systems/system_module.hpp>


namespace client {

class packager;


namespace entity {

class network_system : public ::entity::system_module {

	public:
		static ::entity::system_module_id const s_id;

	private:
		client::packager* m_network;
		::entity::id m_target_id;


	public:
		network_system(::entity::core_system* core_system, client::packager* network);
		~network_system();


	public:
		void on_entity_creation(::entity::entity * entity) override;
		void on_entity_destruction(::entity::entity * entity) override;
		void on_event(::entity::event * event) override;

		void update();
		void process_packet(nlohmann::json const& data);
		void set_target(::entity::id target_id);

	private:
		void create_entity(nlohmann::json const& packet);
		void destroy_entity(nlohmann::json const& packet);

};

} // namespace entity

} // namespace client
