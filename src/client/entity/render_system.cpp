#include <ktf/opengl/composer.hpp>
#include <ktf/opengl/gl.hpp>

#include <client/rendering/resources.hpp>
#include <common/entity/entity.hpp>
#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/item/crafting.hpp>
#include <common/item/inventory.hpp>

#include "models/models.hpp"
#include "render_system.hpp"


::entity::system_module_id const ::entity::render_system::s_id = 513;


entity::render_system::render_system(::entity::core_system* core_system, resources* resources)
: system_module(core_system, s_id)
, m_core_system(core_system)
, m_resources(resources)
, m_composer(nullptr)
{
}


void entity::render_system::on_entity_creation(::entity::entity* entity)
{
	std::unique_ptr<::entity::model> model;
	switch (entity->m_type) {
	case ::entity::type::e_item: {
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core_system->get_module(::entity::inventory_system::s_id));
		::item::item_stack i = inv->get_item(entity->m_id, ::entity::inventory_group::e_items, 0);
		model = std::make_unique<::entity::item_model>(entity, i.type);
		this->m_sorted_item_models[i.type].emplace(dynamic_cast<item_model*>(model.get()));
		break;
	}
	case ::entity::type::e_player:
		model = std::make_unique<::entity::player_model>(entity, ""); // TODO
		break;
	case ::entity::type::e_pig:
		model = std::make_unique<::entity::pig_model>(entity);
		break;
	case ::entity::type::e_cow:
		model = std::make_unique<::entity::cow_model>(entity);
		break;
	case ::entity::type::e_chicken:
		model = std::make_unique<::entity::chicken_model>(entity);
		break;
	case ::entity::type::e_chest:
	case ::entity::type::e_furnace:
	case ::entity::type::e_workbench:
	case ::entity::type::e_max_num_entity_types:
		return;
	}
	if (model) {
		model->reload(m_resources);
		this->m_models[entity->m_type].emplace(entity->m_id, std::move(model));
	}
}

void entity::render_system::on_entity_destruction(::entity::entity* entity)
{
	auto model_iter = m_models[entity->m_type].find(entity->m_id);
	if (model_iter != m_models[entity->m_type].end()) {
		if (auto* imodel = dynamic_cast<item_model*>(model_iter->second.get())) {
			this->m_sorted_item_models[imodel->get_item_type()].erase(imodel);
		}
		this->m_models[entity->m_type].erase(entity->m_id);
	}
}

void entity::render_system::on_event(::entity::event* event)
{
	auto* entity = m_core->get_entity(event->entity_id);
	switch (event->type) {
	case e_position:
		break;
	case e_rotation:
		break;
	case e_item_change: {
		if (entity->m_type == ::entity::e_item) {
			auto& model = m_models[entity->m_type].at(entity->m_id);
			if (auto* imodel = dynamic_cast<item_model*>(model.get())) {
				auto* inv = dynamic_cast<::entity::inventory_system*>(m_core_system->get_module(::entity::inventory_system::s_id));
				::item::item_stack i = inv->get_item(entity->m_id, ::entity::inventory_group::e_items, 0);
				imodel->set_item_type(i.type);
				imodel->reload(m_resources);
				// TODO double reload? (0 -> 4 -> 4)
			}
		}
	}	break;
	case e_selection_change:
	case e_hitbox:
	case e_damage:
	case e_container:
		return;
	}
}

void entity::render_system::init(ktf::composer* composer)
{
	this->m_composer = composer;

	this->m_sorted_item_models.resize(::item::num_item_types);
}

void entity::render_system::clear()
{
	for (uint8_t type=0; type<e_max_num_entity_types; type++) {
		this->m_models[type].clear();
	}

	this->m_sorted_item_models.clear();
	this->m_sorted_item_models.resize(::item::num_item_types);
}

void entity::render_system::reload()
{
	for (uint8_t i=0; i<e_max_num_entity_types; ++i) {
		for (auto const& [id, model] : m_models[i]) {
			model->reload(m_resources);
		}
	}

	this->m_items_block_tex.clear();
	this->m_items_item_tex.clear();
	for (::item::item_type_id id=::item::e_none; id<::item::num_item_types;) {
		if (m_resources->uses_block_texture(id)) {
			this->m_items_block_tex.emplace(id);
		} else {
			this->m_items_item_tex.emplace(id);
		}
		id = (::item::item_type_id)(id + 1);
	}
}

void entity::render_system::update(double dt)
{
	ktf::vec3<double> camera_position = m_composer->get_camera()->get_position();
	for (uint8_t i=0; i<e_max_num_entity_types; ++i) {
		for (auto const& [id, model] : m_models[i]) {
			// update model state
			model->update(dt, camera_position);
		}
	}
}

void entity::render_system::render()
{
	ktf::gl::enable_blending();

	// render all non special entity models
	this->m_resources->use_shader("game::shader.entity");
	this->m_resources->use_texture("entity::texture.pig");
	this->m_composer->use_perspective_projection();
	for (size_t type=0; type<e_max_num_entity_types; ++type) {
		if (type == ::entity::type::e_item || type == ::entity::type::e_player) {
			continue;
		}
		for (auto const& [id, model] : m_models[type]) {
			model->render(m_composer);
		}
	}

	// render player models
	this->m_resources->use_texture("entity::texture.player");
	for (auto& [id, m] : m_models[::entity::type::e_player]) {
		// m->render(m_composer);
	}

	// render item models
	this->m_resources->use_shader("shader::flat_textured");
	this->m_composer->use_perspective_projection();
	this->m_resources->use_texture("texture::blocks");
	for (auto id : m_items_block_tex) {
		for (auto* model : m_sorted_item_models[id]) {
			model->render(m_composer);
		}
	}
	this->m_resources->use_texture("texture::items");
	for (auto id : m_items_item_tex) {
		for (auto* model : m_sorted_item_models[id]) {
			model->render(m_composer);
		}
	}
}
