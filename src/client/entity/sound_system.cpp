#include "sound_system.hpp"


entity::sound_system::sound_system(::entity::core_system* core_system)
: system_module(core_system, 2) // TODO
{
}

entity::sound_system::~sound_system()
{
}


void entity::sound_system::on_entity_creation(::entity::entity* entity)
{
}

void entity::sound_system::on_entity_destruction(::entity::entity* entity)
{
}

void entity::sound_system::on_event(::entity::event* event)
{
}

void entity::sound_system::update()
{
}
