#pragma once

#include <ktf/math/matrix.hpp>

#include "model.hpp"


namespace entity {

class item_model : public ::entity::model {

	private:
		size_t m_id;
		int32_t m_item_type;
		ktf::matrix<float> m_model;
		double m_rot;


	public:
		item_model(::entity::entity* entity, int32_t type);
		~item_model();


	public:
		void reload(resources* res) override final;
		void update(double dt, ktf::vec3<double> const& ref_pos) override final;
		void render(ktf::composer* composer) override final;
		int32_t get_item_type() const;
		void set_item_type(int32_t type);

}; // class item_model

} // namespace entity
