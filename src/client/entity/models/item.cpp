#include <ktf/math/transform.hpp>
#include <ktf/opengl/composer.hpp>

#include <client/rendering/resources.hpp>
#include <common/entity/entity.hpp>

#include "item.hpp"


entity::item_model::item_model(::entity::entity* entity, int32_t type)
: model(entity)
, m_id((size_t)-1)
, m_item_type(type)
, m_rot(0.0)
{
}

entity::item_model::~item_model()
{
}

void entity::item_model::reload(resources* res)
{
	this->m_id = res->get_mesh("item-3d:" + std::string(item::all_types[m_item_type]->name));
}

void entity::item_model::update(double dt, const ktf::vec3<double>& ref_pos)
{
	this->m_rot += dt;
	ktf::vec3<float> pos = m_entity->get_position() - ref_pos;
	this->m_model = ktf::matrix<float>(1.0f);
	this->m_model = ktf::scale(m_model, ktf::vec3<float>(0.25));
	this->m_model = ktf::rotate(m_model, float(m_rot), ktf::vec3<float>(0, 1, 0));
	this->m_model = ktf::translate(m_model, pos);
}

void entity::item_model::render(ktf::composer* composer)
{
	composer->render_mesh(m_id, m_model);
}

int32_t entity::item_model::get_item_type() const
{
	return m_item_type;
}

void entity::item_model::set_item_type(int32_t type)
{
	this->m_item_type = type;
}
