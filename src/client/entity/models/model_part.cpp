#include <ktf/math/transform.hpp>

#include "model_part.hpp"


entity::model_part::model_part(ktf::vec3<double> const& offset, std::vector<ktf::vec3<double>> const& axis)
: m_transform(1.0f)
, m_offset(offset)
, m_axis(axis)
{
	this->update();
}

entity::model_part::~model_part()
{
}


void entity::model_part::set_angles(std::vector<double> const& angles)
{
	this->m_angles = angles;
	this->update();
}

ktf::matrix<float> entity::model_part::get_transform() const
{
	return m_transform;
}

void entity::model_part::update()
{
	ktf::matrix<double> transform(1.0f);
	for (size_t i=0; i<std::min(m_axis.size(), m_angles.size()); i++) {
		transform = ktf::rotate(transform, m_angles[i], m_axis[i]);
	}
	transform = ktf::translate(transform, m_offset);
	this->m_transform = transform;
}
