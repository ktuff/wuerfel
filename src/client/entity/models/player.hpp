#pragma once

#include <string>

#include <ktf/math/spring.hpp>

#include "model.hpp"
#include "model_part.hpp"


namespace entity {

class entity;


class player_model : public ::entity::model {

	private:
		size_t m_meshes[6];
		::entity::model_part m_parts[6];
		ktf::spring m_pd;
		ktf::spring m_mov;
		std::string m_skin;
		size_t m_skin_id;
		ktf::vec3<double> m_pos;
		ktf::vec3<double> m_rot;
		ktf::matrix<float> m_transform;


	public:
		player_model(::entity::entity* p_entity, std::string const& skin);
		~player_model();


	public:
		void reload(resources* res) override;
		void update(double dt, ktf::vec3<double> const& ref_pos) override;
		void render(ktf::composer* composer) override;

}; // class player_model

} // namespace entity
