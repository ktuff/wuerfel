#pragma once

#include "model.hpp"
#include "model_part.hpp"


namespace entity {

class entity;


class chicken_model : public ::entity::model {

	private:
		size_t m_meshes[6];
		::entity::model_part m_parts[6];
		size_t m_tex_id;
		ktf::vec3<double> m_pos;
		ktf::vec3<double> m_rot;
		ktf::matrix<float> m_transform;


	public:
		chicken_model(::entity::entity* p_entity);
		~chicken_model();


	public:
		void reload(resources* res) override;
		void update(double dt, ktf::vec3<double> const& ref_pos) override;
		void render(ktf::composer* composer) override;

}; // class chicken_model

} // namespace entity
