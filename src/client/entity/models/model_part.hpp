#pragma once

#include <vector>
#include <ktf/math/matrix.hpp>
#include <ktf/math/vec3.hpp>


namespace entity {

class model_part {

	private:
		ktf::matrix<float> m_transform;
		ktf::vec3<double> const m_offset;
		std::vector<ktf::vec3<double>> const m_axis;
		std::vector<double> m_angles;


	public:
		model_part(ktf::vec3<double> const& offset, std::vector<ktf::vec3<double>> const& axis);
		~model_part();


	public:
		void set_angles(std::vector<double> const& angles);
		ktf::matrix<float> get_transform() const;

	private:
		void update();
};

} // namespace entity
