#include <ktf/math/transform.hpp>
#include <ktf/math/radians.hpp>
#include <ktf/opengl/composer.hpp>

#include <client/rendering/resources.hpp>
#include <common/entity/entity.hpp>

#include "player.hpp"


entity::player_model::player_model(::entity::entity* p_entity, std::string const& skin)
: model(p_entity)
, m_parts {
	{ktf::vec3<double>(0.0, 1.40625, 0.0), {ktf::vec3<double>(0.0, 1.0, 0.0), ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(0.0, 0.703125, 0.0), {ktf::vec3<double>(0.0, 1.0, 0.0)}},
	{ktf::vec3<double>(0.322265625, 1.2890625, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(-0.322265625, 1.2890625, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(0.1171875, 0.703125, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(-0.1171875, 0.703125, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}}
}
, m_pd(50.0, 15.0, ktf::radians(35.0))
, m_mov(120.0, 10.0, 0.0)
, m_skin(skin)
{
	this->m_meshes[0] = (size_t)-1;
	this->m_meshes[1] = (size_t)-1;
	this->m_meshes[2] = (size_t)-1;
	this->m_meshes[3] = (size_t)-1;
	this->m_meshes[4] = (size_t)-1;
	this->m_meshes[5] = (size_t)-1;
}

entity::player_model::~player_model()
{
}


void entity::player_model::reload(resources* res)
{
	this->m_skin_id = res->get_texture(m_skin);

	this->m_meshes[0] = res->get_mesh("entity.player.head");
	this->m_meshes[1] = res->get_mesh("entity.player.torso");
	this->m_meshes[2] = res->get_mesh("entity.player.left_arm");
	this->m_meshes[3] = res->get_mesh("entity.player.right_arm");
	this->m_meshes[4] = res->get_mesh("entity.player.left_leg");
	this->m_meshes[5] = res->get_mesh("entity.player.right_leg");
}

void entity::player_model::update(double dt, ktf::vec3<double> const& ref_pos)
{
	ktf::vec3<double> const& pos = m_entity->get_position();
	ktf::vec3<double> const& rot = m_entity->get_rotation();

	// rotate arms and legs
	double off = ktf::vec2<float>(pos.x - m_pos.x, pos.z - m_pos.z).length() * 7.5;
	off = std::min(1.5, off);
	this->m_mov.push(off);
	this->m_mov.step(dt);
	double phi = m_mov.get_x();

	float base_rotation = ktf::radians(90.0f - rot.y);
	this->m_parts[0].set_angles({ktf::radians(90.0 - rot.y) - base_rotation, ktf::radians(-rot.x)});
	this->m_parts[2].set_angles({phi});
	this->m_parts[3].set_angles({-phi});
	this->m_parts[4].set_angles({-phi});
	this->m_parts[5].set_angles({phi});

	this->m_pos = pos;
	ktf::vec3<float> model_pos = pos - ref_pos;
	this->m_transform = ktf::matrix<float>(1.0f);
	this->m_transform = ktf::rotate(m_transform, base_rotation, ktf::vec3<float>(0.0f, 1.0f, 0.0f));
	this->m_transform = ktf::translate(m_transform, model_pos);

	// adjust head
	double x = std::abs(rot.y) - std::abs(m_rot.y);
	// rotate body to match head rotation
	this->m_pd.set_x(ktf::radians(x));
	this->m_pd.step(dt);
	//this->m_rot.y = ktf::degree(m_pd.get_x());
}

void entity::player_model::render(ktf::composer* composer)
{
	composer->bind_texture(m_skin_id);
	for (int i=5; i>=0; i--) {
		composer->render_mesh(m_meshes[i], m_transform * m_parts[i].get_transform());
	}
}
