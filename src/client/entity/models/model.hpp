#pragma once

#include <ktf/math/vec3.hpp>


class resources;


namespace ktf {

class composer;

} // namespace ktf


namespace entity {

class entity;


class model {

	protected:
		::entity::entity* m_entity;

	public:
		::entity::entity const* const& m_entity_ptr = m_entity;


	public:
		model(::entity::entity* entity);
		virtual ~model();


	public:
		virtual void reload(resources* res) = 0;
		virtual void update(double dt, const ktf::vec3<double>& ref_pos) = 0;
		virtual void render(ktf::composer* composer) = 0;

}; // class model

} // namespace entity
