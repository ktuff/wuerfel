#include <ktf/math/transform.hpp>
#include <ktf/math/radians.hpp>
#include <ktf/opengl/composer.hpp>

#include <client/rendering/resources.hpp>
#include <common/entity/entity.hpp>

#include "pig.hpp"


entity::pig_model::pig_model(::entity::entity* p_entity)
: model(p_entity)
, m_parts {
	{ktf::vec3<double>(0.0, 1.40625, 0.0), {ktf::vec3<double>(0.0, 1.0, 0.0), ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(0.0, 0.703125, 0.0), {ktf::vec3<double>(0.0, 1.0, 0.0)}},
	{ktf::vec3<double>(0.322265625, 1.2890625, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(-0.322265625, 1.2890625, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(0.1171875, 0.703125, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}},
	{ktf::vec3<double>(-0.1171875, 0.703125, 0.0), {ktf::vec3<double>(1.0, 0.0, 0.0)}}
}
{
	this->m_meshes[0] = (size_t)-1;
	this->m_meshes[1] = (size_t)-1;
	this->m_meshes[2] = (size_t)-1;
	this->m_meshes[3] = (size_t)-1;
	this->m_meshes[4] = (size_t)-1;
	this->m_meshes[5] = (size_t)-1;
}

entity::pig_model::~pig_model()
{
}


void entity::pig_model::reload(resources* res)
{
	this->m_tex_id = res->get_texture("entity::texture.pig");

	this->m_meshes[0] = res->get_mesh("entity.pig.head");
	this->m_meshes[1] = res->get_mesh("entity.pig.torso");
	this->m_meshes[2] = res->get_mesh("entity.pig.leg_br");
	this->m_meshes[3] = res->get_mesh("entity.pig.leg_bl");
	this->m_meshes[4] = res->get_mesh("entity.pig.leg_fr");
	this->m_meshes[5] = res->get_mesh("entity.pig.leg_fl");
}

void entity::pig_model::update(double dt, ktf::vec3<double> const& ref_pos)
{
	ktf::vec3<double> const& pos = m_entity->get_position();
	ktf::vec3<double> const& rot = m_entity->get_rotation();

	float base_rotation = ktf::radians(90.0f - rot.y);
	this->m_parts[0].set_angles({ktf::radians(90.0 - rot.y) - base_rotation, ktf::radians(-rot.x)});

	this->m_pos = pos;
	ktf::vec3<float> model_pos = pos - ref_pos;
	this->m_transform = ktf::matrix<float>(1.0f);
	this->m_transform = ktf::rotate(m_transform, base_rotation, ktf::vec3<float>(0.0f, 1.0f, 0.0f));
	this->m_transform = ktf::translate(m_transform, model_pos);
}

void entity::pig_model::render(ktf::composer* composer)
{
	composer->bind_texture(m_tex_id);
	for (int i=5; i>=0; i--) {
		composer->render_mesh(m_meshes[i], m_transform * m_parts[i].get_transform());
	}
}

