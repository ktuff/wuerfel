#pragma once

#include <memory>
#include <unordered_map>

#include "models/model.hpp"
#include <common/entity/systems/system_module.hpp>


class resources;


namespace ktf {

class composer;

} // namespace ktf


namespace entity {

class item_model;


class render_system : public ::entity::system_module {

	public:
		static ::entity::system_module_id const s_id;

	private:
		::entity::core_system* m_core_system;
		resources* m_resources;
		ktf::composer* m_composer;
		std::unordered_map<::entity::id, std::unique_ptr<::entity::model>> m_models[e_max_num_entity_types];
		std::vector<std::set<::entity::item_model*>> m_sorted_item_models;
		std::set<int32_t> m_items_block_tex;
		std::set<int32_t> m_items_item_tex;


	public:
		render_system(::entity::core_system* manager, resources* resources);


	public:
		void on_entity_creation(::entity::entity* entity) override;
		void on_entity_destruction(::entity::entity* entity) override;
		void on_event(::entity::event* event) override;

		void init(ktf::composer* composer);
		void clear();
		void reload();
		void update(double dt);
		void render();

}; // class render_system

} // namespace entity
