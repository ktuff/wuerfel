#include <client/game/packager.hpp>
#include <common/entity/core_system.hpp>
#include <common/entity/systems/inventory_system.hpp>
#include <common/entity/systems/net_types.hpp>
#include <common/net/types.hpp>

#include "network.hpp"


::entity::system_module_id const client::entity::network_system::s_id = 258;


client::entity::network_system::network_system(::entity::core_system* core_system, client::packager* network)
: system_module(core_system, s_id)
, m_network(network)
, m_target_id(0)
{
}

client::entity::network_system::~network_system()
{
}


void client::entity::network_system::on_entity_creation(::entity::entity*)
{
}

void client::entity::network_system::on_entity_destruction(::entity::entity*)
{
}

void client::entity::network_system::on_event(::entity::event* event)
{
	if (event->origin == this) {
		return;
	}

	// only send events for target entity ("player")
	// TODO
// 	if (event->entity_id != m_target_id) {
// 		return;
// 	}

	nlohmann::json data;
	data["id"] = event->entity_id;
	switch (event->type) {
	case ::entity::e_position: {
		auto* pe = static_cast<::entity::event_position*>(event);
		data["op"] = ::entity::e_net_pos;
		data["x"] = pe->position.x;
		data["y"] = pe->position.y;
		data["z"] = pe->position.z;
	}	break;
	case ::entity::e_rotation: {
		auto* pe = static_cast<::entity::event_rotation*>(event);
		data["op"] = ::entity::e_net_rot;
		data["x"] = pe->rotation.x;
		data["y"] = pe->rotation.y;
		data["z"] = pe->rotation.z;
	}	break;
	case ::entity::e_item_change: {
		auto* pe = static_cast<::entity::event_item_change*>(event);
		data["op"] = ::entity::e_net_item;
		data["target"] = pe->target;
		data["index"] = pe->index;
		data["type"] = pe->new_item.type;
		data["count"] = pe->new_item.count;
	}	break;
	case ::entity::e_selection_change: {
		auto* pe = static_cast<::entity::event_selection_change*>(event);
		data["op"] = ::entity::e_net_item_selection;
		data["oldIndex"] = pe->old_index;
		data["newIndex"] = pe->new_index;
	}	break;
	case ::entity::e_hitbox:
	case ::entity::e_damage:
		return;
	case ::entity::e_container: {
		auto* pe = static_cast<::entity::event_container*>(event);
		data["op"] = ::entity::e_net_container;
		data["container"] = pe->container;
	}	break;
	}
	this->m_network->push_entity_packet(data);
}

void client::entity::network_system::update()
{
}

void client::entity::network_system::process_packet(nlohmann::json const& data)
{
	::entity::op_code op = data["op"].get<::entity::op_code>();
	::entity::id id = data["id"].get<::entity::id>();
	switch (op) {
	case ::entity::e_net_invalid:
		break;
	case ::entity::e_net_creation:
		this->create_entity(data);
		break;
	case ::entity::e_net_destruction:
		this->destroy_entity(data);
		break;
	case ::entity::e_net_pos: {
		ktf::vec3<double> position;
		data["x"].get_to(position.x);
		data["y"].get_to(position.y);
		data["z"].get_to(position.z);
		if (id != m_target_id) {
			// TODO validate
			auto* entity = m_core->get_entity(id);
			if (entity) {
				entity->set_position(position);
			}
		}
	}	break;
	case ::entity::e_net_rot: {
		ktf::vec3<double> rotation;
		data["x"].get_to(rotation.x);
		data["y"].get_to(rotation.y);
		data["z"].get_to(rotation.z);
		if (id != m_target_id) {
			// TODO validate
			auto* entity = m_core->get_entity(id);
			if (entity) {
				entity->set_rotation(rotation);
			}
		}
	}	break;
	case ::entity::e_net_item: {
		::entity::inventory_group target = data["target"].get<::entity::inventory_group>();
		int index = data["index"].get<int>();
		::item::item_type_id type = data["type"].get<::item::item_type_id>();
		::item::stack_size_t count = data["count"].get<::item::stack_size_t>();
		::item::item_stack stack(type, count);
		// TODO validate
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
		inv->set_item(id, target, index, stack);
	} 	break;
	case ::entity::e_net_item_selection: {
		if (id != m_target_id) {
			int new_index = data["newIndex"].get<int>();
			// TODO validate
			auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
			inv->set_selected_index(id, new_index);
		}
	}	break;
	case ::entity::e_net_container: {
		::entity::id container_id = data["container"].get<::entity::id>();
		auto* inv = dynamic_cast<::entity::inventory_system*>(m_core->get_module(::entity::inventory_system::s_id));
		inv->set_container_id(id, container_id);
	}	break;
	}
}

void client::entity::network_system::set_target(::entity::id target_id)
{
	this->m_target_id = target_id;
}

void client::entity::network_system::create_entity(nlohmann::json const& packet)
{
	::entity::id id = packet["id"].get<::entity::id>();
	::entity::type type = packet["type"].get<::entity::type>();
	if (!m_core->get_entity(id)) {
		switch (type) {
		case ::entity::e_item:
			this->m_core->create_entity_item(id);
			break;
		case ::entity::e_player:
			this->m_core->create_entity_player(id);
			break;
		case ::entity::e_pig:
			this->m_core->create_entity_pig(id);
			break;
		case ::entity::e_cow:
			this->m_core->create_entity_chicken(id);
			break;
		case ::entity::e_chicken:
			this->m_core->create_entity_cow(id);
			break;
		case ::entity::e_chest: {
			ktf::vec3<coord_t> pos;
			auto const& voxel_data = packet["voxel"];
			voxel_data["x"].get_to(pos.x);
			voxel_data["y"].get_to(pos.y);
			voxel_data["z"].get_to(pos.z);
			this->m_core->create_entity_chest(pos, id);
		} break;
		case ::entity::e_furnace: {
			ktf::vec3<coord_t> pos;
			auto const& voxel_data = packet["voxel"];
			voxel_data["x"].get_to(pos.x);
			voxel_data["y"].get_to(pos.y);
			voxel_data["z"].get_to(pos.z);
			this->m_core->create_entity_furnace(pos, id);
		} break;
		case ::entity::e_workbench: {
			ktf::vec3<coord_t> pos;
			auto const& voxel_data = packet["voxel"];
			voxel_data["x"].get_to(pos.x);
			voxel_data["y"].get_to(pos.y);
			voxel_data["z"].get_to(pos.z);
			this->m_core->create_entity_workbench(pos, id);
		} break;
		default:
			throw std::runtime_error("Invalid entity type");
		}
	}
}

void client::entity::network_system::destroy_entity(nlohmann::json const& packet)
{
	::entity::id id = packet["id"].get<::entity::id>();
	this->m_core->destroy_entity(id);
}
