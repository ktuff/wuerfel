#include <time.h>

#include "util.hpp"


std::string client::get_date()
{
	time_t now = ::time(0);
	tm tstruct = *::localtime(&now);
	char buf[80];
	::strftime(buf, sizeof(buf), "%Y%m%d_%H_%M_%S", &tstruct);
	return std::string(buf);
}
