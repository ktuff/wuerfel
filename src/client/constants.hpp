#pragma once

namespace constants {
    extern const char* working_directory;
    extern const char* window_title;
    extern const char* icon_path;
    extern const long network_timeout;
};
