# Wuerfel
The aim of this project is to recreate the Alpha 1.2.6 of Minecraft in C++ with full functioning multiplayer and redstone.

## Features
* uses very low resources
* completely redesignable GUI
* own design of redstone
* mostly written from scratch
* \>90 chunks render distance on good hardware at 60 fps possible
* multiplayer support
* crafting

## Installation
In order to build this project just run:
```
mkdir -p build &&
cmake -B build &&
cd build &&
make
```

## Authors
* **KTuff** - renderer, soundsystem, world, network and ui

## Libraries used
* [libktf](https://gitlab.com/ktuff/libktf) - Engine, math functions, util
* [opensimplexnoise](https://gitlab.com/ktuff/opensimplexnoise) - Noise generation
* [nlohmann-json](https://github.com/nlohmann/json) - JSON parser for data packing
* [pugixml](https://github.com/zeux/pugixml/) - XML Parser for UI

## Textures
The used textures are taken from the [Coterie Craft Texture Pack](https://www.planetminecraft.com/texture-pack/16x-beta-17_01-coterie-craft-v16-regular-upd-6302011/).

## License
GPL 2.0
